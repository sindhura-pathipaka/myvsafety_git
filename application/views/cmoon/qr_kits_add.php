<section>
   <div class="container-fluid">
      <div class="row justify-content-center">
         <div class="col-md-12">
            <div class="card">
    <?php if($this->uri->segment(3) != '') { 
               foreach ($result as $row);  } ?>
            <form id="edit_details"  method="POST" action="qr_kits_add/<?php echo $row->id; ?>" enctype="multipart/form-data" autocomplete="off">
               <div style="padding-bottom: 20px;" class="card-header">Add or Edit  QR Kits  <a href="qr_kits_view"><button style="float: right;" type="button" class="btn btn-outline-dark">Back</button></a></div>
               <div class="card-body">




         <div class="form-group row">
           <label class="col-md-2 col-form-label">Name</label>
           <div class="col-md-5">
            <input type="text" class="form-control" name="name" value="<?php echo $row->name; ?>"  placeholder="Name" />
          </div>
         </div>
    
     <?php if($row->image != ''){ ?>

                    <div class="form-group row">
                     <label class="col-md-2 col-form-label"></label>
                    <div class="col-md-5"><a data-fancybox="gallery" href="<?php echo base_url(); ?>cmoon_images/<?php echo $row->image; ?>"><img style="width: 300px" src="<?php echo base_url(); ?>cmoon_images/<?php echo $row->image; ?>" class="img-fluid"></a></div>
                  </div>
              <?php } ?>

       <div class="form-group row">
         <label class="col-md-2 col-form-label">Image</label>
          <div class="col-md-5">


            <input type="file" class="form-control" name="image" id="file_type" onchange="Checkfiles()"/>
          <i><b>Note :</b> Only Jpeg, jpg, png format images are allowed Please upload ( 550 X 550 ) pixel images to maintain design <br>  </i>

        </div>
      </div>

   <div class="form-group row">
           <label class="col-md-2 col-form-label">Context</label>
           <div class="col-md-8">
            <textarea class="form-control " rows="5" name="description"  placeholder="Description"><?php echo $row->description; ?></textarea>
          </div>
         </div>


        <div class="form-group row">
           <label class="col-md-2 col-form-label">Actual Price (Only digits)</label>
           <div class="col-md-5">
            <input type="text" class="form-control" name="actual_price" value="<?php echo $row->actual_price; ?>" placeholder="Actual Price" />
          <i><b>Note :</b> Only Digits, Please don't include '.' , ',' ,'/-' <br>  </i>
          </div>
         </div>


   <div class="form-group row">
           <label class="col-md-2 col-form-label">Discount Percentage (Only digits)</label>
           <div class="col-md-5">
            <input type="text" class="form-control" name="discount_percentage" value="<?php echo $row->discount_percentage; ?>" placeholder="Discount Percentage" />
          <i><b>Note :</b> Only Digits, Please don't include '.' , ',' ,'/-' <br>  </i>
          </div>
         </div>


 <div class="form-group row">
           <label class="col-md-2 col-form-label">Deal of the day</label>
           <div class="col-md-5">
            <!-- <input type="text" class="form-control" name="deal_of_the_day" value="<?php echo $row->deal_of_the_day; ?>"> -->
          <select name="deal_of_the_day" class="form-control">
              <option value="">Please Select</option>
              <option value=YES <?php if($row->deal_of_the_day == YES){ echo 'Selected'; } ?>>YES</option>
              <option value=NO <?php if($row->deal_of_the_day == NO){ echo 'Selected'; } ?> >NO</option>
            </select> 


          </div>
         </div>


         <div class="form-group row">
           <label class="col-md-2 col-form-label"></label>
              <div class="col-md-5">
                 <button type="submit" class="btn btn-primary">Submit</button>
                 <button type="reset" class="btn btn-secondary">Reset</button>
              </div>
         </div>
               </div>
               </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
 <script type="text/javascript">
    $(function(){
      // Setup form validation on the #register-form element
        $("#edit_details").validate({
          // Specify the validation rules
        rules: {
          heading: {
              required: true
            },
            location: {
              required: true
            },
            category: {
              required: true
            },
            description: {
              required: true
            }
        },       
        // Specify the validation error messages
        messages: {
          heading: {
              required: 'Field should not be empty'
            },
             location: {
              required: 'Field should not be empty'
            },
            category: {
              required: 'Field should not be empty'
            },
             description: {
              required: 'Field should not be empty'
            }
        },
      });
    });
function Checkfiles()
{
var fup = document.getElementById('file_type');
var fileName = fup.value;
var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
if(ext == "jpeg"  || ext == "jpg" || ext == "png")
{
return true;
} 
else
{
alert("Upload jpeg,jpg,png Files only");
document.getElementById('file_type').value="";
fup.focus();
return false;
}
}
</script>
