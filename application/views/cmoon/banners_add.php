<section>
   <div class="container-fluid">
      <div class="row justify-content-center">
         <div class="col-md-12">
            <div class="card">

    <?php if($this->uri->segment(3) != ''){ 
               foreach ($result as $row);  } ?>

    
            <form id="banners" method="POST" action="banners_add/<?php echo $row->id; ?>" enctype="multipart/form-data" autocomplete="off">
               <div style="padding-bottom: 20px;" class="card-header">Add or Edit Banner Images <a href="banners_view"><button style="float: right;" type="button" class="btn btn-outline-dark">Back</button></a></div>
               <div class="card-body">



<!--          <div class="form-group row">
           <label class="col-md-2 col-form-label">Heading</label>
           <div class="col-md-5"><input type="text" class="form-control" name="heading" value="<?php echo $row->heading; ?>"  placeholder="Heading" /></div>
         </div>
 -->

         
 <?php if($row->image != ''){ ?>

                    <div class="form-group row">
                     <label class="col-md-2 col-form-label"></label>
                    <div class="col-md-5"><a data-fancybox="gallery" href="<?php echo base_url(); ?>cmoon_images/<?php echo $row->image; ?>"><img style="width: 300px" src="<?php echo base_url(); ?>cmoon_images/<?php echo $row->image; ?>" class="img-fluid"></a></div>
                  </div>
              <?php } ?>

       <div class="form-group row">
         <label class="col-md-2 col-form-label">Image</label>
          <div class="col-md-5">

    <?php if($this->uri->segment(3) != '') { ?>

            <input type="file" class="form-control" name="image" id="file_type" onchange="Checkfiles()"/>
          <i><b>Note :</b> Only Jpeg, jpg, png format images are allowed Please upload ( 240 X 530 ) pixel images to maintain design <br>  </i>

<?php }else{ ?>

           <input type="file" class="form-control" name="image[]" multiple id="file_type" onchange="Checkfiles()"/>
                  <i><b>Note :</b> Only Jpeg, jpg, png format images are allowed <br> Please upload ( 240 X 530 ) pixel images to maintain design <br> Can select multiple images by holding Ctrl  </i>
<?php } ?>
        </div>
      </div>


       <!--   <?php if($row->video != ''){ ?>

                <div class="form-group row">
                     <label class="col-md-2 col-form-label"></label>
                    <div class="col-md-5"><iframe width="200" height="200" src="<?php echo base_url(); ?>cmoon_images/<?php echo $row->video; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
                  </div>

             <?php } ?>
                
                  <div class="form-group row">
                     <label class="col-md-2 col-form-label">Video</label>
                  <div class="col-md-5"><input type="file" class="form-control" name="video" id="file_type2"  onchange="Checkfiles2()"/>
                  <i><b>Note :</b> Only mp4, 3gp, gif format videos  are allowed</i></div>
                  </div> -->
              
<!--       <div class="form-group row">
         <label class="col-md-2 col-form-label">Image</label>
          <div class="col-md-5">

    <?php if($this->uri->segment(3) != '') { ?>

            <input type="file" class="form-control" name="image" id="file_type" onchange="Checkfiles()"/>
          <i><b>Note :</b> Only Jpeg, jpg, png format images are allowed Please upload ( 1600 X 600 ) pixel images to maintain design <br>  </i>

<?php }else{ ?>

           <input type="file" class="form-control" name="image[]" multiple id="file_type" onchange="Checkfiles()"/>
                  <i><b>Note :</b> Only Jpeg, jpg, png format images are allowed <br> Please upload ( 1600 X 600 ) pixel images to maintain design <br> Can select multiple images by holding Ctrl  </i>
<?php } ?>
        </div>
      </div> -->
      <!--   <div class="form-group row">
           <label class="col-md-2 col-form-label">Context</label>
           <div class="col-md-8">

            <textarea class="form-control" rows="5" name="description"  placeholder="Description"><?php echo $row->description; ?></textarea>
          </div>
         </div> -->

  
         <div class="form-group row">
                      <label class="col-md-2 col-form-label"></label>
                      <div class="col-md-5">
                         <button type="submit" class="btn btn-primary">Submit</button>
                         <button type="reset" class="btn btn-secondary">Reset</button>
                      </div>
                   </div>
    
               </div>
                  
               </form>
             
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

 <script type="text/javascript">
   


function Checkfiles()
{
var fup = document.getElementById('file_type');
var fileName = fup.value;
var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
if(ext == "jpeg"  || ext == "jpg" || ext == "png")
{
return true;
} 
else
{
alert("Upload jpeg,jpg,png Files only");
document.getElementById('file_type').value="";
fup.focus();
return false;
}
}


function Checkfiles2()

{
var fup = document.getElementById('file_type2');
var fileName = fup.value;
var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
if(ext == "mp4"  || ext == "3gp" || ext == "gif")
{
return true;
} 
else
{
alert("Upload mp4,3gp,gif Files only");
document.getElementById('file_type2').value="";
fup.focus();
return false;
}
}

</script>