<section>
   <div class="container-fluid">
      <div class="row justify-content-center">
         <div class="col-md-12">
            <div class="card">


    <?php if($this->uri->segment(3) != ''){ 
               foreach ($result as $row);  } ?>
          
            <form   method="POST" enctype="multipart/form-data" autocomplete="off">
               <div style="padding-bottom: 20px;" class="card-header"> Edit  <a href="manage_uploads_view"><button style="float: right;" type="button" class="btn btn-outline-dark">Back</button></a></div>
               <div class="card-body">
            
     <?php if($row->image != ''){ ?>

                    <div class="form-group row">
                     <label class="col-md-2 col-form-label"></label>
                    <div class="col-md-5"><a data-fancybox="gallery" href="<?php echo base_url(); ?>cmoon_images/<?php echo $row->image; ?>"><img style="width: 300px" src="<?php echo base_url(); ?>cmoon_images/<?php echo $row->image; ?>" class="img-fluid"></a></div>
                  </div>
              <?php } ?>

      <div class="form-group row">
         <label class="col-md-2 col-form-label">Image</label>
          <div class="col-md-5"><input type="file" class="form-control" name="image" accept="image/*" id="file_type" onchange="Checkfiles()"/>
          <i><b>Note :</b> Only Jpeg, jpg, png format images are allowed Please upload below 2MB images  </i>

        </div>
      </div>
                      <div class="form-group row">
                      <label class="col-md-2 col-form-label"></label>
                      <div class="col-md-5">
                         <button type="submit"  class="btn btn-primary">Submit</button>
                         <button type="reset" class="btn btn-secondary">Reset</button>
                      </div>
                   </div>
    
               </div>
                  
               </form>
             
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

  <script type="text/javascript">

    function Checkfiles()
{
var fup = document.getElementById('file_type');
var fileName = fup.value;
var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
if(ext == "jpeg"  || ext == "jpg" || ext == "png")
{
return true;
} 
else
{
alert("Upload jpeg,jpg,png Files only");
document.getElementById('file_type').value="";
fup.focus();
return false;
}
}

</script>