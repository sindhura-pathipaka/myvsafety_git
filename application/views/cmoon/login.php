<!doctype html>
<html lang="en">
<head>
  <base href="<?php echo base_url(); ?>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="cmoon_assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="cmoon_assets/css/f5.css">
    <link rel="stylesheet" type="text/css" href="cmoon_assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="cmoon_assets/css/custom.css">
    <script src="http://code.jquery.com/jquery.js"></script>

    <link rel='shortcut icon' href='<?php echo base_url(); ?>cmoon_images/<?php echo $site_details->site_favicon; ?>' type='image' />    


    <title>Dashboard/<?php echo $site_details->site_name; ?></title>


</head>
<body>
    <section class="login-interface d-flex align-items-center">
        <div class="flex-grow-1">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-5">
             <div class="card">
                 <div class="card-body p-4">
                    <img src="<?php echo base_url(); ?>cmoon_images/<?php echo $site_details->site_logo; ?>" height="80" class="d-block mx-auto mb-4">


                                   <form id="login" method="post" action="<?php echo base_url(); ?>cmoon_login" >

          <?php if($this->session->flashdata('logout_success')){  ?>
            <div class="alert alert-success"> <?php echo $this->session->flashdata('logout_success'); ?> </div>
          <?php  }  ?>
          <?php if($this->session->flashdata('login_error')){  ?>
            <div class="alert alert-danger"> <?php echo $this->session->flashdata('login_error'); ?> </div>
          <?php  }  ?>


                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <label>User Name</label>
                              <input type="text" class="form-control" name="username"  placeholder="User Name">
                           </div>
                        </div>
                        <div class="col-md-12">
                           <div class="form-group">
                              <label>Password</label>
                              <input type="password" class="form-control" name="password"  placeholder="Password">
                           </div>
                        </div>
                    
                        <div class="col-md-12 py-4 text-center">

                           <button type="submit" class="btn btn-primary">Submit</button>
                           <button type="reset" class="btn btn-secondary">Reset</button>
                           
                              
                        </div>
                        <div class="col-md-12 text-center">
                             <a href="<?php echo base_url(); ?>cmoon_login/forgot" class="text-muted">Forgot Password ?</a>
                        </div>
                     </div>
                  </form>



                 </div>
             </div>
                    </div>
                </div>
    
</div>
        </div>
    </section>

    <script type="text/javascript" src="cmoon_assets/js/popper.min.js"></script>
    <script type="text/javascript" src="cmoon_assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="cmoon_assets/js/wow.min.js"></script>
    <script type="text/javascript" src="cmoon_assets/js/custom.js"></script>
</body>
</html>

    <script type="text/javascript" src="http://code.jquery.com/jquery.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>

<script type="text/javascript">
    $(function(){
      // Setup form validation on the #register-form element
        $("#login").validate({

        // Specify the validation rules
        rules: {
          username: {
              required: true
            },

            password: {
              required: true
            }
        },       
        // Specify the validation error messages
        messages: {
          username: {
              required: 'Please Enter Username'
            },
    
           password: {
              required: 'Please Enter Password'
            }
          }
      });
    });



</script>
