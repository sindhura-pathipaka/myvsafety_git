    <section>
        <div class="container-fluid">
                  <div class="card">
              <div class="card-header">
                <div class="row align-items-center">
                  <div class="col"> Vehicles </div>
                        <div style="padding-bottom: 20px;"><a href="<?php echo base_url(); ?>cmoon"><button style="float: right;" class="btn btn-outline-dark" type="button"><span class="far fa-arrow-alt-circle-left "> </span>  Back to Menu</button></a></div>
                        <!-- <div style="padding-bottom: 20px;"><button style="float: right;" class="btn btn-primary" type="button" data-toggle="modal" data-target="#exampleModal">Generate QR CODES</button></div> -->

              </div>
          </div>
<div class="card-body">
<div class="row mb-3">
</div>
<div class="table-responsive">
   <table class="example table table-striped table-bordered nowrap" id="excel_export" style="width:100%">
    <thead>
      <tr>
        <th>Sl.No</th>
        <th>Mobile</th>
        <th>Vehicle num</th>
        <th>Vehicle image</th>
        <th>Pending challans</th>
        <th>Paid challans</th>
        <th>Update challans</th>
       
        <!-- <th>View</th> -->
      </tr>
    </thead>
    <tbody>
      <?php $no=1; foreach ($result as $row) {   ?>
            <tr>
                <td> <?php echo $no; ?> </td>
                <td> <?php echo $row->users->mobile; ?> </td>
              <td> <?php echo $row->vehicle_num; ?> </td>
                 <td> <a data-fancybox="gallery" href="<?php echo base_url(); ?>vehicle_images/<?php echo $row->vehicle_image; ?>"><img width="100" height="100" src="<?php echo base_url(); ?>vehicle_images/<?php echo $row->vehicle_image; ?>"class="img-fluid"></a>  </td>
              <td> <?php echo $row->pending_challans; ?> </td>
              <td> <?php echo $row->paid_challans; ?> </td>

                <td><button class="btn btn-primary" type="button" data-toggle="modal" data-target="#update_challans<?php echo $row->id ?>">Update challans</button> </td>
                <!-- <td><button class="btn btn-primary" type="button" data-toggle="modal" data-target="#details<?php echo $row->id ?>"> View All Details </button> </td> -->

              
              <!--   <td>
                  <a href="<?php echo base_url(); ?>cmoon/orders_add/<?php echo $row->id; ?>" class="btn btn-outline-success"> Edit</a> &nbsp; &nbsp;
                <a onclick="ConfirmDelete(<?php echo $row->id; ?>)" href="JavaScript:Void(0);" class="btn btn-outline-danger">Delete</a>
                 </td> -->
            </tr>
          <?php $no++; } ?>
    </tbody>
</table>
</div></div></div></div>
    </section>
<!-- <script type="text/javascript">
    $('.example').DataTable({
  responsive: true
});
</script> -->
      <?php $no=1; foreach ($result as $row) {   ?>

<div class="modal fade" data-backdrop="false" id="update_challans<?php echo $row->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> Update challans</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form action="vehicles_update" method="post">


           <div class="form-group row">
           <label class="col-md-6 col-form-label">Pending challans</label>
           <div class="col-md-8">
            <input type="text" class="form-control" name="pending_challans" placeholder="No of Pending challans" / required>
            <input type="hidden" name="id" value="<?php echo $row->id ?>">
          </div>
         </div>


     <div class="form-group row">
           <label class="col-md-6 col-form-label">Paid challans</label>
           <div class="col-md-8">
            <input type="text" class="form-control" name="paid_challans" placeholder="No of Paid challans" / required>
            <input type="hidden" name="id" value="<?php echo $row->id ?>">
          </div>
         </div>


    <div class="form-group row">
           <label class="col-md-2 col-form-label"></label>
              <div class="col-md-5">
                 <button type="submit" class="btn btn-primary">Submit</button>
                 <button type="reset" class="btn btn-secondary">Reset</button>
              </div>
         </div>

       </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

          <?php $no++; } ?>

        

                <?php  foreach ($result as $row) {   ?>
               <!-- Modal -->
<div class="modal fade" data-backdrop="false" id="details<?php echo $row->id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Full Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
   <div class="row">
        <div class="col-md-6">

             <b> Mobile </b> :  <?php echo $row->users->mobile; ?> </br></br>
             <b>  Order ID </b> :  <?php echo $row->order_id; ?> </br></br>
             <b>  order Status </b> :  <?php echo $row->order_status; ?> </br></br>
             <b>  Payment ID </b> :  <?php echo $row->payment_id; ?> </br></br>
             <b>  Payment status </b> :  <?php echo $row->payment_status; ?> </br></br>
             <b>  Actual price </b> :  <?php echo $row->actual_price; ?> </br></br>
             <b>  Discount percentage </b> :  <?php echo $row->discount_percentage; ?> </br></br>
             <b>  Final price   </b> :  <?php echo $row->discount_price  ; ?> </br></br>

              </div>
        <div class="col-md-6">


             <b> Name </b> :  <?php echo $row->name; ?> </br></br>
             <b>  House No   </b> :  <?php echo $row->house_no  ; ?> </br></br>
             <b>  Area   </b> :  <?php echo $row->area  ; ?> </br></br>
             <b>  State   </b> :  <?php echo $row->state  ; ?> </br></br>
             <b>  PINCODE     </b> :  <?php echo $row->pin_code    ; ?> </br></br>
             <b> Payment done on     </b> :  <?php echo $row->payment_done    ; ?> </br></br>
             <b>  Referral Code    </b> :  <?php echo $row->referral_code   ; ?> </br></br>
             <b>  QR Code    </b> :  <?php echo $row->qr_code   ; ?> </br></br>

      </div>
      </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php } ?>



<script type="text/javascript">
      function ConfirmDelete(id)
      {
        swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this data!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
window.location.href='orders_delete/'+id;
}
});
}
</script>


