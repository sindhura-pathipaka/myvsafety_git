    <section>
        <div class="container-fluid">
                  <div class="card">
              <div class="card-header">
                <div class="row align-items-center">
                  <div class="col"> QR CODES </div>
                        <div style="padding-bottom: 20px;"><a href="<?php echo base_url(); ?>cmoon"><button style="float: right;" class="btn btn-outline-dark" type="button"><span class="far fa-arrow-alt-circle-left "> </span>  Back to Menu</button></a></div>
                        <div style="padding-bottom: 20px;"><button style="float: right;" class="btn btn-primary" type="button" data-toggle="modal" data-target="#exampleModal">Generate QR CODES</button></div>

              </div>
          </div>
<div class="card-body">

  

     <form action="qr_code_genereation_view" method="post">

<div class="row align-items-center">
           <div class="form-group col-md-2 mb-0">
           <div class="">
            <input type="text" class="form-control mb-0" name="qr_code"   placeholder="QR CODE"  >
          </div>
         </div>

     <div class="form-group col-md-2 mb-0">
           <div class="">
            <input type="date" class="form-control mb-0"  name="created_at_time"   placeholder="Select Date"  >
          </div>
         </div>

             <div class="form-group col-md-2 mb-0">
           <div class="">

             <select name="status" class="form-control">
              <option value="">Select Status</option>
              <option value="FRESH" <?php if($row->order_status == 'FRESH'){ echo 'Selected'; } ?>>FRESH</option> 
              <option value="ASSIGNED" <?php if($row->order_status == 'ASSIGNED'){ echo 'Selected'; } ?> >ASSIGNED</option>
              <option value="ACTIVATED" <?php if($row->order_status == 'ACTIVATED'){ echo 'Selected'; } ?> >ACTIVATED</option>
              <option value="VEHICLE_NOT_ASSIGNED" <?php if($row->order_status == 'VEHICLE_NOT_ASSIGNED'){ echo 'Selected'; } ?> >VEHICLE NOT ASSIGNED</option>
              <option value="VEHICLE_ASSIGNED" <?php if($row->order_status == 'VEHICLE_ASSIGNED'){ echo 'Selected'; } ?> >VEHICLE ASSIGNED</option>
            </select>

          </div>
         </div>

     <!-- <div class="form-group col-md-2 mb-0">
           <div class="">
            <input type="text" class="form-control mb-0" name="mobile"   placeholder="Mobile Number"  >
          </div>
         </div> -->

        <div class="col-md-2">
          <button type="submit" class="btn btn-primary d-block w-100">Search</button>
        </div>

           <div class="col-md-2">
                 <button type="reset" class="btn btn-secondary d-block w-100">Reset</button>
        </div>
                 
       </div>
    
       </form>

<div class="row mb-3">
</div>    
<div class="table-responsive">
   <table class="example table table-striped table-bordered nowrap" id="excel_export" style="width:100%">
    <thead>
      <tr>
        <th>Sl.No</th>
        <th>QR CODE</th>
        <th>Created at</th>
        <th>Status</th>
        <th>Mobile</th>
        <th>Assigned at</th>
        <!-- <th>Action</th> -->
      </tr>
    </thead>
    <tbody>
      <?php $no=1;  foreach ($result as $row) {   ?>
            <tr>
                <td> <?php echo $no; ?> </td>
                <td> <?php echo $row->qr_code; ?> </td>
                <td> <?php echo $row->created_at; ?> </td>
                <td> <?php echo $row->status; ?> </td>
                <td> <?php echo $row->users->mobile; ?> </td>
                <td> <?php echo $row->assigned_at; ?> </td>
                <!-- <td> <a data-fancybox="gallery" href="<?php echo base_url(); ?>cmoon_images/<?php echo $row->image; ?>"><img width="100" height="100" src="<?php echo base_url(); ?>cmoon_images/<?php echo $row->image; ?>"class="img-fluid"></a>  </td> -->
              <!--   <td> 
                  <a href="<?php echo base_url(); ?>cmoon/qr_code_genereation_add/<?php echo $row->id; ?>" class="btn btn-outline-success"> Edit</a> &nbsp; &nbsp;
                <a onclick="ConfirmDelete(<?php echo $row->id; ?>)" href="JavaScript:Void(0);" class="btn btn-outline-danger">Delete</a>
                 </td> -->
            </tr>
          <?php $no++; } ?>
    </tbody>
</table>
</div></div></div></div>
    </section>
<!-- <script type="text/javascript">
    $('.example').DataTable({
  responsive: true
});
</script> -->

<div class="modal fade" data-backdrop="false" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Generating QR CODES</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form action="qr_code_genereation_add" method="post">


           <div class="form-group row">
           <label class="col-md-6 col-form-label">No. of QR Codes</label>
           <div class="col-md-8">
            <input type="text" class="form-control" name="no_of_qr_codes"   placeholder="No. of QR Codes" / required>
          </div>
         </div>
    

    <div class="form-group row">
           <label class="col-md-2 col-form-label"></label>
              <div class="col-md-5">
                 <button type="submit" class="btn btn-primary">Submit</button>
                 <button type="reset" class="btn btn-secondary">Reset</button>
              </div>
         </div>
    
       </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
      function ConfirmDelete(id)
      {
        swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this data!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
window.location.href='qr_code_genereation_delete/'+id;       
}
});
}
</script>