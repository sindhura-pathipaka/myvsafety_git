    <section>
        <div class="container-fluid">
                  <div class="card">
              <div class="card-header">
                <div class="row align-items-center">
                  <div class="col"> ORDERS </div>
                        <div style="padding-bottom: 20px;"><a href="<?php echo base_url(); ?>cmoon"><button style="float: right;" class="btn btn-outline-dark" type="button"><span class="far fa-arrow-alt-circle-left "> </span>  Back to Menu</button></a></div>
                        <!-- <div style="padding-bottom: 20px;"><button style="float: right;" class="btn btn-primary" type="button" data-toggle="modal" data-target="#exampleModal">Generate QR CODES</button></div> -->

              </div>
          </div>
<div class="card-body">
<div class="row mb-3">
</div>
<div class="table-responsive">
   <table class="example table table-striped table-bordered nowrap" id="excel_export" style="width:100%">
    <thead>
      <tr>
        <th>Sl.No</th>
        <th>Amount</th>
        <th>Paid On</th>
        <th>Mobile</th>
        <th>QR CODE</th>
        <th>Order status</th>
        <th>Order status</th>
        <th>View</th>
      </tr>
    </thead>
    <tbody>
      <?php $no=1; foreach ($result as $row) {   ?>
            <tr>
                <td> <?php echo $no; ?> </td>
              <td> <?php echo $row->discount_price; ?> </td>
                <td> <?php echo $row->payment_done; ?> </td>
                <td> <?php echo $row->users->mobile; ?> </td>
                <?php if($row->qr_code !=''){ ?>
                <td> <?php echo $row->qr_code; ?> </td>
              <?php }else{ ?>
                <td><button class="btn btn-primary" type="button" data-toggle="modal" data-target="#qr_code_assign<?php echo $row->id ?>"> Assign QR Code </button> </td>
                <?php } ?>
                <td> <?php echo $row->order_status; ?> </td>
                <td><button class="btn btn-primary" type="button" data-toggle="modal" data-target="#order_status<?php echo $row->id ?>">Update Order Status</button> </td>
                <td><button class="btn btn-primary" type="button" data-toggle="modal" data-target="#details<?php echo $row->id ?>"> View All Details </button> </td>

                <!-- <td> <a data-fancybox="gallery" href="<?php echo base_url(); ?>cmoon_images/<?php echo $row->image; ?>"><img width="100" height="100" src="<?php echo base_url(); ?>cmoon_images/<?php echo $row->image; ?>"class="img-fluid"></a>  </td> -->
              <!--   <td>
                  <a href="<?php echo base_url(); ?>cmoon/orders_add/<?php echo $row->id; ?>" class="btn btn-outline-success"> Edit</a> &nbsp; &nbsp;
                <a onclick="ConfirmDelete(<?php echo $row->id; ?>)" href="JavaScript:Void(0);" class="btn btn-outline-danger">Delete</a>
                 </td> -->
            </tr>
          <?php $no++; } ?>
    </tbody>
</table>
</div></div></div></div>
    </section>
<!-- <script type="text/javascript">
    $('.example').DataTable({
  responsive: true
});
</script> -->
      <?php $no=1; foreach ($result as $row) {   ?>

<div class="modal fade" data-backdrop="false" id="qr_code_assign<?php echo $row->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> Assigning QR CODES</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form action="orders_add" method="post">


           <div class="form-group row">
           <label class="col-md-6 col-form-label">Assign QR Code</label>
           <div class="col-md-8">
            <input type="search" class="form-control auto_search " name="qr_code" placeholder="Assign QR Code" / required>
            <input type="hidden" name="id" value="<?php echo $row->id ?>">
          </div>
         </div>


    <div class="form-group row">
           <label class="col-md-2 col-form-label"></label>
              <div class="col-md-5">
                 <button type="submit" class="btn btn-primary">Submit</button>
                 <button type="reset" class="btn btn-secondary">Reset</button>
              </div>
         </div>

       </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

          <?php $no++; } ?>

            <?php $no=1; foreach ($result as $row) {   ?>

<div class="modal fade" data-backdrop="false" id="order_status<?php echo $row->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Order Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form action="order_status_update" method="post">


           <div class="form-group row">
           <label class="col-md-6 col-form-label">Order Status</label>
           <div class="col-md-8">
            <input type="hidden" name="id" value="<?php echo $row->id ?>">
            <select name="order_status" class="form-control">
              <option value="">Please Select</option>
              <!-- <option value="PLACED" <?php if($row->order_status == 'PLACED'){ echo 'Selected'; } ?>>PLACED</option> -->
              <option value="PREPARING" <?php if($row->order_status == 'PREPARING'){ echo 'Selected'; } ?> >PREPARING</option>
              <option value="ONGOING" <?php if($row->order_status == 'ONGOING'){ echo 'Selected'; } ?> >ONGOING</option>
              <option value="COMPLETED" <?php if($row->order_status == 'COMPLETED'){ echo 'Selected'; } ?> >COMPLETED</option>
            </select>
          </div>
         </div>


    <div class="form-group row">
           <label class="col-md-2 col-form-label"></label>
              <div class="col-md-5">
                 <button type="submit" class="btn btn-primary">Submit</button>
                 <button type="reset" class="btn btn-secondary">Reset</button>
              </div>
         </div>

       </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

          <?php $no++; } ?>

                <?php  foreach ($result as $row) {   ?>
               <!-- Modal -->
<div class="modal fade" data-backdrop="false" id="details<?php echo $row->id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Full Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
   <div class="row">
        <div class="col-md-6">

             <b> Mobile </b> :  <?php echo $row->users->mobile; ?> </br></br>
             <b>  Order ID </b> :  <?php echo $row->order_id; ?> </br></br>
             <b>  order Status </b> :  <?php echo $row->order_status; ?> </br></br>
             <b>  Payment ID </b> :  <?php echo $row->payment_id; ?> </br></br>
             <b>  Payment status </b> :  <?php echo $row->payment_status; ?> </br></br>
             <b>  Actual price </b> :  <?php echo $row->actual_price; ?> </br></br>
             <b>  Discount percentage </b> :  <?php echo $row->discount_percentage; ?> </br></br>
             <b>  Final price   </b> :  <?php echo $row->discount_price  ; ?> </br></br>

              </div>
        <div class="col-md-6">


             <b> Name </b> :  <?php echo $row->name; ?> </br></br>
             <b>  House No   </b> :  <?php echo $row->house_no  ; ?> </br></br>
             <b>  Area   </b> :  <?php echo $row->area  ; ?> </br></br>
             <b>  State   </b> :  <?php echo $row->state  ; ?> </br></br>
             <b>  PINCODE     </b> :  <?php echo $row->pin_code    ; ?> </br></br>
             <b> Payment done on     </b> :  <?php echo $row->payment_done    ; ?> </br></br>
             <b>  Referral Code    </b> :  <?php echo $row->referral_code   ; ?> </br></br>
             <b>  QR Code    </b> :  <?php echo $row->qr_code   ; ?> </br></br>

      </div>
      </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php } ?>



<script type="text/javascript">
      function ConfirmDelete(id)
      {
        swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this data!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
window.location.href='orders_delete/'+id;
}
});
}
</script>


