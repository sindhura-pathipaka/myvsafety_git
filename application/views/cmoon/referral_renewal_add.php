<section>
   <div class="container-fluid">
      <div class="row justify-content-center">
         <div class="col-md-12">
            <div class="card">
    <?php if($this->uri->segment(3) != '') { 
               foreach ($result as $row);  } ?>
            <form id="edit_details"  method="POST" action="referral_renewal_add/<?php echo $row->id; ?>" enctype="multipart/form-data" autocomplete="off">
               <div style="padding-bottom: 20px;" class="card-header"> Edit  Referral Points / Renewal Option <a href="referral_renewal_view"><button style="float: right;" type="button" class="btn btn-outline-dark">Back</button></a></div>
               <div class="card-body">


<?php if($row->id == 1){ ?>

        <div class="form-group row">
           <label class="col-md-2 col-form-label">Referral Points (Only digits)</label>
           <div class="col-md-5">
            <input type="text" class="form-control" name="referral_points" value="<?php echo $row->referral_points; ?>" placeholder="Referral Points" />
          <i><b>Note :</b> Only Digits, Please don't include '.' , ',' ,'/-' <br>  </i>
          </div>
         </div>

<?php } ?>

<?php if($row->id == 2){ ?>

 <div class="form-group row">
           <label class="col-md-2 col-form-label">Renewal</label>
           <div class="col-md-5">
            <!-- <input type="text" class="form-control" name="deal_of_the_day" value="<?php echo $row->deal_of_the_day; ?>"> -->
          <select name="renewal" class="form-control">
              <option value="">Please Select</option>
              <option value=YES <?php if($row->renewal == YES){ echo 'Selected'; } ?>>YES</option>
              <option value=NO <?php if($row->renewal == NO){ echo 'Selected'; } ?> >NO</option>
            </select> 


          </div>
         </div>
<?php } ?>


         <div class="form-group row">
           <label class="col-md-2 col-form-label"></label>
              <div class="col-md-5">
                 <button type="submit" class="btn btn-primary">Submit</button>
                 <button type="reset" class="btn btn-secondary">Reset</button>
              </div>
         </div>
               </div>
               </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
 <script type="text/javascript">
    $(function(){
      // Setup form validation on the #register-form element
        $("#edit_details").validate({
          // Specify the validation rules
        rules: {
          heading: {
              required: true
            },
            location: {
              required: true
            },
            category: {
              required: true
            },
            description: {
              required: true
            }
        },       
        // Specify the validation error messages
        messages: {
          heading: {
              required: 'Field should not be empty'
            },
             location: {
              required: 'Field should not be empty'
            },
            category: {
              required: 'Field should not be empty'
            },
             description: {
              required: 'Field should not be empty'
            }
        },
      });
    });
function Checkfiles()
{
var fup = document.getElementById('file_type');
var fileName = fup.value;
var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
if(ext == "jpeg"  || ext == "jpg" || ext == "png")
{
return true;
} 
else
{
alert("Upload jpeg,jpg,png Files only");
document.getElementById('file_type').value="";
fup.focus();
return false;
}
}
</script>
