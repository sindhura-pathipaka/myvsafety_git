</div>
<footer>
    <div class="container-fluid">
        <div class="d-none d-md-block"><div class="row">
                    <div class="col">Admin Panel - Developed by Colour Moon</div>
<?php date_default_timezone_set('Asia/Kolkata');?>
                    <div class="col-auto">Developer : Sindhura | <?php echo date('Y') ?></div>
                </div></div>
  <div class="d-block d-md-none">      <div class="row">
              <div class="col-12 text-center">Admin Panel @Cmoon</div>
          </div></div>
    </div>
</footer>
    <script type="text/javascript" src="<?php echo base_url(); ?>cmoon_assets/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>cmoon_assets/js/slick.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>cmoon_assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>cmoon_assets/js/wow.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>cmoon_assets/js/custom.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>cmoon_assets/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>cmoon_assets/../ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script type="text/javascript" src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!--     <script type="text/javascript" src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script> -->
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script> 


  

 <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js"></script>

<!--  <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script type="text/javascript">
     $(function () {

       var availableTags = <?php echo json_encode($search); ?>;

       $(".auto_search").autocomplete({

           minLength: 1,

           source: availableTags

       });

   });

</script>

<script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>

<?php if($this->uri->segment(3) == 'Success' || $this->uri->segment(4) == 'Success' || $this->uri->segment(5) == 'Success'){ 
    echo '<script type="text/javascript">';
    echo 'setTimeout(function () { swal("GOOD JOB!","Sucessfully updated to database!","success");';
    echo '}, 1000);</script>'; 
 } ?>

<?php if($this->uri->segment(3) == 'error' || $this->uri->segment(4) == 'error' || $this->uri->segment(5) == 'error'){ 
    echo '<script type="text/javascript">';
    echo 'setTimeout(function () { swal("Error!","Unable to Updated please tryagain refreshing the page","warning");';
    echo '}, 1000);</script>'; 
 } ?>

<?php if($this->uri->segment(3) == 'image_error'){ 
        echo '<script type="text/javascript">';
    echo 'setTimeout(function () { swal("IMAGE ERROR!","Image is not selected or Image size exceeds 2MB ,Please select only jpg,jpeg and png format images below 2MB size","warning");';
    echo '}, 1000);</script>';  }  ?>

<?php if($this->uri->segment(3) == 'dSuccess' || $this->uri->segment(4) == 'dSuccess' || $this->uri->segment(4) == 'dSuccess'){ 
   echo '<script type="text/javascript">';
    echo 'setTimeout(function () { swal("DELETED!","Data Deleted Successfully","success");';
    echo '}, 1000);</script>';
 } ?>

<?php if($this->uri->segment(3) == 'psw_doesnot_match'){ 
   echo '<script type="text/javascript">';
    echo 'setTimeout(function () { swal("Error!","Password doesnot match with the password in the database","error");';
    echo '}, 1000);</script>';
 } ?>

<?php if($this->uri->segment(3) == 'psw_Success'){ 
    echo '<script type="text/javascript">';
    echo 'setTimeout(function () { swal("Success!","Password changed successfully","success");';
    echo '}, 1000);</script>';
 } ?>

<?php if($this->uri->segment(3) == 'qr_code_error'){ 
   echo '<script type="text/javascript">';
    echo 'setTimeout(function () { swal("Error!","QR Code doesnot present in the database","error");';
    echo '}, 1000);</script>';
 } ?>

<!-- <script type="text/javascript">
    $('.example').DataTable({
  responsive: true
});
</script> -->


<!-- 
<script type="text/javascript">
    
$(document).ready(function() {
    $('.example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );

</script> -->



<script type="text/javascript">
    

$(document).ready(function() {
    $('.example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
             'excel'
        ]
    } );
} );


</script>





</body>
</html>