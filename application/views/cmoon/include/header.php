<?php @session_start();
$session_id = $this->session->userdata('logged_in');
if (!isset($session_id['id']) && $session_id['username'] == "") {?>
        <script>
            document.location.href='<?php echo base_url(); ?>cmoon_login';
        </script>
<?php } ?>
<!doctype html>
<html lang="en">
<head>
  <base href="<?php echo base_url(); ?>cmoon/">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="<?php echo base_url(); ?>cmoon_assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>cmoon_assets/css/slick.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>cmoon_assets/css/f5.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>cmoon_assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>cmoon_assets/css/custom.css">
    <script src="http://code.jquery.com/jquery.js"></script>
    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"> -->
    <link rel="stylesheet" type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css">


 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
 

  

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css">

 <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <link rel="shortcut icon" type="image" / href="<?php echo base_url(); ?>cmoon_images/<?php echo $site_details->site_favicon; ?>">
    <title>Dashboard/<?php echo $site_details->site_name; ?></title>
</head>
<body>
    <header>                                                
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col">
                    <!-- <a class="logo" href="<?php echo base_url(); ?>cmoon"><img src="<?php echo base_url(); ?>cmoon_assets/images/logo.svg"></a> -->
                    <div class="nav-link text-white font-weight-bold"> <a style="color: white;" href="<?php echo base_url(); ?>cmoon"> <?php echo $site_details->site_name; ?></a></div>
                </div>
                    <div class="col-auto">
                         <ul class="nav nav-menu">
                     <li class="nav-item dropdown">
                        <a class="nav-link text-white" title="HOME" href="<?php echo base_url(); ?>cmoon"><i class="fas fa-home"></i></a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link text-white" title="Database Download" href="<?php echo base_url(); ?>cmoon/backup_db"><i class="fas fa-download"></i></a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link text-white" title="LOGOUT" href="<?php echo base_url(); ?>cmoon_login/logout"><i class="fas fa-sign-out icon"></i></a>
                     </li>
                  </ul>
                </div>
            </div>
        </div>     
</header>
<div class="wrapper">
