    <section>
        <div class="container-fluid">
                  <div class="card">
              <div class="card-header">
                <div class="row align-items-center">
                  <div class="col"> Referral Points / Renewal Option </div>
                        <div style="padding-bottom: 20px;"><a href="<?php echo base_url(); ?>cmoon"><button style="float: right;" class="btn btn-outline-dark" type="button"><span class="far fa-arrow-alt-circle-left "> </span>  Back to Menu</button></a></div>
                        <!-- <div style="padding-bottom: 20px;"><a href="<?php echo base_url(); ?>cmoon/referral_renewal_add"><button style="float: right;" class="btn btn-outline-dark" type="button">Add  QR Kits </button></a></div> -->
              </div>
          </div>
<div class="card-body">
<div class="row mb-3">
</div>    
<div class="table-responsive">
   <table class="example table table-striped table-bordered nowrap" style="width:100%">
    <thead>
      <tr>
        <th>Sl.No</th>
        <th>Heading</th>
        <th>Value</th>

        <!-- <th>Referral Points / Renewal Option</th> -->

        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <?php $no=1;  foreach ($result as $row) {   ?>
            <tr>
                <td> <?php echo $no; ?> </td>
                <!-- <td> <a data-fancybox="gallery" href="<?php echo base_url(); ?>cmoon_images/<?php echo $row->image; ?>"><img width="100" height="100" src="<?php echo base_url(); ?>cmoon_images/<?php echo $row->image; ?>"class="img-fluid"></a>  </td> -->
<?php if($row->id == 1){ ?>

                <td> Referral Points</td>
                <td> <?php echo $row->referral_points; ?> </td>
<?php } ?>
<?php if($row->id == 2){ ?>

                <td> Renewal Option </td>
                <td> <?php echo $row->renewal; ?> </td>
<?php } ?>

                <td> <a href="<?php echo base_url(); ?>cmoon/referral_renewal_add/<?php echo $row->id; ?>" class="btn btn-outline-success"> Edit</a> &nbsp; &nbsp;
                <!-- <a onclick="ConfirmDelete(<?php echo $row->id; ?>)" href="JavaScript:Void(0);" class="btn btn-outline-danger">Delete</a> -->
                 </td>
            </tr>
          <?php $no++; } ?>
    </tbody>
</table>
</div></div></div></div>
    </section>
<!-- <script type="text/javascript">
    $('.example').DataTable({
  responsive: true
});
</script> -->
<script type="text/javascript">
      function ConfirmDelete(id)
      {
        swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this data!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
window.location.href='referral_renewal_delete/'+id;       
}
});
}
</script>