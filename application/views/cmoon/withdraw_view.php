    <section>
        <div class="container-fluid">
                  <div class="card">
              <div class="card-header">
                <div class="row align-items-center">
                  <div class="col"> Withdraws </div>
                        <div style="padding-bottom: 20px;"><a href="<?php echo base_url(); ?>cmoon"><button style="float: right;" class="btn btn-outline-dark" type="button"><span class="far fa-arrow-alt-circle-left "> </span>  Back to Menu</button></a></div>
                        <!-- <div style="padding-bottom: 20px;"><button style="float: right;" class="btn btn-primary" type="button" data-toggle="modal" data-target="#exampleModal">Generate QR CODES</button></div> -->

              </div>
          </div>
<div class="card-body">
<div class="row mb-3">
</div>    
<div class="table-responsive">
   <table class="example table table-striped table-bordered  nowrap" id="excel_export" style="width:100%">
    <thead>
      <tr>
        <th>Sl.No</th>
        <th>Amount</th>
        <th>Withdraw On</th>
        <th>Mobile</th>
        <th>Withdraw status</th>
        <th>Withdraw status</th>
        <th>Message</th>
        <th>View</th>
      </tr>
    </thead>
    <tbody>
      <?php $no=1; foreach ($result as $row) {   ?>
            <tr>
                <td> <?php echo $no; ?> </td>
              <td> <?php echo $row->amount; ?> </td>
                <td> <?php echo $row->withdraw_time; ?> </td>
                <td> <?php echo $row->users->mobile; ?> </td>
               
                <td> <?php echo $row->withdraw_status; ?> </td>
                <td><button class="btn btn-primary" type="button" data-toggle="modal" data-target="#withdraw_status<?php echo $row->id ?>">Update Withdraw Status</a> </td> 
                   <td><button class="btn btn-primary" type="button" data-toggle="modal" data-target="#message<?php echo $row->id ?>">Update Message </a> </td> 
                <td><button class="btn btn-primary" type="button" data-toggle="modal" data-target="#details<?php echo $row->id ?>"> View All Details </a> </td> 

                <!-- <td> <a data-fancybox="gallery" href="<?php echo base_url(); ?>cmoon_images/<?php echo $row->image; ?>"><img width="100" height="100" src="<?php echo base_url(); ?>cmoon_images/<?php echo $row->image; ?>"class="img-fluid"></a>  </td> -->
              <!--   <td> 
                  <a href="<?php echo base_url(); ?>cmoon/withdraws_add/<?php echo $row->id; ?>" class="btn btn-outline-success"> Edit</a> &nbsp; &nbsp;
                <a onclick="ConfirmDelete(<?php echo $row->id; ?>)" href="JavaScript:Void(0);" class="btn btn-outline-danger">Delete</a>
                 </td> -->
            </tr>
          <?php $no++; } ?>
    </tbody>
</table>
</div></div></div></div>
    </section>
<!-- <script type="text/javascript">
    $('.example').DataTable({
  responsive: true
});
</script> -->
    

            <?php $no=1; foreach ($result as $row) {   ?>

<div class="modal fade" data-backdrop="false" id="message<?php echo $row->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form action="update_message" method="post">


           <div class="form-group row">
           <label class="col-md-6 col-form-label">Message</label>
           <div class="col-md-12">
            <input type="hidden" name="id" value="<?php echo $row->id ?>">
            <textarea type="text" class="form-control" name="message"   placeholder="Message" / required></textarea>
          </div>
         </div>
    

    <div class="form-group row">
           <label class="col-md-2 col-form-label"></label>
              <div class="col-md-5">
                 <button type="submit" class="btn btn-primary">Submit</button>
                 <button type="reset" class="btn btn-secondary">Reset</button>
              </div>
         </div>
    
       </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

          <?php $no++; } ?>

 <?php $no=1; foreach ($result as $row) {   ?>

<div class="modal fade" data-backdrop="false" id="withdraw_status<?php echo $row->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Withdraw Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form action="withdraw_status_update" method="post">


           <div class="form-group row">
           <label class="col-md-6 col-form-label">Withdraw Status</label>
           <div class="col-md-8">
            <input type="hidden" name="id" value="<?php echo $row->id ?>">
            <select name="withdraw_status" class="form-control">
              <option value="">Please Select</option>
              <!-- <option value="Pending" <?php if($row->withdraw_status == 'Pending'){ echo 'Selected'; } ?>>Pending</option> -->
              
              <option value="ONGOING" <?php if($row->withdraw_status == 'ONGOING'){ echo 'Selected'; } ?> >ONGOING</option>
              <option value="Successfull" <?php if($row->withdraw_status == 'Successfull'){ echo 'Selected'; } ?> >Successfull</option>
            </select>
          </div>
         </div>
    

    <div class="form-group row">
           <label class="col-md-2 col-form-label"></label>
              <div class="col-md-5">
                 <button type="submit" class="btn btn-primary">Submit</button>
                 <button type="reset" class="btn btn-secondary">Reset</button>
              </div>
         </div>
    
       </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

          <?php $no++; } ?>


                <?php  foreach ($result as $row) {   ?>
               <!-- Modal -->
<div class="modal fade" data-backdrop="false" id="details<?php echo $row->id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Full Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
   <div class="row">
        <div class="col-md-6"> 

             <b> Mobile </b> :  <?php echo $row->users->mobile; ?> </br></br> 
             <b>  Withdraw Status </b> :  <?php echo $row->withdraw_status; ?> </br></br> 
             <b>  Amount </b> :  <?php echo $row->amount; ?> </br></br> 
             <b>  Message </b> :  <?php echo $row->message; ?> </br></br> 
           

      </div>
      </div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php } ?>



<script type="text/javascript">
      function ConfirmDelete(id)
      {
        swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this data!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
window.location.href='withdraw_delete/'+id;       
}
});
}
</script>