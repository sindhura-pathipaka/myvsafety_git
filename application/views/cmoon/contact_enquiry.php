    <section>
        <div class="container-fluid">
                  <div class="card">
              <div class="card-header">
                <div class="row align-items-center">
                  <div class="col">Contact Enquiries</div>
                        <div style="padding-bottom: 20px;"><a href="<?php echo base_url(); ?>cmoon"><button style="float: right;" class="btn btn-outline-dark" type="button"><span class="far fa-arrow-alt-circle-left "> </span>  Back to Menu</button></a></div>
                       <!--  <div style="padding-bottom: 20px;"><a href="<?php echo base_url(); ?>cmoon/careers_add"><button style="float: right;" class="btn btn-outline-dark" type="button">Add careers</button></a></div> -->
              </div>
          </div>
<div class="card-body">

<!-- <a href="data_export_to_excel/contact_enquiry"><button style="float: right;" class="btn btn-primary" type="button">Export Data</button></a> -->
                        </br></br></br>

<div class="row mb-3">
</div>    
<div class="table-responsive">
   <table class="example table table-striped table-bordered nowrap" style="width:100%">
    <thead>
      <tr>
        <th>Sl.No</th>
        <th>User Name</th>
        <th>Name</th>
        <th>Phone</th>
        <th>Email</th>
        <!-- <th>Context</th> -->
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <?php $no=1;  foreach ($result as $row) {   ?>
            <tr>
                <td> <?php echo $no; ?> </td>
                <td> <?php echo $row->users->name; ?> </td>
                <td> <?php echo $row->name; ?> </td>
                <td> <?php echo $row->mobile; ?> </td>
                <td> <?php echo $row->email; ?> </td> 
                   <!-- Resume: <a href='$resume_full_path' download> click here </a> to download the resume."; -->
                <!-- <td> <a target="_blank"  href="<?php echo $row->resume; ?>">click here</a> to view  </td>                           -->
                <!-- <td> <?php echo $row->resume; ?> </td> -->
              <!--   <td> <a data-fancybox="gallery" href="<?php echo base_url(); ?>cmoon_images/<?php echo $row->image; ?>"><img width="100" height="100" src="<?php echo base_url(); ?>cmoon_images/<?php echo $row->image; ?>"class="img-fluid"></a>  </td> -->
                <!-- <td> <?php echo $row->description; ?> </td> -->
                <td> 
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal<?php echo $row->id; ?>">  View </button>
                  <!-- <a href="<?php echo base_url(); ?>cmoon/careers_add/<?php echo $row->id; ?>" class="btn btn-outline-success"> Edit</a> &nbsp; &nbsp; --> 
                <a onclick="ConfirmDelete(<?php echo $row->id; ?>)" href="JavaScript:Void(0);" class="btn btn-outline-danger">Delete</a> </td>
            </tr>
          <?php $no++; } ?>
    </tbody>
</table>
</div></div></div></div>
    </section>
      <?php  foreach ($result as $row) {   ?>
               <!-- Modal -->
<div class="modal fade" data-backdrop="false" id="exampleModal<?php echo $row->id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Full Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
             <!-- <u>Problem</u> :  <?php echo $row->problem; ?> </br></br>  -->
             <u>Name</u> :  <?php echo $row->name; ?> </br></br> 
             <!-- <u>Aadhar Number</u> :       <?php echo $row->aadhar_num; ?> </br></br>  -->
             <!-- <u>District</u> :       <?php echo $row->district; ?> </br></br>  -->
             <!-- <u>Mandal</u> : <?php echo $row->mandal; ?> </br></br>  -->
             <u>Email</u> :      <?php echo $row->email; ?></br></br> 
             <u>Mobile</u> :      <?php echo $row->mobile; ?></br></br> 
             <u>Message</u> :      <?php echo $row->description; ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<script type="text/javascript">
    $('.example').DataTable({
  responsive: true
});
</script>
<script type="text/javascript">
      function ConfirmDelete(id)
      {
        swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this data!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
window.location.href='contact_enquiry_delete/'+id;       
}
});
}
</script>