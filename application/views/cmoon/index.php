    <section>
        <div class="container-fluid">
            <div class="card mb-3">
                <div class="card-header font-weight-bold "><span class="far fa-window-restore"></span> APP Content</div>
                <div class="card-body">
                    <div class="row">
                       <!--  <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/home_banners_view" class="anchor">
                                <div class="anchor__icon fas fa-align-left"></div>
                                <div class="anchor__info"><strong>Banners</strong></div>
                            </a>
                        </div>   -->   
                        <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/banner_images_view" class="anchor">
                                <div class="anchor__icon fas fa-align-left"></div>
                                <div class="anchor__info"><strong>Dashboard Images</strong></div>
                            </a>
                        </div>             
                        <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/cms_pages_view" class="anchor">
                                <div class="anchor__icon fas fa-align-left"></div>
                                <div class="anchor__info"><strong>About & T&C & Privacy Policy</strong></div>
                            </a>
                        </div>    

                          <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/referral_renewal_view" class="anchor">
                                <div class="anchor__icon fas fa-align-left"></div>
                                <div class="anchor__info"><strong>Referral Points & Renewal Option</strong></div>
                            </a>
                        </div>                
                       <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/faqs_view" class="anchor">
                                <div class="anchor__icon fas fa-align-left"></div>
                                <div class="anchor__info"><strong>FAQ's</strong></div>
                            </a>
                        </div>                

                         <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/qr_kits_view" class="anchor">
                                <div class="anchor__icon fas fa-align-left"></div>
                                <div class="anchor__info"><strong>QR Kits</strong></div>
                            </a>
                        </div> 


                         <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/bug_issues_view" class="anchor">
                                <div class="anchor__icon fas fa-align-left"></div>
                                <div class="anchor__info"><strong>BUG Issues</strong></div>
                            </a>
                        </div>    

                         <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/services_types_view" class="anchor">
                                <div class="anchor__icon fas fa-align-left"></div>
                                <div class="anchor__info"><strong>Vechicle service types</strong></div>
                            </a>
                        </div> 


                         <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/document_types_view" class="anchor">
                                <div class="anchor__icon fas fa-align-left"></div>
                                <div class="anchor__info"><strong>Document types</strong></div>
                            </a>
                        </div>

                         <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/notifications_view" class="anchor">
                                <div class="anchor__icon fas fa-align-left"></div>
                                <div class="anchor__info"><strong>Notifications</strong></div>
                            </a>
                        </div>                

                        <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/contact_enquiry" class="anchor">
                                <div class="anchor__icon fas fa-users"></div>
                                <div class="anchor__info"><strong>App Enquiries</strong></div>
                            </a>
                        </div>   



                         <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/qr_code_genereation_view" class="anchor">
                                <div class="anchor__icon fas fa-align-left"></div>
                                <div class="anchor__info"><strong>QR Code Generation</strong></div>
                            </a>
                        </div> 


                         <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/user_accounts" class="anchor">
                                <div class="anchor__icon fas fa-users"></div>
                                <div class="anchor__info"><strong>User Registrations</strong></div>
                            </a>
                        </div>   

                          <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/orders_view" class="anchor">
                                <div class="anchor__icon fas fa-users"></div>
                                <div class="anchor__info"><strong>User ORDERS</strong></div>
                            </a>
                        </div> 

                        <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/withdraw_view" class="anchor">
                                <div class="anchor__icon fas fa-users"></div>
                                <div class="anchor__info"><strong>User WITHDRAW Details</strong></div>
                            </a>
                        </div>   


                        <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/vehicles" class="anchor">
                                <div class="anchor__icon fas fa-align-left"></div>
                                <div class="anchor__info"><strong>Vehicles</strong></div>
                            </a>
                        </div>   
                          
                    </div>
                </div>
            </div>
    </div>
     <div class="container-fluid">
            <div class="card mb-3">
                <div class="card-header font-weight-bold "><span class="fas fa-cog"></span> Website Content</div>
                <div class="card-body">
                    <div class="row">

                            <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/banners_view" class="anchor">
                                <div class="anchor__icon fas fa-align-left"></div>
                                <div class="anchor__info"><strong>Banner Images</strong></div>
                            </a>
                        </div>   

                            <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/features_view" class="anchor">
                                <div class="anchor__icon fas fa-align-left"></div>
                                <div class="anchor__info"><strong>Features</strong></div>
                            </a>
                        </div>   

                            <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/steps_view" class="anchor">
                                <div class="anchor__icon fas fa-align-left"></div>
                                <div class="anchor__info"><strong>Steps</strong></div>
                            </a>
                        </div> 

                         <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/contact_enquiry_website" class="anchor">
                                <div class="anchor__icon fas fa-users"></div>
                                <div class="anchor__info"><strong>Website Enquiries</strong></div>
                            </a>
                        </div>     
                     
                    </div>
                </div>
            </div>
          </div>
               <div class="container-fluid">
            <div class="card mb-3">
                <div class="card-header font-weight-bold "><span class="fas fa-cog"></span> SITE SETTINGS</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/site_details" class="anchor">
                                <div class="anchor__icon fas fa-suitcase"></div>
                                <div class="anchor__info"><strong>Site details</strong></div>
                            </a>
                        </div>
                       <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/social_links" class="anchor">
                                <div class="anchor__icon fas fa-users"></div>
                                <div class="anchor__info"><strong>Social Links & Seo Tags</strong></div>
                            </a>
                        </div>
                       <!--    <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/seo_tags_view" class="anchor">
                                <div class="anchor__icon fas fa-align-left"></div>
                                <div class="anchor__info"><strong>SEO Tags</strong></div>
                            </a>
                        </div> -->
                        <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/manage_uploads_view" class="anchor">
                                <div class="anchor__icon fas fa-cloud-upload-alt"></div>
                                <div class="anchor__info"><strong>Manage Uploads</strong></div>
                            </a>
                        </div> 
                        <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/change_password" class="anchor">
                                <div class="anchor__icon fas fa-key"></div>
                                <div class="anchor__info"><strong>Change Password</strong></div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/logs" class="anchor">
                                <div class="anchor__icon fas fa-address-book"></div>
                                <div class="anchor__info"><strong>Logs</strong></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
          </div>
    </section>