<!DOCTYPE html>
<html>
<body>
<section>
   <div class="container-fluid">
      <div class="row justify-content-center">
         <div class="col-md-12">
            <div class="card">

            <form id="change_password"  method="POST" enctype="multipart/form-data" autocomplete="off">
               <div style="padding-bottom: 20px;" class="card-header">Change Password <a href="<?php echo base_url(); ?>cmoon"><button style="float: right;" type="button" class="btn btn-dark"><span class="far fa-arrow-alt-circle-left "> </span> Back to menu</button></a></div>
               <div class="card-body">  

                  <div class="form-group row">
                     <label class="col-md-2 col-form-label">Current Password</label>
                     <div class="col-md-5"><input type="password" id="password" class="form-control" name="password" placeholder="Enter Old Password" /></div>
                  </div>

                  <div class="form-group row">
                     <label class="col-md-2 col-form-label">New Password</label>
                     <div class="col-md-5"><input type="password" id="new_password" class="form-control" name="new_password" placeholder="Enter New Password" /></div>
                  </div>

                  <div class="form-group row">
                     <label class="col-md-2 col-form-label">Confirm New Password </label>
                     <div class="col-md-5"><input type="password" class="form-control"  id="confirm_password" name="confirm_password" placeholder="Confirm New Password" /></div>
                  </div>               


               <div class="form-group row">
                  <label class="col-md-2 col-form-label"></label>
                              <div class="col-md-9 ">
                                   <button type="submit" class="btn btn-primary" >Confirm</button>
                                   <button type="reset" class="btn btn-secondary">Reset</button>
               </div>

               </div>
                   
               </form>
             
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

<script type="text/javascript">
    $(function(){
      // Setup form validation on the #register-form element
        $("#change_password").validate({
        // Specify the validation rules
       rules:
         {
           password:
           {
             required: true
           },
           new_password: {
           required : true           
         },
            confirm_password: {
             required : true,
             equalTo: "#new_password"
           }
         },
         // Messages for form validation
         messages:
         {
           password:
           {
             required: 'Please enter Old Password'
           },
           new_password:
           {
             required: 'Please enter New Password'
            
           },
           confirm_password:
           {
             required: 'Confirm password',
             equalTo: 'Password doesnot match with new password'
           }
         }, 
      });
    });
  </script>