<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<!-- <title>My V Safety</title> -->
    <base href="<?php echo base_url(); ?>">

   <?php if($pagewise_metatags->meta_title != ''){ ?>
    <title><?php echo $pagewise_metatags->meta_title; ?></title>
    <?php }else{ ?>
    <title><?php echo $social_links->meta_title; ?></title>
    <?php } ?>
    <?php if($pagewise_metatags->meta_description != ''){ ?>
    <meta name="description" content="<?php echo $pagewise_metatags->meta_description; ?>">
    <?php }else{ ?>
    <meta name="description" content="<?php echo $social_links->meta_description; ?>">
    <?php } ?>
    <?php if($pagewise_metatags->meta_keywords != ''){ ?>
    <meta name="keywords" content="<?php echo $pagewise_metatags->meta_keywords; ?>">
    <?php }else{ ?>
    <meta name="keywords" content="<?php echo $social_links->meta_keywords; ?>">
    <?php } ?>


<!-- Stylesheets -->
<link href="site_assets/css/bootstrap.css" rel="stylesheet">
<link href="site_assets/css/main.css" rel="stylesheet">
<link href="site_assets/css/responsive.css" rel="stylesheet">

<link rel="shortcut icon" href="site_assets/img/favicon.png" type="image/x-icon">
<link rel="icon" href="site_assets/img/favicon.png" type="image/x-icon">

<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

  <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
</head>

<body>

<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>
 	
    <!-- Main Header-->
    <header class="main-header">
    	
    	<!--Header-Upper-->
        <div class="header-upper">
        	<div class="auto-container">
            	<div class="clearfix">
                	
                	<div class="pull-left logo-box">
                    	<div class="logo"><a href="index"><img src="cmoon_images/<?php echo $site_details->site_logo ?>" alt="" title=""></a></div>
                    </div>
                   	
                   	<div class="nav-outer clearfix">
						<!--Mobile Navigation Toggler-->
                        <div class="mobile-nav-toggler"><span class="icon icons-menu-button"></span></div>
						<!-- Main Menu -->
						<nav class="main-menu show navbar-expand-md">
							<div class="navbar-header">
								<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>

							<div class="navbar-collapse collapse scroll-nav clearfix" id="navbarSupportedContent">
								<ul class="navigation clearfix">
									<li class="current"><a href="#home-banner">Home</a></li>
									<li><a href="#featured">About Us</a></li>
									<li><a href="#how-works">How it works</a></li>
									<li><a href="#contact">Contact us</a></li>
								</ul>
							</div>
							
						</nav>
						
						<div class="outer-box">
							<a href="<?php echo $social_links->linkedin ?>" target="_blank" class="theme-btn btn-style-one"><span class="txt">Download app</span></a>
						</div>
						
					</div>
                   
                </div>
            </div>
        </div>
        <!--End Header Upper-->
        
		
		
		<!-- Mobile Menu  -->
        <div class="mobile-menu">
            <div class="menu-backdrop"></div>
            <div class="close-btn"><span class="icon icons-multiply"></span></div>
            
            <nav class="menu-box">
                <div class="nav-logo"><a href="index"><img src="cmoon_images/<?php echo $site_details->site_logo ?>" alt="" title=""></a></div>
                <div class="menu-outer"><!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header--></div>
            </nav>
        </div><!-- End Mobile Menu -->
		
    </header>
    <!--End Main Header -->
	
	<!--Banner Section-->
    <section class="banner-section" id="home-banner">
		<!-- <div class="patern-layer-one" style="background-image: url(images/background/banner-bg.png)"></div>
		<div class="bg-layer" style="background-image: url(images/background/2.jpg)"></div> -->
		<img src="site_assets/img/hero-bg.svg" class="shape-image" alt="shape">
		<div class="auto-container">
			<div class="row clearfix">
			
				<!-- Content Column -->
				<div class="content-column col-lg-7 col-md-12 col-sm-12">
					<div class="inner-column">
						<h1>Get connect to other. <br> Just ask MY V SAFETY</h1>
						<div class="text">Access to opportunities for career advancement and <br> professional growth relies heavily on connections.</div>
						<div class="newsletter-form">
							<form method="post" action="#">
								<div class="form-group">
									<button type="submit" class="theme-btn btn-style-two"><span class="txt">Take free trial</span></button>
								</div>
							</form>
						</div>
						
					</div>
				</div>
				
				<!-- Carousel Column -->
				<div class="carousel-column col-lg-5 col-md-12 col-sm-12">
					<div class="inner-column">
						
						<div class="slider-outer">
            	
							<!-- Custom Pager -->
							<!-- <div class="pager-box">
								<div class="inner-box">
									<div class="banner-pager clearfix" id="banner-pager">
									  <a class="pager one wow zoomIn" data-wow-delay="100ms" data-wow-duration="1500ms" data-slide-index="0" href="#"><div class="image img-circle">1</div></a>
									  <a class="pager two wow zoomIn" data-wow-delay="200ms" data-wow-duration="1500ms" data-slide-index="1" href="#"><div class="image img-circle">2</div></a>
									  <a class="pager three wow zoomIn" data-wow-delay="300ms" data-wow-duration="1500ms" data-slide-index="2" href="#"><div class="image img-circle">3</div></a>
									</div>
								</div>
							</div> -->
						
							<!-- Column / Slides -->
							<div class="slides-box" style="background-image: url(site_assets/img/mobile.png)">
								<div class="inner-box">
									
									<div class="banner-slider">
									
									<?php foreach ($banners as $banners_row) { ?>
										<div class="slide-item">
											<div class="image">
												<img src="cmoon_images/<?php echo $banners_row->image ?>" alt="" />
											</div>
										</div>
									<?php } ?>
										
									<!-- 	<div class="slide-item">
											<div class="image">
												<img src="site_assets/img/screenshot01.png" alt="" />
											</div>
										</div>
										
										<div class="slide-item">
											<div class="image">
												<img src="site_assets/img/screenshot01.png" alt="" />
											</div>
										</div> -->
										
									</div>
								</div>
								
<!-- 								<div class="mobile-small-image">
									<img src="site_assets/images/resource/play.png" alt="" />
								</div>
								
								<div class="heart-image-icon">
									<img src="site_assets/images/icons/heart.png" alt="" />
								</div>
								
								<div class="plus-small-image">
									<img src="site_assets/images/icons/plus.png" alt="" />
								</div> -->
								
							</div>
								
						</div>
						
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!--End Banner Section-->
	
	<!-- Featured Section -->
	<section class="featured-section" id="featured">
		<div class="auto-container">
			<div class="row clearfix">
				
				<!-- Content Column -->
				<div class="content-column col-lg-5 col-md-12 col-sm-12">
					<div class="iner-column">
						<div class="sec-title">
							<div class="title"><span>Fe</span>atures</div>
							<h2><?php echo $features1->heading ?></h2>
						</div>
					<?php echo $features1->description ?>
						<a href="#contact" class="theme-btn btn-style-three"><span class="txt">Contact Us</span></a>
					</div>
				</div>
				
				<!-- Blocks Column -->
				<div class="blocks-column col-lg-7 col-md-12 col-sm-12">
					<div class="inner-column">
						<!-- Cloud Icon -->
						<div class="cloud-icon">
							<img src="site_assets/images/icons/cloud-icon.png" alt="" />
						</div>
						<div class="row clearfix">
							
							<!-- Featured Block -->
							<div class="featured-block titlt col-lg-6 col-md-6 col-sm-12" data-tilt data-tilt-max="8">
								<div class="inner-box">
									<div class="icon-box">
										<span class="icon flaticon-writing"></span>
									</div>
									<h3><?php echo $features2->heading ?></h3>
									<div class="text"><?php echo $features2->description ?></div>
								</div>
							</div>
							
							<!-- Featured Block -->
							<div class="featured-block titlt style-two col-lg-6 col-md-6 col-sm-12" data-tilt data-tilt-max="6">
								<div class="inner-box">
									<div class="icon-box">
										<span class="icon flaticon-protection-shield-with-a-check-mark"></span>
									</div>
									<h3><?php echo $features3->heading ?></h3>
									<div class="text"><?php echo $features3->description ?></div>
								</div>
							</div>
							
							<!-- Featured Block -->
							<div class="featured-block titlt style-three col-lg-6 col-md-6 col-sm-12" data-tilt data-tilt-max="10">
								<div class="inner-box">
									<div class="icon-box">
										<span class="icon flaticon-dashboard"></span>
									</div>
									<h3><?php echo $features4->heading ?></h3>
									<div class="text"><?php echo $features4->description ?></div>
								</div>
							</div>
							
							<!-- Featured Block -->
							<div class="featured-block style-four col-lg-6 col-md-6 col-sm-12">
								<div class="inner-box">
									<div class="icon-box">
										<span class="icon flaticon-writing"></span>
									</div>
									<h3><?php echo $features5->heading ?></h3>
									<div class="text"><?php echo $features5->description ?></div>
								</div>
							</div>
							
						</div>
						<!-- Side Image -->
						<div class="side-image">
							<img src="site_assets/images/resource/girl-icon.png" alt="" />
						</div>
						
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!-- End Featured Section -->
	
	<!-- Steps Section -->
	<section class="steps-section" id="how-works">
		<div class="patern-layer-one" style="background-image: url(site_assets/images/background/pattern-2.png)"></div>
		<div class="patern-layer-two" style="background-image: url(site_assets/images/background/pattern-3.png)"></div>
		<div class="patern-layer-three" style="background-image: url(site_assets/images/background/pattern-4.png)"></div>
		<div class="auto-container">
			
			<!-- Carousel Wrapper -->
			<div id="steps-thumb" class="carousel slide carousel-thumbnails" data-ride="carousel">
				<div class="row clearfix">
				
					<!-- Carousel Column -->
					<div class="carousel-column col-lg-6 col-md-12 col-sm-12">
						<!-- Slides -->
						<div class="carousel-inner" role="listbox" style="background-image: url(site_assets/images/resource/mobile.png)">
							<div class="slides">
							
								<?php $no=1; foreach ($steps_images as $steps_images_row) { ?>

								<div class="carousel-item <?php if($no==1){ ?>active <?php } ?>">
									<div class="content">
										<div class="image">
											<a href="cmoon_images/<?php echo $steps_images_row->image ?>" data-fancybox="steps" data-caption="" class="image-link lightbox-image"><img src="cmoon_images/<?php echo $steps_images_row->image ?>" alt="" /></a>
										</div>
									</div>
								</div>
								<?php $no++; } ?>
								
								<!-- <div class="carousel-item">
									<div class="content">
										<div class="image">
											<a href="site_assets/images/resource/mokeup-2.jpg" data-fancybox="steps" data-caption="" class="image-link lightbox-image"><img src="site_assets/images/resource/mokeup-2.jpg" alt="" /></a>
										</div>
									</div>
								</div>
								
								<div class="carousel-item">
									<div class="content">
										<div class="image">
											<a href="site_assets/images/resource/mokeup-3.jpg" data-fancybox="steps" data-caption="" class="image-link lightbox-image"><img src="site_assets/images/resource/mokeup-3.jpg" alt="" /></a>
										</div>
									</div>
								</div> -->
							
							</div>
						</div>
					</div>
					
					<div class="blocks-column col-lg-6 col-md-12 col-sm-12">
						<div class="inner-column">
						
							<!-- Sec Title -->
							<div class="sec-title style-two">
								<div class="title"><span>St</span>eps</div>
								<h2>We have some <span> eas</span>y <span>ste</span>p<span>s</span>! </h2>
							</div>
							
							<!-- Controls-->
							<ul class="carousel-indicators">
								
								<li data-target="#steps-thumb" data-slide-to="0" class="tab-btn active wow" data-wow-delay="0ms" data-wow-duration="1500ms">
									<span class="number">1</span>
									<span class="icon"><i class="flaticon-logout"></i></span>
									<strong><?php echo $steps5->heading ?></strong>
									<?php echo $steps5->description ?>
								</li>
								
								<li data-target="#steps-thumb" data-slide-to="1" class="tab-btn wow" data-wow-delay="0ms" data-wow-duration="1500ms">
									<span class="number">2</span>
									<span class="icon"><i class="flaticon-credit-card"></i></span>
									<strong><?php echo $steps4->heading ?></strong>
									<?php echo $steps4->description ?>								</li>
								
								<li data-target="#steps-thumb" data-slide-to="2" class="tab-btn wow" data-wow-delay="0ms" data-wow-duration="1500ms">
									<span class="number">3</span>
									<span class="icon"><i class="flaticon-gift"></i></span>
									<strong><?php echo $steps3->heading ?></strong>
									<?php echo $steps3->description ?>
								</li>
								
							</ul>
						
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
	</section>
	<!-- End Steps Section -->
	
	<!-- Contact Section -->
	<section class="contact-section" id="contact">
		<div class="auto-container">
			<div class="row clearfix">
				
				<!-- Info Column -->
				<div class="info-column col-lg-6 col-md-12 col-sm-12">
					<div class="inner-column">
						<div class="patern-layer-one" style="background-image: url(site_assets/images/background/pattern-12.png)"></div>
						<div class="patern-layer-two" style="background-image: url(site_assets/images/background/pattern-13.png)"></div>
						<div class="patern-layer-three paroller" data-paroller-factor="-0.10" data-paroller-factor-lg="0.08" data-paroller-type="foreground" data-paroller-direction="horizontal" style="background-image: url(site_assets/images/background/pattern-14.png)"></div>
						<ul class="info-list">
							<li>
								<span class="icon flaticon-address"></span>
								<strong>Our head office address:</strong>
								<?php echo $site_details->address ?>
							</li>
							<li>
								<span class="icon flaticon-telephone"></span>
								<strong>Call for help:</strong>
								<a href="tel:<?php echo $site_details->site_number ?>"><?php echo $site_details->site_number ?></a><br>
								<a href="tel:<?php echo $site_details->phone_number ?>"><?php echo $site_details->phone_number ?></a>
							</li>
							<li>
								<span class="icon flaticon-invite"></span>
								<strong>Mail us:</strong>
								<a href="mailto:<?php echo $site_details->site_email ?>" ><?php echo $site_details->site_email ?></a><br>
								<a href="mailto:<?php echo $site_details->email_id ?>" ><?php echo $site_details->email_id ?></a>
							</li>
						</ul>
					</div>
				</div>
				
				<!-- Form Column -->
				<div class="form-column col-lg-6 col-md-12 col-sm-12">
					<div class="inner-column">
						<div class="sec-title style-three">
							<div class="title"><span>Co</span>ntact</div>
							<h2>Contact with our support <br> during <span>emer</span>g<span>enc</span>y!</h2>
						</div>
						
						<!-- Default Form -->
						<div class="default-form">

							<h5 style="color: green;" id="email_success"></h5>
                            <h5 style="color: red;" id="email_error"></h5>

  <form id="form_details" method="post"  action="<?php echo base_url(); ?>view/contact_form" enctype="multipart/form-data">

							<!-- <form method="post" action="#" id="contact-form"> -->
								<div class="row clearfix">
									
									<div class="col-lg-6 col-md-6 col-sm-12 form-group">
										<input type="text" name="name" id="name" placeholder="Name"  oninput="this.value = this.value.replace(/[^a-zA-Z.\s]/g, '').replace(/(\..*)\./g, '$1');"  placeholder="Full Name *" required>
									</div>
									
									<div class="col-lg-6 col-md-6 col-sm-12 form-group">
										<input type="text" name="lastname" placeholder="Last Name *" oninput="this.value = this.value.replace(/[^a-zA-Z.\s]/g, '').replace(/(\..*)\./g, '$1');"  required>
									</div>
									
									<div class="col-lg-6 col-md-6 col-sm-12 form-group">
										<input type="email" name="email" placeholder="Your mail *" required>
									</div>
									
									<div class="col-lg-6 col-md-6 col-sm-12 form-group">
										<input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" name="mobile"  placeholder="Phone" minlength="10" maxlength="10"  placeholder="Phone number *" required>
									</div>
									
									<div class="col-lg-12 col-md-12 col-sm-12 form-group">
										<textarea name="message" placeholder="Message..."></textarea>
									</div>
        <div class="form-group">
        <div class="g-recaptcha" data-sitekey="6LeKDK4UAAAAAAiCx72ClW1HwcG_JEfjymWy-I3O" style="transform:scale(2);-webkit-transform:scale(1);transform-origin:0 0;-webkit-transform-origin:0 0; ">
        </div></div>

        						<div class="col-lg-12 col-md-12 col-sm-12 form-group">
										<button class="theme-btn submit-btn" type="submit" name="submit"><span class="txt"> <i class="fa fa-arrow-circle-right"></i> &nbsp; Send now</span></button>
									</div>
									
								</div>
							</form>
								
						</div>
						
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!-- End Contact Section -->
	
	
	<!-- Main Footer -->
    <footer class="main-footer">
    	<div class="auto-container">
        	<!--Widgets Section-->
            <div class="widgets-section">
            	<div class="row clearfix">
                	
                    <!-- Footer Column -->
					<div class="footer-column col-lg-4 col-md-6 col-sm-12">
						<div class="footer-widget logo-widget">
							<div class="logo">
								<a href="index"><img src="cmoon_images/<?php echo $site_details->footer_logo ?>" alt="" /></a>
							</div>
							<div class="text"><?php echo $site_details->footer_context ?></div>
							
						</div>
					</div>
							
					<!-- Footer Column -->
					<div class="footer-column col-lg-4 col-md-6 col-sm-12">
						<div class="footer-widget links-widget">
							<div class="row clearfix">
								<div class="column col-lg-6 offset-lg-3 col-md-6 col-xs-12">
									<ul>

										<li><a href="#home-banner">Home</a></li>
									<li><a href="#featured">About Us</a></li>
									<li><a href="#how-works">How it works</a></li>
									<li><a href="#contact">Contact us</a></li>
									
									</ul>
								</div>
								
							</div>
						</div>
					</div>
					
					<!-- Footer Column -->
					<div class="footer-column col-lg-4 col-md-6 col-sm-12">
						<div class="footer-widget email-widget">
							<div class="text">Download a trial version to make life easy!</div>
							
							<div class="btns">
								<a href="<?php echo $social_links->linkedin ?>" target="_blank"><img src="site_assets/images/icons/google-1.png" alt="" /></a>
							</div>
							<ul class="social-icon-one">
								<li class="facebook"><a href="<?php echo $social_links->facebook ?>" target="_blank" ><span class="fa fa-facebook"></span></a></li>
								<li class="twitter"><a href="<?php echo $social_links->twitter ?>" target="_blank" ><span class="fa fa-twitter"></span></a></li>
								<li class="dribbble"><a href="<?php echo $social_links->youtube ?>" target="_blank" ><span class="fa fa-dribbble"></span></a></li>
								<li class="behance"><a href="<?php echo $social_links->skype ?>" target="_blank" ><span class="fa fa-behance"></span></a></li>
							</ul>
						</div>
					</div>
					
				</div>
			</div>
		
			<div class="footer-bottom">
				<div class="clearfix">
					<div class="pull-left">
						<div class="copyright">&copy; Copyright 2021 <a href="#">myvsafety.com</a> All Rights Reserved.</div>
					</div>
					<div class="pull-right">
						<ul class="footer-nav">
							<li>Designed & Developed by <a href="https://www.thecolourmoon.com/" target="_blank">Colourmoon</a></li>
							
						</ul>
					</div>
				</div>
			</div>
		
		</div>
	</footer>
	<!-- End Main Footer -->
	
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-arrow-circle-up"></span></div>


<script src="site_assets/js/jquery.js"></script>
<script src="site_assets/js/popper.min.js"></script>
<script src="site_assets/js/pagenav.js"></script>
<script src="site_assets/js/jquery.scrollTo.js"></script>
<script src="site_assets/js/bootstrap.min.js"></script>
<script src="site_assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="site_assets/js/jquery.fancybox.js"></script>
<script src="site_assets/js/appear.js"></script>
<script src="site_assets/js/swiper.min.js"></script>
<script src="site_assets/js/jquery.paroller.min.js"></script>
<script src="site_assets/js/parallax.min.js"></script>
<script src="site_assets/js/validate.js"></script>
<script src="site_assets/js/bxslider.js"></script>

<script src="site_assets/js/swiper.min.js"></script>
<script src="site_assets/js/tilt.jquery.min.js"></script>
<script src="site_assets/js/owl.js"></script>
<script src="site_assets/js/wow.js"></script>
<script src="site_assets/js/jquery-ui.js"></script>
<script src="site_assets/js/script.js"></script>


    

<script type="text/javascript">
    $(function(){
      // Setup form validation on the #register-form element
        $("#form_details").validate({
        // Specify the validation rules
        rules: {
          name: {
              required: true
            },
         phone: {
              required: true
            },
          email: {
              required: true,
              email: true
            },
        lastname: {
              required: true
            }, 
        message: {
              required: true
            }
        },       
        // Specify the validation error messages
        messages: {
        name: {
              required: 'Field should not be empty'
            },
             phone: {
              required: 'Field should not be empty'
            },
          email: {
              required: 'Field should not be empty',
              email: 'Please enter a valid email id'
            },
        lastname: {
               required: 'Field should not be empty'
            }, 
        message: {
               required: 'Field should not be empty'
            }
        },
      });
    });
  </script>

<script>

        $(function () {
        $('#form_details').on('submit', function (e) {
          e.preventDefault();
          $.ajax({
            type: 'post',
            dataType: 'json',
            url: '<?php echo base_url(); ?>view/contact_form',
            data: $('#form_details').serialize(),
            success: function (responce) {

              if(responce.msg == 'success'){
                  $('#email_error').hide();
                  $('#email_success').show();
                  $('#email_success').html(responce.text);
                  $("#form_details")[0].reset();
              }

              if(responce.msg == 'error'){
                  $('#email_success').hide();
                  $('#email_error').show();
                  $('#email_error').html(responce.text);
                  // $("#form_details")[0].reset();
              }

            }
          });
        });
      });

      </script> 



</body>
</html>