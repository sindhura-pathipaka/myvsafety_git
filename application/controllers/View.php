<?php
 ob_start();
 defined('BASEPATH') or exit('No direct script access allowed');
class View extends CI_Controller
{
    public $data;
    public function __construct()
    { 
        parent::__construct();
        // $this->load->model('login_model','',True);
        $this->load->model('site_model', '', true);
        $this->load->model('cmoon_model', '', true);
        // $this->load->helper('sms');
    }
    //------------------------------  pages --------------------------------------
    // public function error()
    // {      
    //     $this->load->view('includes/header', $this->data);
    //     $this->load->view('error_page', $data);
    //     $this->load->view('includes/footer');
    // }
    public function index()
    {
        $data["site_details"]=$this->cmoon_model->get_row_by_row('site_details', '1');
        $data["social_links"]=$this->cmoon_model->get_row_by_row('social_links', '1');
        $data['features1'] = $this->site_model->get_cms('features','1');
        $data['features2'] = $this->site_model->get_cms('features','2');
        $data['features3'] = $this->site_model->get_cms('features','3');
        $data['features4'] = $this->site_model->get_cms('features','4');
        $data['features5'] = $this->site_model->get_cms('features','5');
        $data['steps3'] = $this->site_model->get_cms('steps','3');
        $data['steps4'] = $this->site_model->get_cms('steps','4');
        $data['steps5'] = $this->site_model->get_cms('steps','5');
        $data['banners'] = $this->site_model->get('banners');
        $data['steps_images'] = $this->site_model->get('steps_images');
        $this->load->view('index', $data);
    }
// ---------------------------- Contact Us Page -----------------------------------------------------------------
public function contact_form(){
  if($this->input->post('name', TRUE) != '' && $this->input->post('email', TRUE) != '' &&  $this->input->post('mobile', TRUE) != '' ){
 if($this->input->post()){
  // $p_link = $this->input->post('p_link', TRUE);
    if(!empty($_POST['g-recaptcha-response'])){
        $secret = '6LeKDK4UAAAAAORJGSge58hWR5iwGcrfaESvaX0L';
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
        $responseData = json_decode($verifyResponse);
        if($responseData->success)
        {
  $lastname = $this->input->post('lastname', TRUE);
  $name = $this->input->post('name', TRUE);
  $email = $this->input->post('email', TRUE);
  $mobile = $this->input->post('mobile', TRUE);
  $message = $this->input->post('message', TRUE);
  $to_mail = $this->data["site_details"]->site_email;
  $from_email = $this->data["site_details"]->from_email;
  $site_name = $this->data["site_details"]->site_name;
  $email_message = "Hi Team, <br>
                    Below are the details of the client Enquired in the site.<br><br>
                    Name: $name <br>
                    Last Name: $lastname <br>
                    Email: $email <br>
                    Phone Number: $mobile <br>
                    Message: $message ";
       $this->load->library('email');
$client_mail=$email;
require_once (APPPATH.'libraries/vendor/autoload.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/PHPMailer.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/SMTP.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/Exception.php');
//PHPMailer Object  
$mail = new PHPMailer\PHPMailer\PHPMailer();
//-------------------- END SMTP --------------------
//From email address and name
$mail->From = $from_email;
$mail->FromName = $site_name;
//To address and name
// $mail->addAddress('sindhura.colourmoon@gmail.com'); //Recipient name is optional
$mail->addAddress($to_mail); //Recipient name is optional
//Address to which recipient will reply
$mail->addReplyTo($from_email, "Reply");
//CC and BCC
// $mail->addCC("cc@example.com");
// $mail->addBCC("bcc@example.com");
//Send HTML or Plain Text email
       // $subject="$first_name-Profile";
$subject1=" Enquiry mail in the site by $name";
$mail->isHTML(true);
$mail->Sender = $from_email;
$mail->Subject = $subject1;
$mail->Body = $email_message;
// $mail->AltBody = "This is the plain text version of the email content";
$sucess = $mail->send();
  if ($sucess) { 
   $form_data['name'] = $this->input->post('name', TRUE);
   $form_data['lastname'] = $this->input->post('lastname', TRUE);
   $form_data['mobile'] = $this->input->post('mobile', TRUE);
   $form_data['email'] = $this->input->post('email', TRUE);
   $form_data['message'] = $this->input->post('message', TRUE);
date_default_timezone_set('Asia/Kolkata');
        $form_data['created_at'] =  date( 'd-m-Y h:i:s A');
$this->cmoon_model->insert('contact_enquiry_website',$form_data);
        $responce=array('msg'=>'success','text'=>'Thank you for showing your interest, We will address your request very soon.');
            echo json_encode($responce);
    } else { 
      $responce=array('msg'=>'error','text'=>'*Sorry something went wrong please try again.');
            echo json_encode($responce);
        // redirect($p_link);
    }
   }else{
         $responce=array('msg'=>'error','text'=>'*Invalid captcha please try again');
            echo json_encode($responce); 
          // redirect($p_link);     
   }
  }else{
       $responce=array('msg'=>'error','text'=>'*Invalid captcha please try again');
            echo json_encode($responce);
          // redirect($p_link); 
  }
}
}
}
// ---------------------------- Image upload code -----------------------------------------------------------------
    private function upload_file($file_name, $path = null)
    {
        $upload_path1             = "cmoon_images".$path;
        $config1['upload_path']   = $upload_path1;
        $config1['allowed_types'] = "gif|jpg|png|jpeg";
        $config1['max_size']      = "100480000";
        $img_name1                = strtolower($_FILES[$file_name]['name']);
        $img_name1                = preg_replace('/[^a-zA-Z0-9\.]/', "_", $img_name1);
        $config1['file_name']     = date("YmdHis") . rand(0, 999) . "_" . $img_name1;
        $this->load->library('upload', $config1);
        $this->upload->initialize($config1);
        $this->upload->do_upload($file_name);
        $fileDetailArray1 = $this->upload->data();
        return $fileDetailArray1['file_name'];
    }
    private function upload_file_brochure($file_name, $path = null)
    {
        $upload_path1             = "cmoon_images" . $path;
        $config1['upload_path']   = $upload_path1;
        $config1['allowed_types'] = "*";
        $config1['max_size']      = "100480000";
        $img_name1                = strtolower($_FILES[$file_name]['name']);
        $img_name1                = preg_replace('/[^a-zA-Z0-9\.]/', "_", $img_name1);
        $config1['file_name']     = date("YmdHis") . rand(0, 999) . "_" . $img_name1;
        $this->load->library('upload', $config1);
        $this->upload->initialize($config1);
        $this->upload->do_upload($file_name);
        $fileDetailArray1 = $this->upload->data();
        return $fileDetailArray1['file_name'];
    }
}