<?php ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Cmoon extends CI_Controller {


	    public function __construct()
    {
        parent::__construct();
        // $this->load->model('login_model','',True);
        $this->load->model('cmoon_model', '', true);
        $this->load->model('site_model', '', true);
        $this->data["site_details"]=$this->cmoon_model->get_row_by_row('site_details', '1');
    }


	public function index()
	{
		$this->load->view('cmoon/include/header',$this->data);
		$this->load->view('cmoon/index');
		$this->load->view('cmoon/include/footer');
	}


// ----------------------------------------------  Change password code    -------------------------------------------

    public function change_password()
    {

        if ($this->input->post()) {

            // $form_data = $this->input->post();
            $password = sha1($this->input->post('password'));
            $row_there = $this->cmoon_model->get_psw('login', '1', $password);
            if ($row_there == 1) {
                $new_password = sha1($this->input->post('new_password'));
                $result = $this->cmoon_model->update_psw('login', '1', $new_password);
                if ($result == true) {
                    redirect('cmoon/change_password/psw_Success');
                } else {
                    redirect('cmoon/change_password/psw_error');
                }
            } else {
                redirect('cmoon/change_password/psw_doesnot_match');
            }
        } else {
            $this->load->view('cmoon/include/header',$this->data);
            $data['result'] = $this->cmoon_model->get('login', '1');
            $this->load->view('cmoon/change_password', $data);
            $this->load->view('cmoon/include/footer');
        }
    }


// -------------------------------------------------------------------------------------------------------




    // --------------------------------------    Home Banners code    ------------------------------------------

  public function home_banners_view()
  {
    $this->load->view('cmoon/include/header',$this->data);
    $data['result'] = $this->cmoon_model->get('home_banners');
    $this->load->view('cmoon/home_banners_view',$data);
    $this->load->view('cmoon/include/footer');
  }

  public function home_banners_add($id = '')
  {

    if($this->input->post()){


      $form_data=$this->input->post();

      if($id != ''){
                $del_existing_img = $this->cmoon_model->get_row_by_row('home_banners', $id);
       if ($_FILES['image']['name'] != '') {
                    unlink("cmoon_images/".$del_existing_img->image);
                    $form_data['image'] = $this->upload_file('image');
                }
        $result = $this->cmoon_model->update('home_banners',$form_data,$id);

      if ($result == true) {
                redirect('cmoon/home_banners_view/Success');
            } else {
                redirect('cmoon/home_banners_add/error');
            }
      }else{

            if ($_FILES['image']['name'] != '') {
                    $form_data['image'] = $this->upload_file('image');
                }

                       $result = $this->cmoon_model->insert('home_banners',$form_data);

               if ($result == true) {
                   redirect('cmoon/home_banners_view/Success');
               } else {
                   redirect('cmoon/home_banners_add/error');
               }



      }

    }else{

      $this->load->view('cmoon/include/header',$this->data);
      if($id != ''){
        $data['result'] = $this->cmoon_model->get_row_by_result('home_banners',$id);
      }
      $this->load->view('cmoon/home_banners_add',$data);
      $this->load->view('cmoon/include/footer');
    }
  }

     public function home_banners_delete($id)
    {


        $del_existing_img = $this->cmoon_model->get_row_by_row('home_banners', $id);
            unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('home_banners', $id);
        if ($result == true) {
            redirect('cmoon/home_banners_view/dSuccess');
        } else {
            redirect('cmoon/home_banners_add/error');
        }
    }




    // --------------------------------------    Home banner_images code    ------------------------------------------

  public function banner_images_view()
  {
    $this->load->view('cmoon/include/header',$this->data);
    $data['result'] = $this->cmoon_model->get('banner_images');
    $this->load->view('cmoon/banner_images_view',$data);
    $this->load->view('cmoon/include/footer');
  }

  public function banner_images_add($id = '')
  {

    if($this->input->post()){


      $form_data=$this->input->post();

      if($id != ''){
                $del_existing_img = $this->cmoon_model->get_row_by_row('banner_images', $id);
       if ($_FILES['image']['name'] != '') {
                    unlink("cmoon_images/".$del_existing_img->image);
                    $form_data['image'] = $this->upload_file('image');
                }
        $result = $this->cmoon_model->update('banner_images',$form_data,$id);

      if ($result == true) {
                redirect('cmoon/banner_images_view/Success');
            } else {
                redirect('cmoon/banner_images_add/error');
            }
      }else{


                $images = $_FILES['image']['name'];
               $this->load->library('upload');
               $this->load->library('image_lib');
               $upload_conf = array(
                   'upload_path'   => realpath('cmoon_images'),
                   'allowed_types' => '*',
                   'max_size'      => '30000',
                   // 'encrypt_name'  => true,
               );
               $this->upload->initialize($upload_conf);
               foreach ($_FILES['image'] as $key => $val) {
                   $i = 1;
                   foreach ($val as $v) {
                       $field_name                = "file_" . $i;
                       $_FILES[$field_name][$key] = $v;
                       $i++;
                   }
               }
               unset($_FILES['image']);
               $error   = array();
               $success = array();
               foreach ($_FILES as $field_name => $file) {
                   if (!$this->upload->do_upload($field_name)) {
                       $error['upload'][] = $this->upload->display_errors();
                   } else {
                       $upload_data        = $this->upload->data();
                       $success[]          = $upload_data;
                       $form_data['image'] = $upload_data['file_name'];
                       $result = $this->cmoon_model->insert('banner_images',$form_data);
                   }
               }
               if ($result == true) {
                   redirect('cmoon/banner_images_view/Success');
               } else {
                   redirect('cmoon/banner_images_add/error');
               }



      }

    }else{

      $this->load->view('cmoon/include/header',$this->data);
      if($id != ''){
        $data['result'] = $this->cmoon_model->get_row_by_result('banner_images',$id);
      }
      $this->load->view('cmoon/banner_images_add',$data);
      $this->load->view('cmoon/include/footer');
    }
  }

     public function banner_images_delete($id)
    {


        $del_existing_img = $this->cmoon_model->get_row_by_row('banner_images', $id);
            unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('banner_images', $id);
        if ($result == true) {
            redirect('cmoon/banner_images_view/dSuccess');
        } else {
            redirect('cmoon/banner_images_add/error');
        }
    }



// --------------------------------------     CMS Pages code    ------------------------------------------

	public function cms_pages_view()

	{
		$this->load->view('cmoon/include/header',$this->data);
		$data['result'] = $this->cmoon_model->get('cms_pages');
		$this->load->view('cmoon/cms_pages_view',$data);
		$this->load->view('cmoon/include/footer');
	}

	public function cms_pages_edit($id = '')
	{
		if($this->input->post()){

			$form_data=$this->input->post();
            $form_data['p_link'] = $this->permalink($form_data['heading']);
            $redirect =$form_data['redirect'];


			if($id != ''){

    //             $del_existing_img = $this->cmoon_model->get_row_by_row('cms_pages', $id);

			 // if ($_FILES['image']['name'] != '') {
    //                 unlink("cmoon_images/".$del_existing_img->image);
    //                 $form_data['image'] = $this->upload_file('image');
    //             }



				$result = $this->cmoon_model->update('cms_pages',$form_data,$id);

			if ($result == true) {
                redirect('cmoon/'. $redirect .'/Success');
            } else {
                redirect('cmoon/cms_pages_edit/error');
            }

			}else{


              // if ($_FILES['image']['name'] != '') {
              //       $form_data['image'] = $this->upload_file('image');
              //   }

				$result = $this->cmoon_model->insert('cms_pages',$form_data);

			if ($result == true) {
                redirect('cmoon/cms_pages_view/Success');
            } else {
                redirect('cmoon/cms_pages_edit/error');
            }
			}

		}else{

			$this->load->view('cmoon/include/header',$this->data);
			if($id != ''){
				$data['result'] = $this->cmoon_model->get_row_by_result('cms_pages',$id);
			}
			$this->load->view('cmoon/cms_pages_edit',$data);
			$this->load->view('cmoon/include/footer');
		}
	}

	   public function cms_pages_delete($id)
    {


        // $del_existing_img = $this->cmoon_model->get_row_by_row('cms_pages', $id);
        //     unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('cms_pages', $id);
        if ($result == true) {
            redirect('cmoon/cms_pages_view/dSuccess');
        } else {
            redirect('cmoon/cms_pages_edit/error');
        }
    }


// --------------------------------------     faqs code    ------------------------------------------

  public function faqs_view()
  {
    $this->load->view('cmoon/include/header',$this->data);
    $data['result'] = $this->cmoon_model->get('faqs');
    $this->load->view('cmoon/faqs_view',$data);
    $this->load->view('cmoon/include/footer');
  }

  public function faqs_add($id = '')
  {
    if($this->input->post()){

      $form_data=$this->input->post();
            // $form_data['p_link'] = $this->permalink($form_data['heading']);


      if($id != ''){
       //          $del_existing_img = $this->cmoon_model->get_row_by_row('faqs', $id);
       // if ($_FILES['image']['name'] != '') {
       //              unlink("cmoon_images/".$del_existing_img->image);
       //              $form_data['image'] = $this->upload_file('image');
       //          }
        $result = $this->cmoon_model->update('faqs',$form_data,$id);

      if ($result == true) {
                redirect('cmoon/faqs_view/Success');
            } else {
                redirect('cmoon/faqs_add/error');
            }
      }else{

              // if ($_FILES['image']['name'] != '') {
              //       $form_data['image'] = $this->upload_file('image');
              //  }else {
              //        redirect('cmoon/faqs_add/image_error');
              //      }
        $result = $this->cmoon_model->insert('faqs',$form_data);

      if ($result == true) {
                redirect('cmoon/faqs_view/Success');
            } else {
                redirect('cmoon/faqs_add/error');
            }
      }

    }else{

      $this->load->view('cmoon/include/header',$this->data);
      if($id != ''){
        $data['result'] = $this->cmoon_model->get_row_by_result('faqs',$id);
      }
      $this->load->view('cmoon/faqs_add',$data);
      $this->load->view('cmoon/include/footer');
    }
  }

     public function faqs_delete($id)
    {


        // $del_existing_img = $this->cmoon_model->get_row_by_row('faqs', $id);
        //     unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('faqs', $id);
        if ($result == true) {
            redirect('cmoon/faqs_view/dSuccess');
        } else {
            redirect('cmoon/faqs_add/error');
        }
    }




// --------------------------------------     notifications code    ------------------------------------------

  public function notifications_view()
  {
    $this->load->view('cmoon/include/header',$this->data);
    $data['result'] = $this->cmoon_model->get_by_coloum_id('notifications','admin','user_id');
    $this->load->view('cmoon/notifications_view',$data);
    $this->load->view('cmoon/include/footer');
  }

  public function notifications_add($id = '')
  {
    if($this->input->post()){

      $form_data=$this->input->post();
            // $form_data['p_link'] = $this->permalink($form_data['heading']);


      if($id != ''){
       //          $del_existing_img = $this->cmoon_model->get_row_by_row('notifications', $id);
       // if ($_FILES['image']['name'] != '') {
       //              unlink("cmoon_images/".$del_existing_img->image);
       //              $form_data['image'] = $this->upload_file('image');
       //          }
        $result = $this->cmoon_model->update('notifications',$form_data,$id);

      if ($result == true) {
                redirect('cmoon/notifications_view/Success');
            } else {
                redirect('cmoon/notifications_add/error');
            }
      }else{

              // if ($_FILES['image']['name'] != '') {
              //       $form_data['image'] = $this->upload_file('image');
              //  }else {
              //        redirect('cmoon/notifications_add/image_error');
              //      }
        $result = $this->cmoon_model->insert('notifications',$form_data);

     $data=$this->site_model->get('user_accounts');

        foreach ($data as $append_data) {

            $user_data['notification_count'] = $append_data->notification_count + 1;

        $update = $this->cmoon_model->update('user_accounts',$user_data,$append_data->id);
        }

      if ($result == true) {
                redirect('cmoon/notifications_view/Success');
            } else {
                redirect('cmoon/notifications_add/error');
            }
      }

    }else{

      $this->load->view('cmoon/include/header',$this->data);
      if($id != ''){
        $data['result'] = $this->cmoon_model->get_row_by_result('notifications',$id);
      }
      $this->load->view('cmoon/notifications_add',$data);
      $this->load->view('cmoon/include/footer');
    }
  }

     public function notifications_delete($id)
    {


        // $del_existing_img = $this->cmoon_model->get_row_by_row('notifications', $id);
        //     unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('notifications', $id);


     $data=$this->site_model->get('user_accounts');

        foreach ($data as $append_data) {

            $user_data['notification_count'] = $append_data->notification_count - 1;

        $update = $this->cmoon_model->update('user_accounts',$user_data,$append_data->id);
        }


        if ($result == true) {
            redirect('cmoon/notifications_view/dSuccess');
        } else {
            redirect('cmoon/notifications_add/error');
        }
    }



// --------------------------------------     bug_issues code    ------------------------------------------

  public function bug_issues_view()
  {
    $this->load->view('cmoon/include/header',$this->data);
    $data['result'] = $this->cmoon_model->get('bug_issues');
    $this->load->view('cmoon/bug_issues_view',$data);
    $this->load->view('cmoon/include/footer');
  }

  public function bug_issues_add($id = '')
  {
    if($this->input->post()){

      $form_data=$this->input->post();
            // $form_data['p_link'] = $this->permalink($form_data['heading']);


      if($id != ''){
       //          $del_existing_img = $this->cmoon_model->get_row_by_row('bug_issues', $id);
       // if ($_FILES['image']['name'] != '') {
       //              unlink("cmoon_images/".$del_existing_img->image);
       //              $form_data['image'] = $this->upload_file('image');
       //          }
        $result = $this->cmoon_model->update('bug_issues',$form_data,$id);

      if ($result == true) {
                redirect('cmoon/bug_issues_view/Success');
            } else {
                redirect('cmoon/bug_issues_add/error');
            }
      }else{

              // if ($_FILES['image']['name'] != '') {
              //       $form_data['image'] = $this->upload_file('image');
              //  }else {
              //        redirect('cmoon/bug_issues_add/image_error');
              //      }
        $result = $this->cmoon_model->insert('bug_issues',$form_data);

      if ($result == true) {
                redirect('cmoon/bug_issues_view/Success');
            } else {
                redirect('cmoon/bug_issues_add/error');
            }
      }

    }else{

      $this->load->view('cmoon/include/header',$this->data);
      if($id != ''){
        $data['result'] = $this->cmoon_model->get_row_by_result('bug_issues',$id);
      }
      $this->load->view('cmoon/bug_issues_add',$data);
      $this->load->view('cmoon/include/footer');
    }
  }

     public function bug_issues_delete($id)
    {


        // $del_existing_img = $this->cmoon_model->get_row_by_row('bug_issues', $id);
        //     unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('bug_issues', $id);
        if ($result == true) {
            redirect('cmoon/bug_issues_view/dSuccess');
        } else {
            redirect('cmoon/bug_issues_add/error');
        }
    }


// --------------------------------------     qr_kits code    ------------------------------------------

  public function qr_kits_view()
  {
    $this->load->view('cmoon/include/header',$this->data);
    $data['result'] = $this->cmoon_model->get('qr_kits');
    $this->load->view('cmoon/qr_kits_view',$data);
    $this->load->view('cmoon/include/footer');
  }

  public function qr_kits_add($id = '')
  {
    if($this->input->post()){

      $form_data=$this->input->post();

        if($form_data['discount_percentage'] != '' || $form_data['discount_percentage'] == '0'){
            $discount_value = $form_data['actual_price'] * ($form_data['discount_percentage'] / 100);
            $form_data['discount_price'] = $form_data['actual_price'] - $discount_value ;
          }else{
            $form_data['discount_price'] = $form_data['actual_price'];

          }


      if($id != ''){
                $del_existing_img = $this->cmoon_model->get_row_by_row('qr_kits', $id);
       if ($_FILES['image']['name'] != '') {
                    unlink("cmoon_images/".$del_existing_img->image);
                    $form_data['image'] = $this->upload_file('image');
                }
        $result = $this->cmoon_model->update('qr_kits',$form_data,$id);

      if ($result == true) {
                redirect('cmoon/qr_kits_view/Success');
            } else {
                redirect('cmoon/qr_kits_add/error');
            }
      }else{

              if ($_FILES['image']['name'] != '') {
                    $form_data['image'] = $this->upload_file('image');
               }else {
                     redirect('cmoon/qr_kits_add/image_error');
                   }
        $result = $this->cmoon_model->insert('qr_kits',$form_data);

      if ($result == true) {
                redirect('cmoon/qr_kits_view/Success');
            } else {
                redirect('cmoon/qr_kits_add/error');
            }
      }

    }else{

      $this->load->view('cmoon/include/header',$this->data);
      if($id != ''){
        $data['result'] = $this->cmoon_model->get_row_by_result('qr_kits',$id);
      }
      $this->load->view('cmoon/qr_kits_add',$data);
      $this->load->view('cmoon/include/footer');
    }
  }

     public function qr_kits_delete($id)
    {


        $del_existing_img = $this->cmoon_model->get_row_by_row('qr_kits', $id);
            unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('qr_kits', $id);
        if ($result == true) {
            redirect('cmoon/qr_kits_view/dSuccess');
        } else {
            redirect('cmoon/qr_kits_add/error');
        }
    }





// --------------------------------------     qr_code_genereation code    ------------------------------------------

  public function qr_code_genereation_view()
  {
    $this->load->view('cmoon/include/header',$this->data);

    if($this->input->post()){

      $qr_code = $this->input->post('qr_code');
      $created_at_time = $this->input->post('created_at_time');
      $status = $this->input->post('status');

    $data['result'] = $this->cmoon_model->get_search_data('qr_code_genereation',$qr_code,$created_at_time,$status);
 foreach ( $data['result'] as $row) {

        $row->users = $this->site_model->get_row_by_id('user_accounts','id',$row->user_id);
   }

    }else{

    $data['result'] = $this->cmoon_model->get('qr_code_genereation');
 foreach ( $data['result'] as $row) {

        $row->users = $this->site_model->get_row_by_id('user_accounts','id',$row->user_id);
   }

    }

    $this->load->view('cmoon/qr_code_genereation_view',$data);
    $this->load->view('cmoon/include/footer');
  }

  public function qr_code_genereation_add($id = '')
  {
    if($this->input->post()){

     for($i=0;$i<$this->input->post('no_of_qr_codes');$i++){


        $qr_code =  $this->generate_random_no();

         $codes = $this->site_model->get_by_where_column_2('qr_code_genereation', 'qr_code',$qr_code);


       if(count($codes) == 0){

        $form_data['qr_code'] =  $qr_code;

        date_default_timezone_set('Asia/Kolkata');
        $form_data['strtotime_value'] = (date('Y-m-d'));
        $form_data['created_at'] =  date( 'd-m-Y h:i:s A');
        $form_data['status'] =  'FRESH';

      }
        $result = $this->cmoon_model->insert('qr_code_genereation',$form_data);
    }
  }

      if ($result == true) {
                redirect('cmoon/qr_code_genereation_view/Success');
            } else {
                redirect('cmoon/qr_code_genereation_view/error');
            }


  }

     public function qr_code_genereation_delete($id)
    {


        // $del_existing_img = $this->cmoon_model->get_row_by_row('qr_code_genereation', $id);
        //     unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('qr_code_genereation', $id);
        if ($result == true) {
            redirect('cmoon/qr_code_genereation_view/dSuccess');
        } else {
            redirect('cmoon/qr_code_genereation_view/error');
        }
    }





// --------------------------------------     referral_renewal code    ------------------------------------------

  public function referral_renewal_view()
  {
    $this->load->view('cmoon/include/header',$this->data);
    $data['result'] = $this->cmoon_model->get('referral_renewal');
    $this->load->view('cmoon/referral_renewal_view',$data);
    $this->load->view('cmoon/include/footer');
  }

  public function referral_renewal_add($id = '')
  {
    if($this->input->post()){

      $form_data=$this->input->post();



      if($id != ''){

        $result = $this->cmoon_model->update('referral_renewal',$form_data,$id);

      if ($result == true) {
                redirect('cmoon/referral_renewal_view/Success');
            } else {
                redirect('cmoon/referral_renewal_add/error');
            }
      }else{


        $result = $this->cmoon_model->insert('referral_renewal',$form_data);

      if ($result == true) {
                redirect('cmoon/referral_renewal_view/Success');
            } else {
                redirect('cmoon/referral_renewal_add/error');
            }
      }

    }else{

      $this->load->view('cmoon/include/header',$this->data);
      if($id != ''){
        $data['result'] = $this->cmoon_model->get_row_by_result('referral_renewal',$id);
      }
      $this->load->view('cmoon/referral_renewal_add',$data);
      $this->load->view('cmoon/include/footer');
    }
  }

     public function referral_renewal_delete($id)
    {


        // $del_existing_img = $this->cmoon_model->get_row_by_row('referral_renewal', $id);
        //     unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('referral_renewal', $id);
        if ($result == true) {
            redirect('cmoon/referral_renewal_view/dSuccess');
        } else {
            redirect('cmoon/referral_renewal_add/error');
        }
    }





// --------------------------------------     vehicles code    ------------------------------------------

  public function vehicles()
  {
    $this->load->view('cmoon/include/header',$this->data);
    $data['result'] = $this->cmoon_model->get('vehicles');

   foreach ( $data['result'] as $row) {

        $row->users = $this->site_model->get_row_by_id('user_accounts','id',$row->user_id);
   }

    $this->load->view('cmoon/vehicles',$data);
    $this->load->view('cmoon/include/footer');
  }

  public function vehicles_update()
  {
    if($this->input->post()){

            
            $id = $this->input->post('id');


        date_default_timezone_set('Asia/Kolkata');
        $form_data['updated_on'] =  date( 'd-m-Y h:i:s A');

            $form_data['pending_challans'] = $this->input->post('pending_challans');

            $form_data['paid_challans'] = $this->input->post('paid_challans');

        $result = $this->cmoon_model->update('vehicles',$form_data,$id);
    }else{


                redirect('cmoon/vehicles');

    }
  

      if ($result == true) {
                redirect('cmoon/vehicles/Success');
            } else {
                redirect('cmoon/vehicles/error');
            }


  }


// --------------------------------------     orders code    ------------------------------------------

  public function orders_view()
  {
    $this->load->view('cmoon/include/header',$this->data);
    $data['result'] = $this->cmoon_model->get('orders');

   foreach ( $data['result'] as $row) {

        $row->users = $this->site_model->get_row_by_id('user_accounts','id',$row->user_id);
   }

$search = $this->cmoon_model->suggestions('qr_code_genereation','status','FRESH');

        foreach($search  as $item){
          $data['search'][] = $item->qr_code;

         }


    $this->load->view('cmoon/orders_view',$data);
    $this->load->view('cmoon/include/footer');
  }

  public function orders_add()
  {
    if($this->input->post()){

            $qr_code = $this->input->post('qr_code');
            $id = $this->input->post('id');

        $qr_code_details = $this->site_model->get_row_by_id('qr_code_genereation','qr_code',$qr_code);
        if(count($qr_code_details) != 0){


        $user_details = $this->site_model->get_row_by_id('orders','id',$id);
        $qr_data['status'] = 'ASSIGNED';
        $qr_data['user_id'] = $user_details->user_id;
        $update_qr_table = $this->cmoon_model->update('qr_code_genereation',$qr_data,$qr_code_details->id);

        date_default_timezone_set('Asia/Kolkata');
        $form_data['qr_code_assigned_at'] =  date( 'd-m-Y h:i:s A');

            $form_data['qr_code'] = $qr_code_details->qr_code;

            $form_data['qr_code_id'] = $qr_code_details->id;

        $result = $this->cmoon_model->update('orders',$form_data,$id);
    }else{


                redirect('cmoon/orders_view/qr_code_error');

    }
  }

      if ($result == true) {
                redirect('cmoon/orders_view/Success');
            } else {
                redirect('cmoon/orders_view/error');
            }


  }

    public function order_status_update()
  {
    if($this->input->post()){

            $form_data['order_status'] = $this->input->post('order_status');
            $id = $this->input->post('id');

            if($form_data['order_status'] == 'PREPARING'){

              date_default_timezone_set('Asia/Kolkata');
        $form_data['order_prepared_date'] =  date( 'd-m-Y h:i:s A');

            }

                if($form_data['order_status'] == 'ONGOING'){

              date_default_timezone_set('Asia/Kolkata');
        $form_data['order_ongoing_date'] =  date( 'd-m-Y h:i:s A');

            }

                if($form_data['order_status'] == 'COMPLETED'){

              date_default_timezone_set('Asia/Kolkata');
        $form_data['order_completed_date'] =  date( 'd-m-Y h:i:s A');

            }

        $result = $this->cmoon_model->update('orders',$form_data,$id);

        //  $order_datails = $this->site_model->get_by_number('orders','id',$id);

        //  $notification_data['heading']  = $form_data['order_status'];

        //  if($form_data['order_status'] == 'PREPARING'){

        //     $notification_data['description']  = 'Your order is being prepared';

        //     }

        //         if($form_data['order_status'] == 'ONGOING'){

        //     $notification_data['description']  = 'Your order is ongoing';

        //     }

        //         if($form_data['order_status'] == 'COMPLETED'){

        //     $notification_data['description']  = 'Your order is completed';

        //     }

        //     $notification_data['user_id']  = $query->user_id;

          
        //   $notifications = $this->cmoon_model->insert('notifications',$notification_data);

        //   $user_accounts = $this->site_model->get_by_number('user_accounts','id',$order_datails->user_id);

        //   $user_data['notification_count'] = $user_accounts->notification_count + 1;
        // $update = $this->cmoon_model->update('user_accounts',$user_data,$order_datails->user_id);



  }

      if ($result == true) {
                redirect('cmoon/orders_view/Success');
            } else {
                redirect('cmoon/orders_view/error');
            }


  }

     public function orders_delete($id)
    {


        // $del_existing_img = $this->cmoon_model->get_row_by_row('orders', $id);
        //     unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('orders', $id);
        if ($result == true) {
            redirect('cmoon/orders_view/dSuccess');
        } else {
            redirect('cmoon/orders_view/error');
        }
    }


// --------------------------------------     withdraw code    ------------------------------------------

  public function withdraw_view()
  {
    $this->load->view('cmoon/include/header',$this->data);
    $data['result'] = $this->cmoon_model->get('transactions');

   foreach ( $data['result'] as $row) {

        $row->users = $this->site_model->get_row_by_id('user_accounts','id',$row->user_id);
   }

    $this->load->view('cmoon/withdraw_view',$data);
    $this->load->view('cmoon/include/footer');
  }

   public function update_message()
  {
    if($this->input->post()){

            $form_data['message'] = $this->input->post('message');
            $id = $this->input->post('id');


        $result = $this->cmoon_model->update('transactions',$form_data,$id);

  }

      if ($result == true) {
                redirect('cmoon/withdraw_view/Success');
            } else {
                redirect('cmoon/withdraw_view/error');
            }


  }

    public function withdraw_status_update()
  {
    if($this->input->post()){

            $form_data['withdraw_status'] = $this->input->post('withdraw_status');
            $id = $this->input->post('id');



                if($form_data['withdraw_status'] == 'ONGOING'){

              date_default_timezone_set('Asia/Kolkata');
        $form_data['withdraw_status_updated_on'] =  date( 'd-m-Y h:i:s A');

            }

                if($form_data['withdraw_status'] == 'Successfull'){

              date_default_timezone_set('Asia/Kolkata');
        $form_data['withdraw_status_updated_on'] =  date( 'd-m-Y h:i:s A');

            }

        $result = $this->cmoon_model->update('transactions',$form_data,$id);

  }

      if ($result == true) {
                redirect('cmoon/withdraw_view/Success');
            } else {
                redirect('cmoon/withdraw_view/error');
            }


  }

     public function withdraw_delete($id)
    {


        // $del_existing_img = $this->cmoon_model->get_row_by_row('withdraw', $id);
        //     unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('transactions', $id);
        if ($result == true) {
            redirect('cmoon/withdraw_view/dSuccess');
        } else {
            redirect('cmoon/withdraw_view/error');
        }
    }


// --------------------------------------     features code    ------------------------------------------

  public function features_view()
  {
    $this->load->view('cmoon/include/header',$this->data);
    $data['result'] = $this->cmoon_model->get('features');
    $this->load->view('cmoon/features_view',$data);
    $this->load->view('cmoon/include/footer');
  }

  public function features_add($id = '')
  {
    if($this->input->post()){

      $form_data=$this->input->post();
            // $form_data['p_link'] = $this->permalink($form_data['heading']);


      if($id != ''){
       //          $del_existing_img = $this->cmoon_model->get_row_by_row('features', $id);
       // if ($_FILES['image']['name'] != '') {
       //              unlink("cmoon_images/".$del_existing_img->image);
       //              $form_data['image'] = $this->upload_file('image');
       //          }
        $result = $this->cmoon_model->update('features',$form_data,$id);

      if ($result == true) {
                redirect('cmoon/features_view/Success');
            } else {
                redirect('cmoon/features_add/error');
            }
      }else{

              // if ($_FILES['image']['name'] != '') {
              //       $form_data['image'] = $this->upload_file('image');
              //  }else {
              //        redirect('cmoon/features_add/image_error');
              //      }
        $result = $this->cmoon_model->insert('features',$form_data);

      if ($result == true) {
                redirect('cmoon/features_view/Success');
            } else {
                redirect('cmoon/features_add/error');
            }
      }

    }else{

      $this->load->view('cmoon/include/header',$this->data);
      if($id != ''){
        $data['result'] = $this->cmoon_model->get_row_by_result('features',$id);
      }
      $this->load->view('cmoon/features_add',$data);
      $this->load->view('cmoon/include/footer');
    }
  }

     public function features_delete($id)
    {


        // $del_existing_img = $this->cmoon_model->get_row_by_row('features', $id);
        //     unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('features', $id);
        if ($result == true) {
            redirect('cmoon/features_view/dSuccess');
        } else {
            redirect('cmoon/features_add/error');
        }
    }




// --------------------------------------     steps code    ------------------------------------------

  public function steps_view()
  {
    $this->load->view('cmoon/include/header',$this->data);
    $data['result'] = $this->cmoon_model->get('steps');
    $this->load->view('cmoon/steps_view',$data);
    $this->load->view('cmoon/include/footer');
  }

  public function steps_add($id = '')
  {
    if($this->input->post()){

      $form_data=$this->input->post();
            // $form_data['p_link'] = $this->permalink($form_data['heading']);


      if($id != ''){
       //          $del_existing_img = $this->cmoon_model->get_row_by_row('steps', $id);
       // if ($_FILES['image']['name'] != '') {
       //              unlink("cmoon_images/".$del_existing_img->image);
       //              $form_data['image'] = $this->upload_file('image');
       //          }
        $result = $this->cmoon_model->update('steps',$form_data,$id);

      if ($result == true) {
                redirect('cmoon/steps_view/Success');
            } else {
                redirect('cmoon/steps_add/error');
            }
      }else{

              // if ($_FILES['image']['name'] != '') {
              //       $form_data['image'] = $this->upload_file('image');
              //  }else {
              //        redirect('cmoon/steps_add/image_error');
              //      }
        $result = $this->cmoon_model->insert('steps',$form_data);

      if ($result == true) {
                redirect('cmoon/steps_view/Success');
            } else {
                redirect('cmoon/steps_add/error');
            }
      }

    }else{

      $this->load->view('cmoon/include/header',$this->data);
      if($id != ''){
        $data['result'] = $this->cmoon_model->get_row_by_result('steps',$id);
      }
      $this->load->view('cmoon/steps_add',$data);
      $this->load->view('cmoon/include/footer');
    }
  }

     public function steps_delete($id)
    {


        // $del_existing_img = $this->cmoon_model->get_row_by_row('steps', $id);
        //     unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('steps', $id);
        if ($result == true) {
            redirect('cmoon/steps_view/dSuccess');
        } else {
            redirect('cmoon/steps_add/error');
        }
    }




// --------------------------------------     document_types code    ------------------------------------------

  public function document_types_view()
  {
    $this->load->view('cmoon/include/header',$this->data);
    $data['result'] = $this->cmoon_model->get('document_types');
    $this->load->view('cmoon/document_types_view',$data);
    $this->load->view('cmoon/include/footer');
  }

  public function document_types_add($id = '')
  {
    if($this->input->post()){

      $form_data=$this->input->post();
            // $form_data['p_link'] = $this->permalink($form_data['heading']);


      if($id != ''){
       //          $del_existing_img = $this->cmoon_model->get_row_by_row('document_types', $id);
       // if ($_FILES['image']['name'] != '') {
       //              unlink("cmoon_images/".$del_existing_img->image);
       //              $form_data['image'] = $this->upload_file('image');
       //          }
        $result = $this->cmoon_model->update('document_types',$form_data,$id);

      if ($result == true) {
                redirect('cmoon/document_types_view/Success');
            } else {
                redirect('cmoon/document_types_add/error');
            }
      }else{

              // if ($_FILES['image']['name'] != '') {
              //       $form_data['image'] = $this->upload_file('image');
              //  }else {
              //        redirect('cmoon/document_types_add/image_error');
              //      }
        $result = $this->cmoon_model->insert('document_types',$form_data);

      if ($result == true) {
                redirect('cmoon/document_types_view/Success');
            } else {
                redirect('cmoon/document_types_add/error');
            }
      }

    }else{

      $this->load->view('cmoon/include/header',$this->data);
      if($id != ''){
        $data['result'] = $this->cmoon_model->get_row_by_result('document_types',$id);
      }
      $this->load->view('cmoon/document_types_add',$data);
      $this->load->view('cmoon/include/footer');
    }
  }

     public function document_types_delete($id)
    {


        // $del_existing_img = $this->cmoon_model->get_row_by_row('document_types', $id);
        //     unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('document_types', $id);
        if ($result == true) {
            redirect('cmoon/document_types_view/dSuccess');
        } else {
            redirect('cmoon/document_types_add/error');
        }
    }




// --------------------------------------     services_types code    ------------------------------------------

  public function services_types_view()
  {
    $this->load->view('cmoon/include/header',$this->data);
    $data['result'] = $this->cmoon_model->get('services_types');
    $this->load->view('cmoon/services_types_view',$data);
    $this->load->view('cmoon/include/footer');
  }

  public function services_types_add($id = '')
  {
    if($this->input->post()){

      $form_data=$this->input->post();
            // $form_data['p_link'] = $this->permalink($form_data['heading']);


      if($id != ''){
       //          $del_existing_img = $this->cmoon_model->get_row_by_row('services_types', $id);
       // if ($_FILES['image']['name'] != '') {
       //              unlink("cmoon_images/".$del_existing_img->image);
       //              $form_data['image'] = $this->upload_file('image');
       //          }
        $result = $this->cmoon_model->update('services_types',$form_data,$id);

      if ($result == true) {
                redirect('cmoon/services_types_view/Success');
            } else {
                redirect('cmoon/services_types_add/error');
            }
      }else{

              // if ($_FILES['image']['name'] != '') {
              //       $form_data['image'] = $this->upload_file('image');
              //  }else {
              //        redirect('cmoon/services_types_add/image_error');
              //      }
        $result = $this->cmoon_model->insert('services_types',$form_data);

      if ($result == true) {
                redirect('cmoon/services_types_view/Success');
            } else {
                redirect('cmoon/services_types_add/error');
            }
      }

    }else{

      $this->load->view('cmoon/include/header',$this->data);
      if($id != ''){
        $data['result'] = $this->cmoon_model->get_row_by_result('services_types',$id);
      }
      $this->load->view('cmoon/services_types_add',$data);
      $this->load->view('cmoon/include/footer');
    }
  }

     public function services_types_delete($id)
    {


        // $del_existing_img = $this->cmoon_model->get_row_by_row('services_types', $id);
        //     unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('services_types', $id);
        if ($result == true) {
            redirect('cmoon/services_types_view/dSuccess');
        } else {
            redirect('cmoon/services_types_add/error');
        }
    }



    // --------------------------------------    Home Banners code    ------------------------------------------

  public function banners_view()
  {
    $this->load->view('cmoon/include/header',$this->data);
    $data['result'] = $this->cmoon_model->get('banners');
    $this->load->view('cmoon/banners_view',$data);
    $this->load->view('cmoon/include/footer');
  }

  public function banners_add($id = '')
  {

    if($_FILES['image']['name'] != ''){


      $form_data=$this->input->post();

      if($id != ''){
                $del_existing_img = $this->cmoon_model->get_row_by_row('banners', $id);
       if ($_FILES['image']['name'] != '') {
                    unlink("cmoon_images/".$del_existing_img->image);
                    $form_data['image'] = $this->upload_file('image');
                }
        $result = $this->cmoon_model->update('banners',$form_data,$id);

      if ($result == true) {
                redirect('cmoon/banners_view/Success');
            } else {
                redirect('cmoon/banners_add/error');
            }
      }else{


                $images = $_FILES['image']['name'];
               $this->load->library('upload');
               $this->load->library('image_lib');
               $upload_conf = array(
                   'upload_path'   => realpath('cmoon_images'),
                   'allowed_types' => '*',
                   'max_size'      => '30000',
                   // 'encrypt_name'  => true,
               );
               $this->upload->initialize($upload_conf);
               foreach ($_FILES['image'] as $key => $val) {
                   $i = 1;
                   foreach ($val as $v) {
                       $field_name                = "file_" . $i;
                       $_FILES[$field_name][$key] = $v;
                       $i++;
                   }
               }
               unset($_FILES['image']);
               $error   = array();
               $success = array();
               foreach ($_FILES as $field_name => $file) {
                   if (!$this->upload->do_upload($field_name)) {
                       $error['upload'][] = $this->upload->display_errors();
                   } else {
                       $upload_data        = $this->upload->data();
                       $success[]          = $upload_data;
                       $form_data['image'] = $upload_data['file_name'];
                       $result = $this->cmoon_model->insert('banners',$form_data);
                   }
               }
               if ($result == true) {
                   redirect('cmoon/banners_view/Success');
               } else {
                   redirect('cmoon/banners_add/error');
               }



      }

    }else{

      $this->load->view('cmoon/include/header',$this->data);
      if($id != ''){
        $data['result'] = $this->cmoon_model->get_row_by_result('banners',$id);
      }
      $this->load->view('cmoon/banners_add',$data);
      $this->load->view('cmoon/include/footer');
    }
  }

     public function banners_delete($id)
    {


        $del_existing_img = $this->cmoon_model->get_row_by_row('banners', $id);
            unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('banners', $id);
        if ($result == true) {
            redirect('cmoon/banners_view/dSuccess');
        } else {
            redirect('cmoon/banners_add/error');
        }
    }


    // --------------------------------------    Home steps_images code    ------------------------------------------

  public function steps_images_view()
  {
    $this->load->view('cmoon/include/header',$this->data);
    $data['result'] = $this->cmoon_model->get('steps_images');
    $this->load->view('cmoon/steps_images_view',$data);
    $this->load->view('cmoon/include/footer');
  }

  public function steps_images_add($id = '')
  {

    if($_FILES['image']['name'] != ''){


      $form_data=$this->input->post();

      if($id != ''){
                $del_existing_img = $this->cmoon_model->get_row_by_row('steps_images', $id);
       if ($_FILES['image']['name'] != '') {
                    unlink("cmoon_images/".$del_existing_img->image);
                    $form_data['image'] = $this->upload_file('image');
                }
        $result = $this->cmoon_model->update('steps_images',$form_data,$id);

      if ($result == true) {
                redirect('cmoon/steps_images_view/Success');
            } else {
                redirect('cmoon/steps_images_add/error');
            }
      }else{


                $images = $_FILES['image']['name'];
               $this->load->library('upload');
               $this->load->library('image_lib');
               $upload_conf = array(
                   'upload_path'   => realpath('cmoon_images'),
                   'allowed_types' => '*',
                   'max_size'      => '30000',
                   // 'encrypt_name'  => true,
               );
               $this->upload->initialize($upload_conf);
               foreach ($_FILES['image'] as $key => $val) {
                   $i = 1;
                   foreach ($val as $v) {
                       $field_name                = "file_" . $i;
                       $_FILES[$field_name][$key] = $v;
                       $i++;
                   }
               }
               unset($_FILES['image']);
               $error   = array();
               $success = array();
               foreach ($_FILES as $field_name => $file) {
                   if (!$this->upload->do_upload($field_name)) {
                       $error['upload'][] = $this->upload->display_errors();
                   } else {
                       $upload_data        = $this->upload->data();
                       $success[]          = $upload_data;
                       $form_data['image'] = $upload_data['file_name'];
                       $result = $this->cmoon_model->insert('steps_images',$form_data);
                   }
               }
               if ($result == true) {
                   redirect('cmoon/steps_images_view/Success');
               } else {
                   redirect('cmoon/steps_images_add/error');
               }



      }

    }else{

      $this->load->view('cmoon/include/header',$this->data);
      if($id != ''){
        $data['result'] = $this->cmoon_model->get_row_by_result('steps_images',$id);
      }
      $this->load->view('cmoon/steps_images_add',$data);
      $this->load->view('cmoon/include/footer');
    }
  }

     public function steps_images_delete($id)
    {


        $del_existing_img = $this->cmoon_model->get_row_by_row('steps_images', $id);
            unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('steps_images', $id);
        if ($result == true) {
            redirect('cmoon/steps_images_view/dSuccess');
        } else {
            redirect('cmoon/steps_images_add/error');
        }
    }




// --------------------------------------    user_accounts code    ------------------------------------------

    public function user_accounts()
    {
        $this->load->view('cmoon/include/header',$this->data);
        $data['result'] = $this->cmoon_model->get('user_accounts');
        $this->load->view('cmoon/user_accounts',$data);
        $this->load->view('cmoon/include/footer');
    }


public function user_accounts_edit($id = '')
  {
    if($this->input->post()){

      $form_data=$this->input->post();
            // $form_data['p_link'] = $this->permalink($form_data['heading']);


      if($id != ''){
       //          $del_existing_img = $this->cmoon_model->get_row_by_row('faqs', $id);
       // if ($_FILES['image']['name'] != '') {
       //              unlink("cmoon_images/".$del_existing_img->image);
       //              $form_data['image'] = $this->upload_file('image');
       //          }
        $result = $this->cmoon_model->update('user_accounts',$form_data,$id);

      if ($result == true) {
                redirect('cmoon/user_accounts/Success');
            } else {
                redirect('cmoon/user_accounts_edit/error');
            }
      }else{

              // if ($_FILES['image']['name'] != '') {
              //       $form_data['image'] = $this->upload_file('image');
              //  }else {
              //        redirect('cmoon/user_accounts_edit/image_error');
              //      }
        $result = $this->cmoon_model->insert('user_accounts',$form_data);

      if ($result == true) {
                redirect('cmoon/user_accounts/Success');
            } else {
                redirect('cmoon/user_accounts_edit/error');
            }
      }

    }else{

      $this->load->view('cmoon/include/header',$this->data);
      if($id != ''){
        $data['result'] = $this->cmoon_model->get_row_by_result('user_accounts',$id);
      }
      $this->load->view('cmoon/user_accounts_edit',$data);
      $this->load->view('cmoon/include/footer');
    }
  }
       public function user_accounts_delete($id)
    {


        // $del_existing_img = $this->cmoon_model->get_row_by_row('user_accounts', $id);
            // unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('user_accounts', $id);

        if ($result == true) {
            redirect('cmoon/user_accounts/dSuccess');
        } else {
            redirect('cmoon/user_accounts/error');
        }
    }




// --------------------------------------    contact_enquiry_website code    ------------------------------------------

    public function contact_enquiry_website()
    {
        $this->load->view('cmoon/include/header',$this->data);
        $data['result'] = $this->cmoon_model->get('contact_enquiry_website');
                foreach ( $data['result'] as $row) {

        $row->users = $this->site_model->get_row_by_id('user_accounts','id',$row->user_id);
}
        $this->load->view('cmoon/contact_enquiry_website',$data);
        $this->load->view('cmoon/include/footer');
    }


public function contact_enquiry_website_edit($id = '')
  {
    if($this->input->post()){

      $form_data=$this->input->post();
            // $form_data['p_link'] = $this->permalink($form_data['heading']);


      if($id != ''){
       //          $del_existing_img = $this->cmoon_model->get_row_by_row('contact_enquiry_website', $id);
       // if ($_FILES['image']['name'] != '') {
       //              unlink("cmoon_images/".$del_existing_img->image);
       //              $form_data['image'] = $this->upload_file('image');
       //          }
        $result = $this->cmoon_model->update('contact_enquiry_website',$form_data,$id);

      if ($result == true) {
                redirect('cmoon/contact_enquiry_website/Success');
            } else {
                redirect('cmoon/contact_enquiry_website_edit/error');
            }
      }else{

              // if ($_FILES['image']['name'] != '') {
              //       $form_data['image'] = $this->upload_file('image');
              //  }else {
              //        redirect('cmoon/contact_enquiry_website_edit/image_error');
              //      }
        $result = $this->cmoon_model->insert('contact_enquiry_website',$form_data);

      if ($result == true) {
                redirect('cmoon/contact_enquiry_website/Success');
            } else {
                redirect('cmoon/contact_enquiry_website_edit/error');
            }
      }

    }else{

      $this->load->view('cmoon/include/header',$this->data);
      if($id != ''){
        $data['result'] = $this->cmoon_model->get_row_by_result('contact_enquiry_website',$id);
      }
      $this->load->view('cmoon/contact_enquiry_website_edit',$data);
      $this->load->view('cmoon/include/footer');
    }
  }
       public function contact_enquiry_website_delete($id)
    {


        // $del_existing_img = $this->cmoon_model->get_row_by_row('contact_enquiry_website', $id);
            // unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('contact_enquiry_website', $id);

        if ($result == true) {
            redirect('cmoon/contact_enquiry_website/dSuccess');
        } else {
            redirect('cmoon/contact_enquiry_website/error');
        }
    }





// --------------------------------------    contact_enquiry code    ------------------------------------------

    public function contact_enquiry()
    {
        $this->load->view('cmoon/include/header',$this->data);
        $data['result'] = $this->cmoon_model->get('contact_enquiry');
                foreach ( $data['result'] as $row) {

        $row->users = $this->site_model->get_row_by_id('user_accounts','id',$row->user_id);
}
        $this->load->view('cmoon/contact_enquiry',$data);
        $this->load->view('cmoon/include/footer');
    }


public function contact_enquiry_edit($id = '')
  {
    if($this->input->post()){

      $form_data=$this->input->post();
            // $form_data['p_link'] = $this->permalink($form_data['heading']);


      if($id != ''){
       //          $del_existing_img = $this->cmoon_model->get_row_by_row('contact_enquiry', $id);
       // if ($_FILES['image']['name'] != '') {
       //              unlink("cmoon_images/".$del_existing_img->image);
       //              $form_data['image'] = $this->upload_file('image');
       //          }
        $result = $this->cmoon_model->update('contact_enquiry',$form_data,$id);

      if ($result == true) {
                redirect('cmoon/contact_enquiry/Success');
            } else {
                redirect('cmoon/contact_enquiry_edit/error');
            }
      }else{

              // if ($_FILES['image']['name'] != '') {
              //       $form_data['image'] = $this->upload_file('image');
              //  }else {
              //        redirect('cmoon/contact_enquiry_edit/image_error');
              //      }
        $result = $this->cmoon_model->insert('contact_enquiry',$form_data);

      if ($result == true) {
                redirect('cmoon/contact_enquiry/Success');
            } else {
                redirect('cmoon/contact_enquiry_edit/error');
            }
      }

    }else{

      $this->load->view('cmoon/include/header',$this->data);
      if($id != ''){
        $data['result'] = $this->cmoon_model->get_row_by_result('contact_enquiry',$id);
      }
      $this->load->view('cmoon/contact_enquiry_edit',$data);
      $this->load->view('cmoon/include/footer');
    }
  }
       public function contact_enquiry_delete($id)
    {


        // $del_existing_img = $this->cmoon_model->get_row_by_row('contact_enquiry', $id);
            // unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('contact_enquiry', $id);

        if ($result == true) {
            redirect('cmoon/contact_enquiry/dSuccess');
        } else {
            redirect('cmoon/contact_enquiry/error');
        }
    }




// ---------------------------------------------------------------------------------------------------------

// --------------------------------------    Site Details code    ------------------------------------------


    public function site_details()
    {
        if ($this->input->post()) {
            $form_data = $this->input->post();
        $del_existing_img = $this->cmoon_model->get_row_by_row('site_details', '1');
            if ($_FILES['site_logo']['name'] != '') {
                unlink("cmoon_images/".$del_existing_img->site_logo);
                $form_data['site_logo'] = $this->upload_file('site_logo');
            }
            if ($_FILES['site_favicon']['name'] != '') {
                unlink("cmoon_images/".$del_existing_img->site_favicon);
                $form_data['site_favicon'] = $this->upload_file('site_favicon');
            }
            if ($_FILES['footer_logo']['name'] != '') {
                unlink("cmoon_images/".$del_existing_img->footer_logo);
                $form_data['footer_logo'] = $this->upload_file('footer_logo');
            }
            $result = $this->cmoon_model->update('site_details', $form_data,'1');
            if ($result == true) {
                redirect('cmoon/site_details/Success');
            } else {
                redirect('cmoon/site_details/error');
            }
        } else {
            $this->load->view('cmoon/include/header',$this->data);
            $data['result'] = $this->cmoon_model->get_row_by_row('site_details', '1');
            // $data['result'] = $this->db->get_where('site_details')->row();
            $this->load->view('cmoon/site_details', $data);
            $this->load->view('cmoon/include/footer');
        }
    }
// --------------------------------------     Social links code    ------------------------------------------

    public function social_links()
    {
        if ($this->input->post()) {
            $form_data = $this->input->post();
            $result = $this->cmoon_model->update('social_links',$form_data, '1');
            if ($result == true) {
                redirect('cmoon/social_links/Success');
            } else {
                redirect('cmoon/social_links/error');
            }
        } else {
            $this->load->view('cmoon/include/header',$this->data);
            // $data['result'] = $this->db->get_where('social_links')->row();
            $data['result'] = $this->cmoon_model->get_row_by_row('social_links', '1');
            $this->load->view('cmoon/social_links', $data);
            $this->load->view('cmoon/include/footer');
        }
    }


// --------------------------------------     Seo Tags code    ------------------------------------------

	public function seo_tags_view()
	{
		$this->load->view('cmoon/include/header',$this->data);
		$data['result'] = $this->cmoon_model->get('seo_tags');
		$this->load->view('cmoon/seo_tags_view',$data);
		$this->load->view('cmoon/include/footer');
	}

		public function seo_tags_add($id = '')

	{
		if($this->input->post()){

			$form_data=$this->input->post();
            // $form_data['p_link'] = $this->permalink($form_data['heading']);

			if($id != ''){
    //             $del_existing_img = $this->cmoon_model->get_row_by_row('seo_tags', $id);
			 // if ($_FILES['image']['name'] != '') {
    //                 unlink("cmoon_images/".$del_existing_img->image);
    //                 $form_data['image'] = $this->upload_file('image');
    //             }
				$result = $this->cmoon_model->update('seo_tags',$form_data,$id);

			if ($result == true) {
                redirect('cmoon/seo_tags_view/Success');
            } else {
                redirect('cmoon/seo_tags_add/error');
            }
			}else{


              // if ($_FILES['image']['name'] != '') {
              //       $form_data['image'] = $this->upload_file('image');
              //   }
				$result = $this->cmoon_model->insert('seo_tags',$form_data);

			if ($result == true) {
                redirect('cmoon/seo_tags_view/Success');
            } else {
                redirect('cmoon/seo_tags_add/error');
            }
			}

		}else{

			$this->load->view('cmoon/include/header',$this->data);
			if($id != ''){
				$data['result'] = $this->cmoon_model->get_row_by_result('seo_tags',$id);
			}
			$this->load->view('cmoon/seo_tags_add',$data);
			$this->load->view('cmoon/include/footer');
		}
	}


// --------------------------------------    Manage Uploads code    ------------------------------------------

	public function manage_uploads_view()
	{
		$this->load->view('cmoon/include/header',$this->data);
		$data['result'] = $this->cmoon_model->get('manage_uploads');
		$this->load->view('cmoon/manage_uploads_view',$data);
		$this->load->view('cmoon/include/footer');
	}

	public function manage_uploads_add($id = '')
	{


		if($_FILES['image']['name'] != ''){

			$form_data=$this->input->post();
            // $form_data['p_link'] = $this->permalink($form_data['heading']);

			if($id != ''){
                $del_existing_img = $this->cmoon_model->get_row_by_row('manage_uploads', $id);
			 if ($_FILES['image']['name'] != '') {
                    unlink("cmoon_images/".$del_existing_img->image);
                    $form_data['image'] = $this->upload_file('image');
                }
				$result = $this->cmoon_model->update('manage_uploads',$form_data,$id);

			if ($result == true) {
                redirect('cmoon/manage_uploads_view/Success');
            } else {
                redirect('cmoon/manage_uploads_add/error');
            }
			}else{


              if ($_FILES['image']['name'] != '') {
                    $form_data['image'] = $this->upload_file('image');
                }
				$result = $this->cmoon_model->insert('manage_uploads',$form_data);

			if ($result == true) {
                redirect('cmoon/manage_uploads_view/Success');
            } else {
                redirect('cmoon/manage_uploads_add/error');
            }
			}

		}else{

			$this->load->view('cmoon/include/header',$this->data);
			if($id != ''){
				$data['result'] = $this->cmoon_model->get_row_by_result('manage_uploads',$id);
			}
			$this->load->view('cmoon/manage_uploads_add',$data);
			$this->load->view('cmoon/include/footer');
		}
	}

	   public function manage_uploads_delete($id)
    {


        $del_existing_img = $this->cmoon_model->get_row_by_row('manage_uploads', $id);
            unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('manage_uploads', $id);
        if ($result == true) {
            redirect('cmoon/manage_uploads_view/dSuccess');
        } else {
            redirect('cmoon/manage_uploads_add/error');
        }
    }



// --------------------------------------     Logs code    ------------------------------------------

    public function logs()

	{
		$this->load->view('cmoon/include/header',$this->data);
		$data['result'] = $this->cmoon_model->get('logs');
		$this->load->view('cmoon/logs',$data);
		$this->load->view('cmoon/include/footer');
	}

// ----------------------------------------------- plink generator  -------------------------------------------------------------

// $post_title = "Welcome to php exampless";
    // echo $permalink = permalink($post_title);
    private function permalink($string)
    {
    	// $string=$string."-".rand(0,9999);
        $string = str_replace(" ", "-", $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }

// -------------------------------------- Image upload code -----------------------------------------------------

    private function upload_file($file_name, $path = null)
    {
        $upload_path1             = "cmoon_images".$path;
        $config1['upload_path']   = $upload_path1;
        $config1['allowed_types'] = "gif|jpg|png|jpeg|svg";
        $config1['max_size']      = "100480000";
        $img_name1                = strtolower($_FILES[$file_name]['name']);
        $img_name1                = preg_replace('/[^a-zA-Z0-9\.]/', "_", $img_name1);
        $config1['file_name']     = date("YmdHis") . rand(0, 999) . "_" . $img_name1;
        $this->load->library('upload', $config1);
        $this->upload->initialize($config1);
        $this->upload->do_upload($file_name);
        $fileDetailArray1 = $this->upload->data();
        return $fileDetailArray1['file_name'];
    }


// ------------------------------------------------ pdf upload code --------------------------------------------------
    private function upload_file_brochure($file_name, $path = null)
    {
        $upload_path1             = "cmoon_pdf" . $path;
        $config1['upload_path']   = $upload_path1;
        $config1['allowed_types'] = "pdf";
        $config1['max_size']      = "100480000";
        $img_name1                = strtolower($_FILES[$file_name]['name']);
        $img_name1                = preg_replace('/[^a-zA-Z0-9\.]/', "_", $img_name1);
        $img_name1                = preg_replace('-', "_", $img_name1);
        $config1['file_name']     = $img_name1;
        // $config1['file_name']     = date("YmdHis") . rand(0, 999) . "_" . $img_name1;
        $this->load->library('upload', $config1);
        $this->upload->initialize($config1);
        $this->upload->do_upload($file_name);
        $fileDetailArray1 = $this->upload->data();
        return $fileDetailArray1['file_name'];
    }

    //--------------------- Back up db--------------//
    public function backup_db()
    {
        $site_name=$this->data["site_details"]->site_name;
        $this->load->dbutil();
        $prefs = array(
            'format'   => 'zip',
            'filename' => $site_name,
        );
        $backup = &$this->dbutil->backup($prefs);
        $db_name = $site_name.'-'. date("Y-m-d-H-i-s") . '.zip';
        $save    = 'C:/Users/cmoon/Downloads/' . $db_name;
        $this->load->helper('file');
        write_file($save, $backup);
        $this->load->helper('download');
        force_download($db_name, $backup);
    }


private function generate_random_no()  {

  // $seed = str_split('abcdefghijklmnopqrstuvwxyz'
  //               .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
  //               .'0123456789'); // and any other characters

        $seed = str_split('ABCDEFGHIJKLMNOPQRSTUVWXYZ'.'0123456789'); // and any other characters
        shuffle($seed); // probably optional since array is randomized; this may be redundant
        $otp = '';
        foreach (array_rand($seed, 8) as $k)
        {
           $otp .= $seed[$k];
        }
        return $otp;
    }

}