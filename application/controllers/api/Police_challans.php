<?php
require (APPPATH . '/libraries/REST_Controller.php');
class police_challans extends CI_Controller
{
    function __construct(){
      parent:: __construct();
      $this->load->model('login_model','',True);
      $this->load->model('cmoon_model','',True);
      $this->load->model('site_model','',True);
   }
  

    function index() {

          $vehicle_num =$this->input->get_post('vehicle_num');

     $data=$this->site_model->get_by_number('vehicles','vehicle_num',$vehicle_num);
     $social_links = $this->site_model->get_by_number('social_links','id','1');

     $data->ap_link = $social_links->police_challan_link1;
     $data->tg_link = $social_links->police_challan_link2;

          if ($data->vehicle_image != '') {
                $data->vehicle_image ="http://demoworks.in/php/myvsafety/vehicle_images/".$data->vehicle_image;
            }
        
        if (count($data) > 0) {


            $arr = array(
                         'status' => "valid",
                         'message' => "Found",
                          "data" => $data,
                    );
        } else {

            $arr = [
                'status' => "invalid",
                'message' => "We're sorry, Theis vehicle number is not present on our list. "
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }




    }