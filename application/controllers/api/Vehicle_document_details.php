<?php
require (APPPATH . '/libraries/REST_Controller.php');
class vehicle_document_details extends CI_Controller
{
    function __construct(){
      parent:: __construct();
      $this->load->model('login_model','',True);
      $this->load->model('cmoon_model','',True);
      $this->load->model('site_model','',True);
   }


    function document_types() {


     $data=$this->site_model->get('document_types');
       
        if (count($data) > 0) {


            $arr = array(
                         'status' => "valid",
                         'message' => "Found",
                          "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "We're sorry, There is no document types  available . "
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }


    function add() {




               if (!$this->input->get_post('document_type')) {
            $arr = array('err_code' => "invalid", "error_type" => "document_type required", "message" => "Field should not be empty");
            echo json_encode($arr);
            die;
        }
         if (!$this->input->get_post('note')) {
            $arr = array('err_code' => "invalid", "error_type" => "note required", "message" => "Field should not be empty");
            echo json_encode($arr);
            die;
        }
          if (!$this->input->get_post('issued_date')) {
            $arr = array('err_code' => "invalid", "error_type" => "issued_date required", "message" => "Field should not be empty");
            echo json_encode($arr);
            die;
        }
        if (!$this->input->get_post('expiry_date')) {
            $arr = array('err_code' => "invalid", "error_type" => "expiry_date required", "message" => "Field should not be empty");
            echo json_encode($arr);
            die;
        }
      

        if (!isset($_FILES['document_photo']["name"])) {
           $arr = array('err_code' => "invalid", "error_type" => "document_photo required", "message" => "Please select pic to upload");
           echo json_encode($arr);
           die;
       }


          $data['user_id']  = $this->input->get_post('user_id');
          $data['vehicle_id']  = $this->input->get_post('vehicle_id');
          $data['document_type']  = $this->input->get_post('document_type');
          $data['note']  = $this->input->get_post('note');
          $data['issued_date']  = $this->input->get_post('issued_date');
          $data['expiry_date']  = $this->input->get_post('expiry_date');

          $document_photo = $this->input->get_post('document_photo', TRUE);
               if ($_FILES['document_photo']['name'] != '') {
                    $data['document_photo'] = $this->upload_file('document_photo');
                }


        $result = $this->cmoon_model->insert('vehicle_document_details',$data);

        $vehicle_details = $this->site_model->get_by_number('vehicles','id',$this->input->get_post('vehicle_id'));


            $notification_data['heading']  = 'Vechicle document details';
     $notification_data['description']  = 'You added vehicle document details to '. $vehicle_details->vehicle_num;
          $notification_data['user_id']  = $this->input->get_post('user_id');

          
          $notifications = $this->cmoon_model->insert('notifications',$notification_data);

          $user_accounts = $this->site_model->get_by_number('user_accounts','id',$this->input->get_post('user_id'));

          $user_data['notification_count'] = $user_accounts->notification_count + 1;
        $update = $this->cmoon_model->update('user_accounts',$user_data,$this->input->get_post('user_id'));


            $this->push_notification('Vechicle document details','You added vehicle document details of '. $vehicle_details->vehicle_num,$user_accounts->token);

        if ($result) {
            $arr = array(
                         'status' => "valid",
                         'message' => "This vehicle document successfully added.",
                          // "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "Failed to add vehicle document,Please try again"
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }


    function fetch() {

          $user_id =$this->input->get_post('user_id');

     $data=$this->site_model->get_by_value('vehicle_document_details','user_id',$user_id);
        $i = 0;
        foreach ($data as $append_data) {

             $qr_code_data = $this->site_model->get_by_number('vehicles','id',$append_data->vehicle_id);
           $append_data->qr_code = $qr_code_data->qr_code;
           $append_data->vehicle_num = $qr_code_data->vehicle_num;
           
          if ($append_data->document_photo != '') {
                $append_data->document_photo ="http://demoworks.in/php/myvsafety/vehicle_document_images/".$append_data->document_photo;
            }
            $i++;
        }
        if (count($data) > 0) {


            $arr = array(
                         'status' => "valid",
                         'message' => "Found",
                          "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "We're sorry, There is no vehicle documents available in your list. Please add vehicle document in your list. "
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }


    function edit() {

          // $user_id =$this->input->get_post('user_id');
          $id =$this->input->get_post('id');
     $vehicle['data'] = $this->site_model->get_by_number('vehicle_document_details','id',$id);



 if($this->input->get_post('document_type', TRUE)!=''){
                  $data['document_type'] = $this->input->get_post('document_type', TRUE);
                }else{

                  $data['document_type'] = $vehicle['data']->document_type;
                }

      if($this->input->get_post('note', TRUE)!=''){
                 $data['note'] = $this->input->get_post('note', TRUE);
                }else{

                  $data['note'] = $vehicle['data']->note;
                }
   if($this->input->get_post('issued_date', TRUE)!=''){
                 $data['issued_date'] = $this->input->get_post('issued_date', TRUE);
                }else{

                  $data['issued_date'] = $vehicle['data']->issued_date;
                }
   if($this->input->get_post('expiry_date', TRUE)!=''){
                 $data['expiry_date'] = $this->input->get_post('expiry_date', TRUE);
                }else{

                  $data['expiry_date'] = $vehicle['data']->expiry_date;
                }

$document_photo = $this->input->get_post('document_photo', TRUE);
                   if ($_FILES['document_photo']['name'] != '') {
            unlink("vehicle_document_images/".$vehicle['data']->document_photo);
                    $data['document_photo'] = $this->upload_file('document_photo');
                }else{

                  $data['document_photo'] = $vehicle['data']->document_photo;

                }

        $result = $this->site_model->update('vehicle_document_details',$data,'id',$id);


         $vehicle_details = $this->site_model->get_by_number('vehicles','id',$id);


            $notification_data['heading']  = 'Vechicle document details';
            $notification_data['description']  = 'You edited vehicle document details of '. $vehicle_details->vehicle_num;
            $notification_data['user_id']  = $vehicle_details->user_id;

          
          $notifications = $this->cmoon_model->insert('notifications',$notification_data);

          $user_accounts = $this->site_model->get_by_number('user_accounts','id',$vehicle_details->user_id);

          $user_data['notification_count'] = $user_accounts->notification_count + 1;
        $update = $this->cmoon_model->update('user_accounts',$user_data,$vehicle_details->user_id);

              $this->push_notification('Vechicle document details','You edited vehicle document details of '. $vehicle_details->vehicle_num,$user_accounts->token);

        $i = 0;
        foreach ($data as $append_data) {
          // if ($append_data->image != '') {
          //       $append_data->image ="http://demoworks.in/php/myvsafety/vehicle_document_images/".$append_data->image;
          //   }
          //   $i++;
        }
        if ($result) {
            $arr = array(
                         'status' => "valid",
                         'message' => "This vehicle document successfully edited.",
                          // "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "Failed to edit the vehicle document"
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }


     function delete() {

          // $user_id =$this->input->get_post('user_id');
          $id =$this->input->get_post('id');

$del_existing_img = $this->cmoon_model->get_row_by_row('vehicle_document_details', $id);
            unlink("vehicle_document_images/".$del_existing_img->document_photo);

        $result = $this->cmoon_model->delete('vehicle_document_details', $id);



     // $data=$this->site_model->get_by_value('vehicle_document_details','user_id',$user_id);
        $i = 0;
        foreach ($data as $append_data) {
          // if ($append_data->image != '') {
          //       $append_data->image ="http://demoworks.in/php/myvsafety/vehicle_document_images/".$append_data->image;
          //   }
            $i++;
        }
        if ($result) {
            $arr = [
                'status' => "valid",
                'message' => "This vehicle document successfully deleted.",
                // "data" => $data
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "Failed to delete the vehicle document"
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }





   function push_notification($title, $text, $token) {

   $data = array(
       "to" => $token,
       "priority" => 'high',
       "data" => array(
           "title" => $title,
           "body" => $text,
           
       )
   );

   $data_string = json_encode($data);

   $headers = array(
       'Authorization: key=' . 'AAAAZLscPaE:APA91bG5iuowe78YHLFKLAgi5whHC46GvgXJ1EmbGCEzMmubcI-J1t3rSU1ZyFHUbt66ogxhOH0jYwUBdCFUScdHof3xRLYnGuUCYJ43ycbZFerBw7Cr1tdr3fxv6Vmq0WKr7vr2OJyP',
       'Content-Type: application/json'
   );

   $ch = curl_init();

   curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
   curl_setopt($ch, CURLOPT_POST, true);
   curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
   curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

   $result = curl_exec($ch);

   curl_close($ch);

   $arr = array(
       'status' => "valid",
       'data' => strip_tags($result),
       'response' => $data
   );

   // echo json_encode($arr, JSON_PRETTY_PRINT);
   return $arr;
}




// -------------------------------------- Image upload code -----------------------------------------------------
    private function upload_file($file_name, $path = null)
    {
        $upload_path1             = "vehicle_document_images".$path;
        $config1['upload_path']   = $upload_path1;
        $config1['allowed_types'] = "*";
        $config1['max_size']      = "16000000";
        $img_name1                = strtolower($_FILES[$file_name]['name']);
        $img_name1                = preg_replace('/[^a-zA-Z0-9\.]/', "_", $img_name1);
        $config1['file_name']     =  $img_name1;
        $this->load->library('upload', $config1);
        $this->upload->initialize($config1);
        $this->upload->do_upload($file_name);
        $fileDetailArray1 = $this->upload->data();
        return $fileDetailArray1['file_name'];
    }


    }