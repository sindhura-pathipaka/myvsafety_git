<?php
require (APPPATH . '/libraries/REST_Controller.php');
class Bank extends CI_Controller
{
    function __construct(){
      parent:: __construct();
      $this->load->model('login_model','',True);
      $this->load->model('cmoon_model','',True);
      $this->load->model('site_model','',True);
   }
    function add() {

          $data['user_id']  = $this->input->get_post('user_id');
          $data['account_number']  = $this->input->get_post('account_number');
          $data['bank_holder_name']  = $this->input->get_post('bank_holder_name');
          $data['bank_ifsc_code']  = $this->input->get_post('bank_ifsc_code');
          $data['bank_name']  = $this->input->get_post('bank_name');

        $result = $this->cmoon_model->insert('bank_details',$data);

        if ($result) {
            $arr = array(
                         'status' => "valid",
                         'message' => "Bank Details was successfully added.",
                          // "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "Failed to add Bank Details,Please try again"
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }


    function fetch() {

          $user_id =$this->input->get_post('user_id');

     $data=$this->site_model->get_by_value('bank_details','user_id',$user_id);
        $i = 0;
        foreach ($data as $append_data) {
          // if ($append_data->image != '') {
          //       $append_data->image ="http://demoworks.in/php/leofin/cmoon_images/".$append_data->image;
          //   }
          //   $i++;
        }
        if (count($data) > 0) {


            $arr = array(
                         'status' => "valid",
                         'message' => "Found",
                          "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "We're sorry, Your list is empty. Please add Bank Details. "
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }


    function edit() {

          // $user_id =$this->input->get_post('user_id');
          $id =$this->input->get_post('id');
     $contact['data'] = $this->site_model->get_by_number('bank_details','id',$id);



 if($this->input->get_post('account_number', TRUE)!=''){
                  $data['account_number'] = $this->input->get_post('account_number', TRUE);
                }else{

                  $data['account_number'] = $contact['data']->account_number;
                }


        if($this->input->get_post('bank_holder_name', TRUE)!=''){
                 $data['bank_holder_name'] = $this->input->get_post('bank_holder_name', TRUE);
                }else{

                  $data['bank_holder_name'] = $contact['data']->bank_holder_name;
                }


        if($this->input->get_post('bank_ifsc_code', TRUE)!=''){
                 $data['bank_ifsc_code'] = $this->input->get_post('bank_ifsc_code', TRUE);
                }else{

                  $data['bank_ifsc_code'] = $contact['data']->bank_ifsc_code;
                }


        if($this->input->get_post('bank_name', TRUE)!=''){
                 $data['bank_name'] = $this->input->get_post('bank_name', TRUE);
                }else{

                  $data['bank_name'] = $contact['data']->bank_name;
                }



        $result = $this->site_model->update('bank_details',$data,'id',$id);

        $i = 0;
        foreach ($data as $append_data) {
          // if ($append_data->image != '') {
          //       $append_data->image ="http://demoworks.in/php/leofin/cmoon_images/".$append_data->image;
          //   }
          //   $i++;
        }
        if ($result) {
            $arr = array(
                         'status' => "valid",
                         'message' => "Bank Details was successfully edited.",
                          // "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "Failed to edit Bank Details."
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }


     function delete() {

          // $user_id =$this->input->get_post('user_id');
          $id =$this->input->get_post('id');

        $result = $this->cmoon_model->delete('bank_details', $id);



     // $data=$this->site_model->get_by_value('bank_details','user_id',$user_id);
        $i = 0;
        foreach ($data as $append_data) {
          // if ($append_data->image != '') {
          //       $append_data->image ="http://demoworks.in/php/e_pasu/cmoon_images".$append_data->image;
          //   }
            $i++;
        }
        if ($result) {
            $arr = [
                'status' => "valid",
                'message' => "Bank Details successfully deleted.",
                // "data" => $data
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "Failed to delete Bank Details"
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }



    }