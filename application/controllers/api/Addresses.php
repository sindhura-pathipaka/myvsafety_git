<?php
require (APPPATH . '/libraries/REST_Controller.php');
class Addresses extends CI_Controller
{
    function __construct(){
      parent:: __construct();
      $this->load->model('login_model','',True);
      $this->load->model('cmoon_model','',True);
      $this->load->model('site_model','',True);
   }
    function add() {
      
          $data['user_id']  = $this->input->get_post('user_id');
          $data['name']  = $this->input->get_post('name');
          $data['mobile']  = $this->input->get_post('mobile');
          $data['house_no']  = $this->input->get_post('house_no');
          $data['area']  = $this->input->get_post('area');
          $data['city']  = $this->input->get_post('city');
          $data['district']  = $this->input->get_post('district');
          $data['state']  = $this->input->get_post('state');
          $data['pin_code']  = $this->input->get_post('pin_code');

        $result = $this->cmoon_model->insert('addresses',$data);
        $i = 0;
        foreach ($data as $append_data) {
          // if ($append_data->image != '') {
          //       $append_data->image ="http://demoworks.in/php/leofin/cmoon_images/".$append_data->image;
          //   } 
          //   $i++;
        }
        if ($result) {
            $arr = array(
                         'status' => "valid",
                         'message' => "This address was successfully saved.",
                          // "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "Failed to add address,Please try again"
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }


    function fetch() {
      
          $user_id =$this->input->get_post('user_id');

     $data=$this->site_model->get_by_value('addresses','user_id',$user_id);
        $i = 0;
        foreach ($data as $append_data) {
          // if ($append_data->image != '') {
          //       $append_data->image ="http://demoworks.in/php/leofin/cmoon_images/".$append_data->image;
          //   } 
          //   $i++;
        }
        if (count($data) > 0) {


            $arr = array(
                         'status' => "valid",
                         'message' => "Found",
                          "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "We're sorry, There is no address available in your list. Please add address in your list. "
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }


    function edit() {
      
          // $user_id =$this->input->get_post('user_id');
          $id =$this->input->get_post('id');
     $contact['data'] = $this->site_model->get_by_number('addresses','id',$id);


      if($this->input->get_post('name', TRUE)!=''){
                  $data['name'] = $this->input->get_post('name', TRUE);
                }else{

                  $data['name'] = $contact['data']->name;
                }

        if($this->input->get_post('mobile', TRUE)!=''){
                 $data['mobile'] = $this->input->get_post('mobile', TRUE);
                }else{

                  $data['mobile'] = $contact['data']->mobile;
                }

         if($this->input->get_post('house_no', TRUE)!=''){
                 $data['house_no'] = $this->input->get_post('house_no', TRUE);
                }else{

                  $data['house_no'] = $contact['data']->house_no;
                }

         if($this->input->get_post('area', TRUE)!=''){
                 $data['area'] = $this->input->get_post('area', TRUE);
                }else{

                  $data['area'] = $contact['data']->area;
                }

                if($this->input->get_post('city', TRUE)!=''){
                 $data['city'] = $this->input->get_post('city', TRUE);
                }else{

                  $data['city'] = $contact['data']->city;
                }

                if($this->input->get_post('district', TRUE)!=''){
                 $data['district'] = $this->input->get_post('district', TRUE);
                }else{

                  $data['district'] = $contact['data']->district;
                }

         if($this->input->get_post('state', TRUE)!=''){
                 $data['state'] = $this->input->get_post('state', TRUE);
                }else{

                  $data['state'] = $contact['data']->state;
                }

         if($this->input->get_post('pin_code', TRUE)!=''){
                 $data['pin_code'] = $this->input->get_post('pin_code', TRUE);
                }else{

                  $data['pin_code'] = $contact['data']->pin_code;
                }



        $result = $this->site_model->update('addresses',$data,'id',$id); 

        $i = 0;
        foreach ($data as $append_data) {
          // if ($append_data->image != '') {
          //       $append_data->image ="http://demoworks.in/php/leofin/cmoon_images/".$append_data->image;
          //   } 
          //   $i++;
        }
        if ($result) {
            $arr = array(
                         'status' => "valid",
                         'message' => "This address was successfully edited.",
                          // "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "Failed to edit the address"
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }


     function delete() {

          // $user_id =$this->input->get_post('user_id');
          $id =$this->input->get_post('id');

        $result = $this->cmoon_model->delete('addresses', $id);

      

     // $data=$this->site_model->get_by_value('addresses','user_id',$user_id);
        $i = 0;
        foreach ($data as $append_data) {
          // if ($append_data->image != '') {
          //       $append_data->image ="http://demoworks.in/php/e_pasu/cmoon_images".$append_data->image;
          //   } 
            $i++;
        }
        if ($result) {
            $arr = [
                'status' => "valid",
                'message' => "This address was successfully deleted.",
                // "data" => $data
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "Failed to delete the address"
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }



    }