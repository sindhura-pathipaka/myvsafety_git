<?php
require (APPPATH . '/libraries/REST_Controller.php');
class orders extends CI_Controller
{
    function __construct(){
      parent:: __construct();
      $this->load->model('login_model','',True);
      $this->load->model('cmoon_model','',True);
      $this->load->model('site_model','',True);
   }
    function add() {


      $referral_amount = $this->site_model->get_cms('referral_renewal','1')->referral_points;

          $data['user_id']  = $this->input->get_post('user_id');
          $data['kit_id']  = $this->input->get_post('kit_id');

          $kit_details = $this->site_model->get_by_where_column_2('qr_kits','id',$data['kit_id']);

          $data['kit_name']  = $kit_details->name;
          $data['actual_price']  = $kit_details->actual_price;
          $data['discount_percentage']  = $kit_details->discount_percentage;
          $data['discount_price']  = $kit_details->discount_price;

          $data['order_id']  = $this->generate_order_id();
          $data['order_status']  = 'PLACED';
          $data['payment_status']  = 'PAID';

          $data['payment_id']  = $this->input->get_post('payment_id');
          $data['referral_code']  = $this->input->get_post('referral_code');

          $data['address_id']  = $this->input->get_post('address_id');

          $address = $this->site_model->get_by_where_column_2('addresses','id',$data['address_id']);

          $data['name']  = $address->name;
          $data['mobile']  = $address->mobile;
          $data['house_no']  = $address->house_no;
          $data['area']  = $address->area;
          $data['state']  = $address->state;
          $data['pin_code']  = $address->pin_code;


          date_default_timezone_set('Asia/Kolkata');
        $data['payment_done'] =  date( 'd-m-Y h:i:s A');


          // $data['referral_code']  = $this->input->get_post('referral_code');

          if($data['referral_code'] != ''){

         $referral_data = $this->site_model->get_row_by_id('user_accounts','referral_code',$data['referral_code'] );

         if(count($referral_data) != 0){



          $referral_points_data['referral_code_used_by']  = $data['user_id'];
          $referral_points_data['referral_code']  = $data['referral_code'];
          $referral_points_data['referral_code_by']  = $referral_data->id;
          $referral_points_data['points_added']  = $referral_amount;
          $referral_points_data['withdraw_status']  ='NO';

           date_default_timezone_set('Asia/Kolkata');
        $referral_points_data['added_on'] =  date( 'd-m-Y h:i:s A');

          $result = $this->cmoon_model->insert('referral_points',$referral_points_data);




        $referral_points_count = $this->site_model->get_by_value2_and('referral_points','referral_code_by',$referral_data->id,'withdraw_status','NO' );

             foreach($referral_points_count as $count){

            $balance_referral_points+=$count->points_added;
        }

        if ($balance_referral_points != '') {

          $user_data['balance_referral_points']  = $balance_referral_points;
      }else{

          $user_data['balance_referral_points']  = $referral_amount;

      }

          $user_data['total_referral_points']  = $referral_data->total_referral_points + $referral_amount;


        $result = $this->site_model->update('user_accounts',$user_data,'id',$referral_data->id);




          $data['referred_by_user_id']  = $referral_data->id;



          $result = $this->cmoon_model->insert('orders',$data);

             $inserted_id = $this->db->insert_id();
         $query = $this->site_model->get_row_by_id('orders','id',$inserted_id );


            $notification_data['heading']  = 'Order Placed';
            $notification_data['description']  = 'Your order is successfully placed';
            $notification_data['user_id']  = $query->user_id;

          
          $notifications = $this->cmoon_model->insert('notifications',$notification_data);

          $user_accounts = $this->site_model->get_by_number('user_accounts','id',$query->user_id);

          $user_data['notification_count'] = $user_accounts->notification_count + 1;
        $update = $this->cmoon_model->update('user_accounts',$user_data,$query->user_id);

              $this->push_notification('Order Placed','Your order is successfully placed',$user_accounts->token);


        if ($result) {
            $arr = array(
                         'status' => "valid",
                         'message' => "Order Placed successfully .",
                          "data" => $query->order_id,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "Failed to Order,Please try again"
            ];
        }

         }else{

          $arr = [
                'status' => "invalid",
                'message' => "Invalid Referral Code"
            ];

         }


          }else{

        $result = $this->cmoon_model->insert('orders',$data);

             $inserted_id = $this->db->insert_id();
         $query = $this->site_model->get_row_by_id('orders','id',$inserted_id );


            $notification_data['heading']  = 'Order Placed';
            $notification_data['description']  = 'Your order is successfully placed';
            $notification_data['user_id']  = $query->user_id;

          
          $notifications = $this->cmoon_model->insert('notifications',$notification_data);

          $user_accounts = $this->site_model->get_by_number('user_accounts','id',$query->user_id);

          $user_data['notification_count'] = $user_accounts->notification_count + 1;
        $update = $this->cmoon_model->update('user_accounts',$user_data,$query->user_id);

              $this->push_notification('Order Placed','Your order is successfully placed',$user_accounts->token);


        if ($result) {
            $arr = array(
                         'status' => "valid",
                         'message' => "Order Placed successfully .",
                          "data" => $query->order_id,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "Failed to Order,Please try again"
            ];
        }

      }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }




   function push_notification($title, $text, $token) {

   $data = array(
       "to" => $token,
       "priority" => 'high',
       "data" => array(
           "title" => $title,
           "body" => $text,
           
       )
   );

   $data_string = json_encode($data);

   $headers = array(
       'Authorization: key=' . 'AAAAZLscPaE:APA91bG5iuowe78YHLFKLAgi5whHC46GvgXJ1EmbGCEzMmubcI-J1t3rSU1ZyFHUbt66ogxhOH0jYwUBdCFUScdHof3xRLYnGuUCYJ43ycbZFerBw7Cr1tdr3fxv6Vmq0WKr7vr2OJyP',
       'Content-Type: application/json'
   );

   $ch = curl_init();

   curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
   curl_setopt($ch, CURLOPT_POST, true);
   curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
   curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

   $result = curl_exec($ch);

   curl_close($ch);

   $arr = array(
       'status' => "valid",
       'data' => strip_tags($result),
       'response' => $data
   );

   // echo json_encode($arr, JSON_PRETTY_PRINT);
   return $arr;
}



    function fetch() {

          $user_id = $this->input->get_post('user_id');
          $page_no = $this->input->get_post('page_no');
          $type = $this->input->get_post('type');

          $per_page =20;

     if($type == 'All'){




     $data_no=$this->site_model->get_by_value('orders','user_id',$user_id,'2',$page_no);


     $data_count = count($data_no);


     $total_pages = ceil($data_count/$per_page);

     $data=$this->site_model->get_page_data('orders','user_id',$user_id,$per_page,$page_no);


   }elseif ($type == 'Ongoing') {

     $data_no=$this->site_model->get_by_value2_and('orders','user_id',$user_id,'order_status','ONGOING');

     $data_count = count($data_no);


     $total_pages = ceil($data_count/$per_page);

     $data=$this->site_model->get_page_data_type2('orders','user_id',$user_id,'order_status','ONGOING','PREPARING',$per_page,$page_no);
   }elseif ($type == 'Completed') {

     $data_no=$this->site_model->get_by_value2_and('orders','user_id',$user_id,'order_status','COMPLETED');

     $data_count = count($data_no);


     $total_pages = ceil($data_count/$per_page);

     $data=$this->site_model->get_page_data_type('orders','user_id',$user_id,'order_status','COMPLETED',$per_page,$page_no);

   }


  if(count($data) > 0){
         $data = array(
                          "total_pages" => $total_pages,
                          "result" => $data,

                    );

            $arr = array(
                         'status' => "valid",
                         'message' => "Found",
                          "data" => $data,
                    );

      }else {

            $arr = [
                'status' => "invalid",
                'message' => "We're sorry, There is no orders in your list. "
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);


    }


    function details() {

          $id = $this->input->get_post('id');


     $data = $this->site_model->get_by_number('orders','id',$id);


  if(count($data) > 0){


            $arr = array(
                         'status' => "valid",
                         'message' => "Found",
                          "data" => $data,
                    );

      }else {

            $arr = [
                'status' => "invalid",
                'message' => "We're sorry, There is no orders in your list. "
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);


    }

private function generate_order_id()  {

  // $seed = str_split('abcdefghijklmnopqrstuvwxyz'
  //               .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
  //               .'0123456789'); // and any other characters

        $seed = str_split('0123456789'); // and any other characters
        shuffle($seed); // probably optional since array is randomized; this may be redundant
        $otp = '';
        foreach (array_rand($seed, 6) as $k)
        {
           $otp .= $seed[$k];
        }
        return $otp;
    }


    }