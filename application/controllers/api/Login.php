<?php
require (APPPATH . '/libraries/REST_Controller.php');
class Login extends CI_Controller
{
    function __construct(){
      parent:: __construct();
      $this->load->model('login_model','',True);
      $this->load->model('cmoon_model','',True);
      $this->load->model('site_model','',True);
      $this->load->helper('sms');
   }


function index() {


        if (!$this->input->get_post('mobile')) {
            $arr = array('status' => "invalid", "status" => "mobile required", "message" => "Mobile is required");
            echo json_encode($arr);
            die;
        }
       $mobile = $this->input->get_post('mobile', TRUE);

        $data['mobile'] =  $mobile;
        $generated_token =  $this->generatereg_no();
        $data['reg_no'] =$generated_token;
         $otp = $this->generateOTP();
        // $otp = '1234';
              $data['otp'] = $otp;
                   $data['otp_status'] = 'Not Verified';

        date_default_timezone_set('Asia/Kolkata');
        $data['created_at'] =  date( 'd-m-Y h:i:s A');

              $data['referral_code'] = $this->generate_referal_code();
              $data['total_referral_points'] = 0;

                   $data['account_status'] = 'NOT ACTIVATED';


         $query = $this->site_model->get_row_by_id('user_accounts','mobile',$mobile);
   if(count($query) == 0){

        $result = $this->cmoon_model->insert('user_accounts',$data);
             $user_id= $this->db->insert_id();

        date_default_timezone_set('Asia/Kolkata');
        $user_log_data['login_time'] =  date( 'd-m-Y h:i:s A');
        $user_log_data['mobile'] =  $mobile;
        $user_log_data['ip_address'] = $_SERVER['REMOTE_ADDR'];

              $user_log_data['reg_no'] = $generated_token;
              $user_log_data['user_id'] = $user_id;
        $result = $this->cmoon_model->insert('user_logs',$user_log_data);
      }else{

        date_default_timezone_set('Asia/Kolkata');
        $user_log_data['login_time'] =  date( 'd-m-Y h:i:s A');
        $user_log_data['mobile'] =  $mobile;
        $user_log_data['ip_address'] = $_SERVER['REMOTE_ADDR'];

              $user_log_data['reg_no'] = $query->reg_no;
              $user_log_data['user_id'] = $query->id;
              $user_id = $query->id;
              // $otp = $query->otp;
        $result = $this->cmoon_model->insert('user_logs',$user_log_data);


                   $form_data['otp_status'] = 'Not Verified';
              $form_data['otp'] = $otp;

          $result = $this->cmoon_model->update('user_accounts', $form_data,$user_id);

      }



if($result){





        $data['user_accounts'] = $this->site_model->get_by_number('user_accounts','id',$user_id);

          sendsms('Dear '. $data['user_accounts']->name. ' your one time password (OTP) '.$data['user_accounts']->otp.' Regards CMTOTP ',$data['user_accounts']->mobile);


                  $data = array("otp" => $otp,
                        "user_id" => $user_id,
                        "name" => $data['user_accounts']->name,
                      );

                    $arr = array(
                        'status' => "valid",
                        "message" => "OTP has been sent to your Mobile",
                        "data" => $data,


                    );
                    echo json_encode($arr);
        } else {
            $arr = array('status' => "invalid", "message" => "Plaese Try again");
            echo json_encode($arr);
        }

}


function auto_login() {


        if (!$this->input->get_post('mobile')) {
            $arr = array('status' => "invalid", "status" => "mobile required", "message" => "Mobile is required");
            echo json_encode($arr);
            die;
        }
       $mobile = $this->input->get_post('mobile', TRUE);

        $data['mobile'] =  $mobile;
        $generated_token =  $this->generatereg_no();
        $data['reg_no'] =$generated_token;
         $otp = $this->generateOTP();
        // $otp = '1234';
              $data['otp'] = $otp;
                   $data['otp_status'] = 'Not Verified';

        date_default_timezone_set('Asia/Kolkata');
        $data['created_at'] =  date( 'd-m-Y h:i:s A');



         $query = $this->site_model->get_row_by_id('user_accounts','mobile',$mobile);
   if(count($query) == 0){

        $result = $this->cmoon_model->insert('user_accounts',$data);
             $user_id= $this->db->insert_id();

        date_default_timezone_set('Asia/Kolkata');
        $user_log_data['login_time'] =  date( 'd-m-Y h:i:s A');
        $user_log_data['mobile'] =  $mobile;
        $user_log_data['ip_address'] = $_SERVER['REMOTE_ADDR'];

              $user_log_data['reg_no'] = $generated_token;
              $user_log_data['user_id'] = $user_id;
        $result = $this->cmoon_model->insert('user_logs',$user_log_data);
      }else{

        date_default_timezone_set('Asia/Kolkata');
        $user_log_data['login_time'] =  date( 'd-m-Y h:i:s A');
        $user_log_data['mobile'] =  $mobile;
        $user_log_data['ip_address'] = $_SERVER['REMOTE_ADDR'];

              $user_log_data['reg_no'] = $query->reg_no;
              $user_log_data['user_id'] = $query->id;
              $user_id = $query->id;
              // $otp = $query->otp;
        $result = $this->cmoon_model->insert('user_logs',$user_log_data);


                   $form_data['otp_status'] = 'Not Verified';
              $form_data['otp'] = $otp;

          $result = $this->cmoon_model->update('user_accounts', $form_data,$user_id);

      }



if($result){




        $data['user_accounts'] = $this->site_model->get_by_number('user_accounts','id',$user_id);

    // sendsms('Dear '. $data['user_accounts']->name. ' your one time password (OTP) '.$data['user_accounts']->otp.' Regards CMTOTP ',$data['user_accounts']->mobile);


                  $data = array(
                        "user_id" => $user_id,
                        "name" => $data['user_accounts']->name,
                      );

                    $arr = array(
                        'status' => "valid",
                        "data" => $data,


                    );
                    echo json_encode($arr);
        } else {
            $arr = array('status' => "invalid", "message" => "Plaese Try again");
            echo json_encode($arr);
        }

}

  function otp_verification() {

        if (!$this->input->get_post('otp')) {
            $arr = array('status' => "invalid", "error_type" => "otp required", "message" => "OTP is required");
            echo json_encode($arr);
            die;
        }

        $user_id=$this->input->get_post('user_id');
       // $reg_no=$this->input->get_post('reg_no');
       $otp = $this->input->get_post('otp', TRUE);
        $data['user_accounts'] = $this->site_model->get_by_number2('user_accounts','id',$user_id,'otp',$otp);

          if(count($data['user_accounts']) == 0){


            $arr = array('status' => "invalid",
                        "message" => "Please enter correct OTP."

                        );

                    echo json_encode($arr,JSON_PRETTY_PRINT);


          }else{


            $form_data['otp_status'] = 'Verified';

                  $result = $this->cmoon_model->update('user_accounts', $form_data,$user_id);



                    $data = array(
                        "name" => $data['user_accounts']->name,
                        // "mobile" => $data['user_accounts']->mobile,
                    );

           $arr = array(
                        'status' => "valid",
                        "message" => "successful",
                        "data" => $data,
                    );
                    echo json_encode($arr,JSON_PRETTY_PRINT);
      }
}




 function resend_otp() {

        $user_id=$this->input->get_post('user_id');
       // $reg_no=$this->input->get_post('reg_no');

        $data['user_accounts'] = $this->site_model->get_row_by_id('user_accounts','id',$user_id);

          if(count($data['user_accounts']) != 0){

             $otp = $this->generateOTP();
              $form_data['otp'] = $otp;
              $form_data['otp_status'] = 'Not Verified';

              
    sendsms('Dear '. $data['user_accounts']->name. ' your one time password (OTP) '.$form_data['otp'].' Regards CMTOTP ',$data['user_accounts']->mobile);

                  $result = $this->cmoon_model->update('user_accounts', $form_data,$user_id);

        $data['otp'] = $this->db->get_where('user_accounts',['id'=>$user_id])->row();


        $data = array( "user_id" => $data['otp']->id,
                        // "reg_no" => $data['otp']->reg_no,
                        "otp" => $data['otp']->otp,
                      );

            $arr = array('status' => "valid",

                        "title" => " successful",
                         "message" => "OTP has been sent to your Number",
                          "data" => $data,
                        );
            echo json_encode($arr);
          }else{
           $arr = array(
                        'status' => "invalid",
                        "message" => "Failed",
                    );
                    echo json_encode($arr);
      }
}




function profile() {


          $user_id=$this->input->get_post('user_id');

        $data['user_accounts'] = $this->site_model->get_by_number('user_accounts','id',$user_id);



if(count($data['user_accounts']) != 0){

         $data = array( "profile_pic" => "http://demoworks.in/php/myvsafety/profile_images/".$data['user_accounts']->profile_pic,
                        "name" => $data['user_accounts']->name,
                        "blood_group" => $data['user_accounts']->blood_group,
                        "bp" => $data['user_accounts']->bp,
                        "heart_problem" => $data['user_accounts']->heart_problem,
                        // "organ_problem" => $data['user_accounts']->organ_problem,
                        "thyroid" => $data['user_accounts']->thyroid,
                        "other_health_problem" => $data['user_accounts']->other_health_problem,
                        "using_medicines" => $data['user_accounts']->using_medicines,
                        "carrying_medicines" => $data['user_accounts']->carrying_medicines,
                      );

                    $arr = array(
                        'status' => "valid",
                        "message" => " successful",
                          "data" => $data,
                    );
                    echo json_encode($arr,JSON_PRETTY_PRINT);
      }else{
           $arr = array('status' => "invalid", "message" => "Please check");
                    echo json_encode($arr,JSON_PRETTY_PRINT);
      }
}


function update_profile() {


          $user_id=$this->input->get_post('user_id');
       // $reg_no=$this->input->get_post('reg_no');
       // $device_id=$this->input->get_post('device_id');

        $data['user_accounts'] = $this->site_model->get_row_by_id('user_accounts','id',$user_id);

                $del_existing_img = $this->cmoon_model->get_row_by_row('user_accounts', $data['user_accounts']->id);
                  $profile_pic = $this->input->get_post('profile_pic', TRUE);
                   if ($_FILES['profile_pic']['name'] != '') {

                    unlink("profile_images/".$del_existing_img->profile_pic);
                    $form_data['profile_pic'] = $this->upload_file('profile_pic');
                }else{
                  $form_data['profile_pic'] = $data['user_accounts']->profile_pic;

                }

                if($this->input->get_post('name', TRUE)!=''){
                  $form_data['name'] = $this->input->get_post('name', TRUE);
                }else{
                  $form_data['name'] = $data['user_accounts']->name;

                }

             if($this->input->get_post('blood_group', TRUE)!=''){
                  $form_data['blood_group'] = $this->input->get_post('blood_group', TRUE);
                }else{
                  $form_data['blood_group'] = $data['user_accounts']->blood_group;

                }


                if($this->input->get_post('bp', TRUE)!=''){
                  $form_data['bp'] = $this->input->get_post('bp', TRUE);
                }else{
                  $form_data['bp'] = $data['user_accounts']->bp;

                }


                if($this->input->get_post('heart_problem', TRUE)!=''){
                  $form_data['heart_problem'] = $this->input->get_post('heart_problem', TRUE);
                }else{
                  $form_data['heart_problem'] = $data['user_accounts']->heart_problem;

                }

                //   if($this->input->get_post('organ_problem', TRUE)!=''){
                //   $form_data['organ_problem'] = $this->input->get_post('organ_problem', TRUE);
                // }else{
                //   $form_data['organ_problem'] = $data['user_accounts']->organ_problem;

                // }

                  if($this->input->get_post('thyroid', TRUE)!=''){
                  $form_data['thyroid'] = $this->input->get_post('thyroid', TRUE);
                }else{
                  $form_data['thyroid'] = $data['user_accounts']->thyroid;

                }
                  if($this->input->get_post('other_health_problem', TRUE)!=''){
                  $form_data['other_health_problem'] = $this->input->get_post('other_health_problem', TRUE);
                }else{
                  $form_data['other_health_problem'] = $data['user_accounts']->other_health_problem;

                }
                  if($this->input->get_post('using_medicines', TRUE)!=''){
                  $form_data['using_medicines'] = $this->input->get_post('using_medicines', TRUE);
                }else{
                  $form_data['using_medicines'] = $data['user_accounts']->using_medicines;

                }

                  if($this->input->get_post('carrying_medicines', TRUE)!=''){
                  $form_data['carrying_medicines'] = $this->input->get_post('carrying_medicines', TRUE);
                }else{
                  $form_data['carrying_medicines'] = $data['user_accounts']->carrying_medicines;
                }



        $result = $this->site_model->update('user_accounts',$form_data,'id',$user_id);

        $data['new_user_accounts'] = $this->site_model->get_by_number('user_accounts','id',$user_id);

if($result){

         $data = array( "profile_pic" => "http://demoworks.in/php/myvsafety/profile_images/".$data['new_user_accounts']->profile_pic,
                        "name" => $data['new_user_accounts']->name,
                        "blood_group" => $data['new_user_accounts']->blood_group,
                        "bp" => $data['new_user_accounts']->bp,
                        "heart_problem" => $data['new_user_accounts']->heart_problem,
                        // "organ_problem" => $data['new_user_accounts']->organ_problem,
                        "thyroid" => $data['new_user_accounts']->thyroid,
                        "other_health_problem" => $data['new_user_accounts']->other_health_problem,
                        "using_medicines" => $data['new_user_accounts']->using_medicines,
                        "carrying_medicines" => $data['new_user_accounts']->carrying_medicines,
                      );

                    $arr = array(
                        'status' => "valid",
                        "message" => " successful",
                          "data" => $data,
                    );
                    echo json_encode($arr,JSON_PRETTY_PRINT);
      }else{
           $arr = array('status' => "invalid", "message" => "Please check");
                    echo json_encode($arr,JSON_PRETTY_PRINT);
      }
}



 function update_password()
    {


          $user_id=$this->input->get_post('user_id');
       $reg_no=$this->input->get_post('reg_no');
       $device_id=$this->input->get_post('device_id');

        $data['user_accounts'] = $this->site_model->get_by_number3('user_accounts','device_id',$device_id,'id',$user_id,'reg_no',$reg_no);


       $password = sha1($this->input->get_post('password', TRUE));

            $row_there = $this->cmoon_model->get_psw('user_accounts', $data['user_accounts']->id, $password);
            if ($row_there == 1) {
       $new_password = sha1($this->input->get_post('new_password', TRUE));
       $form_data['password'] = $new_password;
        $result = $this->site_model->update('user_accounts',$form_data,'id',$user_id);

if($result){

         $data = array( "device_id" => $data['user_accounts']->device_id,
                        "user_id" => $data['user_accounts']->id,
                        "reg_no" => $data['user_accounts']->reg_no,
                      );

                    $arr = array(
                        'status' => "valid",
                        "message" => " successful",
                          // "data" => $data,
                    );
                    echo json_encode($arr,JSON_PRETTY_PRINT);
      }else{
           $arr = array('status' => "invalid", "message" => "Please check");
                    echo json_encode($arr,JSON_PRETTY_PRINT);
      }
    }else{
           $arr = array('status' => "invalid", "message" => "Password Doesnot match with the present password");
                    echo json_encode($arr,JSON_PRETTY_PRINT);
      }

}




  function token_update() {

        if (!$this->input->get_post('token')) {
            $arr = array('status' => "invalid", "error_type" => "token required", "message" => "token is required");
            echo json_encode($arr);
            die;
        }

        $user_id=$this->input->get_post('user_id');
        $token = $this->input->get_post('token', TRUE);

        $form_data['token'] = $token;

         $result = $this->cmoon_model->update('user_accounts', $form_data,$user_id);


if($result){

           $arr = array(
                        'status' => "valid",
                        "message" => "successful",
                    );
      }else{

                   $arr = array(
                        'status' => "invalid",
                        "message" => "failed",
                    );

      }
                    echo json_encode($arr,JSON_PRETTY_PRINT);
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

private function generateOTP()  {
        $seed = str_split('0123456789'); // and any other characters
        shuffle($seed); // probably optional since array is randomized; this may be redundant
        $otp = '';
        foreach (array_rand($seed, 4) as $k)
        {
           $otp .= $seed[$k];
        }
        return $otp;
    }
private function generatereg_no()  {
        $seed = str_split('0123456789'); // and any other characters
        shuffle($seed); // probably optional since array is randomized; this may be redundant
        $otp = '';
        foreach (array_rand($seed, 6) as $k)
        {
           $otp .= $seed[$k];
        }
        return $otp;
    }


    private function generate_referal_code()  {

  // $seed = str_split('abcdefghijklmnopqrstuvwxyz'
  //               .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
  //               .'0123456789'); // and any other characters

        $seed = str_split('ABCDEFGHIJKLMNOPQRSTUVWXYZ'.'0123456789'); // and any other characters
        shuffle($seed); // probably optional since array is randomized; this may be redundant
        $otp = '';
        foreach (array_rand($seed, 8) as $k)
        {
           $otp .= $seed[$k];
        }
        return $otp;
    }



// -------------------------------------- Image upload code -----------------------------------------------------
    private function upload_file($file_name, $path = null)
    {
        $upload_path1             = "profile_images".$path;
        $config1['upload_path']   = $upload_path1;
        $config1['allowed_types'] = "*";
        $config1['max_size']      = "16000000";
        $img_name1                = strtolower($_FILES[$file_name]['name']);
        $img_name1                = preg_replace('/[^a-zA-Z0-9\.]/', "_", $img_name1);
        $config1['file_name']     =  $img_name1;
        $this->load->library('upload', $config1);
        $this->upload->initialize($config1);
        $this->upload->do_upload($file_name);
        $fileDetailArray1 = $this->upload->data();
        return $fileDetailArray1['file_name'];
    }
    }