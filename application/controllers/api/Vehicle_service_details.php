<?php
require (APPPATH . '/libraries/REST_Controller.php');
class vehicle_service_details extends CI_Controller
{
    function __construct(){
      parent:: __construct();
      $this->load->model('login_model','',True);
      $this->load->model('cmoon_model','',True);
      $this->load->model('site_model','',True);
   }



    function services_types() {


     $data=$this->site_model->get('services_types');
       
        if (count($data) > 0) {


            $arr = array(
                         'status' => "valid",
                         'message' => "Found",
                          "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "We're sorry, There is no service types  available . "
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }

    function add() {



               if (!$this->input->get_post('service_type')) {
            $arr = array('err_code' => "invalid", "error_type" => "service_type required", "message" => "Field should not be empty");
            echo json_encode($arr);
            die;
        }
         if (!$this->input->get_post('service_note')) {
            $arr = array('err_code' => "invalid", "error_type" => "service_note required", "message" => "Field should not be empty");
            echo json_encode($arr);
            die;
        }
          if (!$this->input->get_post('last_service_date')) {
            $arr = array('err_code' => "invalid", "error_type" => "last_service_date required", "message" => "Field should not be empty");
            echo json_encode($arr);
            die;
        }
        if (!$this->input->get_post('next_service_date')) {
            $arr = array('err_code' => "invalid", "error_type" => "next_service_date required", "message" => "Field should not be empty");
            echo json_encode($arr);
            die;
        }
      

        if (!isset($_FILES['service_bill']["name"])) {
           $arr = array('err_code' => "invalid", "error_type" => "service_bill required", "message" => "Please select pic to upload");
           echo json_encode($arr);
           die;
       }

          $data['user_id']  = $this->input->get_post('user_id');
          $data['vehicle_id']  = $this->input->get_post('vehicle_id');
          $data['service_type']  = $this->input->get_post('service_type');
          $data['service_note']  = $this->input->get_post('service_note');
          $data['last_service_date']  = $this->input->get_post('last_service_date');
          $data['next_service_date']  = $this->input->get_post('next_service_date');

          $service_bill = $this->input->get_post('service_bill', TRUE);
               if ($_FILES['service_bill']['name'] != '') {
                    $data['service_bill'] = $this->upload_file('service_bill');
                }


        $result = $this->cmoon_model->insert('vehicle_service_details',$data);

        $vehicle_details = $this->site_model->get_by_number('vehicles','id',$this->input->get_post('vehicle_id'));


            $notification_data['heading']  = 'Vechicle service details';
     $notification_data['description']  = 'You added vehicle service details to '. $vehicle_details->vehicle_num;
          $notification_data['user_id']  = $this->input->get_post('user_id');

          
          $notifications = $this->cmoon_model->insert('notifications',$notification_data);

          $user_accounts = $this->site_model->get_by_number('user_accounts','id',$this->input->get_post('user_id'));

          $user_data['notification_count'] = $user_accounts->notification_count + 1;
        $update = $this->cmoon_model->update('user_accounts',$user_data,$this->input->get_post('user_id'));

            $this->push_notification('Vechicle service details','You added vehicle service details of '. $vehicle_details->vehicle_num,$user_accounts->token);

            
        if ($result) {
            $arr = array(
                         'status' => "valid",
                         'message' => "This vehicle service successfully added.",
                          // "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "Failed to add vehicle service,Please try again"
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }


    function fetch() {

          $user_id =$this->input->get_post('user_id');

     $data=$this->site_model->get_by_value('vehicle_service_details','user_id',$user_id);
        $i = 0;
        foreach ($data as $append_data) {

            $qr_code_data = $this->site_model->get_by_number('vehicles','id',$append_data->vehicle_id);
           $append_data->qr_code = $qr_code_data->qr_code;
           $append_data->vehicle_num = $qr_code_data->vehicle_num;

          if ($append_data->service_bill != '') {
                $append_data->service_bill ="http://demoworks.in/php/myvsafety/vehicle_service_images/".$append_data->service_bill;
            }
            $i++;
        }
        if (count($data) > 0) {


            $arr = array(
                         'status' => "valid",
                         'message' => "Found",
                          "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "We're sorry, There is no vehicle services available in your list. Please add vehicle service in your list. "
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }


    function edit() {

          // $user_id =$this->input->get_post('user_id');
          $id =$this->input->get_post('id');
     $vehicle['data'] = $this->site_model->get_by_number('vehicle_service_details','id',$id);



 if($this->input->get_post('service_type', TRUE)!=''){
                  $data['service_type'] = $this->input->get_post('service_type', TRUE);
                }else{

                  $data['service_type'] = $vehicle['data']->service_type;
                }

      if($this->input->get_post('service_note', TRUE)!=''){
                 $data['service_note'] = $this->input->get_post('service_note', TRUE);
                }else{

                  $data['service_note'] = $vehicle['data']->service_note;
                }
   if($this->input->get_post('last_service_date', TRUE)!=''){
                 $data['last_service_date'] = $this->input->get_post('last_service_date', TRUE);
                }else{

                  $data['last_service_date'] = $vehicle['data']->last_service_date;
                }
   if($this->input->get_post('next_service_date', TRUE)!=''){
                 $data['next_service_date'] = $this->input->get_post('next_service_date', TRUE);
                }else{

                  $data['next_service_date'] = $vehicle['data']->next_service_date;
                }

$service_bill = $this->input->get_post('service_bill', TRUE);
                   if ($_FILES['service_bill']['name'] != '') {
            unlink("vehicle_service_images/".$vehicle['data']->service_bill);
                    $data['service_bill'] = $this->upload_file('service_bill');
                }else{

                  $data['service_bill'] = $vehicle['data']->service_bill;

                }

        $result = $this->site_model->update('vehicle_service_details',$data,'id',$id);


         $vehicle_details = $this->site_model->get_by_number('vehicles','id',$id);


            $notification_data['heading']  = 'Vechicle service details';
            $notification_data['description']  = 'You edited vehicle service details of '. $vehicle_details->vehicle_num;
            $notification_data['user_id']  = $vehicle_details->user_id;

          
          $notifications = $this->cmoon_model->insert('notifications',$notification_data);

          $user_accounts = $this->site_model->get_by_number('user_accounts','id',$vehicle_details->user_id);

          $user_data['notification_count'] = $user_accounts->notification_count + 1;
        $update = $this->cmoon_model->update('user_accounts',$user_data,$vehicle_details->user_id);
        
            $this->push_notification('Vechicle service details','You edited vehicle service details of '. $vehicle_details->vehicle_num,$user_accounts->token);


        $i = 0;
        foreach ($data as $append_data) {
          // if ($append_data->image != '') {
          //       $append_data->image ="http://demoworks.in/php/myvsafety/vehicle_service_images/".$append_data->image;
          //   }
          //   $i++;
        }
        if ($result) {
            $arr = array(
                         'status' => "valid",
                         'message' => "This vehicle service successfully edited.",
                          // "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "Failed to edit the vehicle service"
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }


     function delete() {

          // $user_id =$this->input->get_post('user_id');
          $id =$this->input->get_post('id');

$del_existing_img = $this->cmoon_model->get_row_by_row('vehicle_service_details', $id);
            unlink("vehicle_service_images/".$del_existing_img->service_bill);

        $result = $this->cmoon_model->delete('vehicle_service_details', $id);



     // $data=$this->site_model->get_by_value('vehicle_service_details','user_id',$user_id);
        $i = 0;
        foreach ($data as $append_data) {
          // if ($append_data->image != '') {
          //       $append_data->image ="http://demoworks.in/php/myvsafety/vehicle_service_images/".$append_data->image;
          //   }
            $i++;
        }
        if ($result) {
            $arr = [
                'status' => "valid",
                'message' => "This vehicle service successfully deleted.",
                // "data" => $data
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "Failed to delete the vehicle service"
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }






   function push_notification($title, $text, $token) {

   $data = array(
       "to" => $token,
       "priority" => 'high',
       "data" => array(
           "title" => $title,
           "body" => $text,
           
       )
   );

   $data_string = json_encode($data);

   $headers = array(
       'Authorization: key=' . 'AAAAZLscPaE:APA91bG5iuowe78YHLFKLAgi5whHC46GvgXJ1EmbGCEzMmubcI-J1t3rSU1ZyFHUbt66ogxhOH0jYwUBdCFUScdHof3xRLYnGuUCYJ43ycbZFerBw7Cr1tdr3fxv6Vmq0WKr7vr2OJyP',
       'Content-Type: application/json'
   );

   $ch = curl_init();

   curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
   curl_setopt($ch, CURLOPT_POST, true);
   curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
   curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

   $result = curl_exec($ch);

   curl_close($ch);

   $arr = array(
       'status' => "valid",
       'data' => strip_tags($result),
       'response' => $data
   );

   // echo json_encode($arr, JSON_PRETTY_PRINT);
   return $arr;
}



// -------------------------------------- Image upload code -----------------------------------------------------
    private function upload_file($file_name, $path = null)
    {
        $upload_path1             = "vehicle_service_images".$path;
        $config1['upload_path']   = $upload_path1;
        $config1['allowed_types'] = "*";
        $config1['max_size']      = "16000000";
        $img_name1                = strtolower($_FILES[$file_name]['name']);
        $img_name1                = preg_replace('/[^a-zA-Z0-9\.]/', "_", $img_name1);
        $config1['file_name']     =  $img_name1;
        $this->load->library('upload', $config1);
        $this->upload->initialize($config1);
        $this->upload->do_upload($file_name);
        $fileDetailArray1 = $this->upload->data();
        return $fileDetailArray1['file_name'];
    }


    }