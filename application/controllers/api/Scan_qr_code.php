<?php
require (APPPATH . '/libraries/REST_Controller.php');
class scan_qr_code extends CI_Controller
{
    function __construct(){
      parent:: __construct();
      $this->load->model('login_model','',True);
      $this->load->model('cmoon_model','',True);
      $this->load->model('site_model','',True);
   }

    function index() {

          $user_id = $this->input->get_post('user_id');
          $qr_code = $this->input->get_post('qr_code');

        $qr_code_details = $this->site_model->get_by_number('qr_code_genereation','qr_code',$qr_code);
        $data['user_accounts'] = $this->site_model->get_by_number('user_accounts','id',$user_id);

 if(count($qr_code_details) != 0 && $qr_code_details->status == 'VEHICLE_ASSIGNED'){

        $vehicles_details = $this->site_model->get_by_number('vehicles','qr_code',$qr_code);

$data = array(
                         "type" => "vehicle_details",
                         'vehicle_id' => $vehicles_details->id,
                    );
 $arr = array(
                         'status' => "valid",
                         "message" =>  "QR Code Found",
                          "data" => $data,
                    );

 }elseif(count($qr_code_details) != 0 && $qr_code_details->status == 'FRESH'){

           date_default_timezone_set('Asia/Kolkata');
        $user_data['activated_on'] =  date( 'd-m-Y h:i:s A');
          $user_data['account_status'] = 'ACTIVATED';
          $user_data['qr_code_status'] = '1';

        //   if($data['user_accounts']->myvsafety_id  == ''){
        // $user_data['myvsafety_id'] =  $this->generate_myvsafety_id();
        // }
    $user_table = $this->site_model->update('user_accounts',$user_data,'id',$user_id);

    // date_default_timezone_set('Asia/Kolkata');
    //     $qr_data['assigned_at'] =  date( 'd-m-Y h:i:s A');
          $qr_data['user_id'] = $data['user_accounts']->id;
          $qr_data['status'] = 'VEHICLE_NOT_ASSIGNED';

    $qr_table = $this->site_model->update('qr_code_genereation',$qr_data,'qr_code',$qr_code);
        $data['user_accounts'] = $this->site_model->get_by_number('user_accounts','id',$user_id);

          $vehicle_data['user_id'] = $user_id;
          $vehicle_data['qr_code'] = $qr_code;
          // $vehicle_data['myvsafety_id'] = $data['user_accounts']->myvsafety_id;

          $vehicle_table = $this->cmoon_model->insert('vehicles',$vehicle_data);


        $data = array(
                         "type" => "add_vehicle",
                    );
        
        $arr = array(
                         'status' => "valid",
                         "message" =>  "QR Code Found",
                          "data" => $data,
                    );
          

      }elseif(count($qr_code_details) != 0 && $qr_code_details->status == 'ASSIGNED'){

 $orders_count = $this->site_model->get_by_number2('orders','user_id',$user_id,'qr_code',$qr_code );

 if(count($orders_count) != 0){

           date_default_timezone_set('Asia/Kolkata');
        $user_data['activated_on'] =  date( 'd-m-Y h:i:s A');
          $user_data['account_status'] = 'ACTIVATED';
          $user_data['qr_code_status'] = '1';

        //   if($data['user_accounts']->myvsafety_id  == ''){
        // $user_data['myvsafety_id'] =  $this->generate_myvsafety_id();
        // }

    $user_table = $this->site_model->update('user_accounts',$user_data,'id',$user_id);

    // date_default_timezone_set('Asia/Kolkata');
    //     $qr_data['assigned_at'] =  date( 'd-m-Y h:i:s A');
          $qr_data['status'] = 'VEHICLE_NOT_ASSIGNED';

    $qr_table = $this->site_model->update('qr_code_genereation',$qr_data,'qr_code',$qr_code);
    
        $data['user_accounts'] = $this->site_model->get_by_number('user_accounts','id',$user_id);

     $vehicle_data['user_id'] = $user_id;
          $vehicle_data['qr_code'] = $qr_code;
          // $vehicle_data['myvsafety_id'] = $data['user_accounts']->myvsafety_id;

          $vehicle_table = $this->cmoon_model->insert('vehicles',$vehicle_data);


             $data = array(
                         "type" => "add_vehicle",
                          );


         $arr = array(
                         'status' => "valid",
                         "message" =>  "QR Code Found",
                          "data" => $data,
                    );
          

         }else {

            $arr = [
                'status' => "invalid",
                'message' => "We're sorry, Wrong QR CODE. "
            ];
        }

      }else {

            $arr = [
                'status' => "invalid",
                'message' => "We're sorry, QR Code does not exist. "
            ];

      }

        echo json_encode($arr, JSON_PRETTY_PRINT);

    }


function vehicle_details() {

          $id =$this->input->get_post('id');

     $data=$this->site_model->get_by_number('vehicles','id',$id);
        // $i = 0;
        // foreach ($data as $append_data) {
        //   if ($append_data->vehicle_image != '') {
        //         $append_data->vehicle_image ="http://demoworks.in/php/myvsafety/vehicle_images/".$append_data->vehicle_image;
        //     }
        //     $i++;
        // }
                   if ($data->vehicle_image != '') {
                $data->vehicle_image ="http://demoworks.in/php/myvsafety/vehicle_images/".$data->vehicle_image;
            }
     $user_data=$this->site_model->get_by_number('user_accounts','id',$data->user_id);

           if ($user_data->profile_pic != '') {
                $user_data->profile_pic ="http://demoworks.in/php/myvsafety/profile_images/".$user_data->profile_pic;
            }
        if (count($data) > 0) {

            $data = array(
                         
                          "data_v" => $data,
                          "user_data" => $user_data,
                    );

            $arr = array(
                         'status' => "valid",
                         'message' => "Found",
                          "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "We're sorry,  Please try again. "
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }


private function generate_myvsafety_id()  {

  // $seed = str_split('abcdefghijklmnopqrstuvwxyz'
  //               .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
  //               .'0123456789'); // and any other characters

        $seed = str_split('ABCDEFGHIJKLMNOPQRSTUVWXYZ'.'0123456789'); // and any other characters
        shuffle($seed); // probably optional since array is randomized; this may be redundant
        $otp = '';
        foreach (array_rand($seed, 8) as $k)
        {
           $otp .= $seed[$k];
        }
        return $otp;
    }

    }