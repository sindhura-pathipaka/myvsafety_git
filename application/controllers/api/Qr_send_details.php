<?php
require (APPPATH . '/libraries/REST_Controller.php');
class qr_send_details extends CI_Controller
{
    function __construct(){
      parent:: __construct();
      $this->load->model('login_model','',True);
      $this->load->model('cmoon_model','',True);
      $this->load->model('site_model','',True);
   }
    function index() {

          $qr_data['user_id']  = $this->input->get_post('user_id');
          $qr_data['qr_code']  = $this->input->get_post('qr_code');
          $qr_data['vehicle_num']  = $this->input->get_post('vehicle_num');
          $qr_data['description']  = $this->input->get_post('description');

          $image1 = $this->input->get_post('image1', TRUE);
               if ($_FILES['image1']['name'] != '') {
                    $qr_data['image1'] = $this->upload_file('image1');
                }

         $image2 = $this->input->get_post('image2', TRUE);
               if ($_FILES['image2']['name'] != '') {
                    $qr_data['image2'] = $this->upload_file('image2');
                }

           $image3 = $this->input->get_post('image3', TRUE);
               if ($_FILES['image3']['name'] != '') {
                    $qr_data['image3'] = $this->upload_file('image3');
                }

           date_default_timezone_set('Asia/Kolkata');
        $qr_data['posted_on'] =  date( 'd-m-Y h:i:s A');
        $qr_data['str_posted_on'] =  strtotime(date('d-m-Y h:i:s A', strtotime("+1 day")));

          $vehicle_details = $this->site_model->get_by_number('vehicles','qr_code',$qr_data['qr_code']);

          $qr_data['to_user_id']  = $vehicle_details->user_id;

      if($this->input->get_post('type') == 'parking_problem'){
          $qr_data['type']  ='Parking Problem';
  		}
  	 if($this->input->get_post('type') == 'emergency'){
          $qr_data['type']  ='Emergency';
  		}
  	 if($this->input->get_post('type') == 'missing_info'){
          $qr_data['type']  ='Missing info';
  		}

          $result = $this->cmoon_model->insert('qr_send_details',$qr_data);


   if($this->input->get_post('type') == 'parking_problem'){
          $notification_data['heading']  ='Parking Problem';
        }
     if($this->input->get_post('type') == 'emergency'){
          $notification_data['heading']  ='Emergency';
        }
     if($this->input->get_post('type') == 'missing_info'){
          $notification_data['heading']  ='Missing info';
        }

          $notification_data['description']  = $this->input->get_post('description');
          $notification_data['user_id']  = $vehicle_details->user_id;


          $notifications = $this->cmoon_model->insert('notifications',$notification_data);

          $user_accounts = $this->site_model->get_by_number('user_accounts','id',$vehicle_details->user_id);

            $user_data['notification_count'] = $user_accounts->notification_count + 1;
        $update = $this->cmoon_model->update('user_accounts',$user_data,$user_accounts->id);

              $this->push_notification($notification_data['heading'],$notification_data['description'],$user_accounts->token);


        if ($result) {
            $arr = array(
                         'status' => "valid",
                         'message' => "This vehicle successfully added.",
                          // "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "Failed to add vehicle ,Please try again."
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }





   function push_notification($title, $text, $token) {

   $data = array(
       "to" => $token,
       "priority" => 'high',
       "data" => array(
           "title" => $title,
           "body" => $text,
           
       )
   );

   $data_string = json_encode($data);

   $headers = array(
       'Authorization: key=' . 'AAAAZLscPaE:APA91bG5iuowe78YHLFKLAgi5whHC46GvgXJ1EmbGCEzMmubcI-J1t3rSU1ZyFHUbt66ogxhOH0jYwUBdCFUScdHof3xRLYnGuUCYJ43ycbZFerBw7Cr1tdr3fxv6Vmq0WKr7vr2OJyP',
       'Content-Type: application/json'
   );

   $ch = curl_init();

   curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
   curl_setopt($ch, CURLOPT_POST, true);
   curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
   curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

   $result = curl_exec($ch);

   curl_close($ch);

   $arr = array(
       'status' => "valid",
       'data' => strip_tags($result),
       'response' => $data
   );

   // echo json_encode($arr, JSON_PRETTY_PRINT);
   return $arr;
}



    function fetch_send() {

          $user_id =$this->input->get_post('user_id');
        $present_day = strtotime(date('d-m-Y h:i:s A'));

     $data=$this->site_model->get_by_value2_from_inbox('qr_send_details',$present_day,'user_id',$user_id);
        $i = 0;
        foreach ($data as $append_data) {
          if ($append_data->image1 != '') {
                $append_data->image1 ="http://demoworks.in/php/myvsafety/qr_send_details_images/".$append_data->image1;
            }
             if ($append_data->image2 != '') {
                $append_data->image2 ="http://demoworks.in/php/myvsafety/qr_send_details_images/".$append_data->image2;
            }
             if ($append_data->image3 != '') {
                $append_data->image3 ="http://demoworks.in/php/myvsafety/qr_send_details_images/".$append_data->image3;
            }
            $i++;
        }
        if (count($data) > 0) {


            $arr = array(
                         'status' => "valid",
                         'message' => "Found",
                          "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "We're sorry, There is no items  available in your list.  "
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }


    
    function fetch_received() {

          $user_id =$this->input->get_post('user_id');
        $present_day = strtotime(date('d-m-Y h:i:s A'));

     $data=$this->site_model->get_by_value2_from_inbox('qr_send_details',$present_day,'to_user_id',$user_id);

        $i = 0;
        foreach ($data as $append_data) {
          if ($append_data->image1 != '') {
                $append_data->image1 ="http://demoworks.in/php/myvsafety/qr_send_details_images/".$append_data->image1;
            }
             if ($append_data->image2 != '') {
                $append_data->image2 ="http://demoworks.in/php/myvsafety/qr_send_details_images/".$append_data->image2;
            }
             if ($append_data->image3 != '') {
                $append_data->image3 ="http://demoworks.in/php/myvsafety/qr_send_details_images/".$append_data->image3;
            }
            $i++;
        }
        if (count($data) > 0) {


            $arr = array(
                         'status' => "valid",
                         'message' => "Found",
                          "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "We're sorry, There is no items  available in your list.  "
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }


    


// -------------------------------------- Image upload code -----------------------------------------------------
    private function upload_file($file_name, $path = null)
    {
        $upload_path1             = "qr_send_details_images".$path;
        $config1['upload_path']   = $upload_path1;
        $config1['allowed_types'] = "*";
        $config1['max_size']      = "16000000";
        $img_name1                = strtolower($_FILES[$file_name]['name']);
        $img_name1                = preg_replace('/[^a-zA-Z0-9\.]/', "_", $img_name1);
        $config1['file_name']     =  $img_name1;
        $this->load->library('upload', $config1);
        $this->upload->initialize($config1);
        $this->upload->do_upload($file_name);
        $fileDetailArray1 = $this->upload->data();
        return $fileDetailArray1['file_name'];
    }


    }