<?php
require (APPPATH . '/libraries/REST_Controller.php');
class Contact extends CI_Controller
{
    function __construct(){
      parent:: __construct();
      $this->load->model('login_model','',True);
      $this->load->model('cmoon_model','',True);
      $this->load->model('site_model','',True);
   }
    function index() {
        $data = $this->site_model->get_cms('site_details','1');
        $i = 0;
        foreach ($data as $append_data) {
          // if ($append_data->image != '') {
          //       $append_data->image ="http://demoworks.in/php/e_pasu/cmoon_images/".$append_data->image;
          //   } 
            // $i++;
        }
        if (count($data) > 0) {
            $arr = array(
                         'status' => "valid",
                         'message' => "Found",
                          "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "no records Found"
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }



    function contact_enquiry() {



          $form_data['user_id'] = $this->input->get_post('user_id');
          $form_data['type'] = 'app';
          $form_data['name'] = $this->input->get_post('name');
          $form_data['mobile'] = $this->input->get_post('mobile');
          $form_data['email'] = $this->input->get_post('email');
          $form_data['description'] = $this->input->get_post('description');

        date_default_timezone_set('Asia/Kolkata');
        $form_data['created_at'] =  date( 'd-m-Y h:i:s A');

         $result =    $this->cmoon_model->insert('contact_enquiry',$form_data);
   

        if ($result) {
          
            $arr = array(
                         'status' => "valid",
                         'message' => " Your query has been successfully submitted.  One of our agents will get back to you soon! ",
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "Failed, Please try again."
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }


    }