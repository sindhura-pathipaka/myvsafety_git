<?php
require (APPPATH . '/libraries/REST_Controller.php');
class Emergency_contacts extends CI_Controller
{
    function __construct(){
      parent:: __construct();
      $this->load->model('login_model','',True);
      $this->load->model('cmoon_model','',True);
      $this->load->model('site_model','',True);
   }
    function add() {
      
          $data['user_id']  = $this->input->get_post('user_id');
          $data['name']  = $this->input->get_post('name');
          $data['mobile']  = $this->input->get_post('mobile');

        $result = $this->cmoon_model->insert('emergency_contacts',$data);
        $i = 0;
        foreach ($data as $append_data) {
          // if ($append_data->image != '') {
          //       $append_data->image ="http://demoworks.in/php/leofin/cmoon_images/".$append_data->image;
          //   } 
          //   $i++;
        }
        if ($result) {
            $arr = array(
                         'status' => "valid",
                         'message' => "This emergency contact successfully added.",
                          // "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "Failed to add Emergency contact,Please try again"
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }


    function fetch() {
      
          $user_id =$this->input->get_post('user_id');

     $data=$this->site_model->get_by_value('emergency_contacts','user_id',$user_id);
        $i = 0;
        foreach ($data as $append_data) {
          // if ($append_data->image != '') {
          //       $append_data->image ="http://demoworks.in/php/leofin/cmoon_images/".$append_data->image;
          //   } 
          //   $i++;
        }
        if (count($data) > 0) {


            $arr = array(
                         'status' => "valid",
                         'message' => "Found",
                          "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "We're sorry, There is no emergency contact available in your list. Please add emergency contacts in your list. "
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }


    function edit() {
      
          // $user_id =$this->input->get_post('user_id');
          $id =$this->input->get_post('id');
     $contact['data'] = $this->site_model->get_by_number('emergency_contacts','id',$id);



 if($this->input->get_post('name', TRUE)!=''){
                  $data['name'] = $this->input->get_post('name', TRUE);
                }else{

                  $data['name'] = $contact['data']->name;
                }

        if($this->input->get_post('mobile', TRUE)!=''){
                 $data['mobile'] = $this->input->get_post('mobile', TRUE);
                }else{

                  $data['mobile'] = $contact['data']->mobile;
                }



        $result = $this->site_model->update('emergency_contacts',$data,'id',$id); 

        $i = 0;
        foreach ($data as $append_data) {
          // if ($append_data->image != '') {
          //       $append_data->image ="http://demoworks.in/php/leofin/cmoon_images/".$append_data->image;
          //   } 
          //   $i++;
        }
        if ($result) {
            $arr = array(
                         'status' => "valid",
                         'message' => "This emergency contact successfully edited.",
                          // "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "Failed to edit the emergency contact"
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }


     function delete() {

          // $user_id =$this->input->get_post('user_id');
          $id =$this->input->get_post('id');

        $result = $this->cmoon_model->delete('emergency_contacts', $id);

      

     // $data=$this->site_model->get_by_value('emergency_contacts','user_id',$user_id);
        $i = 0;
        foreach ($data as $append_data) {
          // if ($append_data->image != '') {
          //       $append_data->image ="http://demoworks.in/php/e_pasu/cmoon_images".$append_data->image;
          //   } 
            $i++;
        }
        if ($result) {
            $arr = [
                'status' => "valid",
                'message' => "This emergency contact successfully deleted.",
                // "data" => $data
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "Failed to delete the emergency contact"
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }



    }