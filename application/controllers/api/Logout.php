<?php
require (APPPATH . '/libraries/REST_Controller.php');
class Logout extends CI_Controller
{
    function __construct(){
      parent:: __construct();
      $this->load->model('login_model','',True);
      $this->load->model('cmoon_model','',True);
      $this->load->model('site_model','',True);
   }
    function index() {

          $user_id =$this->input->get_post('user_id');

           $user_data['token'] = '';


        $update = $this->cmoon_model->update('user_accounts',$user_data,$user_id);

        $data = $this->site_model->get_by_number('user_accounts','id',$user_id);

           
        if ($update) {

        
            $arr = array(
                         'status' => "valid",
                         'message' => "Found",
                          "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "no records Found"
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }
    }