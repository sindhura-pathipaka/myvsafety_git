<?php
require (APPPATH . '/libraries/REST_Controller.php');
class vehicle_add extends CI_Controller
{
    function __construct(){
      parent:: __construct();
      $this->load->model('login_model','',True);
      $this->load->model('cmoon_model','',True);
      $this->load->model('site_model','',True);
   }
    function index() {

          $user_id  = $this->input->get_post('user_id');
          $qr_code  = $this->input->get_post('qr_code');
          $data['vehicle_num']  = $this->input->get_post('vehicle_num');

          $vehicle_image = $this->input->get_post('vehicle_image', TRUE);
               if ($_FILES['vehicle_image']['name'] != '') {
                    $data['vehicle_image'] = $this->upload_file('vehicle_image');
                }

        
        $result = $this->site_model->update('vehicles',$data,'qr_code',$qr_code);
        $vehicle_details = $this->site_model->get_by_number('vehicles','qr_code',$qr_code);

           date_default_timezone_set('Asia/Kolkata');
        $qr_data['assigned_at'] =  date( 'd-m-Y h:i:s A');
          $qr_data['vehicle_id'] = $vehicle_details->id;
          $qr_data['status'] = 'VEHICLE_ASSIGNED';

    $qr_table = $this->site_model->update('qr_code_genereation',$qr_data,'qr_code',$qr_code);

     $notification_data['heading']  = 'New vechicle';
     $notification_data['description']  = 'You added vehicle to your list';
          $notification_data['user_id']  = $user_id;

          
          $notifications = $this->cmoon_model->insert('notifications',$notification_data);

          $user_accounts = $this->site_model->get_by_number('user_accounts','id',$user_id);

          $user_data['notification_count'] = $user_accounts->notification_count + 1;
        $update = $this->cmoon_model->update('user_accounts',$user_data,$user_id);

              $this->push_notification('New vechicle','You added vehicle to your list',$user_accounts->token);

        if ($result) {
            $arr = array(
                         'status' => "valid",
                         'message' => "This vehicle successfully added.",
                          // "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "Failed to add vehicle ,Please try again."
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }





   function push_notification($title, $text, $token) {

   $data = array(
       "to" => $token,
       "priority" => 'high',
       "data" => array(
           "title" => $title,
           "body" => $text,
           
       )
   );

   $data_string = json_encode($data);

   $headers = array(
       'Authorization: key=' . 'AAAAZLscPaE:APA91bG5iuowe78YHLFKLAgi5whHC46GvgXJ1EmbGCEzMmubcI-J1t3rSU1ZyFHUbt66ogxhOH0jYwUBdCFUScdHof3xRLYnGuUCYJ43ycbZFerBw7Cr1tdr3fxv6Vmq0WKr7vr2OJyP',
       'Content-Type: application/json'
   );

   $ch = curl_init();

   curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
   curl_setopt($ch, CURLOPT_POST, true);
   curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
   curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

   $result = curl_exec($ch);

   curl_close($ch);

   $arr = array(
       'status' => "valid",
       'data' => strip_tags($result),
       'response' => $data
   );

   // echo json_encode($arr, JSON_PRETTY_PRINT);
   return $arr;
}


    function fetch() {

          $user_id =$this->input->get_post('user_id');

     $data=$this->site_model->get_by_value('vehicles','user_id',$user_id);
        $i = 0;
        foreach ($data as $append_data) {
          if ($append_data->vehicle_image != '') {
                $append_data->vehicle_image ="http://demoworks.in/php/myvsafety/vehicle_images/".$append_data->vehicle_image;
            }
            $i++;
        }
        if (count($data) > 0) {


            $arr = array(
                         'status' => "valid",
                         'message' => "Found",
                          "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "Sorry! There is no vehicle register with us. Please contact to admin or purchase a QR Code kit for adding the vehicles"
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }


    function edit() {

          // $user_id =$this->input->get_post('user_id');
          $id =$this->input->get_post('id');
          $vehicle['data'] = $this->site_model->get_by_number('vehicles','id',$id);



            if($this->input->get_post('vehicle_num', TRUE)!=''){
                  $data['vehicle_num'] = $this->input->get_post('vehicle_num', TRUE);
                }else{

                  $data['vehicle_num'] = $vehicle['data']->vehicle_num;
                }

        $vehicle_image = $this->input->get_post('vehicle_image', TRUE);

                   if ($_FILES['vehicle_image']['name'] != '') {
            unlink("vehicle_service_images/".$vehicle['data']->vehicle_image);
                    $data['vehicle_image'] = $this->upload_file('vehicle_image');
                }else{

                  $data['vehicle_image'] = $vehicle['data']->vehicle_image;

                }

        $result = $this->site_model->update('vehicles',$data,'id',$id);

        $i = 0;
        foreach ($data as $append_data) {
          // if ($append_data->image != '') {
          //       $append_data->image ="http://demoworks.in/php/myvsafety/vehicle_service_images/".$append_data->image;
          //   }
          //   $i++;
        }
        if ($result) {
            $arr = array(
                         'status' => "valid",
                         'message' => "This vehicle successfully edited.",
                          // "data" => $data,
                    );
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "Failed to edit the vehicle"
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }


     function delete() {

          // $user_id =$this->input->get_post('user_id');
          $id =$this->input->get_post('id');

$del_existing_img = $this->cmoon_model->get_row_by_row('vehicles', $id);
            unlink("vehicle_service_images/".$del_existing_img->vehicle_image);

        $result = $this->cmoon_model->delete('vehicles', $id);



     // $data=$this->site_model->get_by_value('vehicles','user_id',$user_id);
        $i = 0;
        foreach ($data as $append_data) {
          // if ($append_data->image != '') {
          //       $append_data->image ="http://demoworks.in/php/myvsafety/vehicle_service_images/".$append_data->image;
          //   }
            $i++;
        }
        if ($result) {
            $arr = [
                'status' => "valid",
                'message' => "This vehicle  successfully deleted.",
                // "data" => $data
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'message' => "Failed to delete the vehicle "
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }


// -------------------------------------- Image upload code -----------------------------------------------------
    private function upload_file($file_name, $path = null)
    {
        $upload_path1             = "vehicle_images".$path;
        $config1['upload_path']   = $upload_path1;
        $config1['allowed_types'] = "*";
        $config1['max_size']      = "16000000";
        $img_name1                = strtolower($_FILES[$file_name]['name']);
        $img_name1                = preg_replace('/[^a-zA-Z0-9\.]/', "_", $img_name1);
        $config1['file_name']     =  $img_name1;
        $this->load->library('upload', $config1);
        $this->upload->initialize($config1);
        $this->upload->do_upload($file_name);
        $fileDetailArray1 = $this->upload->data();
        return $fileDetailArray1['file_name'];
    }


    }