<?php  class Cmoon_model extends CI_Model
{
  function get($table)
  {
      $this->db->order_by('id','desc');
    $query=$this->db->get($table);
    return $query->result();
  }
  function get_row_by_result($table,$id)
  {
    $this->db->where('id',$id);
    $query=$this->db->get($table);
    return $query->result();
  }



    function get_row_by_row($table,$id)
  {
    $this->db->where('id',$id);
    $query=$this->db->get($table);
    return $query->row();
  }
  function insert($table,$form_data)
  {
    $query=$this->db->insert($table,$form_data);
      // echo $this->db->last_query();  die();
    return true;
  }
    function update($table,$form_data,$id)
  {
    $this->db->update($table, $form_data, array('id' => $id));
      // echo $this->db->last_query();  die();
    return true;
  }
 function delete($table,$id)
  {
  $result=$this->db->delete($table, array('id' => $id));
     return true;
  }
 function get_by_coloum_id($table,$coloum_value,$coloum_name)
  {
    $this->db->where($coloum_name,$coloum_value);
    $query=$this->db->get($table);
      // echo $this->db->last_query();  die();
     return $query->result();
  }

   function suggestions($table,$coloum_name,$coloum_value){

     $data = [];
    $this->db->where($coloum_name,$coloum_value);
     $query = $this->db->select('qr_code')->get($table);
     return $query->result();

    }

 public function get_search_data($table, $column1='',$column2='',$column3='')
    {

    if($column1 != ""){

        $this->db->where('qr_code', $column1);
    }
    if($column2 != ""){

        $this->db->where('strtotime_value', $column2);
    }
    if($column3 != ""){

        $this->db->where('status', $column3);
    }
    // if($column4 != ""){

    //     $this->db->where('mobile', $column4);
    // }

        $query = $this->db->get($table);

     // echo $this->db->last_query(); die;

        return $query->result();

    }

// ------------------------------------------------------------ change password code --------------------------------------------------
    function get_psw($table,$id,$data)
  {
    $this->db->where('id',$id);
    $this->db->where('password',$data);
    $query=$this->db->get($table);
    return $query->num_rows();
  }
    function update_psw($table,$id,$data)
  {
    $this->db->set('password', $data);
    $this->db->update($table, array('id' => $id));
     return true;
  }

  public function data_export_to_excel($table)

    {

        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\r\n";
date_default_timezone_set('Asia/Kolkata');
        $filename = "$table-".date('d-M-Y-h-m-A').".csv";

          $this->db->select(

            'problem AS `Problem`,
             name AS `Name`,
             aadhar_num AS `Aadhar Number`,
             district AS `District`,
             mandal AS `Mandal`,
             village AS `Village`,
             mobile AS `Mobile`,
             message AS `Message`', FALSE);

        $result=$this->db->get($table);
        $data = $this->dbutil->csv_from_result($result, $delimiter, $newline);
        force_download($filename, $data);

    }

        public function get_row_by_id($table,$colmn_id,$colmn_value,$limit= '')

    {
        $this->db->where($colmn_id, $colmn_value);
        // $this->db->order_by($order_by_value);
              if($limit != ""){
        $this->db->limit($limit);
    }
        $query = $this->db->get($table);

        // echo $this->db->last_query(); die;

        return $query->row();
    }

         public function get_by_where($table, $column1,$column1_value)
    {

        $this->db->where($column1, $column1_value);
        // $this->db->where($column2, $column2_value);

        $query = $this->db->get($table);
     // echo $this->db->last_query(); die;

        return $query->result();

    }

  }