-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 11, 2021 at 01:26 PM
-- Server version: 10.3.30-MariaDB-log-cll-lve
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demowork_myvsafety`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `mobile` text NOT NULL,
  `house_no` text NOT NULL,
  `area` text NOT NULL,
  `city` varchar(255) NOT NULL,
  `district` varchar(255) NOT NULL,
  `state` text NOT NULL,
  `pin_code` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `user_id`, `name`, `mobile`, `house_no`, `area`, `city`, `district`, `state`, `pin_code`) VALUES
(1, 1, 'test', '1212121212', '123', 'wgl', '', '', 'tg', '506005'),
(6, 1, 'test', '', '', '', '', '', '', ''),
(26, 4, 'Pushpendra Kumar', '+919719325299', '401-B Happy Home 3rd', 'Akkayyapalem', '', '', 'AP', '530016'),
(27, 4, 'PK', '+919719325299', '401-B Happy Home 3rd', 'Akkayapalem', '', '', 'AP', '530016'),
(28, 4, 'Pushpendra', '+919719325299', '401-b Happy Home 3rd', 'Akkayyapalem', '', '', 'AP', '530016'),
(29, 4, 'KK', '+911234567890', '401-B BHEL', 'AKC', '', '', 'AP', '530016'),
(30, 4, 'KMK', '+919719325299', 'Hcbud 3899 Dwjh Cbn', 'Akkayyapalem', '', '', 'U.P.', '530016'),
(31, 4, 'Bakkimn Chand Chatarzi HCL', '+9119325299', '6763/23-34 HBCS', 'Akkaypalem', '', '', 'AP', '530016'),
(32, 4, 'ABC Bhardwarz Chatrazi Aabul Pakir Abdul Kalam', '+917123456789', '1232/212-BHU Bharat ', 'Dwarka Nagar', '', '', 'AP', '530016'),
(33, 3, 'Bdbd', '59595', 'Brbf', 'Fbrbr', '', '', 'Brbr', '92292'),
(34, 8, 'Manohar', '1234567890', '8-10', 'Pendurthi', '', '', 'Visakhapatnam', '531173'),
(35, 8, 'Something', '1234537890', '113', 'Nunc Aaj', '', '', 'Andhra', '123456'),
(36, 14, 'Kosuri Srinivasa Rao', '9959646419', '2-136/A', 'Palacharla, Rajanagaram Mandal, East Godavari District.', '', '', 'Andhra Pradesh', '533102'),
(37, 20, 'Manu', '9866526642', '174', 'Prasanthi Nagar', 'Pendurthi', 'Visakhapatnam', 'Andhra Pradesh', '531173'),
(38, 21, 'Manohar', '9492824933', '8-10', 'Prasanthi Nagar', 'Pendurthi', 'Visakhapatnam', 'Andhrapradesh', '531173'),
(39, 8, 'Manu', '12345687890', 'Jgb', 'Mm Cizn', 'Kdhbz', 'Kyu UFC', 'Abstracts Check', '1234567');

-- --------------------------------------------------------

--
-- Table structure for table `admin_ips`
--

CREATE TABLE `admin_ips` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_ips`
--

INSERT INTO `admin_ips` (`id`, `ip_address`) VALUES
(1, '::1'),
(2, '106.208.108.29'),
(3, '52.114.7.6'),
(4, '49.204.231.105'),
(5, '106.208.108.29'),
(6, '52.114.7.6'),
(7, '49.204.226.89'),
(8, '223.228.64.101'),
(9, '106.208.6.118'),
(10, '52.114.7.6'),
(11, '49.204.230.139'),
(12, '106.208.116.184'),
(13, '49.204.226.15'),
(14, '106.208.61.86'),
(15, '49.204.227.252'),
(16, '106.208.6.39'),
(17, '223.228.64.113'),
(18, '106.208.94.25'),
(19, '223.237.46.154'),
(20, '106.208.60.203'),
(21, '106.208.22.128'),
(22, '223.228.71.129'),
(23, '106.208.111.140'),
(24, '223.237.8.182'),
(25, '106.208.110.139'),
(26, '223.228.114.92'),
(27, '223.228.65.107'),
(28, '223.228.69.36'),
(29, '223.228.69.36'),
(30, '52.114.7.6'),
(31, '223.228.69.173'),
(32, '49.37.147.249'),
(33, '223.238.25.58'),
(34, '106.208.28.228'),
(35, '106.208.20.244'),
(36, '49.37.148.185'),
(37, '49.37.148.185'),
(38, '49.37.145.174'),
(39, '223.182.62.176'),
(40, '223.182.62.176'),
(41, '223.228.71.64'),
(42, '223.228.71.64'),
(43, '49.37.150.28'),
(44, '106.208.44.136'),
(45, '106.208.44.136'),
(46, '223.228.121.147'),
(47, '223.228.121.147'),
(48, '49.37.148.25'),
(49, '106.208.84.113'),
(50, '49.37.148.25'),
(51, '49.37.151.193'),
(52, '49.37.151.193'),
(53, '49.204.188.39'),
(54, '49.204.188.39'),
(55, '49.37.145.188'),
(56, '49.37.145.188'),
(57, '223.237.12.61'),
(58, '66.249.83.86'),
(59, '49.37.150.202'),
(60, '202.53.69.163'),
(61, '175.101.128.29'),
(62, '175.101.128.29');

-- --------------------------------------------------------

--
-- Table structure for table `bank_details`
--

CREATE TABLE `bank_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `account_number` varchar(255) NOT NULL,
  `bank_holder_name` varchar(255) NOT NULL,
  `bank_ifsc_code` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank_details`
--

INSERT INTO `bank_details` (`id`, `user_id`, `account_number`, `bank_holder_name`, `bank_ifsc_code`, `bank_name`) VALUES
(4, 4, '97193252990007', 'Pushpendra Kumar', 'SBI00011256', 'State Bank Of India'),
(5, 4, '123456789', 'ABC', 'ABSCD00067', 'A M D HYD'),
(6, 8, '1234567890', 'Makar', 'ABC00001', 'Abc'),
(7, 14, '349702010021239', 'K Srinivasa Rao ', 'UBIN0534978', 'Union Bank Of India');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `image` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `image`) VALUES
(1, 'screenshot01.png'),
(2, '20210802164708524_20210511073305129_web_pumped_up_profile_780x468.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `banner_images`
--

CREATE TABLE `banner_images` (
  `id` int(11) NOT NULL,
  `image` text DEFAULT NULL,
  `link` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `banner_images`
--

INSERT INTO `banner_images` (`id`, `image`, `link`) VALUES
(1, '20210511073339501_facebook_cover_page_header2.jpg', 'https://www.youtube.com/'),
(2, '20210511073305129_web_pumped_up_profile_780x468.jpg', 'https://www.google.com/');

-- --------------------------------------------------------

--
-- Table structure for table `bug_issues`
--

CREATE TABLE `bug_issues` (
  `id` int(11) NOT NULL,
  `heading` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bug_issues`
--

INSERT INTO `bug_issues` (`id`, `heading`) VALUES
(1, 'Errors'),
(2, 'Error in App');

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages`
--

CREATE TABLE `cms_pages` (
  `id` int(11) NOT NULL,
  `heading` varchar(250) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `p_link` varchar(250) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `redirect` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_pages`
--

INSERT INTO `cms_pages` (`id`, `heading`, `image`, `description`, `p_link`, `redirect`) VALUES
(1, 'About Us', NULL, '<p>We are MY V SAFETY, a small but motivated company specializing in QR Code kits and communication mediums . We believe passionately in great service, which is why we commit ourselves to giving you the best of us.</p>\r\n\r\n<p>If you&rsquo;re looking for a medium to contact vehicle owners, children parents, missing person&#39;s relatives to share information about them and missing important thing&#39;s owners &nbsp;, if yes, you&rsquo;re in the right place. We strive to be industrious and innovative, offering our customers something they want, putting their desires at the top of our priority list.</p>\r\n', 'About-Us', 'cms_pages_view'),
(2, 'TERMS AND CONDITIONS', NULL, '<p>1. GENERAL CONDITIONS<br />\r\n&nbsp;<br />\r\n1.A Scope and Definitions<br />\r\nThis website and application are owned and operated by MY V SAFETY. Information supplied via this website is obtained based on user inputs. Use of this website by a user, hereafter referred to as &ldquo;You&ldquo;, (and &rdquo;Your&rdquo; shall be construed accordingly) is subject to the terms and conditions set out below (&#39;Website Terms&#39;). MY V SAFETY reserves the right to modify the Conditions at any time. In addition, MY V SAFETY may vary or amend the Services provided through the Site, the Site functionality and/ or the &ldquo;look and feel&rdquo; of the Site at any time without notice and without liability to Members.&nbsp;<br />\r\n1.B Acceptance of Conditions<br />\r\nEvery time You use this website You must re-read and accept these Website Terms and by performing a data check using this website, You agree to be legally bound by them. If You do not agree to be bound by these Website Terms, You should not carry out a vehicle, people and things check on this website and should not attempt to contact via notifications.<br />\r\nAll Members agree to comply with the Conditions and accept that their personal data may be processed in accordance with the Privacy Policy.<br />\r\nIn using this App you agree not to gather, extract, reproduce and/or display any vehicle, people and things data on or from this App by the use of spiders or other &#39;screen scraping&#39; software or system used to extract data automatically unless otherwise agreed in writing.<br />\r\nAll personal data about you will be treated in accordance with our Privacy Policy.<br />\r\nIn the event that any Member fails to comply with any of the Conditions, MY V SAFETY reserves the right, but not the obligation at its own discretion, to withdraw the User Account in question and suspend or withdraw all Services to that Member without notice. These Conditions are intended to create binding rights and obligations between Members and MY V SAFETY in accordance with the Indian Contract Act, 1872<br />\r\n&nbsp;1.C Variation of the Conditions, Site, Application and Service<br />\r\nMY V SAFETY reserves the right to modify the Conditions at any time. In addition, MY V SAFETY may vary or amend the Services provided through the App, the App functionality and/ or the &ldquo;look and feel&rdquo; of the App at any time without notice and without liability to Users.<br />\r\nAny modification to the App, Services or Conditions will take effect as soon as such changes are published on the App, subject to communication of any material change to the Conditions to the Users in an e-mail.<br />\r\nUsers will be deemed to have accepted any varied Conditions in the event that they use any Services offered through the App following publication of the varied Conditions.<br />\r\n2. USE OF THE SERVICE<br />\r\n2.A User Account and Accuracy of Information<br />\r\nIn order to use the Services each User must create a User Account and agrees to provide any personal information requested by MY V SAFETY. In particular, Users will be required to provide their first name, last name, email and valid mobile.<br />\r\nMembers agree and accept that all of the information they provide to MY V SAFETY when setting up their User Account and at any other time shall be true, correct, complete and accurate in all respects. Members also agree that any information supplied to MY V SAFETY or posted on the Site will be true, accurate and complete.<br />\r\nUsers agree and accept that all of the information they provide to MY V SAFETY when setting up their User Account and at any other time shall be true, correct, complete and accurate in all respects. Users also agree that any information supplied to MY V SAFETY will be true, accurate and complete.<br />\r\nUnless expressly agreed by MY V SAFETY, Users are limited to one User Account per User. No User Account may be created on behalf of or in order to impersonate another person or with a wrong vehicle registration which does not belong to you.<br />\r\n2.B No Commercial Activity and Status of MY V SAFETY<br />\r\nThe App and the Services are strictly limited to providing a Service for car, motor bike, owners/drivers and public people in a private capacity. The Services may not be used in any commercial or professional context. The Services may be used only for the purpose of registering your OWN vehicle details and to upload documents related to the vehicles along with your valid driving licence, warranty bills of the goods, vehicle service maintenance details and to set periodic reminders. And also only to search and contact vehicle owners/contacts in the case of any valid reasons/emergencies/missing information.<br />\r\n2.C Contacting Users<br />\r\nBy accepting the terms and conditions contained herein, every User hereby agrees and gives consent to MY V SAFETY to communicate via phone calls, SMS, email, notifications and such other means as MY V SAFETY may deem fit. Such communications to Users may be recorded through technical support provided by third parties for the purpose of training, quality and for regularly updating the Users about the services of MY V SAFETY.<br />\r\n3. INDEMNITY AND RELEASE<br />\r\nUsers will indemnify and hold harmless MY V SAFETY, its subsidiaries, affiliates and their respective officers, directors, agents and employees, from any claim or demand, or actions including reasonable attorney&#39;s fees, made by any third party or penalty imposed due to or arising out of your breach of these Conditions or any document incorporated by reference, or your violation of any law, rules, regulations or the rights of a third party.<br />\r\nUsers release MY V SAFETY and/or its affiliates and/or any of its officers and representatives from any cost, damage, liability or other consequence of any of the actions/inactions of the Users and specifically waiver any claims or demands that they may have in this behalf under any statute, contract or otherwise.<br />\r\n4. GENERAL TERMS<br />\r\n4.A Relationship<br />\r\nNo arrangement between the Users and MY V SAFETY shall constitute or be deemed to constitute an agency, partnership, joint venture or the like between the Users and MY V SAFETY.<br />\r\n4.B Suspension or Withdrawal of Site Access<br />\r\nIn the event of non-compliance on your part with all or some of the Conditions, you acknowledge and accept that MY V SAFETY can at any time, without prior notification, interrupt or suspend, temporarily or permanently, all or part of the service or your access to the App (including in particular your User Account).<br />\r\n4.C Intellectual Property<br />\r\nThe format and content included on the App, such as text, graphics, logos, button icons, images, audio clips, digital downloads, data compilations, and software, is the property of MY V SAFETY, its affiliates or its content suppliers and is protected by Indian copyright, authors&#39; rights and database right laws.<br />\r\nAll rights are reserved in relation to any registered and unregistered trademarks (whether owned or licensed to MY V SAFETY) which appear on the App.<br />\r\nThe App or any portion of the App may not be reproduced, duplicated, copied, sold, resold, visited, or otherwise exploited for any commercial purpose without the express written consent of MY V SAFETY. No person is entitled to systematically extract and/or re-utilise parts of the contents of the App without the express written consent of MY V SAFETY. In particular, the use of data mining, robots, or similar data gathering and extraction tools to extract (whether once or many times) for re-utilisation of any substantial parts of this App is strictly prohibited.<br />\r\n4.D Content of the Site Provided by the Members<br />\r\nBy displaying content on this Site, Members expressly grant a license to MY V SAFETY to display the content and to use it for any of our other business purposes.<br />\r\nBy displaying content on this App, Users expressly grant a license to MY V SAFETY to display the content and to use it for any of our other business purposes. Users of this App are expressly asked not to publish any defamatory, misleading or offensive content or any content which infringes any other persons intellectual property rights (e.g. copyright). Any such content which is contrary to MY V SAFETY&rsquo;s policy and MY V SAFETY does not accept liability in respect of such content, and the User responsible will be personally liable for any damages or other liability arising and agrees to indemnify MY V SAFETY in relation to any liability it may suffer as a result of any such content. However as soon as MY V SAFETY becomes aware of infringing content, MY V SAFETY shall do everything it can to remove such content from the App as soon as possible.<br />\r\n4.E Partner Sites<br />\r\nMY V SAFETY reserves the right to reproduce any information that appears on the App or on the partner Apps.<br />\r\nIn particular, ads published on one of the Apps maintained or co-maintained by MY V SAFETY may be reproduced on other Apps maintained or co-maintained by MY V SAFETY or third parties.<br />\r\n5. MY V SAFETY Usage TERMS<br />\r\n5.A Unlawful Use<br />\r\nCustomer agrees not to use the Services for any unlawful or abusive purpose or in any way that interferes with MY V SAFETY. Customer will comply with all laws while using the Services and will not transmit any communication that would violate any central, state, or local law, court, or regulation. Resale of the Services is prohibited except by authorized Dealers. By using the Services and/or the Devices, Customer agrees to abide by the terms and conditions of any software license agreements applicable to any software associated with the Services or Devices.&nbsp;<br />\r\n5.B Unauthorized Usage<br />\r\nIf any Services used fraudulently, Customer must notify MY V SAFETY immediately and provide MY V SAFETY with such information and documentation as MY V SAFETY may request (including, without limitation, police reports, and affidavits). MY V SAFETY has the right to interrupt Services or restrict services, without notice to the Customer, if Customer is using the service in a fraudulent or unlawful manner.<br />\r\n5.C Account Information<br />\r\nIt is Customers responsibility to maintain current and accurate account information on the MY V SAFETY system and to exercise diligence in protecting Customers logon and passwords.<br />\r\n5.D Transferability of Service<br />\r\nIf customer wanted to change his mobile number instead of registered he does not have that right but he can request customer service of MY V SAFETY to change with correct details and identity proofs.<br />\r\n5.F Changes<br />\r\nMY V SAFETY may amend the terms of this Agreement via electronic notification mode to Customer. If Customer does not agree to the amendment, Customer may terminate this Agreement by providing written notice to MY V SAFETY within fifteen (15) days of the date the notification was triggered by MY V SAFETY via the MY V SAFETY mobile app. If Customer does not agree with the amendment but wants to continue Services, MY V SAFETY will continue to provide Services for the term of the original Agreement provided Customer mails written notice to MY V SAFETY within fifteen (15) days of the date the amendment was notified by MY V SAFETY. If Customer continues to use the Services more than fifteen (15) days after MY V SAFETY notify the notice of an amendment, Customer will be deemed to have agreed to that amendment.</p>\r\n\r\n<p><br />\r\n5.7 Limitation of Liability<br />\r\nMY V SAFETY is not responsible for acts or omissions of any other service provider, for information provided through the equipment, for equipment failure or modification, for system failure or modification or for causes beyond the control of MY V SAFETY. MY V SAFETY is not liable for (i) service outages; (ii) incidental or consequential damages such as lost profits; (iii) economic loss or injuries to persons or property arising from the Customer&rsquo;s use of the Services or (v) for any act associated with the proper exercise by MY V SAFETY of rights under the privacy and/or unauthorized usage provisions of this Agreement.<br />\r\n5.8 Indemnification<br />\r\nCustomer agrees to defend, indemnify, and hold MY V SAFETY and its affiliates harmless from claims or damages relating to (i) Customers breach of this Agreement or the Customers statements made in this Agreement and (ii) the use of the Services unless due to sole and/or gross negligence by MY V SAFETY or its affiliates. Customer agrees to pay reasonable attorney&rsquo;s fees and all applicable costs incurred by MY V SAFETY in enforcing this Agreement. This paragraph shall survive the termination of this Agreement.<br />\r\n5.9 Limitation of Action<br />\r\nExcept for actions arising in connection with Indemnification (above), neither MY V SAFETY nor Customer may bring legal action with respect to this Agreement more than one year after the legal action accrues.<br />\r\n5.10 Warranties<br />\r\nMY V SAFETY makes no express warranties regarding the Services and disclaims any and all implied warranties, including, without limitation, any warranties of merchantability or fitness for a particular purpose. MY V SAFETY does not authorize anyone to make any warranties on its behalf and Customer should not rely on any such statement.<br />\r\n5.11 Assignment<br />\r\nMY V SAFETY may assign all or part of the rights or duties of MY V SAFETY under this Agreement without such assignment being considered a change to the Agreement and may provide notice to Customer. As a result of any such assignment, MY V SAFETY shall be released from all liability with respect to such rights or duties, or portions thereof. Customer may not assign this Agreement without prior written consent of MY V SAFETY, which shall not be reasonably withheld.<br />\r\n5.12 Entire Agreement<br />\r\nThis is the entire Agreement between MY V SAFETY and Customer and super cedes any oral or written promises made to the Customer. This Agreement may only be amended as described herein. If the terms of this Agreement conflict with or are inconsistent with the terms of any purchase order or document provided by the Customer, the terms of this Agreement shall control. If any part of this Agreement is found unenforceable or invalid, the balance of this Agreement shall remain intact.<br />\r\n6. LAW AND JURISDICTION<br />\r\nThese terms shall be governed by the law of India and any disputes arising in relation to these terms shall be subject to the jurisdiction of the Courts of Rajahmundry(Rajamahedravaram).<br />\r\n&nbsp;<br />\r\nCOPYRIGHT<br />\r\n________________________________________<br />\r\nAll editorial content, graphics, design, and logo works on this site are protected by copyright laws and other laws of international treaties. All these contents belong to MY V SAFETY and / or its suppliers/partners. The content, Data, logos, graphics, and images may not be copied, reproduced or imitated whether in whole or in part, unless expressly permitted by MY V SAFETY.<br />\r\nFor further clarification email to: myvsafety@gmail.com<br />\r\n&nbsp;<br />\r\nDISCLAIMER<br />\r\n________________________________________<br />\r\n1. Users may access the Services on the App at their own risk and using their best and prudent. MY V SAFETY will neither be liable nor responsible for any actions or inactions of Users nor any breach of conditions, representations or warranties by the Users. MY V SAFETY hereby expressly disclaims and any and all responsibility and liability in arising out of the use of the App.<br />\r\n2. MY V SAFETY expressly disclaims any warranties or representations (express or implied) in respect of accuracy, reliability and completeness of information provided by Users, or the content (including details of the vehicle ownership) on the App and notifications sent and received by Users. While MY V SAFETY will take precautions to avoid inaccuracies in content of the App, all content and information, are provided on an as is where is basis, without warranty of any kind. MY V SAFETY does not implicitly or explicitly support or endorse any of the Users availing Services from the App.<br />\r\n3. MY V SAFETY is not a party to any agreement with a Car Owner and User and will not be liable to any User unless the loss or damage incurred arises due to MY V SAFETY&rsquo;s negligence.<br />\r\n4. MY V SAFETY shall not be liable for any loss or damage arising as a result of:<br />\r\n4.1. &nbsp; &nbsp;A false, misleading, inaccurate or incomplete information being provided by a User<br />\r\n4.2. &nbsp; &nbsp;The claim of ownership of a vehicle<br />\r\n4.3. &nbsp; &nbsp;Notification sent across by user<br />\r\n4.4. &nbsp; &nbsp;Accident/Emergency/Missing information report by any user<br />\r\n4.5. &nbsp; &nbsp;Wrong Reminder service alert<br />\r\n5. MY V SAFETY do not offer any functionality beyond what is listed in the company website and the company will not be liable to any User for any claims arising out of this scope.<br />\r\n6. MY V SAFETY will not be liable to any User for any business, financial or economic loss or for any consequential or indirect loss arising as a result of the services provided by MY V SAFETY (whether suffered or incurred as a result of the MY V SAFETY&rsquo;s negligence or otherwise).<br />\r\n7. MY V SAFETY&rsquo;s service is limited to putting Public and Vehicle Owners in touch with each other via notifications and cannot oversee any such, Users accept that the limitations on the MY V SAFETY&rsquo;s liability set out above are reasonable.<br />\r\n8. The warranty of the company&#39;s products will not apply to any Product that has been operated improperly, neglected, misused, altered, abused, willfully damaged, or repaired without Party-A&#39;s approval.<br />\r\n9. In no event shall the company be responsible to any person or entity for special, incidental or consequential damages (including, but not limited to, loss of profits, loss of data, loss of property or loss of use damages) arising out of the manufacture, sale/ supplying or use of the company&rsquo;s products, also including damages arising out of or related to damage or injury to property or persons.<br />\r\n&nbsp;</p>\r\n\r\n<p style=\"margin-top:40px; margin-bottom:10px\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Raleway&quot;,&quot;serif&quot;\"><span style=\"color:#333333\"><img alt=\"my-v-safety-logo-final.png\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAApUAAADDCAYAAADTGepkAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAAlwSFlzAAAh1QAAIdUBBJy0nQAASSpJREFUeF7tnQl8XFXZ/6dsWcq+WNLM3DtJw/IiKlLldX2rQpuZtCy+NrmTUgTFfxG0KpRMUlCDrxsu8IoiWlnaTFKEurCoyIsLoogIuICK7IpYti602Vq6ZP6/505Skpk7M+ece2cyKb98PvNROuee5XvPPfO7zznP84RC/CMBEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiABEiCB3YBAzTUnzaxOxTqrU/Ef16TiP8f/v626N35VTSp27j69zUeF0qFpu8EwOQQSIAESIAESIAESIIGSEFgxe28IySQE5Kaa3nja85OK7cC/X1qS9lkpCZAACZAACZAACZDA1Caw7+oFh8IqeTs+I3kF5ajQrO6NPTy1R8vekwAJkAAJkAAJkAAJBE6gKjWvCVvcfygmJt3vYamsTcXfH3gnWCEJkAAJkAAJkAAJkMDUJbBPT/xIsTwqCUrXUhm7IdQ9Z6+pO2L2nARIgARIgARIgARIIFAC+1w3/whsd/9dXVDGH6/tba4LtBOsjARIgARIgARIgARIYOoSmH79yTPglKMjKLdUpeJzp+6I2XMSIAESIAESIAESIIFACRywev5BEJQPalgo0yi/PNBOsDISIAESIAESIAESIIEpTGBN6744F/kLLUHZG/9paOWc6ik8anadBEiABEiABEiABEggMAKXvaUG3tur9QRl7LnqngVWYH1gRSRAAiRAAiRAAiRAAlOYwJrWPWt6W76kEofyFdEZ2war5sIpPGp2nQRIgARIgARIgARIIEgCtanYEohFiMQ8mXI8/z12XSidZkrGIG8E6yIBEiABEiABEiCBqUqgqrf5Xdj2HtASlH3xR/a/et7BU3XM7DcJkAAJkAAJkAAJkECABKpWNkeRLWetlqDsjW+p7Y3NC7AbrIoESIAESIAESIAESGDKEnAdc+J3aQpKCR/09VB39x5TdtzsOAmQAAmQAAmQAAmQQEAEukN7QEx+RVdQwjHn4X1XLDg0oF6wGhIgARIgARIgARIggalMAGcoT4ZA1HPMScV21va2zJ/K42bfSYAESIAESIAESIAEAiIAMRnGtve/tK2UqebeUJrb3gHdBlZDAiRAAiRAAiRAAlOYwIrZe8Mx52ZdQVndG99Q29tcN4VHzq6TAAmQAAmQAAmQAAkERaC2L3Y2tr53aonKVHwE5T8eVB9YDwmQAAmQAAmQAAmQwBQmIOkUDcIHpat7Y38JrVhQO4WHvtt3XazINan5rTjWcFpVal5T6IpY1W4/aA6QBEiABEiABEhgEgikQ9NgbezTslBKFp1UfDs+75uEHrNJVQKSYjMVGx8aahgvAg/V9MU/U93XYqtWw3IkQAIkQAIkQAIkUJRAVW/sRFcgaqVhdEXlr0M4h1m0ARaYTALTcJ/u8bq3iCm6AY5Zn6CD1WTeHrZNAiRAAiRAArsLgTWt+8DR5s8GgnJ7VU/zSbsLht15HLBMXlng/o7U9MQ/G4K1endmwLGRAAmQAAmQAAmUmEDNqpZztAUlLJoQKreFuufsVeLusfoACFT3NJ9R8B6nYjumr2p5dwBNsQoSIAESIAESIIFXI4H9ek48BOft/mkgKndW9VGETJU5s09P/Ejc5x2FhSWOMtxGB56pck/ZTxIgARIgARKoKALVPfFlBoISZyljd4WwbV5Rg2Fn8hKYkZo7Hffs0cL3GhmRVsdaiJEESIAESIAESIAEtAjsu3rBoXDg+IeJqKztiy+QxtLddbVDSWvhcDLSNtxltZbyMyBtdDZaWoOsgMLp7sP23dRlzX6pwzq+lJ9B1L+5o/6IfEOGQ84Nxe41ztb+BC8Le1YANnaBBEiABEiABEhgqhCo7m2+oJjI8P4+9lDozsxZyoHlDTMgKv882GkPDyRL/Mm08dl0a2hKiR70OQE+f8Pnr6X+DHRGLikgKpcWvd+p2PDeffHXTZU5zH6SAAmQAAmQAAlMNoHe2P6wUj5WVGRkhxhC9pyqVPwDY91Ph0LTNi+r/8/BpL0egild6s9Ap/XcYGf0DZONT7X9gWWNrxlIWn8sNRepH+08vXW5dUxeUXlD7M1q9zv2BdXxsRwJkAAJkAAJkMCrnEB1T8sZOGMn6RXTOh94fP87O3tOek1oz4EO6wuwyO0stXga6LRHBjut69MrQhUfG/PO7tBeEMGfQ593lJxL0t6Bdpalu0N75J3aOFeJuJSbi9/v2KOhGxfWvMofEQ6fBEiABEiABEigKIHu7j3yBcMuJjhqe2Kf9qp/8BNWHcTeI6UWT5n6rf7NXXbFO5SIRRVCb205mAwl7YcGL7QPL3jvJbNOb+y3xe6xBMGv6p3/rqLziAVIgARIgARIgARe3QQgLN4M4bCtqLjI3frefMDq+Y1e9GQbfCgZPQeCb1s5RBSsoneLA0yl3sn0ktl7Yzt6ZcayWtpjAbCGbh3osk6Xe1CMB3K7f0vlvlf3tlxZrC5+TwIkQAIkQAIk8ConAA/fK1SERXYZXPeDQujWdzftj7OVv/YUUUl7I8TPTwa77JshtG6R/83+QCjeDCH2w/EflPk+yj+TU2fS3r45GT2zUm8lPNXnos/9pRaUo2cpbx5Z2lSlwkI10D22yZ9lzEoVoixDAiRAAiRAAq9WApKSMRV/UVtUwkGntrdlfjFsWzqt98BauSlXTFnPY4v2LcWu9/q+v8M+TbzLs+uE+Hy06JavSYM+rxmBuEbf7imPoLQ3buoKv1m1y9XfbXkbrNTFz9IiUHpVb/O7VOtlORIgARIgARIggVcZgdqe+AJtQSnb4KnYk6FrTtmvGK4HloT2HkxaV3lt+8IS+ePnls2YXqyO7O9lKxl1rs4VlXAMSlqXp+eEKiZVpGxBQ1B+tCzb3kl7BG19UyfE0gErTz0Q93+7yhyo7oldoXuv/Ja3bScaibSfHLETyyOWcy3+92eW7TyIz9qI7Tw7/oN/ewZlHnTLWImVluV8Kmy3nxaNnn5UKNSd32HJbyd5PQmQAAmQAAmQQGgarFSrVASFx9b35ar8sPX7Wglv4yECh4aT0TbVesaXQ11vzIQTmng+Ee28uKkj8iaTOktxzfDFsyKuBbXE5yjdbW84Rm25sGmW7jgwB55WnAOPh+DUpVu/TvnZs5fsbVltb4/YbV+GSPyrZSeG8BnBJ236gdDcAvH5JD7fEYHa1HT6/jp9msyyM2eedgg43B+xEg/rfMDqL7bdfvRk9h28z9Lp82SXxUvI9aGQWqD/SKTtnMnur5/2ww2L8saeDYcXHizPnp/6K+lay1p0UDi8uAn3V/s5csdhO3+LRhP/We5nCS/UcWOOduKaUGhJaSKiYEH9IT73BPHJPHSl+YtEnVOD6OOuOiLOPK+eYgytpu3A+vGDQw75YFHrXMCEpkUiTpdpnyPRRP5zhmta95WzcoqCYnyooZ3VK+NvVx2nhBjCFvgnvULpwFr5QP8FdYeq1jVW7s7uOXthG/wr2WGLxCIoDjHp7mMmPWXkAxnnnEtxrrT0oZWS1svg8WEV55xs1nDWuU1xDoxMv775tbr3SqX8Mce07isiBGLoPsz1baYCUuG6ESzU/0Bbl8yc5URU+jaZZWw78UGFMXkLbitx6WT2HYwvMO67j5cI0zZljQ2FMkkciv1hbBebtlMJ14Ub2k/IN8bGxve+Bn3cUQn9DKIPdXXt8vsyDTsYXzGvz7kLYrtsYdWOOuqD+2GO/cGkv/ISbdttRkfLis1793s08E+Tjnldg8X45RmNi1+j1LBmIQi264Lqp1uP1Xa6VxfESoG2/mLY1gjqTWoOzVdxy1p4DLhvMuuv8/jhhy88LF8HsJ05R1FMTIxdmYo/Erpyjpan9fMfa5gBAXi/h7VyO0TXxel0cU/l7HFs6Qo3DXZZkpUmK8i69dKmC+2YL/ABXDyctN5WriDwaOc3m7tmHmLSbThcXa48D1Kx803ayH9N6554HuePWiV9WSQNnpEX8cLWedhhrVpzOdjx569t9uzZsNo6vzIYlysy8SP6lFhpytXf7HYoKs2t66b3XPW6V6GoDGUssIlHVRlNLOfstKz2D5XrWcKz+wnjHRorcVlJj/sEKSrdQVrt7wsarGx5yfaU2c3O8+DmEZXSd/yIvcf4htnOOizUnmF0guYiWzGYXN835DISthPvLdQnbHt+WVlMjAsnVN0X+5rJWIfyOdggduOmCyPa27bSB5yhPBvWyZywReIYM9IdPtikn0FcsxY50CF2bynHtrfE6USebzf3uslfTV/8w6rzAFbNH4cKBVTX6EBjY+sBmN9XQ1DuNJzjxlviWT8Y92Kr6T80ul6Wora96Dh5kffBBuu101qWzno0QlFJUelj7gb0bCfSo5ZKd4ZmXmDNjtNASz1jWe+tK/XzBPFbj7aeN2Eneq/kx3sCFpWwADrfChpqfWP7ESYAC15TQFSi/2IKv8a8zfYbg2bgVR/ORLWgj9vN+il9LHA+6IHZeyMbzv2qYmJ8uaqe5pNMxp8WoZW0b82xVkrsxg7rf3UcTMba33R++GAIyLu8LKD9iJNp0s8grhnqjJ4KsTtYDlEJC3BPutuuNu030mzOVfIAx4sFrJrrD0ZKT9O2xq4TBxzM6/vM5nbwP9YQQBsjkcRcv+MK8vqI1f5V/3za75D1Lsh+qdZFURn8PPU/HzJ9ejVaKkfnrfz295lyDFuJq1Tnv2k5rEMrDPu3Xc6Mm7arfF3QohL1/V0si8odUCiILahzDSHmf5spLCpDTU0fOMx98zA4uwPrwTacdSrp9uqMGYun463qT0b9y/xAziyEvrqvxUbQ82FdUQkhuj60Rm/re3w/xIkGQusFjy3rTcOdkXcoTJecIi91WCdCWA14OO08Io4yJnX6uWbwIqsO4ZIeKJOgfHZ90vZlZZveF38dROVW1blQvXqB0X0aY9rYeIYl64jJ3C7xNcNlWZQVJpdYcTHWp/2P1xmCk8KxCk0GXoSikqLS//z1z3C8pVImuVgC0a8XTPqG3/4t4XCbcsg23YdKxD7a2GrSN+iFG8ryAhm0qHS3qqzFDbqwCpSfBoBrjCAWEoRFRKX0B1bXM4wtgQhdAueCkjmD4G3qfDMzvdyfxEeK3Z+a1fNbVUXE+HIQlTeH0mljy4ecncSW9dcgAnMyy8B7+fZ1HYdoO0KtXVJXC6tgyivEENr5TMH818VAaX7vjq/LXo6xlD6/d8Yp6fMmFt7xw6pd2XK4Wg7wTF54bIF3amLZVVzOL4pDRODPu8HLYZ4+bCjpIXdFcHCwazN7/nN/hPHS/iXFZgMtRlHpXxCV6jl5FVsq3Tku5yPB1tAZyfmNapQAvQdqzl7QV0ZnqCFEn4tGF9l67RmWDl5UihOMc7Zhd3IuE29qiKAA3sizHmAFUSku95hYt5o+uLBWBuy0kMFzxBFypsLsTQosf60idiEi/tdEVNb0xJb6vfcvddpR77zg1lbkx36/iQfzwPLwsRCV6zwsoOsQEP04v31WvR7OMkeiD7kZf0oSUsh6MJBg73fO2QtCcZ3qfMAW+M2qPLLKTcPacbnp81au6ySMR0NDYobhGIO4TDj9NLjxOs/IzkcQHdOpg6KSojK4OWzOMttSKXPYDV1mO3eY9g87Gu/XeRZUyuK4W8KsP85OGKE+rNJGIGVKISqxWPQF0jlUAmH2xqDeyCfcECVRKe23H43rNhjezHV1da1WUCxG64Hl1vm2SX/wY9gPJ6LZRfsDaxpE5e9URcS4ciOyVVq0/iIF3LzgXQgIjjA4HukW7x1Y1qgdYUCskQNd9pezQwyN1t/3Dx9nDlXHm16BEEKdke+UJ9C5tWU4aSdU+1asHILZP6o6H2CtfkzX+1/aH93a8eN4EtjhfYXn69vosrFFvhjvQt9LsHb0T+JzBjXeEZxhXeSnTybXUlQGdv+Cmge76nm1WyplPuPF8fX4zXzJ7DlznikUWUX3ebHtUw+EQehxk75gDL+07TONz9Tr9jXQkEJjAxZP7aDCcOCN/EITkEWvURSVAtRXH6xET5Du+yKy5cxm0fF5/eBYiS+qTJDp179nBoTBC6oiYqycOGkEFfx6U5d1ELbB7/QMMdRpd5mEGJKzjLCA5gQadx1mOuwWFTZ+ygx0WSehrZz0kaU4WylHBcTxyU9/x1+LM5X3qM+H2Laq1fP1IiD4tAxkPw/uuSPLeWHsI442Rs9MfuE23FAgll9Q3L3qwZg+HfBY0jAu/LKUffaqm6KSojLoeWxSn5elcmy+Qsh1m9Qp14Sx6xLUiyeSPXzGrB/OYDTa9oayPtulsFSKZbG+PvH6IAaCuI+3mcEs8sBqiMq6ugW1+JH6nUk/3EDNlnNiECxk21reOkz6IanrVEMJwEHnzRAQO9VFxOhZup74rUGMc6yOwWS42Q2Hk50VB9vH/YbOJxBbi1HnFo9t8F+NwFM8yP6Pr2v90qb90ebPSyEgc/lYmzHO9wQ5FswJ1QDoozFLm0/RaX+mvfCtmNeGZ5gklJmbXedSJEmYZ1mnS1iPHCtiXd2SWrHU44fiY9gO+jmeJcPICaNri5W4WWeMwZRFGDErEVhs4fFrSbnDJlFUUlSa/ZYFy62QqJRjIVgrjIKM4zd3SGlnsMjC0NDgHGlsMbXaPx3MuqNRS4lEpQTW9X2esO7I9kMld29JJp6GqBScma05Z4tZX5zfB3FmCXmK5UyFdgBoiWWHw/ieGYS8pkptX+xsXUHpOmj0xi/SmHpFi44sbaqCOFrlYa0c6U9a12JLWym7xURxd7CnuEM72xAn0yjjTLGByHb+4IWR92M7PydeZtAiU7b3EVT9a38NOGMQLJXX68wJ3bkAC7xZCC9JrRZJnKK/vTNnL1z3JjxPvzF7pt0ftiE8V0bxU4vNmXzfIzZl3GQNUBqj1YagyOX7MxKVVmJ1NLrwqMn4wONejjIpHXkwyqhjOddNxri82iz0PLW2tu4pQieIvmaOtzlrlebn6K6BeFhHGhJzg2hf6ijmVIPzke8wz+Ll3NHUFKsyf6rc5A836PDZtWOMJC5B6A7tvpdMVFrOT7Q7k3VBpMF5ZwkXUM+MOgX67CONkwNLSuJjfngcffQZh6COR4wml5vnUy1nrfQRThlf0xEQmbKxnYhP2exnjF7XSl5wCK+1HuGAXhrujP6XSXsSCNw7RqT18PpkfdikzkLXDC+L2oi/+ZegBaRnfUn78S2GgeILjQFz4mqtOZGK96pyFAu6QXKDEcnV7feYzetf74bmWmm8zkQT5cygJeepbzRZA9SucdZKqCLV++a3nJmodL7pt91yXG8oKr9ajr5VUhtu+lX9rH7D4VmLm8o4jj2wRnxT7RnKsaLuQB54Y6edSPT0eWYJDmSHNBHobpUyb4Mbqngo2Fk7c+ZpRmnhxjoPK8AlhjeyeB81LZXSp8yPH1KbGRyQlwj4o2+6yvdmfEHJRWzSLq759+h2oHK7EIh3aAkIEZWp2FB1z0lBOyWF1rQiL3jSSno62CQjP3vhvMO0U+hJIHCxgHo4zOwYgjNPGnnDlWEVKSh1of+X53EQykofmZ1OUu+/0cYOfC4oRYgk3RcNOHrdp8pw1FqhlzUH3s8qUQxU+jAa8/Uuk+dLjqOotBFEGYktC1E5aNJP1WuQZas9iL6q1EFRmR2RxKGoVPttLbeohF/FooPw7JnFrYZmaGrKnw4537Mi1mLTrXdJ3qLyDJakTAlF5c5IY7uvIMjom1FMJqUF1EBUyg1AfLhT0C+zs1jY3jC5iTPdMxWOgReaMxKJaoc2mAZB+ZSBqPxnyCA/twqP/qWHHwZhlmPpw3bydogoI6/V/gvso3Htvz28y9fD87y4h7xKx1Hm5Yvs47BV/3xZrJSd9u9NPONVhqIrKvGS8XQoNVcpTA3eqBcrPbO7fnCcbQgwbGSlzjdWvHgdbyjYhoMSt8XuA0Sl5Pst/sLso4ycNQ2FgnupKjQmikqKyiliqXSncSTaKrFhjX77TWLB4vlA0hfscmo/z84zkxryTFdU4s18k/Ig4TlVbKHMr9JbD0c769XacnMDD6iVHTtk36a7/b2rq/gRXK3V1iuTYjtCFL1VlwmcEMy2vCzndl3P8+nXnyye3xt1RSWu+ZHuuHTKD3biTKJ3Du8/Dyxv0I4ZOGoB/ZRXeB+kUPxuEGcS0zjXKEHXyxNCyB6C8J5vEsNT5T5U9cYv0ZkTOFP5UvUaNct12Gr7pObztEHOW6v0W6MMYj+anV2qL4N3pWu1sBytDFqyO4LPZh224jXf0LDId1gwFe4UlRSVU0lUym8pnqcf6jxPr5xvdLbgpfC1Ks+FlJk5E8fd7MSLBm3tkJd01XZKUk5bVNrOjzUG+mvTTmMbJqZxzukJCUis0S8EaDcXlbKNrXu4eFzf7tOxbIiDDdrSDiEk3mISz06X//TU3GMhHrboCAgpC9FxqW5bOuXhmV0Dy+RNXk47Q52RS0Qk6tQnZYfOD9djG/zvHt7lw1uQ2lG3vuzyw11Wa7lCCPV3Wjeml4QCTY86fjzVffHlenMitm36qphSBAjky/281rNrO+sgsg70e3+yr3c9x7WtApLsof19Qfclp29wFnCzlen172s4WnC15jVjoVBKPaQQRSVF5dQSlRJecFEjnkMTsYcdBucORd8G+G84V+o+t5nyzi3l2mnIu0DoikpYzTo0TLIbLOu9Et5D+w/bMF9Wh9p+I9T5ferl5YfAXFTKYHA90jhpL/Lu1pWYtVWAiPgE7z9qjSvzozNiGToQVF039z0QDyN6AgKiMtV8lsqY/JTZ3BX9T4QDysmKA+H2zEsGWXFcr+yk/QFY+HKCrGNr/NebfIQYSiPOJvr1h/Jse1vrgtyy97pH+qIynp5+fVzpoDjiuX1BZ45LFIZSxF6T0CLoh37OX59OeCrPBNaB6zQZbZOIFQgT9C7d4zp4If0X8h+XLLzW2HgpKikqp5qozPz2J5ZqGLzGH1cZCdttRRNSIMLDcYbOOetnut7sk/ynLyrb34drVNMmwut50QKDIYqntXJsKNyo88otKpuallahzf/TWeh3mcLtxLP19acX9TJGCKHzjCav5dx7GLzqDLiL53dCV1Ai3MxIdW/zO03a07lml9MLclp7WCyvlu1mnfqkrAQIR113eOYF77TPN3F6kcDsAx2R88vinNNp7xjqtL5gYqnVYWUiKqt75imddzUJ7Iv14Rvov1J4F/Vxdu8xmpptGM/dsARMzwRNd9ZldiactVjs/4HvnsCz/6jkKJePbbeW1LlFHO3Q9vM6aw369ZBs143GttXbxZGXUp8v3SrMKSopKqeiqJS41Xj+79R5HseVfSJ8TP4XtowhKWESmxtZsRLLVJ67kpfRFZWRyMK5eGu+SRUo6tf2aIOXdRj1q6YhG5GzCuUWlXJj5OyR7pmlXcIyklhR6EfxcHiLSRJ4Vc676oUVB1vmxgKvprdlqbao7I0P7rd6/hEln6xoYHNH9ChYKx/zEIEbBzrsd5v0Qa5DnZs9whY9JnnIdeuUkD4QlE+WxUqJUEVDF4XrdfuoW756dfwi7XmRiilZ5CUvrf48T2zFdrUTtLCUlz3LWniMfGbOciLy33LoXSI/ZJIHdAcWGUD1HsCSe7Y2H9vpGqsfa6PumVWxrtylex5bdTxj5SgqKSqnoqiU+esmUTCMxIB5/9l865YcpTHZAZXkLCJ2dZ/BkpTXFZV4gz0JPwIaXojOg7odz4BVC/INmE9jq6ZmMkRlZnK1m6ZMG663296Sh42cqbhC94fELW8lvgb7m7EFB167/6MrHhA+5vnQ1fNKvl0mrMRyCMvcUoi27Tmircv+xfrug/HDr/cnTjnYqr4m27Io/41znJ/XsVY+tjRUhe30q3BtjjU1aJGJNl4eXG6drTdas9KYF5dpz4ve2C5hU6jVcLR9jslcly0inMf8nN9YlWZEynaVOAfco8dHUrO9cp5aMuXo/gCiTaS5XBRYFAQvWhSVFJVTVVSKKNQ/Cz7mJOxsRvSKY7OfCYkRi99v3V0FOUe5fbJSxnqugiaicqbdepzGIjccjS6ydZZg1P1t5frhGS03eLJEpUTLx4/bn5X7O+6gPcT53bKNns0Gwv14k+w9qO9Jiaelwzq7LMTDV3XFA8o/UapwQl5j2djZeAAE1d1eecH7OyIfNhl//3LrGAjIpz2cdp7VOa+I9t8Jq+dLQQvIPIHOf4GYmweajFf3Gu2QQpJhCc49Ku1k4i8mNpg8Q6PX/Btzf7mfOLAq/ZyMMvjxebObx1zLQUccAib+YXfpdr063LPfXy/lmCkqKSqnsKgMZdI3O2YJSZAcZs6ciaG7IFJNdhTSiCjzlVI+p9p1m4hKsQxqpE/E4VS9gLo6N2rM6WWyRKUAH83887Luoi1m7ki07ZzxN230DJTRmQpE7j9ZewJkXaCdOcXNphP/rd92da+Hg00cQmsgRwR2WA8PdURm6tYn5yD7O+2LvM5BDnRGbxq5oqloqi2c6dwX3uS3lUlQrjfNKKTLRsrj3Oy12i8bqRbFtH8I1YH4iPrPT3bMRgQGx3kkrAlnjZ5ZNrbYmzAqxTUQg/+ry8UrgwfOW7Xr1iNnSY866oP7lWJc7rppOxdo98liRp1S3Y/JqHcqi0p3DkcSc8VSqDuP8aK4AzGv28aYh8OLm1CHYgjF8eue83gpImH4mgsmolK2V7XiNVnOtaqdFKum6ta3lJs50zlS6p5MUYnmYQpvu0p3YmXKO2tx0P/wMT74EWnFv2tPUtSzJogzUCaisrq35SbV+xtUuX8hxBDEW5+HZXGHZODR2bIe65OkaBxK2g95WEAHh5bbpxXqu+tJ3ml/EJ/hUovKTNxL69smjkmm/CEo1+iKSsSqvFy1Pbxtf8Ds+fEOBu4mC7ASvxYLJp6pt1XcwqsARgSd7rlqKT9+PRlrZtSz/d+ajJE8IXGmQleNilBU0lI51UWl/PbrRmbY9QxazmOZc9oh6CmD2NeWsw3nyk81evhKeZGZqJS3TPVzlVg8nlIVPPKWrb7wOc/IDakAURlqbFz8GoP8xWMhhq6UMdj2qQdqx9t0z1E664Pa+jMSlamWq0s5R/PVvTVp/wcsi8/mZsWxXkRAdKUYiePrFmE40BF1sA2+xUMY/la23fP1ZQiCtFz5vSUTUP9y++hyMoel8k5dUVmjbKl0D74fJOuE+rOvk1kGob8QJgdr3U8jkfYuSUAwekykoi2ZssOj8YKdWUsiiVS+eYHvVhjwRazhVu0YsCpzk6KSonI3EJVirZyJ32CTFI4j0FGfkTPlRiGEIERL9WyqPL95y5iKSjn3p7PgRZqcWSodRZ09qgsfbkjfKxa+8sap9BqLmLPFrK3a/7Fy7oSSc5RiVdE6O+VaOuUH8yMqbFXKmIhK5ArX9vBX6YtKGeTqvtR7y9pe9Q/k+FapY4KwRIghOAL9xEOovgyx+VGvjDUPLJm9NyyHn4QFcUfJrZRwHupP2hebWGJ1WYwvD0H5F11RiSxLF+u0ORr/raRpCHc9cwjrhWfnVnl2EC1h1uzZS0oWOF6HwfiyEF0/01wPYFl05uVrD0L67Tpr9uhOys5wuDQZdigqs0Vl4lEwXzNpHytxaWtraV4g8s3J3UFUytgyxjCjuNWIcuP8VfM5T2d2JJyo6dpS0utMRaUoZJ0zABI2pNhAJBUZAD+uDNhq/1AliUo5eItxfl+5/+OddhDk3Cw8kXOXToaeYvfAUFRKiIRJ+ZMUjRByD3psgw8OdkXz/sAW6uxwR8M7cTay32Mb/ImhZFNOfFGUe6NXUPYSCcz7RjqOKtk5N08uOG+KrewXDESlkvf3WJvi9IZnRy+JgfZLmJeFE2ebrcSf8Oz+D7aOj5uUiZzVqGUtbgAL3RfUJwrFpx2NravtVCiRKErBxERU4h49hReBm8vxwXn395uOG2O72OR3YHKvcR4o98vV7iIqhRuslT8p2/2zEv/PdG6W/DpzUSlbVurxKjNn/goHK3bjwyn+SEjIi/G5NCf5TOWu+4QxNKBvCJassz1nXLZfou8HOUlgXfqGvnho7giyD7p1DXQ2LPY6ywhheHu6wJZ1vnZgBdwL167yEKojA132l9NZKSE3IXsOROVNZcjxPYx0jCVPCZjNBak7X4OwUZt15wUs2Ofp3ktsTR+dCThu/Ez4tHS6lv+HIF7Ol20t3f4HVR59+Jw2AyvxxWLth612ZETTYytWEclFXKxu3e9NRKVu332WN049S1GpNht2F1Epo5VsNuVZu5w7xACnRngSSvkRldg2+rjqQ4t2npkxY/H0QkOM2O3natT3z/Eu+ZUiKmV8CA3wEdVx+CknlpViQl13SiGk0Oe1xUMq/lHddoIsP7Ls9dMh6H7kEbx8W38yeo7XlnWx9rd+svEIbKvnhBjCuckXhy6yc+KLDnU1vHkoaT1fIutkWurF9vuta5fUlT3A7fS++OtwpnKr7ryY3tuyuBhnr++RSeo0vISqJj/wKSILCqwNeMZWyMuuyThMr5F1EuuC+o5NxuFPaZtavOKxFm/TXXdK4bBDUakn7nXvmX55WipNn9mx62y7bZk+d5154Awi+s7r/PazpNf7EZWZGGrKC5Sk/jq+wGDEA+oHqjdk/HlKqbOSROXoNv5dqmMxLPeQBEsNenLAk7tDVzxAcEyqqBQGAx3WibAWbvIQdX8dvnhWxIQTMu0syw6y7lojk9YNkt5xfJ3uucpk5OulS81oPT/cZb/VZBx+r6lKtcRM8sHXrm5uNmx7WiTaLg57A4bPRqBCE+vSJiuK0D4+Y8CqssBZqbj+uJ375sxRyfbjRu7Q36aznLuC3hqlqNQRE+UoS1Gp+ozmK5dJ4ej8Xv/5Vby/0UR30IYkv2POud6fqES8Sp1UglYimW8A4lo/mldX7Qdh3HnKShOV0p/RpPCaQYsVJ5actUJmo8AnAyqs7Y19cCqKyseWNlUhnuS3sjPZmGTFGeMq5zWxDX6/h1AdgLCcn80f2+CNKJ+TQtKv9dIdQ5d1abmdc8bGJy8N2nOiN7aztre50EtksekLYZk4BeuTVs7rki3m4lltOX+H9/g7SryoT4Oo1D6XLdv1xYC+Yk1pT8CyOaLDyg3AbrUGmmGHolJ5vVf7TdQ81pB7/ykqVZ+hQuXC4fYTjLy5i9y/iNX2sMQID6KPJa3Dj6gcFXM3ayxOt+dbkHF+6U2q3lOSbSaMvNvjwVSSpXKsX3hj+QLYaC3eKiwx/utUQzTpTh4IgfnaAqICLJUyzo0fa7Q8c24n7RcGOywjgSOxJyEUt3p4g9+5vrtpQkpICaAuKSSxRZ6bQhLb16biEmN6dItBDnLde5+vPM7ZXqE9J3pj/VWr5zf67UN41sImPNv/V4rnSOVZ8/jhHUSonw+WSlhKnF7XMqonEF72ik2Zj727vW5y7jvgwOMUlRSVu9OZyvHPm3Ga5TzPfSZCjHOi3/W0LNf7FZVwpVfOAw4wLzQ1LTzMa2AApnOA/AnxFK10USmemGLd0PyBKPhWKpYbvK2ULM92zap5J8AytV1LRFSIqJSzkxB1H8nJ4e0GC7dvfGBJSDtszHPLZuC8pnWLhyf4dpxx/Fj2ec3NH5l5CP79XlMB6XEudDvCJp1nci40qAUEovIOrfkgKRpT8bWh1fN9pQwd3/+w3ZaANe5vQT5LpnXJAj+WySsoxmP1ICRQp26/0Jcf6/YD11yp3Q7WnsPzrN+67Ut5ikqKyt1VVErMaf1z0fnnA56V75g8Y5NyjX9RmXiTxrnKdL3tvMtTVNrOHaqLHBx6erPrqERLpfQxHG1r1s/dm3dyYdvbOaOUE6W6r8WG126/nohovqSUfdKpe9P54YMhKu/zEHXDA8nIXJ26xsqKYw7Oa673sFY+vbmj/ojxdWaEbeRknL0cDERYJu2fiXe5Sb+DugZhpp7Tmw9u6s6/BG3Nc88qW4nFEJd3Y63QDbcT6BaiPNNwVHSCYuyuFdjawm7NQ6rr4Gi5Eezy7Er3ptofOBS8xWiLLuvYkWp7XuUoKikqd1dR6T7PGYdDbae47OdfNJrOToSfZzKQa/2KytFUYk8rL4RW4vPZHT/yyPZDdc5mwmrxgakiKmWbGmO7TplPgW0v1POTkkfQv3FhDUTE83oiIjZpcSqz54Er6pLWQmxBD+WKOutXhbLi5Hug0t1z9kJd38gOGZQ5v2ldJtve4691U0h22WsCEJUDpkI4kMUBldRcf8pMRATYoTcfYKnsiZUsdafEZQ034NySnfjG6PoV+BETxed1A84//kdQrCH03o1nXCtFayYI8pkH6vcBMXUt50+K43xFkFvOvUEdvaGopKjcnUWlG8vbJP3iRA2wI2w7i/Sf70m8wq+olK67gWgVzwChvXtwyR7jhwynlnerHhwXC0FDQybf9/i/SrVUSh/rm04Po3/qwtuTpbPesk4vS2gTbHferyciYl+axCmc03QmzqSdyhWB1nbZHs8WgSp9hwW0CdvaOSGGsDW+bvBC+7jsOiTEEITtRj/CEqK1p5z5vb041PbMb8FxiBG9+QBLZSpeNGaiCvdiZSQCAix1c+UMk+tIg/PWqmtREOUgbH8e1IueZbWv0u0T2r+mGKN832PX40Lt9hDtAxbVE0zbHH8dRSVF5e4tKkMh94y07SB7l+m9dm4JOupCEM9uwTqCEJVY1JXPVQLuBnh6T8hKAuiXqEKX3NhegT8rWVS6wjvqnOHD2QA5Qtu1spP4mTjY/u7RERHItrLCT3uluHZomTUbgu85jy3rR4cvjtq6bYrnNTy+k3lSMd6QnRIyvSa0p2vddK2Z+k46ktN8IBme9Hhk1anmT+rMhbGy1X3NJT2m4XX/XAsmYrjJeUc8azfgRXWtztEc1TUou1w06pyqO5+yyzc0JGagv+s1+7Ddz+F9hEhqNHAKSmOtvcrveOV6ispsoeHcgheL903eJ/GeoKzQqvNjdxeVo7/9S0x++7F2vVRf3z7heJUq10ktF4SolPA5Omec5KzBxDfWxG+VF9NoYqUXsEoXlfKDJwfqlcc5/s0Gca/q6paULeh1dW+zZqzKGBLbV9afZL2BMMvJC+6G5+mwPtcNkajb43S3fSCufyA30461rb/Tfm92fSJesT3+iK6oRBs7sH1e9vzeOTwy6Rlv1RaVsGzWpua9QZdv0OUlbiOE02wrmlgK8XVLybJdYPfFrzUBoYrO0V0b8r1ga3CcBiF7i367zubDDmvdV6Mdz6JGohIxNuEcenI5Pn6ONhhl1LGcr/plOtWufzWISnEqhs4ySY96yVS7n25/gxCVhxxyyn5YmJ5QXZzkLNQYLHlD11ns8+VjrXRRKeOdOdM5UpXR+HKRSKvExivbX1Vq3lwdIQHhgbOelfc33NloeeYFhxXQa8taZQQQfAlsg2/xEJa/29w1MyeVXX/SPhfCcpuWsOywHxpY1vgalf6UtMzKOdU4CqHtpAPP72f36zkt8LR+fsfq5ha32t4umaggyO4xclTJs41l24k3+ukfxN0DumtDxG77jJ825VoksIAzgcHWnNX2Ib9tG4rKb/pttxzXU1SqUX41iMpRnfVL3ecMc+gsNYoVVioIUSlDgiXgRlVogPW3MTO7vHGqnqd0raHYsvFCOBVEZca7U38Bb2xcXFaBUd1zkgUx8ZKqsISI+H2FTetd3UGWmzMhAl8eL+pGs+L8MDsrjsoYXCcchCfySgmJ7faPZAcoF6GJsr9VFZWoYwuEa0UczHbTMyI8kPYnFbs7lO7WtgSr8A+qjFgWYYmKQmB+AmvgH/Fc7jR5Nndd4+GAqNpXCSqO9cvEm10yDsmWufHHaPvbDQSf8G+dtZ0LtJkHHCtT9R7plqOoVCNGUVkwjNCrW1RiS/s8jQViMDxrcZOr4O3EV1SvEzGaL6I8RaXaQ6xUak3rPhCKf1UWE6nYkyGD7WSlvvgsJN7esBTe4RFncmioM2p0Fm74YuttqG+dh1B8dMsyqyG7y8PJaBvKDqsISwjgm17oPsb31qJPbO7lOFu7VHkOjBOfsFxfHkT75arDfdmznFafcTB/09rauqdJn2XnRnUNrJxyzk6/GXZoqcwSFNz+Vg37NTymH0yet8m4Bi+utFTmXbzypAfENsqxWodRR+MtaoW1cDPJeP9RVAb7qMBSuVJVUEgcw9DKUw8MtgfB1QbL5Ltg/duc67Rj352G6NRtKe3m+bavRH0TnHBcp5ykdfmd8D4fX6dr3Uxaq4uKyqT94mTl9/ZggPOUse+rzoGJ5WILdZlWQnkJ7I01bI2JcJOkBHV17YfqjkMsNVi7XjBpc9Kv8emwQ1FJUUlLJS2V8PzzzjntZo+xE08pL3SW0+Oep5ScsopbwsgFfCZFpe7Plln52r6Ws1QFBayam6uu9Z+Sz6ynxa9Ktx6zD6yV3/IIMfRyf4f1oXzZamQru78z+l8vfdzOEcxbuxqOhFD8p4dQXDfUEXlTdq8GL7KOz2PdfCV9I4SqCNbiIyp9iQPwklDdF39GdQ68Ui62rWbV/HrDHiLeZ+uesjU96sldL2cVIbrmI3rCElgTz8axmQmC3bCdvJeJAwra+b3qmvRKOWdnfaO+pya24Bfpt6V/hKYUbUBIvyBruOk9oKikqKSopKjMKyplYbHsduVzlVjknkEO3XYd6yZi0lkUlaZLuN51+1w3/wjldI0Ijl27at5svRbKW1pyZ0NU/ttjG/ypfHm1N10YmQWL5BO4piv7rKQrOOGEA+tkbp7vpH1z9nlNCTGE9r+SLWzH9eepLWivvFTyt4bg5e/QF5RuesY/hHB8QmUcEG8LbBybgbD6LATKtZnoCM79EJH/8nKiEYc+PyJGpU9SBply5umsS2OCLRptn6PaRqYcApDbzs9KIfjKVScMCR/WG/MrpSkqKSopKikqC4rK0fhwquci5GD6r5UXPyvxqCzCFJWmS7jmdZJZp1fjXGVvZW95ijUSAu6i7LzgIur6O6KfzhaNQgvnG7/ohiDqtNcOdkZzQuS45zW7rLs8Mvds7e+wT8sm7gpbD+umxL4cgkA1CcqueVeVi0NQXmoiKnHNV1QbwbOvHDHiFYeYRSep1m9aTs5Gor0NymvT6E4L8nZrndE9HNl4gvRA1+1vQOV/L1ZlE9YUlRSVFJUUlYUtlcj4orlQKadXK5ZQnWcqTZb1wtfgTN23VIUFypYtOLvpSCUvOLas753gCZ60B+BxjaC/E/8GO6zjIQBflLJiXcT/X5MT4FxSQkI8ZnuXZ66xfpd9XjOTQhLWzU57QoghCNe713Ucsp/puAK/7opYFSyOD6re+/HlqnpjJ6r2B4LqNs31Ii2Ofar1+ylnFFdOU1TCUvsF3fFXWnk3uLy16O0mrCkqKSopKikqC4rK0OzZe0sWixIsfCM4W9VeaOGiqDRZ1gtfU9PX8t+qwgLbpZ5B6YPvlb8aN3dFToGoG3SFn+tYE71K0jqOrzW9JLQ3ynwvKwzR8PDyaFv2+UtxwkE9P/LYVt/R32Wfl219lPOZEKGvCFvkKB/qbDjZ36iCvVoCl+O+b1W997uy6PTGNoYgSFV7A1HxJd21QlIxzpixeLpqG6blSi0qkVVsf6yVj+mOvxLL4z5ebcKZopKikqKSorKwqMTKonmuUmmrXLaIJIcmRaXJ0u3jmhULDkVYmX4VcQFL5b0+WirbpSOXuXEmXcEIMfikVwiggWRkLkRlTgggWB8f3tRlHZRj1RQnHHhuezjtPLm5oz4nzZZsjaPtIRG1yEX+XWTqqS4bAIWGqnvin1K55zllUvFehep3FUF61zb9s4vOTlx3ik47+mW794DgW6cr4Gy7rUW1LWTQQXzehPJOjW5fylreSrxoctaVopKikqKSorK4qLQSHwl8QXPPUxb+o6WyGCGD7xHAGsLhdhWBAVG5XtVBw6AngV4y3BF5J0TjBniEd2RbHkXgQez92DP8T9L+mVfAdLFGoq7LsvN8y7Y5nHm+np4z0RI64gZQt24czU3uKxNLoGCksu45e+FFQj1G6Vh8SqRmxHU5qSoLvggesagRa8Ww7nqBLfC7Tc/xqfCS7VwTwQdh9XqV+qUM6r9Zd9wVXH7ExGGHopKikqKSorKoqKzHwuqes1EME6RUzkp8u9hiTVFZjJDZ9zW9LeeoiEqU2bLPNc1HmbVS3qvc7e2OqDPSPWNCpiIRmANdUQfnLnPSMMISuXEY4YXy9TTjhGM/6hliKBk+Ifu6oWTDCRCdyyrJOUf6OOr1vU3xnu/KtuO+VFypnw8auxB/UloDJqwnsFb68DouPNvgpGM5P9Ttk3im23br4SozGcHWm9BGv24blVxeshKFQmmEhFL/o6ikqKSopKgsKirFcxKLxbNBLoDI1pMotlRRVBYjZPZ9bW9zXQ1CBhUVGbBU1fbEF5i1Uv6rvGJTSr5tnHf8o8f5SDjq2Cu8PMTHei7iENvmn8D1E51w3ADp1o05Tj6wCK7trqst/8gLtwiP/6uK3muP1I0Qld8xGYvkrzZZKyBGX/Kb0cWrv5bVfjq2vvVTNloOjn/kj04xvi0I4uX6Y3YGESP4eNs+88BSf7CW3qDfv8R2hId6q84coKikqKSopKgsKiozWzta8SqLnKt0BsPhTErHQn8UlcUImX8PkfEbFaEBQXKReSuTe6VrpcyIwpy4kxCU/9q63DqmWA/7L6g7FGV/kyNK4Rg0lLQqPsvMQStOOgBe3y+q3OusDDrbqlfHNGM0ZmjCavc6CJghAxGDdQNOgcibXey+qH4vQdZNc2GHVXN/I7C7OBtpj9dK/BLj0LIEqo47u1w43N5ssv2v67BDUUlRSVFJUakkKjXzgBcUlVjkH8ZBrz2KLZAUlcUImX9f0xf/sIrQQAaW75m3MrlXbumyGiEGH/EQhDuw9b1cdZsaW+fzISxd7/KJoYusPw4sbzDOPlIOOlWp2NkIeI+zkfFd29oq/x8vE380P0/bvQcsd8YBwLE+vACnl/f7OWPZ1BSrwvrxMYg9wy1pyYPddrzKPZKMZBBsEqNXyUlxVzk3m1B5/g455JT9TISvCHK8JBys2kuKSopKikqKSiVRKanVdNIvFlxgFc5TutZRK3Gf1kJttZ2uuvgFVQ4Lbo1WH0d/eBobF084+xdUf1TrkbR7EBsDCgLjcdU6K6mcbGu7gc4RhNxj6/u+wQttpbNyMibZ0oaoXONxtnLbQGfk45U07gl96e7eAy8F9ync41zB2RM3zqoy+uzON9py3iXM3DOWP0ca11Pq6hbIkQIVi940KSse6BBQ96B9Y09stP1bSS2pcm9RNqW/Bjjr0M+ZKvUHVQaC77P6/UykxaCg2geKyhxReZNEBaicj1PyDF8UlRSViqLyzGosSE+bLErZ16icp6SoVF3GzctBbKwpKjhg5dr32thh5q1MzpUDy8LHQlA+myME3RiSUa0sKTICyb4z6tk9wVoJi+eTL15k1U3OKAu3Wv3d+e8sen+9z1L+e9/VCw71MyaxMkLY/TKI9QIvs8/BCeZGfDqQ/vG/JXXi2Af/dqKba9tyPoWX0JtMwgZ59RFr1Gkq40ea2QNwvba3O/p7vUr9QZbB+cijzQwDzgNi+VXpC0WlprVa17rtt7zlXKhyH/2UoaikqFQSlRmRh4Xd/6QeDM9aWPQ8JUWln8da7VpkSpmn4rBT2xNTjtWn1nLpSw10RpfCuphjpYQI3DCctN6m24OhjsibIFDXezn8DC3PTd+oW3/g5RE6qrq35fsmohIW7M8F0Z9wQ/sJRoKr6BojTjdjn+DjQoqVE+NXsYxiTWxbarIm1ttOPAjGenVIXvLEzw36OwJL2ztU2qKopKikqKSoVBaVeNM9z2BBmnDOCAv2g6rbStz+VlnGfZRZOae6OhV7qKjwSMX+x0crk3Lp8AWzIhCAf/GMTdlp/WD90oP3V+2YxLnEucrVqAte3xPPVSLQ+XNbuxqOVK2rXOWmp+YfC3FokkFnffV18yIB9XManuHP+V0zynu9M9gAMawyflnHsJ7dq9s/CK8nDz74dOX5p9IX1TIwDJyh21+3fDShlF2LopKikqKSolJZVNZH297gN14lwo18Q30B5JlKVVam5WCpPLe4qIz/PISwUqZtTNZ1EIJne2bRSVovI4D5uV4hiLz6OtBhvxt1bfZy+EHe788WCks0KWNHKCQ42qwoel+9nHd6Yl9StdKpjM094wgvZyMhU9RiGfQPuLMTHt+fVBmXlBHxaXJuFGvoV1XbCLqcpJKU+Jv698NZh/Pj9cX6Q1EZ9JwMuD5ufxebwsrfmxzvwfNxlnIDlVQQg/2n1qIB70WV/h92WOu+2D55Uqvu7B8Gq/19Km1JGVoqVUmZl9v/6nkHQ3w8UUiAICTNhtCaOfuatzI5V67rOGo/OOv81MtaiX9/euty++hiPZNsOwhLlJMHXOpEasaHdBx+irUV1PdVq2KzYKXcrCsq5T7XXHNK4M4jlnV6HSxk+iF3yisqR7BuXq96djCzPjnf1F8LnRFxegzqXpvUE7Hbv6Pfb4gby/losfYoKgMWgUE/AxSVxaaw8vcUlYUmp6KodBdSO7HGaEHKtD9YV9dqqd41ikpVUv7K1fbF/18xAYJzlZP6Q2g6wi3LYWXssjZ5Bj/vtK8pZmUc6rTfC2tnbjihTmsrHHcWq1o7Tftvch2sz5cVu59e39f2xj9l0p7KNfX17UeYhLTxsdZohvdJ3Cxhd1TGImVmzjztEJOEEJKlRvX4j2pfdMtFIs47wVU7BBIMCn8o1neKSopKbn9z+1t5+1sWLywa5/pY6B/QiT1HUan7c2FY/opYFbKn/LSwEGlRDiti2IuSXJZeMntvydWdncN7VGQOwgrZlq9hnKU8EOV+5bHtjWw8Vk+6+5h9StJpH5VWfbc5ClG5UVtUpuL/CKXmTvfRdNFLJeWhydu9j/VGTVhGnW9JaLCiAxhXAMLsLJN+4Vz6BTrtlKasxBF1/qbdfyuxPRxuy5vWdPT34QL9ep1vlmacwdYKZhdrjy1oS6Pf+mipDGxSmKxl3P72wI9F5ViTt1x5GAH06zp3lKJSh5a/stOvfs8MCMv784qRvvi1/lqYvKs3LovasDY+4bUNPtSFAOZI5ejVu2EITojHrR6icu3gRfZxkzei/C3D8eo7+oIytgPxLJ1yjGfGjMXTw1abOO8YZtwJ1BL0YsRedFYopHteGGlrzcIlvVxff3q4HJyLtRGJOp2GAqmnUN20VAY6P9VeinSEJkVlsUdD+XuTNYCi0gOvbBFhMXrCaEGy2pTPU0rTFJXK8zuYgqvnHwRBcinO4/0rJwtLKiYp5abkn2TOgUPNuV4hhiA2RyAsL31gSWhCoOt0Z+MB2N6+12PbfOdAl/2ZdAU6Lu3bGzsGorJfW1TCSh1C3vIy3lx4hS+ajef7NjjxvGy0luj8kOaWHUAooJXoQ6PJmBsaEq9Hn/VjU9qJ203aK8U1Mnb8KG7WZY9rXhKLc74+UVRSVHL7m9vfWtvfrtgzyAPu/njgwL7OAklRqUMruLL73zjv4NpUS6wGnsA17rZ47BfYUj05uBbKX9MIQghBQN7v7bRjb8R37xrfq6Hl0XMgQnNzhksdGtl4yjjSabhHxYPZZ3l8wzq9cXpf/HVl7Oe4plr3lCDmsp5gfXhJV+Bolh+RM5CyWxJuWCTjVYpD6cUF69Klmm2LxWlEwvlMDmfvVnFG8kcG48DvRWIpRWWFi8d8L120VAb2CNJSGZCjjtyRsN2mHa9S5ZB39t2mqAxs/rMiEBhMNsyHqBzwEpb9SevOTV3WQQIKgc5nDnZauTnDk9aWoc5IRYrr6lTL22Ch3K5ppRyBA9bFlTA5Zs50IhBdZ2Od+L5kxfEbukxC/aCOLRCrj6DOFUiJeIptn3lgEGPFecpL3Dq1Pu1fh3XQnV+V8heOLvovvTHsGvP5+cYgedB164Q3fHulMCnUj3q7rUV3bBVXPuLMKzVriaCAZ/kLemN3rpwxyamLdbmIVV5vjIkV4KKdeEO3XyUpj8UUoS4k+43aB4fUtSwVWFRnqdY9Vk4cfHQHKxNTpx3VrA+6/ShUXhyPdPo4VlbSuwXZD9ZVnEB6TWhPeHN/J09A9G0DyejHupEzHDEpL/Qsk7R7xfGneEtlLrGmdR9YHO/VFJRpWDbvDt3q5tWuqL85c7r3krPbOPfn4MXyk1jPrsX//gTrAQKNO3/N+ViJP2N9+QX+fQ1EzWUS+gYC7iSVuIoVNXB2hgRIgARIgARIYOoQ6L/APhohhv6RL3blYJcdg5Xy4VznHOu5geXhYytxpEoB7LO3vVPxTdNXNb+2EseTp0/Yru7eI//HfDt7CjFgV0mABEiABEiABCqFgMSl7E9GuyEac/KCj4Yd2oD/3TleVGbOVkY6i8W0nIwxVq1sjsI553ktK2UqPlLT23wO+mt8rnAyxso2SYAESIAESIAESKCiCLx4kVXX32n93nsbfGJubymDsEL39i8//LCKGoR0ZsXsveGlf72WoITFEplzvldmb++KQ8cOkQAJkAAJkAAJkEAgBEYz5QwrCMuBSnXOgVf+QohKPeecVPyRfW+MVZ5ADuSushISIAESIAESIAESKDOBtUuQ07vD/l4hUSkxLAe6rGtHLgtrZVspx1CmX3/yDFgo/6FlpZR84CtjbylH/9gGCZAACZAACZAACbxqCEBQvhEBztflE5Y4S/lU/3L76EoEAgtlr5ag7I1tq0rFP1CJY2GfSIAESIAESIAESGBKE0jPCe2F85JfFIukR+Yc+beL0q2hPSttkNW9ze3aMSl7Wr4USov3NP9IgARIgARIgARIgAQCJyDZcQaT9t9yrJVJ+zebusMHB96gzwprVs2vh7f3czpWSsSw/H7oxoUVt4XvEwUvJwESIAESIAESIIHKIoBsOmfDYvnyOGE5MJCMzK2sXqI3V8Sqanvjt+oJyvi9knaz4sbCDpEACZAACZAACZDA7kZgZNmM6bBW3uo65iTtkaEu++oKjEk5Dd7en9ARlDh3+WR1zwJrd7tfHA8JkAAJkAAJkAAJVCyB/o7oHAjLjRCVz8JKWXGZZiAO3wGROKAqKqt74y9MsYw5FTs32DESIAESIAESIAESUCaQXhLauz9pfw3WyvPTFZZppra3uQ6pGB/TEJQv1qyad4Ly4FmQBEiABEiABEiABEggOAIDyxpfs67jqP2CqzGAmlYs2RuOOT9QFZQot6GqNzYvgJZZBQmQAAmQAAmQAAmQwO5CAILyQlgpd6qISpTdWNszv3l3GTvHQQIkQAIkQAIkQAIkEACBmp74myAmB5UEJc5QVvW1vDuAZlkFCZAACZAACZAACZDAbkNgTeueiC95r4qghCXzydqe2Bt3m7FzICRAAiRAAiRAAiRAAgERWDF7bwjKpwqKylR8pLovfos48gTUKqshARIgARIgARIgARLY3QhUrYrNqk7Fv47PHxEiaD3iVMrZyp0SLqimL/5/+O+Foe45e+1u4+Z4SIAESIAESIAESIAESkEAmXQkK45YJOVzwOr5B4WwPV6KplgnCZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZAACZSXwP8HdjQkG4r0KqwAAAAASUVORK5CYII=\" style=\"width:288px; height:85px\" /></span></span></span></span></span></span></p>\r\n\r\n<p>&copy;2021MY V SAFETY, All Rights Reserved</p>\r\n', 'TERMS-AND-CONDITIONS', 'cms_pages_view');
INSERT INTO `cms_pages` (`id`, `heading`, `image`, `description`, `p_link`, `redirect`) VALUES
(3, 'PRIVACY AND DATA PROTECTION POLICY', NULL, '<p>1. General<br />\r\n1.1. &nbsp; MY V SAFETY take the privacy of your information very seriously. Our Privacy Policy is designed to tell you about our practices regarding the collection, use and disclosure of information that you may provide via the MY V SAFETY App and other associated partner websites, micro sites and sub-sites (the &ldquo;App&rdquo;).<br />\r\n1.2. &nbsp; &nbsp;By using this App or any services we offer, you are consenting to the collection, use, and disclosure of that information about you in accordance with, and are agreeing to be bound by, this Privacy Policy.<br />\r\n&nbsp;<br />\r\n2. Ways that we collect information<br />\r\n2.1. &nbsp; &nbsp;We may collect and process the following information or data (including personal information and information that can be uniquely identified with you) about you:<br />\r\n2.1.1. &nbsp;Vehicle RC, Driving License, Insurance Policy and such other documents and information that may be required from time to time including your name, address(including postcodes), date of birth and if you are a Car Owner certain information about your Vehicle to register with the App or to access other services provided by us;<br />\r\n2.1.2. &nbsp; Your e-mail address and a password;<br />\r\n2.1.3. &nbsp; You mobile phone number along with 5 other mobile numbers which you may seem appropriate to assign under Emergency Contact feature of the App;<br />\r\n2.1.4. &nbsp; A record of any correspondence between you and us;<br />\r\n2.1.5. &nbsp; &nbsp;A record of any communication/ notification you have made through the App;<br />\r\n2.1.6. &nbsp; Your replies to any surveys or questionnaires that we may use for research purposes;<br />\r\n2.1.7. &nbsp; Details of accounting or financial transactions including transactions carried out through the Site or otherwise. This may include information such as your credit card, debit card or bank account details, details of Trips(as described in our Conditions) you have booked or offered through the Site;<br />\r\n2.1.8. &nbsp; Details of your visits to the App and the resources that you access;<br />\r\n2.1.9. &nbsp; Information we may require from you when you report a problem with the App.<br />\r\n2.1.10. &nbsp; Information about you which we may receive from other sources.<br />\r\n2.2. &nbsp; &nbsp;We only collect such information when you choose to supply it to us. You do not have to supply any personal information to us but you may not be able to take advantage of all the services we offer without doing so.<br />\r\n2.3. &nbsp; &nbsp;Information is also gathered without you actively providing it, through the use of various technologies and methods such as Internet Protocol (IP) addresses and cookies. These methods do not collect or store personal information.<br />\r\n2.4. &nbsp; &nbsp;An IP address is a number assigned to your computer by your Internet Service Provider (ISP), so you can access the Internet. It is generally considered to be non-personally identifiable information, because in most cases an IP address can only be traced back to your ISP or the large company or organisation that provides your internet access (such as your employer if you are at work).<br />\r\n2.5. &nbsp; &nbsp;We use your IP address to diagnose problems with our server, report aggregate information, and determine the fastest route for your computer to use in connecting to our site, and to administer and improve the site.<br />\r\n&nbsp;<br />\r\n3. Use<br />\r\n3.1. &nbsp; &nbsp;We may use this information to:<br />\r\n3.1.1. &nbsp; &nbsp;ensure that the content of the App is presented in the most effective manner for you and for your mobile phone and customise the App to your preferences;<br />\r\n3.1.2. &nbsp; &nbsp;assist in making general improvements to the App;<br />\r\n3.1.3. &nbsp; &nbsp;carry out and administer any obligations arising between you and us;<br />\r\n3.1.4. &nbsp; &nbsp;allow you to participate in features of the App and other services;<br />\r\n3.1.5. &nbsp; &nbsp;contact you and notify you about changes to the App or the services we offer (except where you have asked us not to do this);<br />\r\n3.1.6. &nbsp; &nbsp;analyse how users are making use of the App and for internal marketing and research purposes.<br />\r\n3.2. &nbsp; &nbsp;We will not resell your information to any third party nor use it for any third party marketing.<br />\r\n&nbsp;<br />\r\n4. Sharing your information<br />\r\n4.1. &nbsp; &nbsp;We do not disclose any information you provide via the Site to any third parties except:<br />\r\n4.1.1. &nbsp; &nbsp;your mobile number ONLY in the case when you are performing a search action using the vehicle number in the MY V SAFETY App and thus using the search result to report an Emergency/Accident. Your mobile number is only shared with the people who are assigned under the App Emergency Contact of the vehicle you did report and to the vehicle owner to contact back and know the whereabouts of the incidents you did report through the App.<br />\r\n4.1.2. &nbsp; &nbsp;if we are under a duty to disclose or share your personal data in order to comply with any legal obligation (for example, if required to do so by a court order or for the purposes of prevention of fraud or other crime or upon requisition made by government agencies);<br />\r\n4.1.3. &nbsp; &nbsp;in order to enforce any terms of use that apply to any of the App, or to enforce any other terms and conditions or agreements for our Services that may apply;<br />\r\n4.1.4. &nbsp; &nbsp;we may transfer your personal information to a third party as part of a sale of some or all of our business and assets to any third party or as part of any business restructuring or reorganisation, but we will take steps with the aim of ensuring that your privacy rights continue to be protected;<br />\r\n4.1.5. &nbsp; &nbsp;to protect the rights, property, or safety of MY V SAFETY, the App&rsquo;s users, or any other third parties. This includes exchanging information with other companies and organisations for the purposes of fraud protection and credit risk reduction.<br />\r\n4.1.6 &nbsp; for the purpose of verification by third party service providers of the information contained in the IDs and other documents that may be collected from the Members at the time of registration, claim of vehicle or at any other time during their use of the App as may be required. Third party service providers will process data under MY V SAFETY&rsquo;s control and will be bound by the same degree of security and care as MY V SAFETY&rsquo;s under this Privacy and Data Protection Policy.<br />\r\nNotwithstanding anything contained herein or the terms &amp; conditions, MY V SAFETY hereby disclaims any liability arising from the verification of the IDs and other documents by third party service providers including but not limited to its accuracy, reliability, and originality;<br />\r\n4.1.7 &nbsp; &nbsp;for the purpose of maintaining internal quality, training and updating the Members/Users about the Services of MY V SAFETY.<br />\r\n4.2. &nbsp; &nbsp;Other than as set out above, we shall not disclose any of your personal information unless you give us permission to do so.<br />\r\n&nbsp;<br />\r\n5. Cookies<br />\r\n5.1. &nbsp; &nbsp;A cookie is a piece of data stored locally on your computer, mobile and contains information about your activities on the Internet. The information in a cookie does not contain any personally identifiable information you submit to our site.<br />\r\n5.2. &nbsp; &nbsp;On the App, we may use cookies to track users&#39; progress through the App, allowing us to make improvements based on usage data. We may also use cookies if you log in to one of our online services to enable you to remain logged in to that service. A cookie helps you get the best out of the App and helps us to provide you with a more customised service.<br />\r\n5.3. &nbsp; &nbsp;You have the ability to accept or decline cookies.<br />\r\n5.4. &nbsp; &nbsp;By you using the App we assume you are happy for us to use the cookies described above.<br />\r\n5.5. &nbsp; &nbsp;If you choose not to accept the cookies, this will not affect your access to the majority of information available on the App. However, you may not be able to make full use of our online services.<br />\r\n&nbsp;<br />\r\n6. Access to and correction of personal information<br />\r\n6.1. &nbsp; &nbsp;We will take all reasonable steps in accordance with our legal obligations to update or correct personally identifiable information in our possession that you submit via this App.<br />\r\n6.2. &nbsp; &nbsp;The Act gives you the right to access information held about you. Your right of access can be exercised in accordance with the Act. If you wish to see details of any personal information that we hold about you please contact us by way of our contact page.<br />\r\n6.3. &nbsp; &nbsp;We take all appropriate steps to protect your personally identifiable information as you transmit your information from your mobile phone to our App and to protect such information for loss, misuse, and unauthorised access, disclosure, alteration, or destruction. We may use leading technologies and encryption software to safeguard your data, and operate strict security standards to prevent any unauthorised access to it.<br />\r\n6.4. &nbsp; &nbsp;Where you use passwords, usernames, or other special access features on this site, you also have a responsibility to take reasonable steps to safeguard them.<br />\r\n&nbsp;<br />\r\n7. Other websites<br />\r\n7.1. &nbsp; &nbsp;This App may contain links and references to other websites. Please be aware that this Privacy Policy does not apply to those websites.<br />\r\n7.2. &nbsp; &nbsp;We cannot be responsible for the privacy policies and practices of sites that are not operated by us, even if you access them via the App. We recommend that you check the policy of each site you visit and contact its owner or operator if you have any concerns or questions.<br />\r\n7.3. &nbsp; &nbsp;In addition, if you came to this App via a third party site, we cannot be responsible for the privacy policies and practices of the owners or operators of that third party site and recommend that you check the policy of that third party site and contact its owner or operator if you have any concerns or questions.<br />\r\n&nbsp;<br />\r\n8. Transferring your information outside of India<br />\r\n8.1. &nbsp; &nbsp;As part of the services offered to you through the App, the information you provide to us may be transferred to, and stored at, countries outside of India. By way of example, this may happen if any of our servers are from time to time located in a country outside of India or one of our service providers is located in a country outside of India. We may also share information with other equivalent national bodies, which may be located in countries worldwide. The information provided by you may be transferred to a third party if it is necessary for the performance of any contract between us and the said third party or upon receipt of your consent to data transfer. If we transfer your information outside of India in this way, we will take steps with the aim of ensuring that your privacy rights continue to be protected as outlined in this privacy policy and in accordance with applicable laws including but not limited to Information Technology Act, 2000 and the rules framed there under.<br />\r\n8.2. &nbsp; &nbsp;By submitting your personal information to us you agree to the transfer, storing or processing of your information outside India in the manner described above.<br />\r\n&nbsp;<br />\r\n9. Notification of changes to our Privacy Policy<br />\r\nWe will post details of any changes to our Privacy Policy on the website to help ensure you are always aware of the information we collect, how we use it, and in what circumstances, if any, we share it with other parties.<br />\r\n&nbsp;<br />\r\n10. Contact us<br />\r\nIf at any time you would like to contact us with your views about our privacy practices, or with any enquiry relating to your personal information, you can do so by way of our contact page.</p>\r\n', 'PRIVACY-AND-DATA-PROTECTION-POLICY', 'cms_pages_view'),
(4, 'How it work', NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque libero diam, convallis eu lacinia sed, semper vel orci. Proin posuere pharetra nunc nec ornare. Nulla maximus vitae neque non faucibus. Nunc suscipit elementum nulla, faucibus efficitur lorem rhoncus et. Duis non elementum ipsum. Phasellus mattis sagittis ligula, nec malesuada sem gravida id. Nulla finibus molestie augue, nec blandit lectus.</p>\r\n\r\n<p>Nunc commodo finibus lorem, non luctus nibh lobortis id. Sed interdum tincidunt hendrerit. Aliquam erat volutpat. Aliquam volutpat cursus dolor, ac lacinia odio luctus eu. Mauris sit amet ullamcorper lacus. Donec facilisis orci quam, sed euismod lacus viverra id. Aliquam viverra varius aliquet. Quisque magna nisl, tempus ut elit sagittis, facilisis blandit turpis. Sed erat magna, condimentum id interdum id, dictum fringilla erat.</p>\r\n\r\n<p>Etiam ut elit a felis semper bibendum ut a lorem. Aliquam erat volutpat. Curabitur malesuada purus sit amet leo iaculis vulputate. Pellentesque varius malesuada iaculis. Nulla facilisi. Donec eget nulla elit. Vivamus quis mauris ex. Aenean a elit ut lacus posuere pretium.</p>\r\n\r\n<p>Mauris tincidunt nunc id ultrices sodales. Vestibulum vitae rutrum quam, ac imperdiet ipsum. Praesent sagittis est diam, id pretium felis ultricies sed. Cras vitae quam vel neque pretium sollicitudin. Nulla quam augue, ornare sed odio eget, sodales accumsan ex. Ut molestie enim nibh. Suspendisse pulvinar felis non purus faucibus scelerisque. Curabitur sed fermentum lectus, id mattis ante. Nam odio augue, blandit et arcu a, tempus rhoncus tellus.</p>\r\n\r\n<p>Phasellus volutpat semper sem sed tincidunt. Sed est orci, egestas eget gravida sit amet, varius sed arcu. Sed interdum eu felis sit amet sollicitudin. Nullam ut pharetra lectus. Aliquam erat volutpat. Ut quis turpis ac lectus consequat accumsan. Phasellus tincidunt enim ac commodo faucibus. Integer sit amet feugiat quam. Vestibulum in nunc finibus, posuere est in, lobortis neque. Vestibulum ut justo non nisi vehicula semper aliquam vel quam. Praesent commodo diam quis diam mollis lacinia.</p>\r\n\r\n<p>Generated 5 paragraphs, 306 words, 2061 bytes of&nbsp;<a href=\"https://www.lipsum.com/\" title=\"Lorem Ipsum\">Lorem Ipsum</a></p>\r\n', 'How-it-work', 'cms_pages_view'),
(5, 'CANCELLATION AND REFUND POLICY', NULL, '<p>1) Cancellation<br />\r\n&nbsp; &nbsp; In MY V SAFETY there is no cancellation of orders because all products in sale are related to services of my v safety application. Using those products to create and verify and registration purpose so if client wants to register an account or accounts they must purchase so no possibility of cancellation.&nbsp;<br />\r\n2) Refund&nbsp;<br />\r\n&nbsp; &nbsp; There is no cancellation so my v safety not refund any amount to clients who has purchased our products<br />\r\n3) If any failure transactions customer have to contact to their bank for those failure transactions or refunds. My v safety have no involvement in those failure transaction made by client.<br />\r\n&nbsp;</p>\r\n', 'CANCELLATION-AND-REFUND-POLICY', 'cms_pages_view');

-- --------------------------------------------------------

--
-- Table structure for table `contact_enquiry`
--

CREATE TABLE `contact_enquiry` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` text DEFAULT NULL,
  `mobile` text DEFAULT NULL,
  `email` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `created_at` text DEFAULT NULL,
  `type` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contact_enquiry`
--

INSERT INTO `contact_enquiry` (`id`, `user_id`, `name`, `mobile`, `email`, `description`, `created_at`, `type`) VALUES
(1, 1, 'test', '121212121212', 'test@gmail.com', NULL, '05-05-2021 03:49:18 PM', 'app'),
(4, 1, 'test', '121212121212', 'test@gmail.com', 'testing', '15-05-2021 11:50:21 AM', 'app'),
(5, 4, 'Pushpendra Kumar', '+919719325299', 'pushpendra@thecolourmoon.com', 'This is testing description. Please ignore this for the official purpose. ', '15-05-2021 12:33:26 PM', 'app'),
(6, 4, 'Pushpendra Kumar', '9719325299', 'pushpendra@thecolourmoon.com', 'This is the official purpose.', '15-05-2021 12:45:06 PM', 'app'),
(7, 4, 'Pushpendra', '9719325299', 'pushpendra@thecolourmoon.com', 'Testing description for the official purpose.', '15-05-2021 12:51:18 PM', 'app'),
(8, 4, 'Pushpendra', '9123456789', 'pushpendra@thecolourmoon.com', 'Test', '15-05-2021 12:56:19 PM', 'app'),
(9, 4, 'Pushpendra', '9719325299', 'pushpendra@thecolourmoon.com', 'Tdvik kvjbskbuyv vjdbsis. kbisb kbvsib', '15-05-2021 01:01:06 PM', 'app'),
(10, 4, 'Vsjv S', '72384537825', 'vsdjvks@#nvdfks.com', 'vs ksc', '15-05-2021 01:08:40 PM', 'app'),
(11, 4, 'Sdm Vks', '87368578', 'vsm dlvs ', 'vsdv', '15-05-2021 01:10:35 PM', 'app'),
(12, 4, 'Vsvs', '733738', 'xvx', 'vxcv', '15-05-2021 01:13:02 PM', 'app'),
(13, 4, 'Czcz', '292', 'vzdxvv', 'xczc', '15-05-2021 01:14:34 PM', 'app'),
(14, 8, 'Manohar', '1234567890', 'manohar@gmail.com', 'hdhfhj', '24-07-2021 11:53:44 AM', 'app'),
(15, NULL, NULL, NULL, NULL, NULL, '26-07-2021 05:27:40 PM', 'app');

-- --------------------------------------------------------

--
-- Table structure for table `contact_enquiry_website`
--

CREATE TABLE `contact_enquiry_website` (
  `id` int(11) NOT NULL,
  `lastname` text DEFAULT NULL,
  `name` text DEFAULT NULL,
  `mobile` text DEFAULT NULL,
  `email` text DEFAULT NULL,
  `message` text DEFAULT NULL,
  `created_at` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `document_types`
--

CREATE TABLE `document_types` (
  `id` int(11) NOT NULL,
  `heading` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `document_types`
--

INSERT INTO `document_types` (`id`, `heading`) VALUES
(1, 'RC'),
(2, 'Insurance'),
(3, 'Road tax');

-- --------------------------------------------------------

--
-- Table structure for table `emergency_contacts`
--

CREATE TABLE `emergency_contacts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `mobile` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emergency_contacts`
--

INSERT INTO `emergency_contacts` (`id`, `user_id`, `name`, `mobile`) VALUES
(6, 1, 'test', '126'),
(17, 4, 'Pushpendra Office Number', '+918218594954'),
(25, 4, 'Pushpendra Kumar', '+919719325299'),
(29, 8, 'Manohar', '9492824933'),
(30, 8, 'Something', '1234567890'),
(31, 14, 'Uma', '7997889489');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(11) NOT NULL,
  `heading` text DEFAULT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `heading`, `description`) VALUES
(1, 'This is the question for testing.', 'This is the answer for testing the functionality. So that we could be clarify, How it is working?'),
(2, 'How to use MyVSafety application.', 'At the very first please register to application and verify your details. Then one of our agent will contact to you.');

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `id` int(11) NOT NULL,
  `heading` text DEFAULT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`id`, `heading`, `description`) VALUES
(1, 'Learn the features of new Beautiful <span>lifest</span>y<span>le</span> app!', '<div class=\"text\">\r\n<p>Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry&#39;s stan dard dummy text ever since the 1500s.</p>\r\n\r\n<p>The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts.</p>\r\n</div>\r\n\r\n<ul class=\"list-style-one\">\r\n	<li>Identfy the most trending sessions &amp; exibitors.</li>\r\n	<li>24 hours customer supports facilites.</li>\r\n	<li>Take action to improve your event value.</li>\r\n</ul>\r\n'),
(2, 'Easy to edit', '<p>Save time and edit like a pro! Yes! you will be able to edit your application on the easy way.</p>\r\n'),
(3, 'Full protection', '<p>The app needs to be running in the background, as it tracks BLE regions.&nbsp;</p>\r\n'),
(4, 'Hi speedy app', '<p>Speedy App provides instant information on thousands of hire and buy products.</p>\r\n'),
(5, 'Easy to edit', '<p>Save time and edit like a pro! Yes! you will be able to edit your application on the easy way.</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `help_center`
--

CREATE TABLE `help_center` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` text DEFAULT NULL,
  `mobile` text DEFAULT NULL,
  `email` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `created_at` text DEFAULT NULL,
  `bug_issue` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `help_center`
--

INSERT INTO `help_center` (`id`, `user_id`, `name`, `mobile`, `email`, `description`, `created_at`, `bug_issue`) VALUES
(1, 1, 'test', '121212121212', 'test@gmail.com', 'testing', '18-05-2021 03:21:53 PM', 'Error'),
(2, NULL, NULL, NULL, NULL, NULL, '18-05-2021 03:24:02 PM', NULL),
(3, NULL, NULL, NULL, NULL, NULL, '19-05-2021 01:16:42 PM', NULL),
(4, 4, 'Pushpendra Saini', '9719325299', 'pushpendra@thecolourmoon.com', 'Testing of the Help center.', '19-05-2021 01:34:39 PM', 'Errors'),
(5, NULL, NULL, NULL, NULL, NULL, '19-05-2021 01:36:30 PM', NULL),
(6, NULL, NULL, NULL, NULL, NULL, '19-05-2021 06:50:51 PM', NULL),
(7, NULL, NULL, NULL, NULL, NULL, '19-05-2021 06:50:53 PM', NULL),
(8, 8, 'Manohar', '1234567890', 'manohar@gmail.com', 'Jagjit', '24-07-2021 11:53:06 AM', 'Errors'),
(9, NULL, NULL, NULL, NULL, NULL, '26-07-2021 05:19:57 PM', NULL),
(10, NULL, NULL, NULL, NULL, NULL, '26-07-2021 05:19:59 PM', NULL),
(11, NULL, NULL, NULL, NULL, NULL, '26-07-2021 05:25:42 PM', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `home_banners`
--

CREATE TABLE `home_banners` (
  `id` int(11) NOT NULL,
  `image` text DEFAULT NULL,
  `heading` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `home_banners`
--

INSERT INTO `home_banners` (`id`, `image`, `heading`, `description`) VALUES
(1, '2021030212184691_service_11.jpg', 'Heading', '<p>Description</p>\r\n'),
(2, '20210302165043390_service_11.jpg', NULL, NULL),
(3, '20210302170608622_commercial1.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `username` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `username`, `password`) VALUES
(1, 'admin', '34b329d9495816e93f9ff420d891b218676e8b60');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `username` varchar(250) NOT NULL,
  `ip_address` varchar(250) NOT NULL,
  `login_time` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `username`, `ip_address`, `login_time`) VALUES
(1, 'admin', '::1', '04-May-2021 06:14:18 PM'),
(2, 'admin', '::1', '05-May-2021 10:10:24 AM'),
(3, 'admin', '::1', '05-May-2021 03:42:33 PM'),
(4, 'admin', '::1', '06-May-2021 10:07:43 AM'),
(5, 'admin', '106.208.108.29', '11-May-2021 11:44:34 AM'),
(6, 'admin', '49.204.231.105', '11-May-2021 12:08:27 PM'),
(7, 'admin', '49.204.226.89', '15-May-2021 01:21:14 PM'),
(8, 'admin', '223.228.64.101', '18-May-2021 10:05:53 AM'),
(9, 'admin', '106.208.6.118', '18-May-2021 11:37:01 AM'),
(10, 'admin', '49.204.230.139', '18-May-2021 11:37:10 AM'),
(11, 'admin', '49.204.230.139', '18-May-2021 05:44:47 PM'),
(12, 'admin', '49.204.230.139', '19-May-2021 01:36:51 PM'),
(13, 'admin', '106.208.116.184', '19-May-2021 03:44:00 PM'),
(14, 'admin', '49.204.226.15', '19-May-2021 06:52:10 PM'),
(15, 'admin', '106.208.61.86', '20-May-2021 10:42:34 AM'),
(16, 'admin', '49.204.227.252', '20-May-2021 03:04:16 PM'),
(17, 'admin', '106.208.61.86', '20-May-2021 06:20:04 PM'),
(18, 'admin', '106.208.6.39', '21-May-2021 02:25:54 PM'),
(19, 'admin', '49.204.227.252', '21-May-2021 04:38:36 PM'),
(20, 'admin', '223.228.64.113', '22-May-2021 11:08:34 AM'),
(21, 'admin', '106.208.94.25', '24-May-2021 11:38:19 AM'),
(22, 'admin', '223.237.46.154', '05-Jun-2021 10:50:57 AM'),
(23, 'admin', '106.208.60.203', '07-Jun-2021 10:10:42 AM'),
(24, 'admin', '106.208.22.128', '08-Jun-2021 10:07:17 AM'),
(25, 'admin', '106.208.22.128', '08-Jun-2021 01:31:21 PM'),
(26, 'admin', '106.208.22.128', '08-Jun-2021 01:31:23 PM'),
(27, 'admin', '223.228.71.129', '06-Jul-2021 02:29:37 PM'),
(28, 'admin', '106.208.111.140', '06-Jul-2021 05:05:17 PM'),
(29, 'admin', '223.237.8.182', '07-Jul-2021 11:38:48 AM'),
(30, 'admin', '106.208.110.139', '08-Jul-2021 03:46:32 PM'),
(31, 'admin', '223.228.114.92', '09-Jul-2021 10:53:48 AM'),
(32, 'admin', '223.228.65.107', '09-Jul-2021 04:29:38 PM'),
(33, 'admin', '223.228.69.36', '13-Jul-2021 10:08:50 AM'),
(34, 'admin', '223.228.69.173', '19-Jul-2021 06:58:31 PM'),
(35, 'admin', '49.37.147.249', '19-Jul-2021 06:59:27 PM'),
(36, 'admin', '49.37.147.249', '19-Jul-2021 07:12:05 PM'),
(37, 'admin', '49.37.147.249', '19-Jul-2021 07:15:35 PM'),
(38, 'admin', '49.37.147.249', '19-Jul-2021 07:17:43 PM'),
(39, 'admin', '223.238.25.58', '20-Jul-2021 11:45:59 AM'),
(40, 'admin', '106.208.28.228', '24-Jul-2021 10:27:01 AM'),
(41, 'admin', '106.208.20.244', '26-Jul-2021 11:59:20 AM'),
(42, 'admin', '49.37.148.185', '26-Jul-2021 12:11:42 PM'),
(43, 'admin', '49.37.148.185', '27-Jul-2021 10:05:28 AM'),
(44, 'admin', '49.37.145.174', '28-Jul-2021 11:00:46 AM'),
(45, 'admin', '223.182.62.176', '28-Jul-2021 11:50:49 AM'),
(46, 'admin', '223.182.62.176', '28-Jul-2021 04:22:29 PM'),
(47, 'admin', '223.228.71.64', '29-Jul-2021 10:32:21 AM'),
(48, 'admin', '49.37.150.28', '29-Jul-2021 02:31:13 PM'),
(49, 'admin', '106.208.44.136', '29-Jul-2021 05:51:44 PM'),
(50, 'admin', '223.228.121.147', '30-Jul-2021 10:05:26 AM'),
(51, 'admin', '49.37.148.25', '30-Jul-2021 11:40:39 AM'),
(52, 'admin', '106.208.84.113', '30-Jul-2021 02:08:59 PM'),
(53, 'admin', '49.37.148.25', '30-Jul-2021 07:14:54 PM'),
(54, 'admin', '49.37.151.193', '02-Aug-2021 12:53:27 PM'),
(55, 'admin', '49.204.188.39', '02-Aug-2021 04:36:08 PM'),
(56, 'admin', '49.37.145.188', '03-Aug-2021 10:59:23 AM'),
(57, 'admin', '223.237.12.61', '06-Aug-2021 10:27:06 AM'),
(58, 'admin', '49.37.150.202', '06-Aug-2021 10:27:40 AM'),
(59, 'admin', '202.53.69.163', '09-Aug-2021 12:14:47 PM'),
(60, 'admin', '175.101.128.29', '09-Aug-2021 10:30:01 PM');

-- --------------------------------------------------------

--
-- Table structure for table `manage_uploads`
--

CREATE TABLE `manage_uploads` (
  `id` int(11) NOT NULL,
  `image` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manage_uploads`
--

INSERT INTO `manage_uploads` (`id`, `image`) VALUES
(2, '20191213124035582_slide01.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `heading` text DEFAULT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `heading`, `description`) VALUES
(1, 'admin', 'notification 1', 'Access to opportunities for career advancement and\r\nprofessional growth relies heavily on connections.'),
(2, 'admin', 'notification 2', 'Access to opportunities for career advancement and\r\nprofessional growth relies heavily on connections.'),
(3, '8', 'Vechicle service details', 'You edited vehicle service details of '),
(4, '20', 'Order Placed', 'Your order is successfully placed'),
(5, '8', 'Order Placed', 'Your order is successfully placed'),
(6, '8', 'Emergency', NULL),
(7, '13', 'New vechicle', 'You added vehicle to your list'),
(8, '13', 'Parking Problem', 'please take your vehicle from here'),
(9, '1', 'Emergency', 'please help'),
(10, '1', 'Missing info', 'do something'),
(11, '8', 'Emergency', NULL),
(12, NULL, NULL, NULL),
(13, '13', 'Emergency', 'help'),
(14, '14', 'Order Placed', 'Your order is successfully placed'),
(15, '14', 'New vechicle', 'You added vehicle to your list'),
(16, '14', 'Vechicle service details', 'You added vehicle service details to AP39DL1661'),
(17, '13', 'Vechicle service details', 'You added vehicle service details to AP35AA7932'),
(18, '14', 'Parking Problem', 'parking problem'),
(19, '8', 'Parking Problem', 'place where'),
(20, '8', 'Parking Problem', 'nothing here'),
(21, NULL, NULL, NULL),
(22, NULL, NULL, NULL),
(23, '8', 'Parking Problem', 'nakem'),
(24, '8', 'Missing info', 'bridal'),
(25, '8', 'Parking Problem', 'Heidi'),
(26, '8', 'Emergency', 'hdhfhj');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `kit_id` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `order_status` enum('PLACED','PREPARING','ONGOING','COMPLETED') DEFAULT NULL,
  `payment_id` varchar(255) DEFAULT NULL,
  `payment_status` enum('PAID','PENDING') DEFAULT NULL,
  `kit_name` varchar(255) DEFAULT NULL,
  `actual_price` int(11) DEFAULT NULL,
  `discount_percentage` int(11) DEFAULT NULL,
  `discount_price` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `name` text DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `house_no` text DEFAULT NULL,
  `area` text DEFAULT NULL,
  `state` text DEFAULT NULL,
  `pin_code` text DEFAULT NULL,
  `payment_done` text DEFAULT NULL,
  `order_prepared_date` varchar(255) DEFAULT NULL,
  `order_ongoing_date` varchar(255) DEFAULT NULL,
  `order_completed_date` varchar(255) DEFAULT NULL,
  `referral_code` varchar(255) DEFAULT NULL,
  `referred_by_user_id` int(11) DEFAULT NULL,
  `qr_code` varchar(255) NOT NULL,
  `qr_code_id` int(11) DEFAULT NULL,
  `qr_code_assigned_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `kit_id`, `order_id`, `order_status`, `payment_id`, `payment_status`, `kit_name`, `actual_price`, `discount_percentage`, `discount_price`, `address_id`, `name`, `mobile`, `house_no`, `area`, `state`, `pin_code`, `payment_done`, `order_prepared_date`, `order_ongoing_date`, `order_completed_date`, `referral_code`, `referred_by_user_id`, `qr_code`, `qr_code_id`, `qr_code_assigned_at`) VALUES
(1, 4, '2', '253164', 'PREPARING', 'DUMMY6170', 'PAID', NULL, 1000, 10, 900, 27, 'PK', '+919719325299', '401-B Happy Home 3rd', 'Akkayapalem', 'AP', '530016', '20-05-2021 07:02:41 PM', '21-05-2021 06:55:42 PM', NULL, '21-05-2021 06:55:42 PM', NULL, NULL, '', NULL, ''),
(2, 4, '2', '067359', 'PLACED', 'DUMMY9998', 'PAID', NULL, 1000, 10, 900, 32, 'ABC Bhardwarz Chatrazi Aabul Pakir Abdul Kalam', '+917123456789', '1232/212-BHU Bharat ', 'Dwarka Nagar', 'AP', '530016', '20-05-2021 07:02:59 PM', NULL, NULL, NULL, NULL, NULL, '', NULL, ''),
(3, 1, '1', '418963', 'PREPARING', '12345', 'PAID', NULL, 1000, 10, 900, 1, 'test', '1212121212', '123', 'wgl', 'tg', '506005', '20-05-2021 07:03:18 PM', '21-05-2021 06:54:55 PM', NULL, '21-05-2021 06:54:55 PM', NULL, NULL, '', NULL, ''),
(4, 1, '1', '834610', 'PLACED', '12345', 'PAID', NULL, 1000, 10, 900, 1, 'test', '1212121212', '123', 'wgl', 'tg', '506005', '20-05-2021 07:03:23 PM', NULL, NULL, NULL, NULL, NULL, 'TV76DMJK', 1, '13-07-2021 11:45:22 AM'),
(5, 4, '4', '875921', 'COMPLETED', 'DUMMY3729', 'PAID', NULL, 1500, 0, 1500, 32, 'ABC Bhardwarz Chatrazi Aabul Pakir Abdul Kalam', '+917123456789', '1232/212-BHU Bharat ', 'Dwarka Nagar', 'AP', '530016', '20-05-2021 07:03:28 PM', '21-05-2021 04:38:57 PM', '21-05-2021 04:43:47 PM', '21-05-2021 04:38:57 PM', NULL, NULL, '', NULL, ''),
(6, 4, '3', '467215', 'PLACED', 'DUMMY9998', 'PAID', NULL, 1200, 0, 1200, 32, 'ABC Bhardwarz Chatrazi Aabul Pakir Abdul Kalam', '+917123456789', '1232/212-BHU Bharat ', 'Dwarka Nagar', 'AP', '530016', '21-05-2021 10:29:37 AM', NULL, NULL, NULL, NULL, NULL, '', NULL, ''),
(7, 1, '3', '264710', 'ONGOING', 'DUMMY6684', 'PAID', 'MyVSafety Premium ', 1200, 0, 1200, 33, 'Bdbd', '59595', 'Brbf', 'Fbrbr', 'Brbr', '92292', '21-05-2021 03:45:33 PM', '21-05-2021 04:39:40 PM', '21-05-2021 04:43:24 PM', '21-05-2021 04:39:40 PM', NULL, NULL, '', NULL, ''),
(41, 5, '1', '510742', 'PLACED', '1', 'PAID', 'MyVSafety QR Kit 2', 1000, 10, 900, 1, 'test', '1212121212', '123', 'wgl', 'tg', '506005', '07-06-2021 01:41:23 PM', NULL, NULL, NULL, 'A1B2C3', 1, '', NULL, ''),
(42, 4, '2', '291403', 'PLACED', 'DUMMY6611', 'PAID', 'MyVSafety QR Kit 1', 1000, 10, 900, 32, 'ABC Bhardwarz Chatrazi Aabul Pakir Abdul Kalam', '+917123456789', '1232/212-BHU Bharat ', 'Dwarka Nagar', 'AP', '530016', '07-06-2021 02:51:14 PM', NULL, NULL, NULL, NULL, NULL, '', NULL, ''),
(43, 8, '4', '783029', 'PLACED', 'DUMMY8940', 'PAID', 'MyVSafety Premium XX', 1500, 0, 1500, 34, 'Manohar', '1234567890', '8-10', 'Pendurthi', 'Visakhapatnam', '531173', '26-07-2021 05:15:17 PM', NULL, NULL, NULL, NULL, NULL, '', NULL, ''),
(44, 8, '2', '570169', 'PLACED', 'DUMMY2207', 'PAID', 'MyVSafety QR Kit 1', 1000, 10, 900, 34, 'Manohar', '1234567890', '8-10', 'Pendurthi', 'Visakhapatnam', '531173', '26-07-2021 06:33:22 PM', NULL, NULL, NULL, NULL, NULL, '', NULL, ''),
(45, 8, '5', '743056', 'PLACED', 'DUMMY3936', 'PAID', 'MyVSafety Private Security xx', 2000, 11, 1780, 34, 'Manohar', '1234567890', '8-10', 'Pendurthi', 'Visakhapatnam', '531173', '26-07-2021 06:36:48 PM', NULL, NULL, NULL, NULL, NULL, '', NULL, ''),
(46, 8, '2', '139470', 'PLACED', 'DUMMY8652', 'PAID', 'MyVSafety QR Kit 1', 1000, 10, 900, 34, 'Manohar', '1234567890', '8-10', 'Pendurthi', 'Visakhapatnam', '531173', '27-07-2021 01:12:16 PM', NULL, NULL, NULL, NULL, NULL, '', NULL, ''),
(47, 14, '2', '694310', 'PLACED', 'DUMMY1338', 'PAID', 'MyVSafety QR Kit 1', 1000, 10, 900, 36, 'Kosuri Srinivasa Rao', '9959646419', '2-136/A', 'Palacharla, Rajanagaram Mandal, East Godavari District.', 'Andhra Pradesh', '533102', '02-08-2021 11:51:47 AM', NULL, NULL, NULL, NULL, NULL, '', NULL, ''),
(48, 20, '3', '170289', 'PLACED', 'DUMMY7961', 'PAID', 'MyVSafety Premium ', 1200, 0, 1200, 37, 'Manu', '9866526642', '174', 'Prasanthi Nagar', 'Andhra Pradesh', '531173', '03-08-2021 03:20:06 PM', NULL, NULL, NULL, NULL, NULL, '', NULL, ''),
(49, 8, '3', '107425', 'PLACED', 'DUMMY1803', 'PAID', 'MyVSafety Premium ', 1200, 0, 1200, 39, 'Manu', '12345687890', 'Jgb', 'Mm Cizn', 'Abstracts Check', '1234567', '06-08-2021 05:56:59 PM', NULL, NULL, NULL, NULL, NULL, '', NULL, ''),
(50, 14, '2', '197820', 'PLACED', 'DUMMY2661', 'PAID', 'MyVSafety QR Kit 1', 1000, 10, 900, 36, 'Kosuri Srinivasa Rao', '9959646419', '2-136/A', 'Palacharla, Rajanagaram Mandal, East Godavari District.', 'Andhra Pradesh', '533102', '09-08-2021 01:36:14 PM', NULL, NULL, NULL, NULL, NULL, '', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `qr_code_genereation`
--

CREATE TABLE `qr_code_genereation` (
  `id` int(11) NOT NULL,
  `qr_code` varchar(255) DEFAULT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  `strtotime_value` varchar(255) DEFAULT NULL,
  `status` enum('FRESH','ACTIVATED','VEHICLE_ASSIGNED','VEHICLE_NOT_ASSIGNED','ASSIGNED') DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `assigned_at` varchar(255) DEFAULT NULL,
  `vehicle_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qr_code_genereation`
--

INSERT INTO `qr_code_genereation` (`id`, `qr_code`, `created_at`, `strtotime_value`, `status`, `user_id`, `assigned_at`, `vehicle_id`) VALUES
(1, 'TV76DMJK', '07-06-2021 05:13:40 PM', '2021-06-07', 'VEHICLE_NOT_ASSIGNED', 1, '13-07-2021 11:52:40 AM', ''),
(2, 'LTJYBPQS', '08-06-2021 05:13:40 PM', '2021-06-08', 'VEHICLE_ASSIGNED', 1, '19-07-2021 03:21:14 PM', '2'),
(3, 'G6CT18EV', '08-06-2021 05:13:40 PM', '2021-06-08', 'VEHICLE_NOT_ASSIGNED', 8, NULL, ''),
(4, 'KJUM1BGX', '08-06-2021 05:13:40 PM', '2021-06-08', 'VEHICLE_NOT_ASSIGNED', 8, NULL, ''),
(5, '9X3HCR75', '08-06-2021 05:13:40 PM', '2021-06-08', 'VEHICLE_NOT_ASSIGNED', 8, NULL, ''),
(6, 'XQOPZFGS', '08-06-2021 05:13:40 PM', '2021-06-08', 'VEHICLE_NOT_ASSIGNED', 8, NULL, ''),
(7, 'VMYXN0FE', '19-07-2021 07:04:59 PM', '2021-07-19', 'VEHICLE_NOT_ASSIGNED', 8, NULL, ''),
(8, 'WYF10JHL', '26-07-2021 12:25:38 PM', '2021-07-26', 'VEHICLE_NOT_ASSIGNED', 8, NULL, ''),
(9, '6XZHY5BV', '26-07-2021 12:30:42 PM', '2021-07-26', 'VEHICLE_NOT_ASSIGNED', 8, NULL, ''),
(10, 'L2Z3QGJS', '26-07-2021 12:32:59 PM', '2021-07-26', 'VEHICLE_NOT_ASSIGNED', 8, NULL, ''),
(11, '5OBSJX9E', '26-07-2021 12:32:59 PM', '2021-07-26', 'VEHICLE_NOT_ASSIGNED', 8, NULL, ''),
(12, '9U1C4RA5', '26-07-2021 12:39:11 PM', '2021-07-26', 'VEHICLE_NOT_ASSIGNED', 8, NULL, ''),
(13, 'XO3CGS16', '26-07-2021 12:39:11 PM', '2021-07-26', 'VEHICLE_NOT_ASSIGNED', 8, NULL, ''),
(14, 'AMDY2OHT', '26-07-2021 12:39:11 PM', '2021-07-26', 'VEHICLE_NOT_ASSIGNED', 8, NULL, ''),
(15, 'UF65LJXO', '26-07-2021 12:39:11 PM', '2021-07-26', 'VEHICLE_NOT_ASSIGNED', 1, NULL, ''),
(16, 'UY9L4ZVC', '26-07-2021 12:39:11 PM', '2021-07-26', 'VEHICLE_NOT_ASSIGNED', 8, NULL, ''),
(17, '5FT8MWV3', '26-07-2021 01:12:57 PM', '2021-07-26', 'VEHICLE_ASSIGNED', NULL, '26-07-2021 02:29:28 PM', ''),
(18, 'E8L7GHIS', '26-07-2021 01:12:57 PM', '2021-07-26', 'VEHICLE_NOT_ASSIGNED', 8, NULL, ''),
(19, 'J85RGKYD', '26-07-2021 01:12:57 PM', '2021-07-26', 'VEHICLE_ASSIGNED', 8, '26-07-2021 02:33:41 PM', '19'),
(20, 'CZ5M3UQJ', '26-07-2021 01:12:57 PM', '2021-07-26', 'VEHICLE_NOT_ASSIGNED', 8, NULL, ''),
(21, '4P31729D', '26-07-2021 01:12:57 PM', '2021-07-26', 'VEHICLE_NOT_ASSIGNED', 8, NULL, ''),
(22, 'Q0K3V7DS', '26-07-2021 02:45:32 PM', '2021-07-26', 'VEHICLE_NOT_ASSIGNED', 8, NULL, ''),
(23, 'FKRTA8QN', '26-07-2021 02:45:32 PM', '2021-07-26', 'VEHICLE_NOT_ASSIGNED', 8, NULL, ''),
(24, '5OY0LC7B', '26-07-2021 02:45:32 PM', '2021-07-26', 'VEHICLE_NOT_ASSIGNED', 8, NULL, ''),
(25, 'YFOKP6M2', '26-07-2021 02:45:32 PM', '2021-07-26', 'VEHICLE_NOT_ASSIGNED', 8, NULL, ''),
(26, 'D12IRSYM', '26-07-2021 02:45:32 PM', '2021-07-26', 'VEHICLE_NOT_ASSIGNED', 8, NULL, ''),
(27, 'EQADK1JO', '26-07-2021 03:39:23 PM', '2021-07-26', 'VEHICLE_ASSIGNED', 9, '28-07-2021 11:16:20 AM', '30'),
(28, 'W97AH1Q4', '26-07-2021 03:39:23 PM', '2021-07-26', 'VEHICLE_NOT_ASSIGNED', 8, NULL, ''),
(29, 'N634J9AL', '26-07-2021 03:39:23 PM', '2021-07-26', 'VEHICLE_ASSIGNED', 8, '26-07-2021 04:32:25 PM', '28'),
(30, 'U0BH9CV7', '26-07-2021 03:39:23 PM', '2021-07-26', 'VEHICLE_ASSIGNED', 8, '26-07-2021 03:44:43 PM', '27'),
(31, '80PMRLZ1', '26-07-2021 03:39:23 PM', '2021-07-26', 'VEHICLE_ASSIGNED', 8, '26-07-2021 03:40:52 PM', '26'),
(32, 'GFCX52JR', '30-07-2021 10:36:51 AM', '2021-07-30', 'VEHICLE_ASSIGNED', 1, '30-07-2021 10:37:35 AM', '31'),
(33, 'QG851U0T', '02-08-2021 12:53:44 PM', '2021-08-02', 'VEHICLE_ASSIGNED', 14, '09-08-2021 03:28:11 PM', '33'),
(34, 'WUFN2AGX', '02-08-2021 12:53:44 PM', '2021-08-02', 'VEHICLE_ASSIGNED', 13, '09-08-2021 12:16:37 PM', '32'),
(35, '98CQRVL0', '09-08-2021 12:17:11 PM', '2021-08-09', 'FRESH', NULL, NULL, ''),
(36, 'C3YSF4Q8', '09-08-2021 12:17:11 PM', '2021-08-09', 'FRESH', NULL, NULL, ''),
(37, 'P9J4C61D', '09-08-2021 12:17:11 PM', '2021-08-09', 'FRESH', NULL, NULL, ''),
(38, '1FL0AEQI', '09-08-2021 12:17:11 PM', '2021-08-09', 'FRESH', NULL, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `qr_kits`
--

CREATE TABLE `qr_kits` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `image` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `actual_price` text DEFAULT NULL,
  `discount_percentage` int(11) DEFAULT 0,
  `discount_price` text DEFAULT NULL,
  `deal_of_the_day` enum('YES','NO') DEFAULT 'NO' COMMENT 'Yes = available for deal of the day. And no for not available. '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qr_kits`
--

INSERT INTO `qr_kits` (`id`, `name`, `image`, `description`, `actual_price`, `discount_percentage`, `discount_price`, `deal_of_the_day`) VALUES
(1, 'MyVSafety QR Kit 2', '20210518061238108_qr_code_2.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1000', 10, '900', 'NO'),
(2, 'MyVSafety QR Kit 1', '20210518061227619_qr_code.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1000', 10, '900', 'YES'),
(3, 'MyVSafety Premium ', '20210518061927917_qr_code_4.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1200', 0, '1200', 'YES'),
(4, 'MyVSafety Premium XX', '20210518062107263_qr_code_5.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1500', 0, '1500', 'YES'),
(5, 'MyVSafety Private Security xx', '20210518062807224_qr_code_6.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2000', 11, '1780', 'NO'),
(6, 'MyVSafety V2 Controll', '20210518063627271_qr_code_8.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2500', 12, '2200', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `qr_send_details`
--

CREATE TABLE `qr_send_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `vehicle_num` varchar(255) NOT NULL,
  `qr_code` varchar(255) NOT NULL,
  `type` enum('Parking Problem','Emergency','Missing info') NOT NULL,
  `image1` varchar(255) NOT NULL,
  `image2` varchar(255) NOT NULL,
  `image3` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `posted_on` varchar(255) NOT NULL,
  `str_posted_on` varchar(255) DEFAULT NULL,
  `to_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qr_send_details`
--

INSERT INTO `qr_send_details` (`id`, `user_id`, `vehicle_num`, `qr_code`, `type`, `image1`, `image2`, `image3`, `description`, `posted_on`, `str_posted_on`, `to_user_id`) VALUES
(1, 2, '123', 'TV76DMJK', 'Emergency', 'mokeup_17.jpg', 'mokeup_27.jpg', 'mokeup_37.jpg', NULL, '27-07-2021 12:12:19 PM', NULL, 1),
(2, 2, '123', 'TV76DMJK', 'Emergency', '', '', '', '', '27-07-2021 02:28:46 PM', NULL, 1),
(3, 2, '123', 'TV76DMJK', 'Emergency', 'mokeup_18.jpg', 'mokeup_28.jpg', 'mokeup_38.jpg', '', '27-07-2021 02:40:23 PM', NULL, 1),
(5, 8, 'AP31GA0642', '80PMRLZ1', 'Parking Problem', '', '', '', 'dhhdhdh den', '27-07-2021 04:22:26 PM', NULL, 8),
(6, 8, 'AP12CD1538', 'U0BH9CV7', 'Emergency', 'img_20210727_162530996.jpg', 'img_20210727_162543848.jpg', 'img_20210727_162557584.jpg', 'Donna', '27-07-2021 04:26:04 PM', NULL, 8),
(7, 8, '123', 'J85RGKYD', 'Emergency', 'img_20210728_110308157.jpg', 'img_20210728_110322299.jpg', 'img_20210728_110332724.jpg', 'northside', '28-07-2021 11:03:41 AM', NULL, 8),
(8, 9, 'AP31GA0642', '80PMRLZ1', 'Emergency', 'img_20210728_110844489.jpg', 'img_20210728_110855577.jpg', 'img_20210728_110904703.jpg', 'hdhfhj', '28-07-2021 11:09:11 AM', NULL, 8),
(9, 8, 'AP24CD8765', 'EQADK1JO', 'Missing info', 'img_20210728_111950157.jpg', 'img_20210728_111958900.jpg', 'img_20210728_112012279.jpg', 'problem', '28-07-2021 11:20:22 AM', NULL, 9),
(14, 8, 'AP31GA0642', '80PMRLZ1', 'Parking Problem', 'img_20210730_111049754.jpg', '', '', '', '30-07-2021 11:10:53 AM', NULL, 8),
(22, 1, 'AP31GA0642', '80PMRLZ1', 'Emergency', 'mokeup_120.jpg', 'mokeup_220.jpg', 'mokeup_320.jpg', NULL, '05-08-2021 06:06:06 PM', '1628339766', 8),
(23, 8, 'AP35AA7932', 'WUFN2AGX', 'Parking Problem', 'img_20210809_121914677.jpg', 'img_20210809_121924985.jpg', 'img_20210809_121944733.jpg', 'please take your vehicle from here', '09-08-2021 12:20:06 PM', '1628578206', 13),
(24, 8, '123', 'GFCX52JR', 'Emergency', 'img_20210809_123859390.jpg', 'img_20210809_123908695.jpg', 'img_20210809_123919428.jpg', 'please help', '09-08-2021 12:39:36 PM', '1628579376', 1),
(25, 8, '123', 'GFCX52JR', 'Missing info', 'img_20210809_124427600.jpg', 'img_20210809_124437052.jpg', 'img_20210809_124447610.jpg', 'do something', '09-08-2021 12:44:51 PM', '1628579691', 1),
(26, 1, 'AP31GA0642', '80PMRLZ1', 'Emergency', 'mokeup_121.jpg', 'mokeup_221.jpg', 'mokeup_321.jpg', NULL, '09-08-2021 12:48:54 PM', '1628579934', 8),
(27, 8, 'AP35AA7932', 'WUFN2AGX', 'Emergency', 'img_20210809_125255972.jpg', 'img_20210809_125305589.jpg', 'img_20210809_125316448.jpg', 'help', '09-08-2021 12:53:21 PM', '1628580201', 13),
(28, 23, 'AP39DL1661', 'QG851U0T', 'Parking Problem', 'img_20210809_210940981.jpg', 'img_20210809_210948843.jpg', 'img_20210809_210954643.jpg', 'parking problem', '09-08-2021 09:10:12 PM', '1628610012', 14),
(29, 8, 'AP31GA0642', '80PMRLZ1', 'Parking Problem', 'img_20210811_061801035.jpg', 'img_20210811_061815674.jpg', 'img_20210811_061828119.jpg', 'place where', '11-08-2021 06:18:42 AM', '1628729322', 8),
(30, 8, 'AP31GA0642', '80PMRLZ1', 'Parking Problem', 'img_20210811_070508016.jpg', 'img_20210811_070524741.jpg', 'img_20210811_070535947.jpg', 'nothing here', '11-08-2021 07:05:42 AM', '1628732142', 8),
(31, 8, 'AP31GA0642', '80PMRLZ1', 'Parking Problem', 'img_20210811_072924093.jpg', 'img_20210811_072938865.jpg', 'img_20210811_072945544.jpg', 'nakem', '11-08-2021 07:30:00 AM', '1628733600', 8),
(32, 8, 'AP31GA0642', '80PMRLZ1', 'Missing info', 'img_20210811_073357932.jpg', 'img_20210811_073402580.jpg', 'img_20210811_073420113.jpg', 'bridal', '11-08-2021 07:34:32 AM', '1628733872', 8),
(33, 8, 'AP31GA0642', '80PMRLZ1', 'Parking Problem', 'img_20210811_073645384.jpg', 'img_20210811_073651879.jpg', 'img_20210811_073656218.jpg', 'Heidi', '11-08-2021 07:37:01 AM', '1628734021', 8),
(34, 8, 'AP31GA0642', '80PMRLZ1', 'Emergency', 'img_20210811_101355451.jpg', 'img_20210811_101411877.jpg', 'img_20210811_101420731.jpg', 'hdhfhj', '11-08-2021 10:14:33 AM', '1628743473', 8);

-- --------------------------------------------------------

--
-- Table structure for table `referral_points`
--

CREATE TABLE `referral_points` (
  `id` int(11) NOT NULL,
  `referral_code_used_by` int(11) NOT NULL,
  `referral_code` varchar(255) NOT NULL,
  `referral_code_by` int(11) NOT NULL,
  `points_added` int(11) NOT NULL,
  `added_on` varchar(255) NOT NULL,
  `withdraw_status` enum('YES','NO') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `referral_points`
--

INSERT INTO `referral_points` (`id`, `referral_code_used_by`, `referral_code`, `referral_code_by`, `points_added`, `added_on`, `withdraw_status`) VALUES
(1, 5, 'A1B2C3', 1, 100, '07-06-2021 11:09:21 AM', 'YES'),
(2, 5, 'A1B2C3', 1, 100, '07-06-2021 01:41:23 PM', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `referral_renewal`
--

CREATE TABLE `referral_renewal` (
  `id` int(11) NOT NULL,
  `referral_points` int(11) DEFAULT NULL,
  `renewal` enum('YES','NO') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `referral_renewal`
--

INSERT INTO `referral_renewal` (`id`, `referral_points`, `renewal`) VALUES
(1, 100, NULL),
(2, NULL, 'YES');

-- --------------------------------------------------------

--
-- Table structure for table `seo_tags`
--

CREATE TABLE `seo_tags` (
  `id` int(11) NOT NULL,
  `heading` varchar(255) NOT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seo_tags`
--

INSERT INTO `seo_tags` (`id`, `heading`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(1, 'About Us', 'GLP Pharma Standards', '', ''),
(2, 'Services', '', '', ''),
(3, 'Products', '', '', ''),
(4, 'Medicines', '', '', ''),
(5, 'Gallery', '', '', ''),
(6, 'Contact Us', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `services_types`
--

CREATE TABLE `services_types` (
  `id` int(11) NOT NULL,
  `heading` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services_types`
--

INSERT INTO `services_types` (`id`, `heading`) VALUES
(2, 'Cleaning'),
(3, 'Engine oil changing'),
(4, 'tyres changing');

-- --------------------------------------------------------

--
-- Table structure for table `site_details`
--

CREATE TABLE `site_details` (
  `id` int(11) NOT NULL,
  `site_name` varchar(250) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `site_number` varchar(250) NOT NULL,
  `site_email` varchar(250) NOT NULL,
  `from_email` varchar(250) NOT NULL,
  `forgot_email` varchar(250) NOT NULL,
  `address` text NOT NULL,
  `phone_number` text DEFAULT NULL,
  `email_id` text DEFAULT NULL,
  `site_logo` text DEFAULT NULL,
  `site_favicon` text DEFAULT NULL,
  `footer_logo` text DEFAULT NULL,
  `footer_context` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_details`
--

INSERT INTO `site_details` (`id`, `site_name`, `site_number`, `site_email`, `from_email`, `forgot_email`, `address`, `phone_number`, `email_id`, `site_logo`, `site_favicon`, `footer_logo`, `footer_context`) VALUES
(1, 'MyVsafety', '1234567890', 'info@myvsafety.com', 'mysafety@gmail.com', 'info@myvsafety.com', 'address here', '1234567890', 'info@myvsafety.com', '2021050607141044_logo.png', '20210506071410441_favicon.png', '20210506071410791_logo.png', 'Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry\'s stan dard dummy text ever since the 1500s.');

-- --------------------------------------------------------

--
-- Table structure for table `social_links`
--

CREATE TABLE `social_links` (
  `id` int(11) NOT NULL,
  `facebook` varchar(250) NOT NULL,
  `twitter` varchar(250) NOT NULL,
  `youtube` varchar(250) NOT NULL,
  `skype` varchar(250) NOT NULL,
  `linkedin` varchar(250) NOT NULL,
  `google` varchar(255) NOT NULL,
  `meta_title` varchar(250) NOT NULL,
  `meta_keywords` varchar(1500) NOT NULL,
  `meta_description` varchar(1500) NOT NULL,
  `police_challan_link1` varchar(255) DEFAULT NULL,
  `police_challan_link2` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_links`
--

INSERT INTO `social_links` (`id`, `facebook`, `twitter`, `youtube`, `skype`, `linkedin`, `google`, `meta_title`, `meta_keywords`, `meta_description`, `police_challan_link1`, `police_challan_link2`) VALUES
(1, 'https://www.facebook.com/thecolormoon/', 'https://twitter.com/login', 'https://www.', 'https://www.', 'https://', 'https://plus.google.com/', 'My V Safety', 'My V Safety', 'My V Safety', 'www.apechallan.org ', 'www.echallan.tspolice.gov.in ');

-- --------------------------------------------------------

--
-- Table structure for table `steps`
--

CREATE TABLE `steps` (
  `id` int(11) NOT NULL,
  `heading` text DEFAULT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `steps`
--

INSERT INTO `steps` (`id`, `heading`, `description`) VALUES
(3, 'Enjoy the app!', 'Lorem Ipsum is simply dummy text of the printing setting industry. Lorem Ipsum has been industry\'s dard dummy text ever since the 1500s.'),
(4, 'Login your account', 'Lorem Ipsum is simply dummy text of the printing setting industry. Lorem Ipsum has been industry\'s dard dummy text ever since the 1500s.'),
(5, 'Download as a free trial!', 'Lorem Ipsum is simply dummy text of the printing setting industry. Lorem Ipsum has been industry\'s dard dummy text ever since the 1500s.\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `steps_images`
--

CREATE TABLE `steps_images` (
  `id` int(11) NOT NULL,
  `image` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `steps_images`
--

INSERT INTO `steps_images` (`id`, `image`) VALUES
(1, 'mokeup-1.jpg'),
(2, 'mokeup-2.jpg'),
(3, 'mokeup-3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `withdraw_status` enum('Pending','Ongoing','Successfull') NOT NULL,
  `generate_withdraw_id` int(11) NOT NULL,
  `withdraw_time` varchar(255) NOT NULL,
  `withdraw_status_updated_on` varchar(255) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `user_id`, `bank_id`, `amount`, `withdraw_status`, `generate_withdraw_id`, `withdraw_time`, `withdraw_status_updated_on`, `message`) VALUES
(4, 4, 4, 300, 'Ongoing', 39856, '07-06-2021 10:27:03 AM', '07-06-2021 10:39:27 AM', 'We will process your request'),
(7, 4, 4, 0, 'Pending', 906134, '07-06-2021 01:35:12 PM', '', ''),
(8, 4, 4, 0, 'Pending', 367890, '07-06-2021 01:36:33 PM', '', ''),
(9, 4, 4, 0, 'Pending', 502386, '07-06-2021 01:37:32 PM', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

CREATE TABLE `user_accounts` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `mobile` text DEFAULT NULL,
  `email` text DEFAULT NULL,
  `token` text DEFAULT NULL,
  `created_at` text DEFAULT NULL,
  `reg_no` text DEFAULT NULL,
  `referral_code` varchar(255) DEFAULT NULL,
  `balance_referral_points` int(11) NOT NULL DEFAULT 0,
  `total_referral_points` int(11) NOT NULL DEFAULT 0,
  `otp` text DEFAULT NULL,
  `otp_status` text DEFAULT NULL,
  `blood_group` text NOT NULL,
  `bp` text NOT NULL,
  `heart_problem` text NOT NULL,
  `thyroid` text NOT NULL,
  `other_health_problem` text NOT NULL,
  `using_medicines` text NOT NULL,
  `carrying_medicines` text NOT NULL,
  `account_status` enum('ACTIVATED','NOT ACTIVATED') NOT NULL,
  `qr_code_status` enum('0','1') NOT NULL DEFAULT '0',
  `activated_on` varchar(255) NOT NULL,
  `notification_count` int(11) NOT NULL DEFAULT 0,
  `profile_pic` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`id`, `name`, `mobile`, `email`, `token`, `created_at`, `reg_no`, `referral_code`, `balance_referral_points`, `total_referral_points`, `otp`, `otp_status`, `blood_group`, `bp`, `heart_problem`, `thyroid`, `other_health_problem`, `using_medicines`, `carrying_medicines`, `account_status`, `qr_code_status`, `activated_on`, `notification_count`, `profile_pic`) VALUES
(1, 'sindhu', '9618820971', NULL, '', '05-05-2021 10:50:53 AM', '098762', 'A1B2C3', 100, 100, '6573', 'Verified', '', '', '', '', '', '', '', 'ACTIVATED', '1', '30-07-2021 10:37:04 AM', 2, 'mokeup_3.jpg'),
(3, '', '9719325299', NULL, NULL, '08-05-2021 11:18:04 AM', '197253', '123XYZ', 0, 0, '8709', 'Verified', '', '', '', '', '', '', '', 'NOT ACTIVATED', '0', '', 5, ''),
(4, 'Pushpendra Saini', '1234567890', NULL, NULL, '08-05-2021 11:55:33 AM', '457938', 'MVS15VR', 0, 0, '5426', 'Verified', 'B+', 'No', 'No', 'Yes', '', 'Nothing', 'N/A', 'NOT ACTIVATED', '0', '', 5, ''),
(5, '', '12345678900', NULL, NULL, '08-05-2021 12:31:55 PM', '523871', '2345ABC', 0, 0, '7942', 'Verified', '', '', '', '', '', '', '', 'ACTIVATED', '1', '08-06-2021 03:29:16 PM', 5, ''),
(8, 'Manohar1', '9492824933', NULL, 'f-UUPBuyTCu_VQZ6ctbFLG:APA91bEQ78qiisUyGS9IRCEk_gcJoaM_Il0ixL4smj3cBrkPPNoQybOcKfewmfkuDRtj7GxRjSShnRWJbbM5B1IczAaH_Z3WoW7k0ite5ESTZ7fVrM5H_lLi5T3eSltRut5Xy8lLChQv', '18-07-2021 02:43:17 PM', '316894', 'J7MTL8DA', 0, 0, '4170', 'Not Verified', 'O+', 'Yes', 'Yes', 'No', 'Yes Northern Is Here', 'Jdhosvns', 'Yes Iam Taking Medicine', 'ACTIVATED', '1', '26-07-2021 06:01:37 PM', 6, 'img_20210810_211421778.jpg'),
(9, '', '6281781884', NULL, NULL, '28-07-2021 11:07:51 AM', '470185', '1AZFKVN5', 0, 0, '0532', 'Verified', '', '', '', '', '', '', '', 'ACTIVATED', '1', '28-07-2021 11:15:38 AM', 0, ''),
(10, '', '8499052020', NULL, NULL, '29-07-2021 02:59:11 PM', '301742', 'Q63PKBMD', 0, 0, '6148', 'Verified', '', '', '', '', '', '', '', 'NOT ACTIVATED', '0', '', 0, ''),
(11, '', '123453789', NULL, NULL, '30-07-2021 12:27:09 PM', '396028', 'SUQ07XWM', 0, 0, '8035', 'Verified', '', '', '', '', '', '', '', 'NOT ACTIVATED', '0', '', 0, ''),
(12, '', '6504992804', NULL, NULL, '30-07-2021 07:28:37 PM', '584721', 'R5WJDS0L', 0, 0, '0682', 'Not Verified', '', '', '', '', '', '', '', 'NOT ACTIVATED', '0', '', 0, ''),
(13, 'Kishore', '8125505088', NULL, 'ei7gErmTR26L-9jYP1i7w5:APA91bGUdWhBOF6XV2dUaqX6dvANhzPQ8hjnFdrr6l8lP_VKL1trDjPuIbU9vejHcV94RRT4G1RGv3u4Dn8IH5o21e48rEHlDQJsS6QZuPyxTZ_9TnYkjtmInSwMiHqr_ymMG9uJgBRY', '02-08-2021 11:22:41 AM', '523468', 'J18N9SCR', 0, 0, '0391', 'Not Verified', 'B+Ve', '', '', '', 'No', 'No', 'No', 'ACTIVATED', '1', '09-08-2021 12:15:35 PM', 0, ''),
(14, 'Kosuri Srinivasa Rao', '9959646419', NULL, 'dYaNHd2iSMiFIpFCywnx1D:APA91bF5gJZnM_2QEqathqeclf5rzVMAew2bDf8f67e9XJzj_qV8VWTx-q-V3nZk20-XYoqLU06mIOM0-WX3Y0t7E493X8dqA0UB2lichl8CAhEPNzK-6OgJJZ0MlLrVs59my7suooI3', '02-08-2021 11:32:02 AM', '814735', '2PNX17Y5', 0, 0, '8746', 'Not Verified', 'B-ve', 'Yes', 'No', 'No', 'No', 'Telmisartan 40mg', 'No Medicine Carrying', 'ACTIVATED', '1', '09-08-2021 03:25:26 PM', 0, 'img_20210809_205712996.jpg'),
(15, '', '7013449090', NULL, 'f-3_cIaqTfCxtTWSVejobu:APA91bHrQorKfqvzr49FrdkCAs492Bhd4HdSdbfDSyc0Wp0f4pnBaa4htFJYyJumZ_hNIdwwZTs1Sp71TnogFBG19yp9ntOZGa_4qAdMfzl4u9rvL-3ec5V3uM_oEtnxEMWgQHvM6T5f', '02-08-2021 12:21:20 PM', '039248', '1AGXBTP8', 0, 0, '9260', 'Not Verified', '', '', '', '', '', '', '', 'NOT ACTIVATED', '0', '', 0, ''),
(16, '', '9505560361', NULL, 'cRCMrJWeTUme7bpi3iborS:APA91bGoCcXGdNhHwhOUX6qchp4bZhndIvuWFtgR1gPF2z5gGsBuPG1QtjS6Pfd5-hMMrc_z0n4O7tYCow-lQjEQQ8pW1QNTas5a1afX_NuZcHY0SIWp67Q-3tJDkjWBXVP2JMswaxJE', '02-08-2021 12:32:19 PM', '571649', 'YRLUC7Z1', 0, 0, '7143', 'Not Verified', '', '', '', '', '', '', '', 'NOT ACTIVATED', '0', '', 0, ''),
(17, '', '9676600666', NULL, 'eJdaqiTdTsS9gzKqcL8fJv:APA91bFuGm1oWzYakEgWYui5ZWkgrzfCkjgzxE73aOh91kPKVS9eHxnRTXUGlvPytoMv_QjC2qMiL-UoG4CIp3AuJMMwKLylasEXel3MxDtQ8IjWWOWtpKg6Lv_BYPSsjjKAEBdiW1Mv', '02-08-2021 12:38:54 PM', '319625', 'NUG7D4AK', 0, 0, '5417', 'Verified', '', '', '', '', '', '', '', 'NOT ACTIVATED', '0', '', 0, ''),
(18, '', '8340939495', NULL, 'cQKJd8y1RcmGwAxFDgMquM:APA91bGhmn2j_QlIuw4AvX15JJspScJtNgOgC1I237ZgJCkf8RBS-5z65K2VUcHOy9NMWlWsm5d0zLoEiHt1-hCx3dDIyySCjeZegiQcO-7HOG9mNf-ftuD0ZUvgexyK2fwNV_YtLwY-', '02-08-2021 12:40:50 PM', '530928', 'CSOEY1JF', 0, 0, '5984', 'Verified', '', '', '', '', '', '', '', 'NOT ACTIVATED', '0', '', 0, ''),
(19, '', '9852442130', NULL, 'ftdhNp6-QqijOrVjLRPy6V:APA91bEmxi3n-2rSHxp-K-lpE0qlxvdGqR9rMCliteKYLYw0_ffAicDaUk4QUZ5QgGtdYpX_BIbG7SxIcw_f-ZdUcm7XH9sjugbBQL_2sqS8GUBL1yF_xgCuGV-eIwWgcHWJLMB8XEc5', '02-08-2021 12:58:57 PM', '964253', 'FYZD7CPX', 0, 0, '0234', 'Verified', '', '', '', '', '', '', '', 'NOT ACTIVATED', '0', '', 0, ''),
(20, 'Manu', '9866526642', NULL, 'eDoxigVqQyatLJKJ4rHQeN:APA91bHFFx_yQz7FxSv2k9uMD9tedgU1z0Tox7gJusXaERK8mH3pEJbqSd-urHV-18fL0RewwemyUU42omPGnxKWy4nsKuYO_iSURzZEHi_9ME-T4xLU6cJAvnHHx2CC9h9vHkF81D-a', '02-08-2021 03:24:28 PM', '304162', 'YF2X01O3', 0, 0, '0897', 'Not Verified', 'O+', 'No', 'Yes', 'Yes', 'Nothing Like That', 'Brigham', 'Jaisi Hindi', 'NOT ACTIVATED', '0', '', 0, ''),
(21, '', '9059095234', NULL, 'dsJSd5aZSlut-pWxeKfcYe:APA91bFa0QMmM_9PvV_6Dfii0UbS5kW2xLWiy0I_xFVspINh2bJz5Z_sy38uIUtATBUjECiukPxKk8UEGCH9bOcQUx9e-Th2bbA6ad1IY32smghgXy2ukW-bsFjoV5V1kT-_GeVgWHvr', '03-08-2021 03:59:31 PM', '506192', '9XR02YWE', 0, 0, '7695', 'Verified', '', '', '', '', '', '', '', 'NOT ACTIVATED', '0', '', 0, ''),
(22, '', '9492824299', NULL, 'e5PFoTyVTvm0ssCxKvtfhD:APA91bGeL7sa3MTP8j2GbjnBpDPJGdmPvfuXlJr46Vl5FNUiHTdHtVaDIS2noSn9GSXN4JOSUmqZRkdLmTtPITPOwn8dxZcfoUDaxKmIlBYBJs7y1a8iS6TETu5tXtFnKXm1o6rBMKUE', '05-08-2021 06:21:05 PM', '293706', 'X7I28KTJ', 0, 0, '1784', 'Verified', '', '', '', '', '', '', '', 'NOT ACTIVATED', '0', '', 0, ''),
(23, '', '8606942840', NULL, 'cLyL5S2cQsaq4Jne_-1jX0:APA91bGRaW8V40y7lvJ9_8M2N6hsh4g58mhVQHk9ieTF_AkowOtttnh7Yx0HHuETpI1eDchfBtYy9Q6uFX6_3_xuqsWRvTZiHQ_fTre-xF61NPD7pyUih_f1pNwBpgfwV2YBMirRfPkE', '09-08-2021 09:05:49 PM', '928316', 'H65NDO2F', 0, 0, '9350', 'Not Verified', '', '', '', '', '', '', '', 'NOT ACTIVATED', '0', '', 0, ''),
(24, '', '917013449090', NULL, 'd3Wd32JUSLaS_v1WWE8FOu:APA91bG7LfrN4vnINZoUlCeh15u570dhApk8spxnaspJSwR22U523dqyHa8YY3a5n6vi6QRFkcO5pCuMgwdVCn_rpegMg6RbCYwwi4ha8lHCw-h9Y7X20t9o_fEy4B4VqXohMlHqcHON', '10-08-2021 04:42:06 PM', '719635', 'JW4LT8HV', 0, 0, '8456', 'Not Verified', '', '', '', '', '', '', '', 'NOT ACTIVATED', '0', '', 0, ''),
(25, '', '8106874452', NULL, 'fvEdoZXXT16mgDjjhIJotc:APA91bHMGAlygIzwnu_bE0pOfk0s5Xbe5_L5RH_KaDQ9IbItqgWwQ465ARwaDYRSaw07tTQJU9AbRgsubwfno3uCLroKpn4dds_IjYsIE2CTA_dJDDpwm1N8_Qtk__kf5kgz5SyG1vEW', '10-08-2021 09:15:46 PM', '536178', 'RSDCXYNU', 0, 0, '8239', 'Verified', '', '', '', '', '', '', '', 'NOT ACTIVATED', '0', '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `user_logs`
--

CREATE TABLE `user_logs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `reg_no` text DEFAULT NULL,
  `mobile` varchar(250) NOT NULL,
  `ip_address` varchar(250) NOT NULL,
  `login_time` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_logs`
--

INSERT INTO `user_logs` (`id`, `user_id`, `reg_no`, `mobile`, `ip_address`, `login_time`) VALUES
(1, 8, '316894', '9492824933', '175.101.128.29', '18-07-2021 02:43:17 PM'),
(2, 8, '316894', '9492824933', '175.101.128.29', '18-07-2021 06:08:30 PM'),
(3, 8, '316894', '9492824933', '175.101.128.29', '18-07-2021 06:34:03 PM'),
(4, 8, '316894', '9492824933', '175.101.128.29', '18-07-2021 06:42:37 PM'),
(5, 8, '316894', '9492824933', '175.101.128.29', '18-07-2021 06:42:48 PM'),
(6, 8, '316894', '9492824933', '175.101.128.29', '19-07-2021 05:55:11 AM'),
(7, 8, '316894', '9492824933', '175.101.128.29', '19-07-2021 06:09:34 AM'),
(8, 8, '316894', '9492824933', '175.101.128.29', '19-07-2021 06:21:53 AM'),
(9, 8, '316894', '9492824933', '175.101.128.29', '19-07-2021 07:07:09 AM'),
(10, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 12:09:02 PM'),
(11, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 12:49:50 PM'),
(12, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 01:02:52 PM'),
(13, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 01:55:32 PM'),
(14, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 01:57:42 PM'),
(15, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 01:59:11 PM'),
(16, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 02:54:19 PM'),
(17, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 03:38:58 PM'),
(18, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 03:49:28 PM'),
(19, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 04:09:19 PM'),
(20, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 05:42:59 PM'),
(21, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 05:45:34 PM'),
(22, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 05:49:25 PM'),
(23, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 05:54:21 PM'),
(24, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 05:59:11 PM'),
(25, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 06:03:14 PM'),
(26, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 06:07:43 PM'),
(27, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 06:11:03 PM'),
(28, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 06:17:05 PM'),
(29, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 06:24:23 PM'),
(30, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 06:27:46 PM'),
(31, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 06:31:13 PM'),
(32, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 06:34:20 PM'),
(33, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 06:36:37 PM'),
(34, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 06:37:27 PM'),
(35, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 06:49:26 PM'),
(36, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 07:30:25 PM'),
(37, 8, '316894', '9492824933', '49.37.147.249', '19-07-2021 07:39:47 PM'),
(38, 8, '316894', '9492824933', '175.101.128.29', '20-07-2021 06:19:51 AM'),
(39, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 10:10:12 AM'),
(40, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 11:27:34 AM'),
(41, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 11:44:47 AM'),
(42, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 11:45:59 AM'),
(43, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 11:47:07 AM'),
(44, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 11:49:09 AM'),
(45, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 11:51:19 AM'),
(46, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 11:55:00 AM'),
(47, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 11:58:30 AM'),
(48, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 12:00:14 PM'),
(49, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 12:04:54 PM'),
(50, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 12:07:00 PM'),
(51, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 12:10:42 PM'),
(52, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 12:12:09 PM'),
(53, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 12:17:59 PM'),
(54, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 12:25:46 PM'),
(55, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 12:27:59 PM'),
(56, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 12:29:02 PM'),
(57, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 02:19:52 PM'),
(58, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 03:24:56 PM'),
(59, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 03:28:09 PM'),
(60, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 03:29:32 PM'),
(61, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 03:31:30 PM'),
(62, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 03:33:26 PM'),
(63, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 03:38:39 PM'),
(64, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 03:53:33 PM'),
(65, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 03:57:41 PM'),
(66, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 04:01:36 PM'),
(67, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 04:07:57 PM'),
(68, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 04:10:39 PM'),
(69, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 04:11:53 PM'),
(70, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 04:13:31 PM'),
(71, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 04:15:43 PM'),
(72, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 04:19:54 PM'),
(73, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 04:25:56 PM'),
(74, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 04:28:01 PM'),
(75, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 04:29:09 PM'),
(76, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 04:30:10 PM'),
(77, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 04:31:06 PM'),
(78, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 04:32:10 PM'),
(79, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 04:38:22 PM'),
(80, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 04:39:29 PM'),
(81, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 04:46:52 PM'),
(82, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 04:50:04 PM'),
(83, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 05:23:19 PM'),
(84, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 05:26:59 PM'),
(85, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 05:35:57 PM'),
(86, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 05:37:22 PM'),
(87, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 05:39:18 PM'),
(88, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 05:42:14 PM'),
(89, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 05:43:16 PM'),
(90, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 05:43:58 PM'),
(91, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 05:57:10 PM'),
(92, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 05:58:53 PM'),
(93, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 06:00:00 PM'),
(94, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 06:06:05 PM'),
(95, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 06:08:24 PM'),
(96, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 06:08:38 PM'),
(97, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 06:09:29 PM'),
(98, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 06:11:06 PM'),
(99, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 06:12:46 PM'),
(100, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 06:14:33 PM'),
(101, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 06:15:34 PM'),
(102, 8, '316894', '9492824933', '49.37.147.249', '20-07-2021 07:13:14 PM'),
(103, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 10:06:07 AM'),
(104, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 11:08:01 AM'),
(105, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 11:26:48 AM'),
(106, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 11:28:09 AM'),
(107, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 11:39:08 AM'),
(108, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 11:53:01 AM'),
(109, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 11:58:11 AM'),
(110, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 11:59:07 AM'),
(111, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 12:01:30 PM'),
(112, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 12:11:10 PM'),
(113, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 12:13:46 PM'),
(114, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 12:16:08 PM'),
(115, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 12:17:45 PM'),
(116, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 12:19:05 PM'),
(117, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 12:20:51 PM'),
(118, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 12:21:46 PM'),
(119, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 12:23:07 PM'),
(120, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 12:27:33 PM'),
(121, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 12:34:29 PM'),
(122, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 12:48:25 PM'),
(123, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 01:04:30 PM'),
(124, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 01:13:30 PM'),
(125, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 01:51:04 PM'),
(126, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 01:54:18 PM'),
(127, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 01:59:43 PM'),
(128, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 02:52:04 PM'),
(129, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 03:12:07 PM'),
(130, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 03:34:41 PM'),
(131, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 03:52:05 PM'),
(132, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 03:53:19 PM'),
(133, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 03:54:25 PM'),
(134, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 04:26:18 PM'),
(135, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 04:27:55 PM'),
(136, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 04:30:53 PM'),
(137, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 04:32:26 PM'),
(138, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 04:34:29 PM'),
(139, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 04:35:30 PM'),
(140, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 04:36:50 PM'),
(141, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 04:49:19 PM'),
(142, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 04:51:48 PM'),
(143, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 04:58:03 PM'),
(144, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 05:00:01 PM'),
(145, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 05:05:50 PM'),
(146, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 05:06:14 PM'),
(147, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 05:11:03 PM'),
(148, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 05:12:33 PM'),
(149, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 05:17:17 PM'),
(150, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 05:18:50 PM'),
(151, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 05:26:45 PM'),
(152, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 06:41:46 PM'),
(153, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 06:46:33 PM'),
(154, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 06:51:10 PM'),
(155, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 06:59:36 PM'),
(156, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 07:07:46 PM'),
(157, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 07:13:15 PM'),
(158, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 07:16:27 PM'),
(159, 8, '316894', '9492824933', '49.37.147.249', '22-07-2021 07:29:30 PM'),
(160, 8, '316894', '9492824933', '175.101.128.29', '23-07-2021 06:52:35 AM'),
(161, 8, '316894', '9492824933', '175.101.128.29', '23-07-2021 07:59:36 AM'),
(162, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 10:29:29 AM'),
(163, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 10:35:17 AM'),
(164, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 10:37:27 AM'),
(165, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 10:42:26 AM'),
(166, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 10:44:58 AM'),
(167, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 10:46:34 AM'),
(168, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 10:49:10 AM'),
(169, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 10:56:38 AM'),
(170, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 11:09:28 AM'),
(171, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 11:10:40 AM'),
(172, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 11:15:26 AM'),
(173, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 11:20:37 AM'),
(174, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 11:29:37 AM'),
(175, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 11:32:01 AM'),
(176, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 11:35:38 AM'),
(177, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 11:40:40 AM'),
(178, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 12:11:30 PM'),
(179, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 12:13:17 PM'),
(180, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 12:17:04 PM'),
(181, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 12:30:08 PM'),
(182, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 12:39:07 PM'),
(183, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 12:43:08 PM'),
(184, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 12:54:18 PM'),
(185, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 01:04:00 PM'),
(186, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 01:07:11 PM'),
(187, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 01:14:52 PM'),
(188, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 01:55:42 PM'),
(189, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 02:07:25 PM'),
(190, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 02:16:25 PM'),
(191, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 02:30:15 PM'),
(192, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 02:32:16 PM'),
(193, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 02:52:07 PM'),
(194, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 02:58:24 PM'),
(195, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 03:09:48 PM'),
(196, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 03:13:52 PM'),
(197, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 03:48:42 PM'),
(198, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 04:09:38 PM'),
(199, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 04:46:54 PM'),
(200, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 04:52:46 PM'),
(201, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 05:05:36 PM'),
(202, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 05:07:28 PM'),
(203, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 05:08:44 PM'),
(204, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 05:16:00 PM'),
(205, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 05:36:10 PM'),
(206, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 05:48:12 PM'),
(207, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 05:50:35 PM'),
(208, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 05:59:49 PM'),
(209, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 06:05:54 PM'),
(210, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 06:11:11 PM'),
(211, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 06:48:20 PM'),
(212, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 06:49:24 PM'),
(213, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 06:51:17 PM'),
(214, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 06:54:02 PM'),
(215, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 06:55:55 PM'),
(216, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 06:59:08 PM'),
(217, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 07:02:15 PM'),
(218, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 07:05:09 PM'),
(219, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 07:05:26 PM'),
(220, 8, '316894', '9492824933', '49.37.144.236', '23-07-2021 07:10:15 PM'),
(221, 8, '316894', '9492824933', '175.101.128.29', '24-07-2021 06:33:10 AM'),
(222, 8, '316894', '9492824933', '175.101.128.29', '24-07-2021 07:31:56 AM'),
(223, 8, '316894', '9492824933', '175.101.128.29', '24-07-2021 07:46:09 AM'),
(224, 8, '316894', '9492824933', '175.101.128.29', '24-07-2021 07:59:41 AM'),
(225, 8, '316894', '9492824933', '49.37.144.236', '24-07-2021 09:51:28 AM'),
(226, 8, '316894', '9492824933', '49.37.144.236', '24-07-2021 10:05:18 AM'),
(227, 8, '316894', '9492824933', '49.37.144.236', '24-07-2021 10:52:03 AM'),
(228, 8, '316894', '9492824933', '49.37.144.236', '24-07-2021 11:21:09 AM'),
(229, 8, '316894', '9492824933', '49.37.144.236', '24-07-2021 11:33:25 AM'),
(230, 8, '316894', '9492824933', '49.37.144.236', '24-07-2021 11:45:03 AM'),
(231, 8, '316894', '9492824933', '49.37.144.236', '24-07-2021 11:50:21 AM'),
(232, 8, '316894', '9492824933', '49.37.144.236', '24-07-2021 01:08:07 PM'),
(233, 8, '316894', '9492824933', '49.37.144.236', '24-07-2021 01:12:42 PM'),
(234, 8, '316894', '9492824933', '49.37.144.236', '24-07-2021 01:35:19 PM'),
(235, 8, '316894', '9492824933', '49.37.144.236', '24-07-2021 01:43:16 PM'),
(236, 8, '316894', '9492824933', '49.37.144.236', '24-07-2021 01:48:08 PM'),
(237, 8, '316894', '9492824933', '49.37.144.236', '24-07-2021 02:01:18 PM'),
(238, 8, '316894', '9492824933', '49.37.144.236', '24-07-2021 02:06:19 PM'),
(239, 8, '316894', '9492824933', '49.37.144.236', '24-07-2021 02:12:37 PM'),
(240, 3, '197253', '9719325299', '49.37.144.236', '24-07-2021 02:16:12 PM'),
(241, 8, '316894', '9492824933', '175.101.128.29', '24-07-2021 07:59:54 PM'),
(242, 8, '316894', '9492824933', '175.101.128.29', '25-07-2021 02:17:07 PM'),
(243, 8, '316894', '9492824933', '175.101.128.29', '25-07-2021 10:24:23 PM'),
(244, 8, '316894', '9492824933', '175.101.128.29', '26-07-2021 06:51:01 AM'),
(245, 8, '316894', '9492824933', '175.101.128.29', '26-07-2021 07:26:37 AM'),
(246, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 09:56:29 AM'),
(247, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 10:08:56 AM'),
(248, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 10:21:38 AM'),
(249, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 10:28:41 AM'),
(250, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 10:35:29 AM'),
(251, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 10:36:36 AM'),
(252, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 10:42:11 AM'),
(253, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 10:43:59 AM'),
(254, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 10:56:36 AM'),
(255, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 11:10:04 AM'),
(256, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 11:15:06 AM'),
(257, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 11:25:22 AM'),
(258, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 11:30:39 AM'),
(259, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 11:35:06 AM'),
(260, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 11:39:16 AM'),
(261, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 11:40:46 AM'),
(262, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 11:47:56 AM'),
(263, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 11:51:38 AM'),
(264, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 12:01:07 PM'),
(265, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 12:19:07 PM'),
(266, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 12:34:00 PM'),
(267, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 12:37:46 PM'),
(268, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 12:42:24 PM'),
(269, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 01:10:09 PM'),
(270, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 01:13:10 PM'),
(271, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 01:20:10 PM'),
(272, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 01:52:01 PM'),
(273, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 01:55:15 PM'),
(274, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 02:52:24 PM'),
(275, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 03:03:17 PM'),
(276, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 03:03:54 PM'),
(277, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 03:10:28 PM'),
(278, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 03:15:02 PM'),
(279, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 03:18:24 PM'),
(280, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 03:39:06 PM'),
(281, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 03:43:47 PM'),
(282, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 03:52:53 PM'),
(283, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 04:07:59 PM'),
(284, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 04:17:10 PM'),
(285, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 04:23:26 PM'),
(286, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 04:29:39 PM'),
(287, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 04:34:46 PM'),
(288, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 05:58:10 PM'),
(289, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 05:59:54 PM'),
(290, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 06:11:27 PM'),
(291, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 06:15:56 PM'),
(292, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 06:18:36 PM'),
(293, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 06:21:42 PM'),
(294, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 06:26:10 PM'),
(295, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 06:32:36 PM'),
(296, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 06:35:06 PM'),
(297, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 07:03:50 PM'),
(298, 8, '316894', '9492824933', '49.37.148.185', '26-07-2021 07:05:37 PM'),
(299, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 10:00:47 AM'),
(300, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 10:17:34 AM'),
(301, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 10:40:26 AM'),
(302, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 10:42:54 AM'),
(303, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 10:46:35 AM'),
(304, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 10:47:29 AM'),
(305, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 10:48:48 AM'),
(306, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 10:50:26 AM'),
(307, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 10:51:52 AM'),
(308, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 10:54:14 AM'),
(309, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 11:01:14 AM'),
(310, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 11:03:22 AM'),
(311, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 11:05:01 AM'),
(312, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 11:57:06 AM'),
(313, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 11:59:28 AM'),
(314, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 12:01:15 PM'),
(315, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 12:03:18 PM'),
(316, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 12:24:55 PM'),
(317, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 12:28:33 PM'),
(318, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 12:30:09 PM'),
(319, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 12:34:17 PM'),
(320, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 12:48:05 PM'),
(321, 8, '316894', '9492824933', '223.185.118.31', '27-07-2021 12:55:19 PM'),
(322, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 02:01:44 PM'),
(323, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 02:02:07 PM'),
(324, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 02:04:52 PM'),
(325, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 03:24:40 PM'),
(326, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 03:46:31 PM'),
(327, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 03:49:26 PM'),
(328, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 03:55:52 PM'),
(329, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 03:58:47 PM'),
(330, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 04:00:55 PM'),
(331, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 04:07:22 PM'),
(332, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 04:11:14 PM'),
(333, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 04:16:20 PM'),
(334, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 04:21:40 PM'),
(335, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 04:24:20 PM'),
(336, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 04:31:42 PM'),
(337, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 04:36:37 PM'),
(338, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 04:38:43 PM'),
(339, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 04:40:53 PM'),
(340, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 04:42:15 PM'),
(341, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 04:50:00 PM'),
(342, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 05:55:59 PM'),
(343, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 06:00:56 PM'),
(344, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 06:23:12 PM'),
(345, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 06:30:14 PM'),
(346, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 06:30:39 PM'),
(347, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 06:35:06 PM'),
(348, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 06:45:57 PM'),
(349, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 06:53:24 PM'),
(350, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 06:53:45 PM'),
(351, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 06:55:52 PM'),
(352, 8, '316894', '9492824933', '49.37.148.185', '27-07-2021 07:00:08 PM'),
(353, 8, '316894', '9492824933', '175.101.128.29', '28-07-2021 07:00:04 AM'),
(354, 8, '316894', '9492824933', '175.101.128.29', '28-07-2021 07:14:11 AM'),
(355, 8, '316894', '9492824933', '175.101.128.29', '28-07-2021 07:14:46 AM'),
(356, 8, '316894', '9492824933', '175.101.128.29', '28-07-2021 07:36:01 AM'),
(357, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 10:13:09 AM'),
(358, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 10:44:18 AM'),
(359, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 10:46:26 AM'),
(360, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 10:47:36 AM'),
(361, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 10:48:39 AM'),
(362, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 10:58:36 AM'),
(363, 9, '470185', '6281781884', '49.37.145.174', '28-07-2021 11:07:51 AM'),
(364, 9, '470185', '6281781884', '49.37.145.174', '28-07-2021 11:12:06 AM'),
(365, 9, '470185', '6281781884', '49.37.145.174', '28-07-2021 11:13:14 AM'),
(366, 9, '470185', '6281781884', '49.37.145.174', '28-07-2021 11:14:08 AM'),
(367, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 11:18:48 AM'),
(368, 9, '470185', '6281781884', '49.37.145.174', '28-07-2021 11:21:08 AM'),
(369, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 11:29:00 AM'),
(370, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 12:32:29 PM'),
(371, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 12:34:22 PM'),
(372, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 12:41:51 PM'),
(373, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 12:43:21 PM'),
(374, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 12:50:35 PM'),
(375, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 01:04:18 PM'),
(376, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 01:04:43 PM'),
(377, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 01:07:35 PM'),
(378, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 01:07:47 PM'),
(379, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 02:21:45 PM'),
(380, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 02:28:08 PM'),
(381, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 02:29:30 PM'),
(382, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 02:32:22 PM'),
(383, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 02:32:44 PM'),
(384, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 02:32:55 PM'),
(385, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 02:41:33 PM'),
(386, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 02:41:43 PM'),
(387, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 02:46:25 PM'),
(388, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 02:46:44 PM'),
(389, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 02:48:01 PM'),
(390, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 02:49:36 PM'),
(391, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 02:57:59 PM'),
(392, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 02:59:48 PM'),
(393, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 03:00:17 PM'),
(394, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 03:00:36 PM'),
(395, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 03:00:48 PM'),
(396, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 03:01:03 PM'),
(397, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 03:42:26 PM'),
(398, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 03:59:20 PM'),
(399, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 04:01:44 PM'),
(400, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 04:02:26 PM'),
(401, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 04:04:52 PM'),
(402, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 04:05:36 PM'),
(403, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 04:26:54 PM'),
(404, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 04:27:31 PM'),
(405, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 04:29:17 PM'),
(406, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 04:30:13 PM'),
(407, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 04:36:40 PM'),
(408, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 04:37:16 PM'),
(409, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 04:40:11 PM'),
(410, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 04:55:48 PM'),
(411, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 06:29:42 PM'),
(412, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 07:02:13 PM'),
(413, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 07:04:57 PM'),
(414, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 07:07:04 PM'),
(415, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 07:13:14 PM'),
(416, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 07:13:54 PM'),
(417, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 07:19:57 PM'),
(418, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 07:30:35 PM'),
(419, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 07:34:08 PM'),
(420, 8, '316894', '9492824933', '49.37.145.174', '28-07-2021 07:43:37 PM'),
(421, 8, '316894', '9492824933', '175.101.128.29', '29-07-2021 07:31:27 AM'),
(422, 8, '316894', '9492824933', '175.101.128.29', '29-07-2021 07:32:03 AM'),
(423, 8, '316894', '9492824933', '175.101.128.29', '29-07-2021 07:34:16 AM'),
(424, 8, '316894', '9492824933', '175.101.128.29', '29-07-2021 07:35:52 AM'),
(425, 8, '316894', '9492824933', '175.101.128.29', '29-07-2021 07:38:08 AM'),
(426, 8, '316894', '9492824933', '175.101.128.29', '29-07-2021 07:42:19 AM'),
(427, 8, '316894', '9492824933', '175.101.128.29', '29-07-2021 07:43:38 AM'),
(428, 8, '316894', '9492824933', '175.101.128.29', '29-07-2021 07:45:56 AM'),
(429, 8, '316894', '9492824933', '175.101.128.29', '29-07-2021 07:50:04 AM'),
(430, 8, '316894', '9492824933', '175.101.128.29', '29-07-2021 08:04:26 AM'),
(431, 8, '316894', '9492824933', '175.101.128.29', '29-07-2021 08:06:34 AM'),
(432, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 10:31:17 AM'),
(433, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 10:53:38 AM'),
(434, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 11:25:21 AM'),
(435, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 11:28:30 AM'),
(436, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 11:46:15 AM'),
(437, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 11:52:13 AM'),
(438, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 11:53:52 AM'),
(439, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 11:55:43 AM'),
(440, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 12:00:42 PM'),
(441, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 12:10:32 PM'),
(442, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 12:22:07 PM'),
(443, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 01:39:39 PM'),
(444, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 01:44:57 PM'),
(445, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 01:51:10 PM'),
(446, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 01:55:08 PM'),
(447, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 01:57:09 PM'),
(448, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 02:14:52 PM'),
(449, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 02:16:40 PM'),
(450, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 02:20:55 PM'),
(451, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 02:21:21 PM'),
(452, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 02:26:01 PM'),
(453, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 02:30:18 PM'),
(454, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 02:35:28 PM'),
(455, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 02:41:42 PM'),
(456, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 02:56:32 PM'),
(457, 10, '301742', '8499052020', '49.37.150.28', '29-07-2021 02:59:11 PM'),
(458, 10, '301742', '8499052020', '49.37.150.28', '29-07-2021 02:59:53 PM'),
(459, 4, '457938', '1234567890', '106.208.44.136', '29-07-2021 03:30:46 PM'),
(460, 4, '457938', '1234567890', '106.208.44.136', '29-07-2021 03:31:26 PM'),
(461, 4, '457938', '1234567890', '106.208.44.136', '29-07-2021 03:31:35 PM'),
(462, 10, '301742', '8499052020', '49.37.150.28', '29-07-2021 03:56:15 PM'),
(463, 10, '301742', '8499052020', '49.37.150.28', '29-07-2021 04:01:48 PM'),
(464, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 04:02:34 PM'),
(465, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 04:03:19 PM'),
(466, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 04:09:34 PM'),
(467, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 04:35:45 PM'),
(468, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 05:27:52 PM'),
(469, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 05:47:03 PM'),
(470, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 05:58:50 PM'),
(471, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 06:12:24 PM'),
(472, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 06:41:00 PM'),
(473, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 06:45:46 PM'),
(474, 8, '316894', '9492824933', '49.37.150.28', '29-07-2021 06:52:37 PM'),
(475, 8, '316894', '9492824933', '175.101.128.29', '30-07-2021 07:28:00 AM'),
(476, 8, '316894', '9492824933', '175.101.128.29', '30-07-2021 07:29:15 AM'),
(477, 8, '316894', '9492824933', '175.101.128.29', '30-07-2021 07:39:06 AM'),
(478, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 10:08:17 AM'),
(479, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 10:15:08 AM'),
(480, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 10:28:56 AM'),
(481, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 10:35:33 AM'),
(482, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 10:43:26 AM'),
(483, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 10:45:24 AM'),
(484, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 10:48:51 AM'),
(485, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 10:52:17 AM'),
(486, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 10:54:59 AM'),
(487, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 10:58:15 AM'),
(488, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 11:00:15 AM'),
(489, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 11:02:48 AM'),
(490, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 11:07:01 AM'),
(491, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 11:16:47 AM'),
(492, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 11:25:12 AM'),
(493, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 11:31:12 AM'),
(494, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 11:31:37 AM'),
(495, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 11:35:46 AM'),
(496, 1, '098762', '9618820971', '49.37.148.25', '30-07-2021 11:41:21 AM'),
(497, 1, '098762', '9618820971', '49.37.148.25', '30-07-2021 11:43:55 AM'),
(498, 1, '098762', '9618820971', '49.37.148.25', '30-07-2021 11:50:05 AM'),
(499, 1, '098762', '9618820971', '49.37.148.25', '30-07-2021 11:57:58 AM'),
(500, 1, '098762', '9618820971', '49.37.148.25', '30-07-2021 12:07:10 PM'),
(501, 1, '098762', '9618820971', '49.37.148.25', '30-07-2021 12:09:01 PM'),
(502, 1, '098762', '9618820971', '49.37.148.25', '30-07-2021 12:11:12 PM'),
(503, 11, '396028', '123453789', '49.37.148.25', '30-07-2021 12:27:09 PM'),
(504, 11, '396028', '123453789', '49.37.148.25', '30-07-2021 12:28:48 PM'),
(505, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 12:29:59 PM'),
(506, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 02:34:01 PM'),
(507, 1, '098762', '9618820971', '49.37.148.25', '30-07-2021 02:45:08 PM'),
(508, 1, '098762', '9618820971', '49.37.148.25', '30-07-2021 03:18:00 PM'),
(509, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 03:23:17 PM'),
(510, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 04:10:42 PM'),
(511, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 04:16:10 PM'),
(512, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 04:16:37 PM'),
(513, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 04:19:23 PM'),
(514, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 04:25:15 PM'),
(515, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 04:25:52 PM'),
(516, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 04:28:50 PM'),
(517, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 04:37:33 PM'),
(518, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 06:46:58 PM'),
(519, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 06:47:17 PM'),
(520, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 06:49:18 PM'),
(521, 8, '316894', '9492824933', '49.37.148.25', '30-07-2021 06:54:36 PM'),
(522, 12, '584721', '6504992804', '64.233.172.7', '30-07-2021 07:28:37 PM'),
(523, 12, '584721', '6504992804', '64.233.172.7', '30-07-2021 07:30:23 PM'),
(524, 13, '523468', '8125505088', '202.53.69.162', '02-08-2021 11:22:41 AM'),
(525, 14, '814735', '9959646419', '223.185.110.150', '02-08-2021 11:32:02 AM'),
(526, 14, '814735', '9959646419', '223.185.110.150', '02-08-2021 11:42:52 AM'),
(527, 14, '814735', '9959646419', '223.185.110.150', '02-08-2021 11:46:44 AM'),
(528, 14, '814735', '9959646419', '223.185.110.150', '02-08-2021 11:50:15 AM'),
(529, 14, '814735', '9959646419', '223.185.110.150', '02-08-2021 11:53:23 AM'),
(530, 14, '814735', '9959646419', '223.185.110.150', '02-08-2021 12:18:28 PM'),
(531, 15, '039248', '7013449090', '106.66.44.28', '02-08-2021 12:21:20 PM'),
(532, 15, '039248', '7013449090', '106.66.44.28', '02-08-2021 12:23:23 PM'),
(533, 15, '039248', '7013449090', '106.66.44.28', '02-08-2021 12:23:28 PM'),
(534, 14, '814735', '9959646419', '223.185.110.150', '02-08-2021 12:23:47 PM'),
(535, 15, '039248', '7013449090', '106.66.44.28', '02-08-2021 12:31:52 PM'),
(536, 13, '523468', '8125505088', '202.53.69.162', '02-08-2021 12:32:17 PM'),
(537, 16, '571649', '9505560361', '106.66.44.28', '02-08-2021 12:32:19 PM'),
(538, 16, '571649', '9505560361', '106.66.44.28', '02-08-2021 12:33:04 PM'),
(539, 15, '039248', '7013449090', '106.66.44.28', '02-08-2021 12:33:35 PM'),
(540, 14, '814735', '9959646419', '223.185.110.150', '02-08-2021 12:34:19 PM'),
(541, 13, '523468', '8125505088', '202.53.69.162', '02-08-2021 12:36:48 PM'),
(542, 14, '814735', '9959646419', '223.185.110.150', '02-08-2021 12:37:27 PM'),
(543, 17, '319625', '9676600666', '157.48.152.95', '02-08-2021 12:38:54 PM'),
(544, 17, '319625', '9676600666', '157.48.152.95', '02-08-2021 12:38:59 PM'),
(545, 18, '530928', '8340939495', '157.48.152.95', '02-08-2021 12:40:50 PM'),
(546, 13, '523468', '8125505088', '202.53.69.162', '02-08-2021 12:44:30 PM'),
(547, 16, '571649', '9505560361', '106.66.44.28', '02-08-2021 12:47:07 PM'),
(548, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 12:47:14 PM'),
(549, 13, '523468', '8125505088', '202.53.69.162', '02-08-2021 12:51:08 PM'),
(550, 19, '964253', '9852442130', '49.37.151.193', '02-08-2021 12:58:57 PM'),
(551, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 01:03:46 PM'),
(552, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 01:27:07 PM'),
(553, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 01:52:56 PM'),
(554, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 02:03:50 PM'),
(555, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 02:10:15 PM'),
(556, 14, '814735', '9959646419', '223.185.110.150', '02-08-2021 02:25:45 PM'),
(557, 14, '814735', '9959646419', '223.185.110.150', '02-08-2021 02:40:42 PM'),
(558, 14, '814735', '9959646419', '223.185.110.150', '02-08-2021 02:44:19 PM'),
(559, 14, '814735', '9959646419', '223.185.110.150', '02-08-2021 02:45:19 PM'),
(560, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 03:01:28 PM'),
(561, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 03:04:06 PM'),
(562, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 03:07:45 PM'),
(563, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 03:10:03 PM'),
(564, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 03:14:14 PM'),
(565, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 03:14:19 PM'),
(566, 14, '814735', '9959646419', '223.185.110.150', '02-08-2021 03:14:56 PM'),
(567, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 03:17:22 PM'),
(568, 14, '814735', '9959646419', '223.185.110.150', '02-08-2021 03:18:29 PM'),
(569, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 03:18:30 PM'),
(570, 14, '814735', '9959646419', '223.185.110.150', '02-08-2021 03:20:35 PM'),
(571, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 03:21:49 PM'),
(572, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 03:23:44 PM'),
(573, 20, '304162', '9866526642', '49.37.151.193', '02-08-2021 03:24:28 PM'),
(574, 20, '304162', '9866526642', '49.37.151.193', '02-08-2021 03:31:40 PM'),
(575, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 03:32:49 PM'),
(576, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 03:44:22 PM'),
(577, 14, '814735', '9959646419', '223.185.110.150', '02-08-2021 03:52:23 PM'),
(578, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 03:53:07 PM'),
(579, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 03:57:33 PM'),
(580, 16, '571649', '9505560361', '106.76.213.240', '02-08-2021 05:10:09 PM'),
(581, 16, '571649', '9505560361', '106.76.213.240', '02-08-2021 05:13:00 PM'),
(582, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 05:17:07 PM'),
(583, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 05:18:34 PM'),
(584, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 05:18:46 PM'),
(585, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 05:19:19 PM'),
(586, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 05:21:20 PM'),
(587, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 05:21:34 PM'),
(588, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 05:22:20 PM'),
(589, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 05:24:15 PM'),
(590, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 05:25:29 PM'),
(591, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 05:27:30 PM'),
(592, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 05:28:26 PM'),
(593, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 05:36:31 PM'),
(594, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 05:38:26 PM'),
(595, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 05:40:41 PM'),
(596, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 05:44:10 PM'),
(597, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 05:45:43 PM'),
(598, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 05:47:06 PM'),
(599, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 05:52:30 PM'),
(600, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 05:59:02 PM'),
(601, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 06:07:22 PM'),
(602, 20, '304162', '9866526642', '49.37.151.193', '02-08-2021 06:07:43 PM'),
(603, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 06:08:06 PM'),
(604, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 06:12:43 PM'),
(605, 20, '304162', '9866526642', '49.37.151.193', '02-08-2021 06:13:37 PM'),
(606, 14, '814735', '9959646419', '223.185.110.150', '02-08-2021 06:15:00 PM'),
(607, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 06:25:08 PM'),
(608, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 06:37:12 PM'),
(609, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 06:55:06 PM'),
(610, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 07:01:42 PM'),
(611, 8, '316894', '9492824933', '49.37.151.193', '02-08-2021 07:12:16 PM'),
(612, 14, '814735', '9959646419', '223.185.110.150', '02-08-2021 08:22:12 PM'),
(613, 14, '814735', '9959646419', '223.185.110.150', '02-08-2021 09:09:26 PM'),
(614, 14, '814735', '9959646419', '223.185.110.150', '02-08-2021 09:10:18 PM'),
(615, 14, '814735', '9959646419', '223.185.110.150', '02-08-2021 09:10:33 PM'),
(616, 14, '814735', '9959646419', '223.187.78.131', '03-08-2021 07:51:28 AM'),
(617, 14, '814735', '9959646419', '223.187.78.131', '03-08-2021 09:47:51 AM'),
(618, 8, '316894', '9492824933', '49.37.145.188', '03-08-2021 09:56:29 AM'),
(619, 14, '814735', '9959646419', '103.5.115.85', '03-08-2021 10:05:13 AM'),
(620, 8, '316894', '9492824933', '49.37.145.188', '03-08-2021 10:52:22 AM'),
(621, 8, '316894', '9492824933', '49.37.145.188', '03-08-2021 10:55:34 AM'),
(622, 8, '316894', '9492824933', '49.37.145.188', '03-08-2021 10:57:36 AM'),
(623, 20, '304162', '9866526642', '223.227.99.8', '03-08-2021 11:04:17 AM'),
(624, 20, '304162', '9866526642', '223.227.99.8', '03-08-2021 11:14:28 AM'),
(625, 20, '304162', '9866526642', '223.227.99.8', '03-08-2021 11:17:18 AM'),
(626, 20, '304162', '9866526642', '223.227.99.8', '03-08-2021 11:22:17 AM'),
(627, 20, '304162', '9866526642', '223.227.99.8', '03-08-2021 11:25:29 AM'),
(628, 20, '304162', '9866526642', '223.227.99.8', '03-08-2021 11:38:29 AM'),
(629, 20, '304162', '9866526642', '223.227.99.8', '03-08-2021 11:40:11 AM'),
(630, 14, '814735', '9959646419', '103.5.115.85', '03-08-2021 11:55:12 AM'),
(631, 14, '814735', '9959646419', '103.5.115.85', '03-08-2021 11:55:22 AM'),
(632, 20, '304162', '9866526642', '223.227.99.8', '03-08-2021 12:01:26 PM'),
(633, 20, '304162', '9866526642', '223.227.99.8', '03-08-2021 12:05:18 PM'),
(634, 20, '304162', '9866526642', '223.227.99.8', '03-08-2021 12:07:29 PM'),
(635, 20, '304162', '9866526642', '223.227.99.8', '03-08-2021 12:09:04 PM'),
(636, 20, '304162', '9866526642', '223.227.99.8', '03-08-2021 12:15:32 PM'),
(637, 20, '304162', '9866526642', '223.227.99.8', '03-08-2021 12:19:08 PM'),
(638, 14, '814735', '9959646419', '103.5.115.85', '03-08-2021 12:21:52 PM'),
(639, 14, '814735', '9959646419', '103.5.115.85', '03-08-2021 12:22:05 PM'),
(640, 20, '304162', '9866526642', '223.227.99.8', '03-08-2021 12:23:07 PM'),
(641, 20, '304162', '9866526642', '223.227.99.8', '03-08-2021 12:26:38 PM'),
(642, 20, '304162', '9866526642', '202.53.69.163', '03-08-2021 12:34:34 PM'),
(643, 20, '304162', '9866526642', '202.53.69.163', '03-08-2021 12:42:49 PM'),
(644, 20, '304162', '9866526642', '202.53.69.163', '03-08-2021 12:43:07 PM'),
(645, 20, '304162', '9866526642', '202.53.69.163', '03-08-2021 12:46:58 PM'),
(646, 20, '304162', '9866526642', '202.53.69.163', '03-08-2021 12:57:08 PM'),
(647, 20, '304162', '9866526642', '202.53.69.163', '03-08-2021 01:00:58 PM'),
(648, 20, '304162', '9866526642', '202.53.69.163', '03-08-2021 01:04:44 PM'),
(649, 20, '304162', '9866526642', '223.227.99.8', '03-08-2021 01:33:53 PM'),
(650, 14, '814735', '9959646419', '223.187.78.145', '03-08-2021 01:56:46 PM'),
(651, 20, '304162', '9866526642', '202.53.69.163', '03-08-2021 01:59:55 PM'),
(652, 20, '304162', '9866526642', '202.53.69.163', '03-08-2021 02:03:32 PM'),
(653, 20, '304162', '9866526642', '202.53.69.163', '03-08-2021 02:06:37 PM'),
(654, 14, '814735', '9959646419', '223.187.78.145', '03-08-2021 02:16:08 PM'),
(655, 14, '814735', '9959646419', '223.187.78.145', '03-08-2021 02:16:41 PM'),
(656, 20, '304162', '9866526642', '202.53.69.163', '03-08-2021 02:34:52 PM'),
(657, 20, '304162', '9866526642', '202.53.69.163', '03-08-2021 02:41:18 PM'),
(658, 20, '304162', '9866526642', '202.53.69.163', '03-08-2021 02:41:54 PM'),
(659, 20, '304162', '9866526642', '202.53.69.163', '03-08-2021 02:57:23 PM'),
(660, 20, '304162', '9866526642', '202.53.69.163', '03-08-2021 03:11:03 PM'),
(661, 20, '304162', '9866526642', '202.53.69.163', '03-08-2021 03:12:10 PM'),
(662, 20, '304162', '9866526642', '202.53.69.163', '03-08-2021 03:14:11 PM'),
(663, 20, '304162', '9866526642', '202.53.69.163', '03-08-2021 03:16:27 PM'),
(664, 20, '304162', '9866526642', '202.53.69.163', '03-08-2021 03:18:06 PM'),
(665, 8, '316894', '9492824933', '202.53.69.163', '03-08-2021 03:45:22 PM');
INSERT INTO `user_logs` (`id`, `user_id`, `reg_no`, `mobile`, `ip_address`, `login_time`) VALUES
(666, 20, '304162', '9866526642', '202.53.69.163', '03-08-2021 03:45:58 PM'),
(667, 20, '304162', '9866526642', '202.53.69.163', '03-08-2021 03:56:59 PM'),
(668, 21, '506192', '9059095234', '202.53.69.163', '03-08-2021 03:59:31 PM'),
(669, 14, '814735', '9959646419', '223.187.78.145', '03-08-2021 07:19:00 PM'),
(670, 21, '506192', '9059095234', '202.53.69.163', '04-08-2021 10:39:43 AM'),
(671, 8, '316894', '9492824933', '202.53.69.163', '04-08-2021 10:39:57 AM'),
(672, 14, '814735', '9959646419', '223.187.78.145', '04-08-2021 11:58:33 AM'),
(673, 14, '814735', '9959646419', '223.187.78.145', '04-08-2021 11:58:38 AM'),
(674, 14, '814735', '9959646419', '223.187.78.145', '04-08-2021 03:34:30 PM'),
(675, 14, '814735', '9959646419', '223.187.78.145', '04-08-2021 03:34:56 PM'),
(676, 14, '814735', '9959646419', '223.187.78.145', '04-08-2021 03:45:45 PM'),
(677, 8, '316894', '9492824933', '202.53.69.163', '04-08-2021 06:12:06 PM'),
(678, 14, '814735', '9959646419', '223.187.78.145', '04-08-2021 06:46:02 PM'),
(679, 14, '814735', '9959646419', '223.187.78.145', '05-08-2021 11:18:29 AM'),
(680, 8, '316894', '9492824933', '202.53.69.163', '05-08-2021 11:38:37 AM'),
(681, 21, '506192', '9059095234', '49.37.150.202', '05-08-2021 11:41:54 AM'),
(682, 8, '316894', '9492824933', '202.53.69.163', '05-08-2021 11:49:11 AM'),
(683, 8, '316894', '9492824933', '202.53.69.163', '05-08-2021 12:19:17 PM'),
(684, 8, '316894', '9492824933', '202.53.69.163', '05-08-2021 03:16:03 PM'),
(685, 8, '316894', '9492824933', '202.53.69.163', '05-08-2021 06:11:43 PM'),
(686, 22, '293706', '9492824299', '202.53.69.163', '05-08-2021 06:21:05 PM'),
(687, 8, '316894', '9492824933', '202.53.69.163', '05-08-2021 07:00:07 PM'),
(688, 14, '814735', '9959646419', '223.187.78.145', '05-08-2021 09:00:53 PM'),
(689, 1, '098762', '9618820971', '223.237.12.61', '06-08-2021 11:33:01 AM'),
(690, 8, '316894', '9492824933', '202.53.69.163', '06-08-2021 11:35:15 AM'),
(691, 8, '316894', '9492824933', '202.53.69.163', '06-08-2021 11:35:52 AM'),
(692, 20, '304162', '9866526642', '202.53.69.163', '06-08-2021 11:42:20 AM'),
(693, 20, '304162', '9866526642', '202.53.69.163', '06-08-2021 12:00:59 PM'),
(694, 20, '304162', '9866526642', '202.53.69.163', '06-08-2021 12:35:30 PM'),
(695, 20, '304162', '9866526642', '202.53.69.163', '06-08-2021 01:51:17 PM'),
(696, 20, '304162', '9866526642', '202.53.69.163', '06-08-2021 01:56:05 PM'),
(697, 20, '304162', '9866526642', '202.53.69.163', '06-08-2021 02:01:19 PM'),
(698, 20, '304162', '9866526642', '202.53.69.163', '06-08-2021 02:40:45 PM'),
(699, 20, '304162', '9866526642', '202.53.69.163', '06-08-2021 02:51:09 PM'),
(700, 20, '304162', '9866526642', '202.53.69.163', '06-08-2021 02:52:05 PM'),
(701, 20, '304162', '9866526642', '202.53.69.163', '06-08-2021 02:55:51 PM'),
(702, 20, '304162', '9866526642', '202.53.69.163', '06-08-2021 03:11:38 PM'),
(703, 20, '304162', '9866526642', '202.53.69.163', '06-08-2021 03:21:12 PM'),
(704, 20, '304162', '9866526642', '202.53.69.163', '06-08-2021 03:31:06 PM'),
(705, 20, '304162', '9866526642', '202.53.69.163', '06-08-2021 04:19:04 PM'),
(706, 20, '304162', '9866526642', '202.53.69.163', '06-08-2021 04:21:08 PM'),
(707, 20, '304162', '9866526642', '202.53.69.163', '06-08-2021 04:21:24 PM'),
(708, 20, '304162', '9866526642', '202.53.69.163', '06-08-2021 04:23:04 PM'),
(709, 20, '304162', '9866526642', '202.53.69.163', '06-08-2021 04:23:21 PM'),
(710, 20, '304162', '9866526642', '202.53.69.163', '06-08-2021 04:23:29 PM'),
(711, 20, '304162', '9866526642', '202.53.69.163', '06-08-2021 04:33:37 PM'),
(712, 20, '304162', '9866526642', '202.53.69.163', '06-08-2021 04:35:43 PM'),
(713, 8, '316894', '9492824933', '202.53.69.163', '06-08-2021 04:37:52 PM'),
(714, 8, '316894', '9492824933', '202.53.69.163', '06-08-2021 04:43:13 PM'),
(715, 8, '316894', '9492824933', '202.53.69.163', '06-08-2021 04:46:59 PM'),
(716, 8, '316894', '9492824933', '202.53.69.163', '06-08-2021 04:47:33 PM'),
(717, 8, '316894', '9492824933', '202.53.69.163', '06-08-2021 04:50:52 PM'),
(718, 8, '316894', '9492824933', '202.53.69.163', '06-08-2021 04:54:25 PM'),
(719, 8, '316894', '9492824933', '202.53.69.163', '06-08-2021 05:20:36 PM'),
(720, 8, '316894', '9492824933', '202.53.69.163', '06-08-2021 05:34:07 PM'),
(721, 8, '316894', '9492824933', '202.53.69.163', '06-08-2021 05:34:35 PM'),
(722, 8, '316894', '9492824933', '202.53.69.163', '06-08-2021 05:34:53 PM'),
(723, 8, '316894', '9492824933', '202.53.69.163', '06-08-2021 05:37:00 PM'),
(724, 8, '316894', '9492824933', '202.53.69.163', '06-08-2021 05:37:24 PM'),
(725, 8, '316894', '9492824933', '202.53.69.163', '06-08-2021 05:37:44 PM'),
(726, 8, '316894', '9492824933', '202.53.69.163', '06-08-2021 05:51:12 PM'),
(727, 12, '584721', '6504992804', '66.249.80.221', '06-08-2021 07:17:30 PM'),
(728, 14, '814735', '9959646419', '223.187.30.159', '08-08-2021 08:29:58 PM'),
(729, 14, '814735', '9959646419', '106.200.168.42', '09-08-2021 08:01:07 AM'),
(730, 14, '814735', '9959646419', '106.195.71.85', '09-08-2021 11:25:43 AM'),
(731, 14, '814735', '9959646419', '106.195.71.85', '09-08-2021 11:25:47 AM'),
(732, 8, '316894', '9492824933', '202.53.69.163', '09-08-2021 12:11:00 PM'),
(733, 13, '523468', '8125505088', '106.217.236.49', '09-08-2021 12:13:00 PM'),
(734, 13, '523468', '8125505088', '106.217.236.49', '09-08-2021 12:20:13 PM'),
(735, 8, '316894', '9492824933', '202.53.69.163', '09-08-2021 12:22:11 PM'),
(736, 13, '523468', '8125505088', '106.217.236.49', '09-08-2021 12:22:21 PM'),
(737, 13, '523468', '8125505088', '106.217.236.49', '09-08-2021 12:22:42 PM'),
(738, 8, '316894', '9492824933', '202.53.69.163', '09-08-2021 12:27:43 PM'),
(739, 8, '316894', '9492824933', '202.53.69.163', '09-08-2021 12:35:58 PM'),
(740, 8, '316894', '9492824933', '202.53.69.163', '09-08-2021 12:37:04 PM'),
(741, 8, '316894', '9492824933', '202.53.69.163', '09-08-2021 12:43:38 PM'),
(742, 14, '814735', '9959646419', '106.208.27.190', '09-08-2021 01:29:00 PM'),
(743, 14, '814735', '9959646419', '106.208.27.190', '09-08-2021 01:35:51 PM'),
(744, 13, '523468', '8125505088', '202.53.69.162', '09-08-2021 02:49:45 PM'),
(745, 13, '523468', '8125505088', '202.53.69.162', '09-08-2021 03:21:38 PM'),
(746, 13, '523468', '8125505088', '202.53.69.162', '09-08-2021 03:22:57 PM'),
(747, 14, '814735', '9959646419', '106.208.75.239', '09-08-2021 03:24:39 PM'),
(748, 14, '814735', '9959646419', '106.208.75.239', '09-08-2021 03:28:20 PM'),
(749, 14, '814735', '9959646419', '106.208.75.239', '09-08-2021 03:30:11 PM'),
(750, 14, '814735', '9959646419', '106.208.75.239', '09-08-2021 03:31:22 PM'),
(751, 8, '316894', '9492824933', '202.53.69.163', '09-08-2021 04:08:27 PM'),
(752, 8, '316894', '9492824933', '202.53.69.163', '09-08-2021 04:08:57 PM'),
(753, 8, '316894', '9492824933', '202.53.69.163', '09-08-2021 04:10:56 PM'),
(754, 8, '316894', '9492824933', '157.47.71.95', '09-08-2021 04:26:25 PM'),
(755, 8, '316894', '9492824933', '157.47.71.95', '09-08-2021 04:27:52 PM'),
(756, 13, '523468', '8125505088', '202.53.69.162', '09-08-2021 04:30:59 PM'),
(757, 13, '523468', '8125505088', '202.53.69.162', '09-08-2021 04:45:44 PM'),
(758, 14, '814735', '9959646419', '106.208.97.234', '09-08-2021 08:51:47 PM'),
(759, 14, '814735', '9959646419', '106.208.97.234', '09-08-2021 08:53:17 PM'),
(760, 14, '814735', '9959646419', '106.208.97.234', '09-08-2021 09:05:15 PM'),
(761, 23, '928316', '8606942840', '157.44.159.63', '09-08-2021 09:05:49 PM'),
(762, 14, '814735', '9959646419', '106.208.97.234', '09-08-2021 09:10:29 PM'),
(763, 14, '814735', '9959646419', '106.208.97.234', '09-08-2021 09:12:34 PM'),
(764, 14, '814735', '9959646419', '106.208.97.234', '09-08-2021 09:13:42 PM'),
(765, 14, '814735', '9959646419', '106.208.97.234', '09-08-2021 09:18:21 PM'),
(766, 23, '928316', '8606942840', '157.44.159.63', '09-08-2021 09:23:40 PM'),
(767, 16, '571649', '9505560361', '106.78.79.78', '09-08-2021 10:09:51 PM'),
(768, 16, '571649', '9505560361', '106.78.79.78', '09-08-2021 10:10:32 PM'),
(769, 16, '571649', '9505560361', '106.78.79.78', '09-08-2021 10:10:38 PM'),
(770, 8, '316894', '9492824933', '175.101.128.29', '09-08-2021 10:26:29 PM'),
(771, 8, '316894', '9492824933', '175.101.128.29', '09-08-2021 10:45:58 PM'),
(772, 8, '316894', '9492824933', '175.101.128.29', '09-08-2021 10:49:20 PM'),
(773, 8, '316894', '9492824933', '175.101.128.29', '09-08-2021 10:57:18 PM'),
(774, 8, '316894', '9492824933', '175.101.128.29', '09-08-2021 11:00:45 PM'),
(775, 14, '814735', '9959646419', '106.208.97.234', '10-08-2021 06:27:24 AM'),
(776, 14, '814735', '9959646419', '106.208.97.234', '10-08-2021 06:28:49 AM'),
(777, 14, '814735', '9959646419', '106.208.97.234', '10-08-2021 06:29:51 AM'),
(778, 14, '814735', '9959646419', '106.208.97.234', '10-08-2021 06:37:11 AM'),
(779, 14, '814735', '9959646419', '106.208.97.234', '10-08-2021 06:46:50 AM'),
(780, 13, '523468', '8125505088', '49.204.227.164', '10-08-2021 10:24:16 AM'),
(781, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 10:34:11 AM'),
(782, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 10:41:09 AM'),
(783, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 10:41:47 AM'),
(784, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 10:47:26 AM'),
(785, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 11:28:15 AM'),
(786, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 11:31:48 AM'),
(787, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 11:35:57 AM'),
(788, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 11:51:46 AM'),
(789, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 11:59:42 AM'),
(790, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 12:00:58 PM'),
(791, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 12:19:59 PM'),
(792, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 12:57:40 PM'),
(793, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 01:01:58 PM'),
(794, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 02:00:21 PM'),
(795, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 02:01:14 PM'),
(796, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 02:06:14 PM'),
(797, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 02:08:39 PM'),
(798, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 02:15:31 PM'),
(799, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 02:16:43 PM'),
(800, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 02:17:29 PM'),
(801, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 02:33:22 PM'),
(802, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 02:34:58 PM'),
(803, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 02:38:53 PM'),
(804, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 02:43:43 PM'),
(805, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 02:45:03 PM'),
(806, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 02:48:17 PM'),
(807, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 02:49:49 PM'),
(808, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 02:54:00 PM'),
(809, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 02:55:02 PM'),
(810, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 03:09:28 PM'),
(811, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 03:11:15 PM'),
(812, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 03:11:45 PM'),
(813, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 03:12:12 PM'),
(814, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 03:14:53 PM'),
(815, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 03:23:41 PM'),
(816, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 03:29:38 PM'),
(817, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 03:31:40 PM'),
(818, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 03:32:13 PM'),
(819, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 03:44:26 PM'),
(820, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 04:10:13 PM'),
(821, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 04:22:17 PM'),
(822, 16, '571649', '9505560361', '106.66.44.199', '10-08-2021 04:27:33 PM'),
(823, 15, '039248', '7013449090', '106.66.44.199', '10-08-2021 04:41:20 PM'),
(824, 24, '719635', '917013449090', '106.66.44.199', '10-08-2021 04:42:06 PM'),
(825, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 04:57:11 PM'),
(826, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 05:07:32 PM'),
(827, 14, '814735', '9959646419', '106.208.107.56', '10-08-2021 05:23:20 PM'),
(828, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 05:40:05 PM'),
(829, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 05:43:07 PM'),
(830, 8, '316894', '9492824933', '202.53.69.163', '10-08-2021 06:31:45 PM'),
(831, 14, '814735', '9959646419', '106.208.107.56', '10-08-2021 08:01:28 PM'),
(832, 24, '719635', '917013449090', '106.66.45.84', '10-08-2021 09:02:46 PM'),
(833, 24, '719635', '917013449090', '106.66.45.84', '10-08-2021 09:08:40 PM'),
(834, 24, '719635', '917013449090', '106.66.45.84', '10-08-2021 09:08:47 PM'),
(835, 24, '719635', '917013449090', '106.66.45.84', '10-08-2021 09:09:04 PM'),
(836, 24, '719635', '917013449090', '106.66.45.84', '10-08-2021 09:09:13 PM'),
(837, 8, '316894', '9492824933', '157.47.67.3', '10-08-2021 09:10:42 PM'),
(838, 25, '536178', '8106874452', '157.47.67.3', '10-08-2021 09:15:46 PM'),
(839, 8, '316894', '9492824933', '175.101.128.29', '11-08-2021 06:17:00 AM'),
(840, 8, '316894', '9492824933', '175.101.128.29', '11-08-2021 06:55:40 AM'),
(841, 8, '316894', '9492824933', '175.101.128.29', '11-08-2021 07:28:45 AM'),
(842, 8, '316894', '9492824933', '175.101.128.29', '11-08-2021 07:32:59 AM'),
(843, 8, '316894', '9492824933', '175.101.128.29', '11-08-2021 07:36:13 AM'),
(844, 8, '316894', '9492824933', '202.53.69.163', '11-08-2021 10:13:10 AM');

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `vehicle_num` varchar(255) NOT NULL,
  `vehicle_image` varchar(255) NOT NULL,
  `qr_code` varchar(255) NOT NULL,
  `pending_challans` varchar(255) DEFAULT NULL,
  `paid_challans` varchar(255) DEFAULT NULL,
  `updated_on` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `user_id`, `vehicle_num`, `vehicle_image`, `qr_code`, `pending_challans`, `paid_challans`, `updated_on`) VALUES
(1, 1, '123', 'favicon9.png', 'TV76DMJK', '2', '3', '28-07-2021 04:55:09 PM'),
(3, 8, '', '', 'VMYXN0FE', NULL, NULL, NULL),
(4, 8, '', '', 'XQOPZFGS', NULL, NULL, NULL),
(5, 8, '', '', '9X3HCR75', NULL, NULL, NULL),
(6, 8, '', '', 'KJUM1BGX', NULL, NULL, NULL),
(7, 8, '', '', 'G6CT18EV', NULL, NULL, NULL),
(8, 8, '', '', 'WYF10JHL', NULL, NULL, NULL),
(9, 8, '', '', '6XZHY5BV', NULL, NULL, NULL),
(10, 8, '', '', '5OBSJX9E', NULL, NULL, NULL),
(11, 8, '', '', 'L2Z3QGJS', NULL, NULL, NULL),
(12, 8, '', '', 'UY9L4ZVC', NULL, NULL, NULL),
(13, 1, 'rt3r', '', 'UF65LJXO', NULL, NULL, NULL),
(14, 8, '', '', 'AMDY2OHT', NULL, NULL, NULL),
(15, 8, '', '', 'XO3CGS16', NULL, NULL, NULL),
(16, 8, '', '', '9U1C4RA5', NULL, NULL, NULL),
(17, 8, '', '', '4P31729D', NULL, NULL, NULL),
(18, 8, '', '', 'CZ5M3UQJ', NULL, NULL, NULL),
(20, 8, '', '', 'E8L7GHIS', NULL, NULL, NULL),
(21, 8, '', '', 'D12IRSYM', NULL, NULL, NULL),
(22, 8, '', '', 'YFOKP6M2', NULL, NULL, NULL),
(23, 8, '', '', '5OY0LC7B', NULL, NULL, NULL),
(24, 8, '', '', 'FKRTA8QN', NULL, NULL, NULL),
(25, 8, '', '', 'Q0K3V7DS', NULL, NULL, NULL),
(26, 8, 'AP31GA0642', 'img_20210726_154018304.jpg', '80PMRLZ1', NULL, NULL, NULL),
(27, 8, 'AP12CD1538', 'img_20210726_154438587.jpg', 'U0BH9CV7', NULL, NULL, NULL),
(28, 8, 'AP23AS1999', 'img_20210726_163219544.jpg', 'N634J9AL', NULL, NULL, NULL),
(29, 8, '', '', 'W97AH1Q4', NULL, NULL, NULL),
(30, 9, 'AP24CD8765', 'img_20210728_111613519.jpg', 'EQADK1JO', '2', '1', '28-07-2021 04:43:06 PM'),
(31, 1, '123', '20210506071410441_favicon1.png', 'GFCX52JR', NULL, NULL, NULL),
(32, 13, 'AP35AA7932', 'img_20210809_121613569.jpg', 'WUFN2AGX', NULL, NULL, NULL),
(33, 14, 'AP39DL1661', 'img_20210809_152741805.jpg', 'QG851U0T', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_document_details`
--

CREATE TABLE `vehicle_document_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `document_type` text NOT NULL,
  `note` text NOT NULL,
  `issued_date` varchar(255) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `document_photo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_document_details`
--

INSERT INTO `vehicle_document_details` (`id`, `user_id`, `vehicle_id`, `document_type`, `note`, `issued_date`, `expiry_date`, `document_photo`) VALUES
(2, 1, 1, 'tes', 'test', '23-07-2021', '23-07-2021', '20210506071410441_favicon.png'),
(3, 1, 1, 'tes', 'test', '23-07-2021', '23-07-2021', '20210506071410441_favicon1.png'),
(4, 1, 1234, 'RC', 'dhadf', '23-7-2021', '25-7-2021', 'fundus_photograph_of_normal_right_eye.jpg'),
(5, 1, 1234, 'RC', 'dhadf', '23-7-2021', '25-7-2021', 'fundus_photograph_of_normal_right_eye1.jpg'),
(9, 8, 26, 'Insurance', 'custdigig yjx video rkgifi vysuckgu', '9-7-2021', '10-9-2021', 'img_20210729_181550541.jpg'),
(10, 1, 1, 'tes', 'test', '23-07-2021', '23-07-2021', '20210506071410441_favicon2.png'),
(11, 1, 1, 'tes', 'test', '23-07-2021', '23-07-2021', '20210506071410441_favicon3.png'),
(12, 1, 1, 'tes', 'test', '23-07-2021', '23-07-2021', '20210506071410441_favicon4.png'),
(13, 1, 1, 'sdf', 'c', 'f', 'g', '20210506071410441_favicon5.png');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_service_details`
--

CREATE TABLE `vehicle_service_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `service_type` text NOT NULL,
  `service_note` text NOT NULL,
  `last_service_date` varchar(255) NOT NULL,
  `next_service_date` varchar(255) NOT NULL,
  `service_bill` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_service_details`
--

INSERT INTO `vehicle_service_details` (`id`, `user_id`, `vehicle_id`, `service_type`, `service_note`, `last_service_date`, `next_service_date`, `service_bill`) VALUES
(2, 1, 123, 'teswt', 'tset', 'test', 'test', 'hope.jpeg'),
(9, 1, 1234, 'teswt n', 'tset', 'test', 'test', '1821.jpg'),
(11, 8, 27, 'tyres changing', 'jfustxkhud kgjfysjvr ajgov', '2-7-2021', '31-8-2021', 'img_20210729_181433498.jpg'),
(12, 14, 33, 'Engine oil changing', 'test', '9-8-2021', '10-8-2021', 'img_20210809_152932675.jpg'),
(13, 13, 32, 'tyres changing', 'test', '1-8-2021', '30-9-2021', 'img_20210809_160332147.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_warrenty_bill_details`
--

CREATE TABLE `vehicle_warrenty_bill_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `purchased_item` text NOT NULL,
  `note` text NOT NULL,
  `purchase_date` varchar(255) NOT NULL,
  `warrenty_end_date` varchar(255) NOT NULL,
  `bill_photo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_warrenty_bill_details`
--

INSERT INTO `vehicle_warrenty_bill_details` (`id`, `user_id`, `vehicle_id`, `purchased_item`, `note`, `purchase_date`, `warrenty_end_date`, `bill_photo`) VALUES
(3, 1, 1234, 'battery', 'sdflajda', '23-7-2021', '3-8-2021', 'fundus_photograph_of_normal_right_eye.jpg'),
(5, 8, 1, 'battery', 'changing battery', '24-7-2021', '28-7-2021', 'img_20210724_073518357.jpg'),
(6, 8, 27, 'fhdtabog', 'jcya to vjxdv it X vuc', '29-7-2021', '3-9-2021', 'img_20210729_181657601.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_ips`
--
ALTER TABLE `admin_ips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_details`
--
ALTER TABLE `bank_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner_images`
--
ALTER TABLE `banner_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bug_issues`
--
ALTER TABLE `bug_issues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_pages`
--
ALTER TABLE `cms_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_enquiry`
--
ALTER TABLE `contact_enquiry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_enquiry_website`
--
ALTER TABLE `contact_enquiry_website`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_types`
--
ALTER TABLE `document_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emergency_contacts`
--
ALTER TABLE `emergency_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `help_center`
--
ALTER TABLE `help_center`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_banners`
--
ALTER TABLE `home_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manage_uploads`
--
ALTER TABLE `manage_uploads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qr_code_genereation`
--
ALTER TABLE `qr_code_genereation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qr_kits`
--
ALTER TABLE `qr_kits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qr_send_details`
--
ALTER TABLE `qr_send_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_points`
--
ALTER TABLE `referral_points`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_renewal`
--
ALTER TABLE `referral_renewal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seo_tags`
--
ALTER TABLE `seo_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services_types`
--
ALTER TABLE `services_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_details`
--
ALTER TABLE `site_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_links`
--
ALTER TABLE `social_links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `steps`
--
ALTER TABLE `steps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `steps_images`
--
ALTER TABLE `steps_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_accounts`
--
ALTER TABLE `user_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_logs`
--
ALTER TABLE `user_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_document_details`
--
ALTER TABLE `vehicle_document_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_service_details`
--
ALTER TABLE `vehicle_service_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_warrenty_bill_details`
--
ALTER TABLE `vehicle_warrenty_bill_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `admin_ips`
--
ALTER TABLE `admin_ips`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `bank_details`
--
ALTER TABLE `bank_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `banner_images`
--
ALTER TABLE `banner_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bug_issues`
--
ALTER TABLE `bug_issues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cms_pages`
--
ALTER TABLE `cms_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `contact_enquiry`
--
ALTER TABLE `contact_enquiry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `contact_enquiry_website`
--
ALTER TABLE `contact_enquiry_website`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `document_types`
--
ALTER TABLE `document_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `emergency_contacts`
--
ALTER TABLE `emergency_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `help_center`
--
ALTER TABLE `help_center`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `home_banners`
--
ALTER TABLE `home_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `qr_code_genereation`
--
ALTER TABLE `qr_code_genereation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `qr_kits`
--
ALTER TABLE `qr_kits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `qr_send_details`
--
ALTER TABLE `qr_send_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `referral_points`
--
ALTER TABLE `referral_points`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `referral_renewal`
--
ALTER TABLE `referral_renewal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `services_types`
--
ALTER TABLE `services_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `steps`
--
ALTER TABLE `steps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `steps_images`
--
ALTER TABLE `steps_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user_accounts`
--
ALTER TABLE `user_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `user_logs`
--
ALTER TABLE `user_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=845;

--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `vehicle_document_details`
--
ALTER TABLE `vehicle_document_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `vehicle_service_details`
--
ALTER TABLE `vehicle_service_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `vehicle_warrenty_bill_details`
--
ALTER TABLE `vehicle_warrenty_bill_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
